﻿namespace Empresas.Models.Shared
{
    public class Resposta<T>
    {
        public T Dados { get; set; }
        public bool Sucesso { get; set; }
        public string[] Erros { get; set; }
        public string Mensagem { get; set; }
        public Resposta()
        {
        }
        public Resposta(T dados)
        {
            Sucesso = true;
            Mensagem = string.Empty;
            Erros = null;
            Dados = dados;
        }
    }
}