﻿namespace Empresas.Models.Shared
{
    public class FiltroPaginacao
    {
        private readonly int _numeroPaginaMinimo = 1,
                             _tamanhoPaginaMinimo = 5,
                             _tamanhoPaginaMaximo = 20;
        private int _numeroPagina,
                    _tamanhoPagina;

        public int NumeroPagina
        {
            get { return _numeroPagina; }
            set { _numeroPagina = value < _numeroPaginaMinimo ? _numeroPaginaMinimo : value; }
        }
        public int TamanhoPagina
        {
            get { return _tamanhoPagina; }
            set
            {
                if (value < _tamanhoPaginaMinimo)
                {
                    _tamanhoPagina = _tamanhoPaginaMinimo;
                }
                else if (value > _tamanhoPaginaMaximo)
                {
                    _tamanhoPagina = _tamanhoPaginaMaximo;
                }
                else
                {
                    _tamanhoPagina = value;
                }
            }
        }

        public FiltroPaginacao()
        {
            NumeroPagina = -1;
            TamanhoPagina = -1;
        }

        public FiltroPaginacao(int numeroPagina = -1, int tamanhoPagina = -1)
        {
            NumeroPagina = numeroPagina;
            TamanhoPagina = tamanhoPagina;
        }
    }
}