﻿namespace Empresas.Models.Shared
{
    public class RespostaPaginada<T> : Resposta<T>
    {
        public int NumeroPagina { get; set; }
        public int TamanhoPagina { get; set; }
        public int TotalPaginas { get; set; }
        public int TotalRegistros { get; set; }
        public RespostaPaginada(T dados, int numeroPagina, int pageSize)
        {
            NumeroPagina = numeroPagina;
            TamanhoPagina = pageSize;
            Dados = dados;
            Mensagem = null;
            Sucesso = true;
            Erros = null;
        }
    }
}
