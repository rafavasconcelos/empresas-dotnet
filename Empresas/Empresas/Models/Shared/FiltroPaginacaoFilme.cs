﻿namespace Empresas.Models.Shared
{
    public class FiltroPaginacaoFilme
    {
        private readonly int _numeroPaginaMinimo = 1,
                             _tamanhoPaginaMinimo = 5,
                             _tamanhoPaginaMaximo = 20;
        private int _numeroPagina,
                    _tamanhoPagina;
        
        public int NumeroPagina { 
            get { return _numeroPagina; }
            set { _numeroPagina = value < _numeroPaginaMinimo ? _numeroPaginaMinimo : value; }
        }
        public int TamanhoPagina
        {
            get { return _tamanhoPagina; }
            set
            {
                if (value < _tamanhoPaginaMinimo)
                {
                    _tamanhoPagina = _tamanhoPaginaMinimo;
                }
                else if (value > _tamanhoPaginaMaximo)
                {
                    _tamanhoPagina = _tamanhoPaginaMaximo;
                }
                else
                {
                    _tamanhoPagina = value;
                }
            }
        }
        public string[] Atores { get; set; }
        public string[] Diretores { get; set; }
        public string[] Generos { get; set; }
        public string NomeFilme { get; set; }

        public FiltroPaginacaoFilme()
        {
            NumeroPagina = -1;
            TamanhoPagina = -1;
            NomeFilme = string.Empty;
            Atores = new string[0];
            Diretores = new string[0];
            Generos = new string[0];
        }

        public FiltroPaginacaoFilme(int numeroPagina = -1, int tamanhoPagina = -1, 
                                    string nomeFilme = null, string[] atores = null, 
                                    string[] diretores = null, string[] generos = null)
        {
            NumeroPagina = numeroPagina;
            TamanhoPagina = tamanhoPagina;
            NomeFilme = nomeFilme;
            Atores = atores == null ? new string[0] : atores;
            Diretores = diretores == null ? new string[0] : diretores;
            Generos = generos == null ? new string[0] : generos;
        }
    }
}