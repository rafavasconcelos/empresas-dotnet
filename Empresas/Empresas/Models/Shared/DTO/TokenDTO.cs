﻿using System;
namespace Empresas.Models.Shared
{
    public class TokenDto
    {
        public string Token { get; set; }
        public DateTime Criacao { get; set; }
        public DateTime Validade { get; set; }
    }
}
