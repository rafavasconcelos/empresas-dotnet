﻿using System.Collections.Generic;
using System.Linq;
namespace Empresas.Models.Filmes
{
    public class VotoDto
    {
        public ICollection<VotoUsuario> VotoUsuario { get; set; }
        public double Media 
        { 
            get 
            {
                return VotoUsuario.Count == 0 ? 0 : VotoUsuario.Average(v => v.Nota);
            } 
        }

        public VotoDto() { }

        public VotoDto(ICollection<Voto> votos)
        {
            List<VotoUsuario> votoUsuarios = new List<VotoUsuario>();
            foreach (Voto voto in votos)
            {
                votoUsuarios.Add(new VotoUsuario(voto));
            }
            VotoUsuario = votoUsuarios;
        }

    }
}