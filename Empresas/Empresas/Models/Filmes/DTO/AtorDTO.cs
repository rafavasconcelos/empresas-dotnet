﻿using System;
namespace Empresas.Models.Filmes
{
    public class AtorDto
    {
        public Guid IdAtor { get; set; }
        public string Nome { get; set; }

        public AtorDto() { }

        public AtorDto(Ator ator)
        {
            IdAtor = ator.IdAtor;
            Nome = ator.Nome;
        }
    }
}
