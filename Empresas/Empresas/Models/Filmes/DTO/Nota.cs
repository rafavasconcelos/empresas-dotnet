﻿using Empresas.Models.Usuarios;
namespace Empresas.Models.Filmes
{
    public class VotoUsuario
    {
        public UsuarioDto Usuario { get; set; }
        public int Nota { get; set; }

        public VotoUsuario() { }

        public VotoUsuario(Voto voto)
        {
            Usuario = new UsuarioDto(voto.Usuario);
            Nota = voto.Nota;
        }
    }
}