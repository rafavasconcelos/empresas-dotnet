﻿using System;
namespace Empresas.Models.Filmes
{
    public class GeneroDto
    {
        public Guid IdGenero { get; set; }
        public string Nome { get; set; }

        public GeneroDto() { }

        public GeneroDto(Genero genero)
        {
            IdGenero = genero.IdGenero;
            Nome = genero.Nome;
        }
    }
}
