﻿using System;
namespace Empresas.Models.Filmes
{
    public class DiretorDto
    {
        public Guid IdDiretor { get; set; }
        public string Nome { get; set; }

        public DiretorDto() { }

        public DiretorDto(Diretor diretor)
        {
            IdDiretor = diretor.IdDiretor;
            Nome = diretor.Nome;
        }
    }
}
