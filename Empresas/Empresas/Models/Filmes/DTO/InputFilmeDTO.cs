﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Empresas.Models.Filmes
{
    public class InputFilmeDto
    {
        [Required(ErrorMessage = "Campo obrigatório")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Campo obrigatório")]
        public string NomeOriginal { get; set; }

        [Required(ErrorMessage = "Campo obrigatório"), Range(1,9999,ErrorMessage = "Ano Inválido")]
        public int Ano { get; set; }
        
        [Required(ErrorMessage = "Campo obrigatório")]
        public string Sinopse { get; set; }
        
        [Required(ErrorMessage = "Campo obrigatório"), MinLength(1, ErrorMessage = "Obrigatório ao menos 1 elemento")]
        public List<string> Atores { get; set; }
        
        [Required(ErrorMessage = "Campo obrigatório"), MinLength(1, ErrorMessage = "Obrigatório ao menos 1 elemento")]
        public List<string> Diretores { get; set; }
        
        [Required(ErrorMessage = "Campo obrigatório"), MinLength(1, ErrorMessage = "Obrigatório ao menos 1 elemento")]
        public List<string> Generos { get; set; }
    }
}