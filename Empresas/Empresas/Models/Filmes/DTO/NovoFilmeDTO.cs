﻿using System.Collections.Generic;
namespace Empresas.Models.Filmes
{
    public class NovoFilmeDto
    {
        public string Nome { get; set; }
        public string NomeOriginal { get; set; }
        public int Ano { get; set; }
        public string Sinopse { get; set; }
        public bool Inativo { get; set; }
        public List<AtorDto> Atores { get; set; }
        public List<DiretorDto> Diretores { get; set; }
        public List<GeneroDto> Generos { get; set; }

        public NovoFilmeDto() { }
    }
}