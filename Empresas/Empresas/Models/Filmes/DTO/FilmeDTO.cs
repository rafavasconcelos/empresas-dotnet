﻿using System;
using System.Collections.Generic;
namespace Empresas.Models.Filmes
{
    public class FilmeDto
    {
        public Guid IdFilme { get; set; }
        public string Nome { get; set; }
        public string NomeOriginal { get; set; }
        public int Ano { get; set; }
        public string Sinopse { get; set; }
        public bool Inativo { get; set; }
        public List<AtorDto> Atores { get; set; }
        public List<DiretorDto> Diretores { get; set; }
        public List<GeneroDto> Generos { get; set; }
        public VotoDto Votos { get; set; }

        public FilmeDto() { }
        public FilmeDto(Filme filme)
        {
            IdFilme = filme.IdFilme;
            Nome = filme.Nome;
            NomeOriginal = filme.NomeOriginal;
            Ano = filme.Ano;
            Sinopse = filme.Sinopse;
            Inativo = filme.Inativo;
            Atores = new List<AtorDto>();
            foreach(var atorFilme in filme.AtoresFilme)
            {
                Atores.Add(new AtorDto(atorFilme.Ator));
            }
            Diretores = new List<DiretorDto>();
            foreach (var diretorFilme in filme.DiretoresFilme)
            {
                Diretores.Add(new DiretorDto(diretorFilme.Diretor));
            }
            Generos = new List<GeneroDto>();
            foreach (var generoFilme in filme.GenerosFilme)
            {
                Generos.Add(new GeneroDto(generoFilme.Genero));
            }
            Votos = new VotoDto(filme.Votos);
        }
    }
}