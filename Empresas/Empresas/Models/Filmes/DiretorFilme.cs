﻿using System;
namespace Empresas.Models.Filmes
{
    public class DiretorFilme
    {
        public Guid IdDiretor { get; set; }
        public Diretor Diretor { get; set; }
        public Guid IdFilme { get; set; }
        public Filme Filme { get; set; }
    }
}
