﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Empresas.Models.Filmes
{
    public class Genero
    {
        [Key]
        public Guid IdGenero { get; set; }
        public string Nome { get; set; }
        public virtual ICollection<GeneroFilme> GenerosFilmes { get; set; }
    }
}
