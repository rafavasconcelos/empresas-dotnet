﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Empresas.Models.Filmes
{
    public class Diretor
    {
        [Key]
        public Guid IdDiretor { get; set; }
        public string Nome { get; set; }
        public virtual ICollection<DiretorFilme> DiretoresFilmes { get; set; }
    }
}
