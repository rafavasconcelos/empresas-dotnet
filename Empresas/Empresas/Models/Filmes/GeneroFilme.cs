﻿using System;
namespace Empresas.Models.Filmes
{
    public class GeneroFilme
    {
        public Guid IdGenero { get; set; }
        public Genero Genero { get; set; }
        public Guid IdFilme { get; set; }
        public Filme Filme { get; set; }
    }
}
