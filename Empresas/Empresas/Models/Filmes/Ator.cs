﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Empresas.Models.Filmes
{
    public class Ator
    {
        [Key]
        public Guid IdAtor { get; set; }
        public string Nome { get; set; }
        public virtual ICollection<AtorFilme> AtoresFilmes { get; set; }
    }
}
