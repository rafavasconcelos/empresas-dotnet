﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Empresas.Models.Filmes
{
    public class Filme
    {
        [Key]
        public Guid IdFilme { get; set; }
        public string Nome { get; set; }
        public string NomeOriginal { get; set; }
        public int Ano { get; set; }
        public string Sinopse { get; set; }
        public bool Inativo { get; set; }
        public virtual ICollection<AtorFilme> AtoresFilme { get; set; }
        public virtual ICollection<DiretorFilme> DiretoresFilme { get; set; }
        public virtual ICollection<GeneroFilme> GenerosFilme { get; set; }
        public virtual ICollection<Voto> Votos { get; set; }
    }
}