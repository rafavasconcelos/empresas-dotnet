﻿using Empresas.Models.Usuarios;
using System;
using System.ComponentModel.DataAnnotations;
namespace Empresas.Models.Filmes
{
    public class Voto
    {
        private int _nota;
        [Required]
        public Guid IdUsuario { get; set; }
        public Usuario Usuario { get; set; }
        [Required]
        public Guid IdFilme { get; set; }
        public Filme Filme { get; set; }
        [Range(0, 4, ErrorMessage = "O valor deve estar entre 0 e 4")]
        public int Nota 
        { 
            get 
            { 
                return _nota; 
            } 
            
            set 
            { 
                if (value < 0 || value > 4) 
                    throw new ArgumentOutOfRangeException("value", "O valor deve estar entre 0 e 4"); 
                _nota = value;
            } 
        }
    }
}