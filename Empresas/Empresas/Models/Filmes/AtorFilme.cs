﻿using System;
namespace Empresas.Models.Filmes
{
    public class AtorFilme
    {
        public Guid IdAtor { get; set; }
        public Ator Ator { get; set; }
        public Guid IdFilme { get; set; }
        public Filme Filme { get; set; }
    }
}
