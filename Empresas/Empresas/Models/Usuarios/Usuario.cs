﻿using Empresas.Models.Filmes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Empresas.Models.Usuarios
{
    public class Usuario
    {
        [Key]
        public Guid IdUsuario { get; set; }
        public Guid IdRole { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public byte[] HashSenha { get; set; }
        public byte[] SaltSenha { get; set; }
        public bool Inativo { get; set; }
        public virtual ICollection<Voto> Votos { get; set; }
    }
}