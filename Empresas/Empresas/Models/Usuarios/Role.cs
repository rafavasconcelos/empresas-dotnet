﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Empresas.Models.Usuarios
{
    public class Role
    {
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        public string ConcurrencyStamp { get; set; }
        [NotMapped]
        public virtual Guid IdRole { get { return Guid.Parse(Id); } }
    }
}