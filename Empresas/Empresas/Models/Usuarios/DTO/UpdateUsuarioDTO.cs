﻿using System.ComponentModel.DataAnnotations;
namespace Empresas.Models.Usuarios
{
    public class UpdateUsuarioDto
    {
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string SenhaAtual { get; set; }
        public string SenhaNova { get; set; }
    }
}
