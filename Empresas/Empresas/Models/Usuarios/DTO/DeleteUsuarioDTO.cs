﻿using System.ComponentModel.DataAnnotations;
namespace Empresas.Models.Usuarios
{
    public class DeleteUsuarioDto
    {
        [Required]
        public string Senha { get; set; }
    }
}
