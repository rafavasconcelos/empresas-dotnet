﻿using System.ComponentModel.DataAnnotations;
namespace Empresas.Models.Usuarios
{
    public class InputUsuarioDto
    {
        [Required(ErrorMessage = "Campo obrigatório")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Campo obrigatório")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Campo obrigatório"), MinLength(1, ErrorMessage = "Obrigatório no mínimo 8 caractéres")]
        public string Senha { get; set; }
        [Required(ErrorMessage = "Campo obrigatório"), MinLength(1, ErrorMessage = "Obrigatório no mínimo 8 caractéres")]
        public string Confirmacao { get; set; }
    }
}
