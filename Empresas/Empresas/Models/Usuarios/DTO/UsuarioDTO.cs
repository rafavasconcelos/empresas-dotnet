﻿using System;
namespace Empresas.Models.Usuarios
{
    public class UsuarioDto
    {
        public Guid IdUsuario { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }

        public UsuarioDto() { }
        public UsuarioDto(Usuario usuario)
        {
            IdUsuario = usuario.IdUsuario;
            Nome = usuario.Nome;
            Email = usuario.Email;
        }
    }
}
