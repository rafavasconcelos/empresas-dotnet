﻿using System;
namespace Empresas.Models.Usuarios
{
    public class UsuarioRole
    {
        public Guid IdUsuario { get; set; }
        public Guid IdRole { get; set; }
        public Usuario Usuario { get; set; }
    }
}