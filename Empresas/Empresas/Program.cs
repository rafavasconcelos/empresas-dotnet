using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
namespace Empresas
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var iWebHost = CreateWebHostBuilder(args).Build();
            iWebHost.Run();
        }
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>();
    }
}
