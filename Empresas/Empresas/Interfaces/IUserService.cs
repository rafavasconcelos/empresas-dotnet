﻿using Empresas.Models.Usuarios;
using System;
using System.Collections.Generic;

namespace Empresas.Classes
{
    public interface IUserService
    {
        Usuario Autenticar(string email, string senha);
        IEnumerable<Usuario> ObterTodos();
        Usuario ObterPorId(Guid id);
        Usuario ObterPorNome(string nome);
        Usuario Criar(Usuario usuario, string senha);
        void Atualizar(Usuario usuario, string senha = null);
        void Excluir(Guid id);
    }
}