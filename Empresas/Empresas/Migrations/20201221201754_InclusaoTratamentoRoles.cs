﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Empresas.Migrations
{
    public partial class InclusaoTratamentoRoles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("0179c98e-ebcd-4991-9694-379ddcd3d772"), new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("0527e01d-c083-433c-a23b-2f5f0c5be73c"), new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("05f5dd44-3385-426c-9879-558f32b5024c"), new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("2061fdbb-5f20-479d-8788-a945e42f1f45"), new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("22008334-5643-47b1-9e7a-59b9e833c4a7"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("2b6357ad-bb54-4ace-a8a7-2d67694d3c11"), new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("2f87c7c8-7716-4ddf-a5c6-19e44d82359a"), new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("450c571c-021f-4ceb-9555-49b5ec9b8efa"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("450c571c-021f-4ceb-9555-49b5ec9b8efa"), new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("450c571c-021f-4ceb-9555-49b5ec9b8efa"), new Guid("d09363b2-10c0-46d9-9f40-99274c362938") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("548f5214-564c-4a8d-9afe-176a65d14160"), new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("68560754-7f7c-43b3-ab6a-5140c9015566"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("6e9fb0cd-f900-45ca-8915-a689b0e979e4"), new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("73f83e4a-660e-4e83-82c2-da7829bc278c"), new Guid("d09363b2-10c0-46d9-9f40-99274c362938") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("7ca0ebcc-63c7-402a-a98a-077cbf0c6257"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("7de870eb-f251-4789-8038-eb0b8fd0c0d7"), new Guid("626c6544-cd74-4a71-941b-da41935caa66") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("86f2962d-ad8d-48a9-af5b-abf80f19bcd9"), new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("911c2b21-501d-4c93-bd08-86fbaf32abb7"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("942f7575-225c-456a-8864-20b3e61db70d"), new Guid("d09363b2-10c0-46d9-9f40-99274c362938") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("9a73706d-6a0f-4272-8fb2-7ffe94bec684"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("b71bc17a-34e3-48f6-bb3a-c6f32ddca395"), new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("c1b12151-fb82-48f8-925a-11e63cc8939e"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("c963bc11-d8ff-476f-a262-3ede2f035c0d"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("cfa74cf0-c3cc-4085-b132-af1add4cfac6"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("d175379c-98c3-45ca-b396-1587b57cca39"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("d206d821-a9e5-456d-84d5-1c2635dd89f2"), new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("d81fc218-f8c6-44b1-875d-5bf805e25207"), new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("db97e70f-a531-45ac-9f3c-237e6e2644be"), new Guid("626c6544-cd74-4a71-941b-da41935caa66") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("de8739f1-d817-41df-8d6b-1d193bf4592f"), new Guid("626c6544-cd74-4a71-941b-da41935caa66") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("e7cfd58a-c1df-4570-9ed0-e05505cf3afd"), new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("fa99fb86-f2e0-4f83-8cf7-419e2917c4e2"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("fd668aed-a77a-414a-82f9-5126339fadaa"), new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ffe7978e-ff7a-4368-b187-323d2606e5b8"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") });

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("6670c3cd-9acb-4d04-bddd-1b0dcdcbb9fd"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("a495c9d7-464b-4d8b-a37f-b4f3e3eea5a0"));

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("11e32106-9911-45ca-ab9d-2c3f2a2e66a2"), new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("12983b73-5d9f-488a-b6cd-5402d418cae1"), new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("21630c2b-ac4e-4316-8738-c89a33d43c46"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("3a8ba2e3-163f-439c-acb6-976bdcdc3bf2"), new Guid("626c6544-cd74-4a71-941b-da41935caa66") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("3a8ba2e3-163f-439c-acb6-976bdcdc3bf2"), new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("40a926c7-cfcb-4d83-b0d2-7fec4fbe4328"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("44adaece-9f75-4026-a387-94372bc1f5f5"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("4ea7919b-8b72-4032-bd45-204be27722a7"), new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("57a005ce-12b8-41fe-ab4c-31514264efdd"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("65e7faba-a5fc-44a1-8802-faa4dc23166c"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("7c91d8bc-eb3a-4055-aeb0-c946c041f79a"), new Guid("d09363b2-10c0-46d9-9f40-99274c362938") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("7e3a8e79-2b41-4908-8484-4eec8d20ebf4"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("89055e3c-0cc0-428c-85ce-f178aa4f6777"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("a30001aa-b563-4021-89f0-66db4e6b2b52"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("b830e7e8-38b6-46e9-ba13-6f1ad87a9f38"), new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0") });

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("1b08b997-199b-418d-af13-043544306702"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("59644757-1867-4726-a11c-faddecb180c6"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("6477416d-55c4-4ae8-8c46-aa4f64ccd1aa"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("6be0cd64-af9b-475e-a2d4-762b1f02dbe9"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("6ffe6993-db07-448b-9e88-5ef052deb87e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("8000c4ec-f64f-46d3-a26a-67510da15b0c"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("81ed58d2-5ffc-4527-a95c-90c494ac669f"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("91008cee-8b48-4af0-bfb0-519355e6e1d3"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("998dfb58-fafc-4ef4-a6cf-73f8d97e5b81"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("a55afd10-87b6-4e16-a7c0-ea4b1c34bb71"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("cf83b476-a568-4266-8645-986417dca49b"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("d0484c24-ff56-4f17-a330-473938b0aedc"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("e54db6d9-5cb7-4f44-86a9-cae30a9a018e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f1a01323-6ab0-4d38-8109-33e9e8be8f15"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f3c47672-b854-451e-bb81-db609f6ef766"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("fc071b28-673d-4833-8a1e-77928f766903"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("fd2e6992-6983-4436-b96a-ef3e8573a891"));

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8"), new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76"), new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547"), new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("baff59a7-b91d-474f-a741-d16d6928c363"), new Guid("631291f2-40e3-42a7-89d4-6232c7ce44c1") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("08ed366d-07ee-4fc4-98f5-497492679dce"), new Guid("690016e2-ce2f-4ce9-a18d-386ce8f44a54") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("626c6544-cd74-4a71-941b-da41935caa66"), new Guid("690016e2-ce2f-4ce9-a18d-386ce8f44a54") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8"), new Guid("77e00a20-5b1c-43b7-be04-d3ce0622cafc") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547"), new Guid("77e00a20-5b1c-43b7-be04-d3ce0622cafc") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647"), new Guid("7f474172-8942-478a-8e19-e8970a96aa28") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), new Guid("7f474172-8942-478a-8e19-e8970a96aa28") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647"), new Guid("8c1e7ab4-6cac-4e7d-aede-91508452194e") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), new Guid("8c1e7ab4-6cac-4e7d-aede-91508452194e") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547"), new Guid("8c1e7ab4-6cac-4e7d-aede-91508452194e") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), new Guid("9769c6cc-39db-4dc2-b767-8b673ac3c587") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("626c6544-cd74-4a71-941b-da41935caa66"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8"), new Guid("a44ad41d-6e62-4a8c-8ab5-d3a6b73e71d7") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("626c6544-cd74-4a71-941b-da41935caa66"), new Guid("a44ad41d-6e62-4a8c-8ab5-d3a6b73e71d7") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("baff59a7-b91d-474f-a741-d16d6928c363"), new Guid("a44ad41d-6e62-4a8c-8ab5-d3a6b73e71d7") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), new Guid("d41ce0ce-7b09-4d09-b5a6-62e00d167017") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0"), new Guid("e5b01ccf-d60d-412f-bc1e-c732bdb3d056") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("08ed366d-07ee-4fc4-98f5-497492679dce"), new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8"), new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76"), new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d") });

            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "3fc3f0ec-5d0a-4d2c-8961-d13de6ccdaad");

            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "d596a6aa-7281-4f72-a02a-ee6ad2d05528");

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("08ed366d-07ee-4fc4-98f5-497492679dce"), new Guid("f7c280e2-f7b1-4df0-b185-b49e7e26a92c") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("baff59a7-b91d-474f-a741-d16d6928c363"), new Guid("f7c280e2-f7b1-4df0-b185-b49e7e26a92c") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), new Guid("f7c280e2-f7b1-4df0-b185-b49e7e26a92c") });

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("0179c98e-ebcd-4991-9694-379ddcd3d772"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("0527e01d-c083-433c-a23b-2f5f0c5be73c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("05f5dd44-3385-426c-9879-558f32b5024c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("2061fdbb-5f20-479d-8788-a945e42f1f45"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("22008334-5643-47b1-9e7a-59b9e833c4a7"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("2b6357ad-bb54-4ace-a8a7-2d67694d3c11"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("2f87c7c8-7716-4ddf-a5c6-19e44d82359a"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("450c571c-021f-4ceb-9555-49b5ec9b8efa"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("548f5214-564c-4a8d-9afe-176a65d14160"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("68560754-7f7c-43b3-ab6a-5140c9015566"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("6e9fb0cd-f900-45ca-8915-a689b0e979e4"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("73f83e4a-660e-4e83-82c2-da7829bc278c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("7ca0ebcc-63c7-402a-a98a-077cbf0c6257"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("7de870eb-f251-4789-8038-eb0b8fd0c0d7"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("86f2962d-ad8d-48a9-af5b-abf80f19bcd9"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("911c2b21-501d-4c93-bd08-86fbaf32abb7"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("942f7575-225c-456a-8864-20b3e61db70d"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("9a73706d-6a0f-4272-8fb2-7ffe94bec684"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("b71bc17a-34e3-48f6-bb3a-c6f32ddca395"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("c1b12151-fb82-48f8-925a-11e63cc8939e"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("c963bc11-d8ff-476f-a262-3ede2f035c0d"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("cfa74cf0-c3cc-4085-b132-af1add4cfac6"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("d175379c-98c3-45ca-b396-1587b57cca39"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("d206d821-a9e5-456d-84d5-1c2635dd89f2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("d81fc218-f8c6-44b1-875d-5bf805e25207"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("db97e70f-a531-45ac-9f3c-237e6e2644be"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("de8739f1-d817-41df-8d6b-1d193bf4592f"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("e7cfd58a-c1df-4570-9ed0-e05505cf3afd"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("fa99fb86-f2e0-4f83-8cf7-419e2917c4e2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("fd668aed-a77a-414a-82f9-5126339fadaa"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ffe7978e-ff7a-4368-b187-323d2606e5b8"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("11e32106-9911-45ca-ab9d-2c3f2a2e66a2"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("12983b73-5d9f-488a-b6cd-5402d418cae1"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("21630c2b-ac4e-4316-8738-c89a33d43c46"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("3a8ba2e3-163f-439c-acb6-976bdcdc3bf2"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("40a926c7-cfcb-4d83-b0d2-7fec4fbe4328"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("44adaece-9f75-4026-a387-94372bc1f5f5"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("4ea7919b-8b72-4032-bd45-204be27722a7"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("57a005ce-12b8-41fe-ab4c-31514264efdd"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("65e7faba-a5fc-44a1-8802-faa4dc23166c"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("7c91d8bc-eb3a-4055-aeb0-c946c041f79a"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("7e3a8e79-2b41-4908-8484-4eec8d20ebf4"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("89055e3c-0cc0-428c-85ce-f178aa4f6777"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("a30001aa-b563-4021-89f0-66db4e6b2b52"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("b830e7e8-38b6-46e9-ba13-6f1ad87a9f38"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("08ed366d-07ee-4fc4-98f5-497492679dce"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("626c6544-cd74-4a71-941b-da41935caa66"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("baff59a7-b91d-474f-a741-d16d6928c363"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("d09363b2-10c0-46d9-9f40-99274c362938"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("631291f2-40e3-42a7-89d4-6232c7ce44c1"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("690016e2-ce2f-4ce9-a18d-386ce8f44a54"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("77e00a20-5b1c-43b7-be04-d3ce0622cafc"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7f474172-8942-478a-8e19-e8970a96aa28"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("8c1e7ab4-6cac-4e7d-aede-91508452194e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("9769c6cc-39db-4dc2-b767-8b673ac3c587"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("a44ad41d-6e62-4a8c-8ab5-d3a6b73e71d7"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("d41ce0ce-7b09-4d09-b5a6-62e00d167017"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("e5b01ccf-d60d-412f-bc1e-c732bdb3d056"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d"));

            migrationBuilder.DeleteData(
                table: "Usuarios",
                keyColumn: "IdUsuario",
                keyValue: new Guid("f7c280e2-f7b1-4df0-b185-b49e7e26a92c"));

            migrationBuilder.AddColumn<Guid>(
                name: "IdRole",
                table: "Usuarios",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.InsertData(
                table: "Atores",
                columns: new[] { "IdAtor", "Nome" },
                values: new object[,]
                {
                    { new Guid("2b5b6a7e-3066-4a98-a2ac-be8bd4670d4f"), "Ben Burtt" },
                    { new Guid("e03a677c-d3b3-42b9-951d-40f65d3c6797"), "Woody Harrelson" },
                    { new Guid("4334470e-b338-497c-b753-9a345f87702c"), "Tom Sizemore" },
                    { new Guid("ade046bc-73d2-49e7-956d-dea8db8b5f94"), "Robert Downey Jr." },
                    { new Guid("811aad9a-eded-42cd-abbd-a22cfa8f9776"), "Rachel McAdams" },
                    { new Guid("45730ff4-5c5c-45ac-947b-115d7bb092ab"), "Paul Rhys" },
                    { new Guid("b0746c87-c8c7-4db3-8743-fa73aaa11805"), "Patton Oswalt" },
                    { new Guid("8639f5b4-09d8-49bb-974d-0d6d22129fe6"), "Patrick Magee" },
                    { new Guid("76b40c81-1ea3-4cc9-b394-9ed72b6bbcd1"), "Owen Wilson" },
                    { new Guid("1ecba33a-ba16-4860-8186-4bb00c3c767b"), "Mickey Rourke" },
                    { new Guid("759daf80-44a0-4311-9ca7-d4c585296259"), "Michael Bates" },
                    { new Guid("df35e6de-321a-4a69-9f66-5f4318fbfb4f"), "Mark Ruffalo" },
                    { new Guid("758f6cbf-4753-4a51-9144-5aa6bdf11cfd"), "Malcolm McDowell" },
                    { new Guid("54449faf-ea07-43f6-87b7-e2ec4bbee3bf"), "Laurence Fishburne" },
                    { new Guid("3a222dd1-8415-471e-80a6-1a81bf49cc90"), "Keanu Reeves" },
                    { new Guid("e3460c95-e00a-472e-a1a6-57941c36b97f"), "Lou Romano" },
                    { new Guid("be591ae3-876e-42ca-8d1a-e40061a2448f"), "Jude Law" },
                    { new Guid("0f4941c1-041c-42ed-8a01-ef16ccc6adde"), "Brad Garrett" },
                    { new Guid("a90e3757-17bf-41cb-9619-3fa13f90d875"), "Bruce Willis" },
                    { new Guid("3f3e3604-01c1-44a9-a271-ba53e2e4cda8"), "Carrie-Anne Moss" },
                    { new Guid("308f622c-48d4-48b3-9b84-d48404c7b08d"), "Chris Evans" },
                    { new Guid("7d03f636-8636-43a2-af9c-67155f10dbcf"), "Juliette Lewis" },
                    { new Guid("b19adefc-b57f-4209-8483-11f1d1266041"), "Daniel Mays" },
                    { new Guid("a897fb55-69d9-4f9b-8e5d-40ad53426b8b"), "Dean-Charles Chapman" },
                    { new Guid("10614747-876a-47ee-95cc-35b281100e8a"), "Clive Owen" },
                    { new Guid("b044ee80-b3c1-4764-b1e7-b7a7eecc7438"), "Eric Dane" },
                    { new Guid("06583263-1492-43f1-859f-418c72790c9e"), "George MacKay" },
                    { new Guid("e0e7af4b-1983-46b6-ac60-07c91dfa5d06"), "Geraldine Chaplin" },
                    { new Guid("72d6dcc8-264d-4d4d-b2f0-b437c8087655"), "Jeff Garlin" },
                    { new Guid("05b4ec1e-d8c4-4617-a1a2-f1efe95b2af1"), "Jennifer Aniston" },
                    { new Guid("fb0bb621-b76c-4884-bf9d-85ad5f3d520b"), "Elissa Knight" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("37fdab7b-3300-4804-adf8-b7c9ab31cf64"), "Andrew Stanton" },
                    { new Guid("3be9f148-7478-4882-94b1-3fb926189ba6"), "Stanley Kubrick" },
                    { new Guid("89749aa9-b7d1-47c3-95a9-d6397d888a13"), "Oliver Stone" },
                    { new Guid("41e6d7fd-5455-46ae-ad6c-c8250d623fd4"), "Guy Ritchie" },
                    { new Guid("c75ebbe0-0850-4728-ac6f-4472629ced4a"), "David Frankel" },
                    { new Guid("12dfced1-4146-4283-997c-534245ca931f"), "Sam Mendes" },
                    { new Guid("ca777839-d78c-493c-96de-d2df2159392a"), "Joe Russo" },
                    { new Guid("39c9671e-af5b-4ad7-a2ba-fb721329f6d8"), "Richard Attenborough" },
                    { new Guid("e038435e-5897-4cc8-9a7f-334a319f2c1a"), "Lilly Wachowski" },
                    { new Guid("8d908885-685e-4ff9-b7b2-6312308cfbe5"), "Lana Wachowski" },
                    { new Guid("380e6cb6-86c6-4e61-92b5-ad853b9d5988"), "Jan Pinkava" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("dfd9fd54-00d8-43bd-8b3f-eda79a4c11e3"), "Brad Bird" },
                    { new Guid("81acc4b0-fd9e-4d8e-be6d-6ffc0afe455c"), "Robert Rodriguez" },
                    { new Guid("3a7f5e18-b70b-4923-890a-d814390a0a06"), "Anthony Russo" },
                    { new Guid("5f15482d-8678-4355-8945-9ca7d7498f8d"), "Quentin Tarantino" },
                    { new Guid("a2d0e666-f861-4156-bd66-86b3f699b234"), "Frank Miller" }
                });

            migrationBuilder.InsertData(
                table: "Filmes",
                columns: new[] { "IdFilme", "Ano", "Nome", "NomeOriginal", "Sinopse" },
                values: new object[,]
                {
                    { new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932"), 1999, "Matrix", "The Matrix", "Um hacker aprende com os misteriosos rebeldes sobre a verdadeira natureza de sua realidade e seu papel na guerra contra seus controladores." },
                    { new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd"), 2007, "Ratatouille", "Ratatouille", "Um rato que pode cozinhar forja uma aliança incomum com um jovem garoto em um famoso restaurante em Paris." },
                    { new Guid("2b055393-65e7-4281-ad25-600c7088b92c"), 2008, "Marley & Eu", "Marley & Me", "Uma família aprende importantes lições de vida com seu adorável, mas malicioso e neurótico cão." },
                    { new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82"), 2008, "WALL·E", "WALL·E", "Num futuro distante, um pequeno robô faz uma viagem que decidirá o destino da humanidade." },
                    { new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351"), 2019, "Vingadores: Ultimato", "Avengers: Endgame", "Após os eventos devastadores de Vingadores: Guerra Infinita , o universo está em ruínas, e com a ajuda de aliados os Vingadores se reúnem para desfazer as ações de Thanos e restaurar a ordem." },
                    { new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9"), 2005, "Sin City: A Cidade do Pecado", "Sin City", "O filme explora a miserável cidade de Basin City, e conta a historia de três pessoas diferentes, envoltas em violencia e corrupção." },
                    { new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc"), 1994, "Assassinos por Natureza", "Natural Born Killers", "Duas vítimas de uma infância traumatizada tornam-se amantes e assassinos em série, irresponsavelmente glorificados pela mídia." },
                    { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), 1992, "Chaplin", "Chaplin", "Relata a vida convulsiva e controversa do mestre diretor de comédia Charles Chaplin." },
                    { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), 2009, "Sherlock Holmes", "Sherlock Holmes", "O detective Sherlock Holmes e seu colega Watson se engajam numa batalha para lutar contra as ameaças da Inglaterra." },
                    { new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8"), 1971, "Laranja Mecânica", "A Clockwork Orange", "No futuro, um líder duma turma é levado para prisão é decide ser voluntário dum experimento, mais os resultados não são os esperados." },
                    { new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c"), 2019, "1917", "1917", "6 de Abril de 1917. Enquanto um regimento se reúne para lutar uma guerra nas profundezas do território inimigo, dois soldados são designados para uma corrida contra o tempo para entregar uma mensagem que impedirá 1.600 homens de caminhar direto para uma uma armadilha mortal." }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("de24b604-13c4-4619-ad0c-35f976c8658c"), "Fantasia científica" },
                    { new Guid("9c7f5535-6b85-4a46-bbb8-1ad9f38af897"), "Ficção científica" },
                    { new Guid("f30bb750-48b7-484d-8a39-5ad60a341e68"), "Filmes com truques" },
                    { new Guid("7bd6265a-f7a0-47db-a0f0-925ebaac1794"), "Filmes de guerra" },
                    { new Guid("81b1249c-b5d1-436d-935a-64c1db2ed784"), "Musical" },
                    { new Guid("54598646-6189-471b-a68d-7e0b7f50c4e8"), "Pornográfico" },
                    { new Guid("1466ebb0-dc4e-47c7-b102-d7fc2bbf2566"), "Romance" },
                    { new Guid("96e1c824-8547-4991-abf7-1ddbafb8d0a9"), "Seriado" },
                    { new Guid("8ad0221c-4a4f-41c2-a854-49812b0eb65d"), "Suspense" },
                    { new Guid("70dcd2da-cf3b-4c3a-9201-3bb43a17de60"), "Terror" },
                    { new Guid("ed4ed995-911e-43c9-9b19-6b8ea624e836"), "Thriller" },
                    { new Guid("53883f8f-b050-4ce7-b8f4-e617bde5420a"), "Fantasia" },
                    { new Guid("86feff1b-2aef-4825-8103-2252bc4be055"), "Filme policial" },
                    { new Guid("42984352-8d72-4501-be05-cff35c593f3d"), "Faroeste" },
                    { new Guid("7b4923f2-e897-4f32-b841-61289d2d79fc"), "Comédia de ação" },
                    { new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447"), "Drama" },
                    { new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a"), "Ação" },
                    { new Guid("ad6ab100-12e6-4d67-bebf-016a2f1e6a83"), "Animação" },
                    { new Guid("d1e85fda-984f-4911-9899-70fea899257d"), "Aventura" },
                    { new Guid("ef824e8f-2d6a-4d61-9cdc-33df7bb339a5"), "Cinema de arte" },
                    { new Guid("01f94f63-f783-40c7-a208-ce468b51ee15"), "Espionagem" },
                    { new Guid("c27fcae0-3000-4ba7-bc97-a551b81dda8f"), "Comédia" },
                    { new Guid("c909bdb4-da32-414a-96a1-653d573f72da"), "Chanchada" },
                    { new Guid("fd83ba94-6a8e-491a-9427-159059a8e186"), "Comédia dramática" },
                    { new Guid("20df64e2-8d52-4b4e-9b29-378970e155da"), "Comédia romântica" },
                    { new Guid("a6b59ac1-b467-4180-a174-a709fbd1cd14"), "Dança" }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("11d429fe-089e-44cf-8c79-341be8900652"), "Documentário" },
                    { new Guid("2eadc815-7285-4000-b347-5b27d1326aba"), "Docuficção" },
                    { new Guid("321125a6-a002-4a8f-9642-69b8a1dcb7f1"), "Comédia de terror" }
                });

            migrationBuilder.InsertData(
                table: "IdentityRole",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "599ca093-6faa-44af-8d0f-5af415972f16", "519d6690-89ac-47f7-94cf-3303d27a7d87", "Admin", "ADMIN" },
                    { "4b9b2bba-7bd0-43d5-819a-6274697c17bc", "4256d337-67d5-4983-a20a-5e72005a9bfe", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "Usuarios",
                columns: new[] { "IdUsuario", "Email", "IdRole", "Nome", "Senha" },
                values: new object[] { new Guid("543471df-7f59-4ae9-bcb4-11abe34dfcea"), "rafael.av@gmail.com", new Guid("599ca093-6faa-44af-8d0f-5af415972f16"), "Rafael Vasconcelos", "AQAAAAEAACcQAAAAEIuphJx7gDQFwulqsequOhcLGdlUuqERbCNpPKYgcKkz49R0QiioayEkGHN/LNU7bg==" });

            migrationBuilder.InsertData(
                table: "AtoresFilmes",
                columns: new[] { "IdAtor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("758f6cbf-4753-4a51-9144-5aa6bdf11cfd"), new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8") },
                    { new Guid("df35e6de-321a-4a69-9f66-5f4318fbfb4f"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") },
                    { new Guid("2b5b6a7e-3066-4a98-a2ac-be8bd4670d4f"), new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82") },
                    { new Guid("fb0bb621-b76c-4884-bf9d-85ad5f3d520b"), new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82") },
                    { new Guid("72d6dcc8-264d-4d4d-b2f0-b437c8087655"), new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82") },
                    { new Guid("76b40c81-1ea3-4cc9-b394-9ed72b6bbcd1"), new Guid("2b055393-65e7-4281-ad25-600c7088b92c") },
                    { new Guid("05b4ec1e-d8c4-4617-a1a2-f1efe95b2af1"), new Guid("2b055393-65e7-4281-ad25-600c7088b92c") },
                    { new Guid("b044ee80-b3c1-4764-b1e7-b7a7eecc7438"), new Guid("2b055393-65e7-4281-ad25-600c7088b92c") },
                    { new Guid("1ecba33a-ba16-4860-8186-4bb00c3c767b"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") },
                    { new Guid("10614747-876a-47ee-95cc-35b281100e8a"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") },
                    { new Guid("a90e3757-17bf-41cb-9619-3fa13f90d875"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") },
                    { new Guid("0f4941c1-041c-42ed-8a01-ef16ccc6adde"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") },
                    { new Guid("e3460c95-e00a-472e-a1a6-57941c36b97f"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") },
                    { new Guid("3a222dd1-8415-471e-80a6-1a81bf49cc90"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") },
                    { new Guid("54449faf-ea07-43f6-87b7-e2ec4bbee3bf"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") },
                    { new Guid("3f3e3604-01c1-44a9-a271-ba53e2e4cda8"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") },
                    { new Guid("308f622c-48d4-48b3-9b84-d48404c7b08d"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") },
                    { new Guid("ade046bc-73d2-49e7-956d-dea8db8b5f94"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") },
                    { new Guid("b0746c87-c8c7-4db3-8743-fa73aaa11805"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") },
                    { new Guid("8639f5b4-09d8-49bb-974d-0d6d22129fe6"), new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8") },
                    { new Guid("811aad9a-eded-42cd-abbd-a22cfa8f9776"), new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56") },
                    { new Guid("ade046bc-73d2-49e7-956d-dea8db8b5f94"), new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8") },
                    { new Guid("e0e7af4b-1983-46b6-ac60-07c91dfa5d06"), new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8") },
                    { new Guid("45730ff4-5c5c-45ac-947b-115d7bb092ab"), new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8") },
                    { new Guid("759daf80-44a0-4311-9ca7-d4c585296259"), new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8") },
                    { new Guid("ade046bc-73d2-49e7-956d-dea8db8b5f94"), new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56") },
                    { new Guid("e03a677c-d3b3-42b9-951d-40f65d3c6797"), new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc") },
                    { new Guid("be591ae3-876e-42ca-8d1a-e40061a2448f"), new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56") },
                    { new Guid("4334470e-b338-497c-b753-9a345f87702c"), new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc") },
                    { new Guid("b19adefc-b57f-4209-8483-11f1d1266041"), new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c") },
                    { new Guid("a897fb55-69d9-4f9b-8e5d-40ad53426b8b"), new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c") },
                    { new Guid("06583263-1492-43f1-859f-418c72790c9e"), new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c") },
                    { new Guid("7d03f636-8636-43a2-af9c-67155f10dbcf"), new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("8d908885-685e-4ff9-b7b2-6312308cfbe5"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") },
                    { new Guid("e038435e-5897-4cc8-9a7f-334a319f2c1a"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") },
                    { new Guid("39c9671e-af5b-4ad7-a2ba-fb721329f6d8"), new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8") },
                    { new Guid("380e6cb6-86c6-4e61-92b5-ad853b9d5988"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") },
                    { new Guid("dfd9fd54-00d8-43bd-8b3f-eda79a4c11e3"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") },
                    { new Guid("12dfced1-4146-4283-997c-534245ca931f"), new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c") },
                    { new Guid("a2d0e666-f861-4156-bd66-86b3f699b234"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") },
                    { new Guid("41e6d7fd-5455-46ae-ad6c-c8250d623fd4"), new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56") },
                    { new Guid("c75ebbe0-0850-4728-ac6f-4472629ced4a"), new Guid("2b055393-65e7-4281-ad25-600c7088b92c") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("39c9671e-af5b-4ad7-a2ba-fb721329f6d8"), new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8") },
                    { new Guid("37fdab7b-3300-4804-adf8-b7c9ab31cf64"), new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82") },
                    { new Guid("ca777839-d78c-493c-96de-d2df2159392a"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") },
                    { new Guid("3a7f5e18-b70b-4923-890a-d814390a0a06"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") },
                    { new Guid("89749aa9-b7d1-47c3-95a9-d6397d888a13"), new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc") },
                    { new Guid("5f15482d-8678-4355-8945-9ca7d7498f8d"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") }
                });

            migrationBuilder.InsertData(
                table: "GenerosFilmes",
                columns: new[] { "IdFilme", "IdGenero" },
                values: new object[,]
                {
                    { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") },
                    { new Guid("2b055393-65e7-4281-ad25-600c7088b92c"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") },
                    { new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") },
                    { new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") },
                    { new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") },
                    { new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8"), new Guid("9c7f5535-6b85-4a46-bbb8-1ad9f38af897") },
                    { new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9"), new Guid("ed4ed995-911e-43c9-9b19-6b8ea624e836") },
                    { new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c"), new Guid("7bd6265a-f7a0-47db-a0f0-925ebaac1794") },
                    { new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8"), new Guid("86feff1b-2aef-4825-8103-2252bc4be055") },
                    { new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc"), new Guid("86feff1b-2aef-4825-8103-2252bc4be055") },
                    { new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9"), new Guid("86feff1b-2aef-4825-8103-2252bc4be055") },
                    { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), new Guid("8ad0221c-4a4f-41c2-a854-49812b0eb65d") },
                    { new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") },
                    { new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932"), new Guid("9c7f5535-6b85-4a46-bbb8-1ad9f38af897") },
                    { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), new Guid("2eadc815-7285-4000-b347-5b27d1326aba") },
                    { new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc"), new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a") },
                    { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), new Guid("fd83ba94-6a8e-491a-9427-159059a8e186") },
                    { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a") },
                    { new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351"), new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a") },
                    { new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932"), new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a") },
                    { new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82"), new Guid("ad6ab100-12e6-4d67-bebf-016a2f1e6a83") },
                    { new Guid("2b055393-65e7-4281-ad25-600c7088b92c"), new Guid("fd83ba94-6a8e-491a-9427-159059a8e186") },
                    { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), new Guid("d1e85fda-984f-4911-9899-70fea899257d") },
                    { new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd"), new Guid("ad6ab100-12e6-4d67-bebf-016a2f1e6a83") },
                    { new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82"), new Guid("d1e85fda-984f-4911-9899-70fea899257d") },
                    { new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd"), new Guid("d1e85fda-984f-4911-9899-70fea899257d") },
                    { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), new Guid("c27fcae0-3000-4ba7-bc97-a551b81dda8f") },
                    { new Guid("2b055393-65e7-4281-ad25-600c7088b92c"), new Guid("c27fcae0-3000-4ba7-bc97-a551b81dda8f") },
                    { new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd"), new Guid("c27fcae0-3000-4ba7-bc97-a551b81dda8f") },
                    { new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351"), new Guid("d1e85fda-984f-4911-9899-70fea899257d") }
                });

            migrationBuilder.InsertData(
                table: "Votos",
                columns: new[] { "IdFilme", "IdUsuario", "Nota" },
                values: new object[,]
                {
                    { new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9"), new Guid("543471df-7f59-4ae9-bcb4-11abe34dfcea"), (byte)3 },
                    { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), new Guid("543471df-7f59-4ae9-bcb4-11abe34dfcea"), (byte)1 },
                    { new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932"), new Guid("543471df-7f59-4ae9-bcb4-11abe34dfcea"), (byte)4 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("05b4ec1e-d8c4-4617-a1a2-f1efe95b2af1"), new Guid("2b055393-65e7-4281-ad25-600c7088b92c") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("06583263-1492-43f1-859f-418c72790c9e"), new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("0f4941c1-041c-42ed-8a01-ef16ccc6adde"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("10614747-876a-47ee-95cc-35b281100e8a"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("1ecba33a-ba16-4860-8186-4bb00c3c767b"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("2b5b6a7e-3066-4a98-a2ac-be8bd4670d4f"), new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("308f622c-48d4-48b3-9b84-d48404c7b08d"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("3a222dd1-8415-471e-80a6-1a81bf49cc90"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("3f3e3604-01c1-44a9-a271-ba53e2e4cda8"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("4334470e-b338-497c-b753-9a345f87702c"), new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("45730ff4-5c5c-45ac-947b-115d7bb092ab"), new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("54449faf-ea07-43f6-87b7-e2ec4bbee3bf"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("72d6dcc8-264d-4d4d-b2f0-b437c8087655"), new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("758f6cbf-4753-4a51-9144-5aa6bdf11cfd"), new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("759daf80-44a0-4311-9ca7-d4c585296259"), new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("76b40c81-1ea3-4cc9-b394-9ed72b6bbcd1"), new Guid("2b055393-65e7-4281-ad25-600c7088b92c") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("7d03f636-8636-43a2-af9c-67155f10dbcf"), new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("811aad9a-eded-42cd-abbd-a22cfa8f9776"), new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("8639f5b4-09d8-49bb-974d-0d6d22129fe6"), new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a897fb55-69d9-4f9b-8e5d-40ad53426b8b"), new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a90e3757-17bf-41cb-9619-3fa13f90d875"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ade046bc-73d2-49e7-956d-dea8db8b5f94"), new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ade046bc-73d2-49e7-956d-dea8db8b5f94"), new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ade046bc-73d2-49e7-956d-dea8db8b5f94"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("b044ee80-b3c1-4764-b1e7-b7a7eecc7438"), new Guid("2b055393-65e7-4281-ad25-600c7088b92c") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("b0746c87-c8c7-4db3-8743-fa73aaa11805"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("b19adefc-b57f-4209-8483-11f1d1266041"), new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("be591ae3-876e-42ca-8d1a-e40061a2448f"), new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("df35e6de-321a-4a69-9f66-5f4318fbfb4f"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("e03a677c-d3b3-42b9-951d-40f65d3c6797"), new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("e0e7af4b-1983-46b6-ac60-07c91dfa5d06"), new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("e3460c95-e00a-472e-a1a6-57941c36b97f"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("fb0bb621-b76c-4884-bf9d-85ad5f3d520b"), new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82") });

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("3be9f148-7478-4882-94b1-3fb926189ba6"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("81acc4b0-fd9e-4d8e-be6d-6ffc0afe455c"));

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("12dfced1-4146-4283-997c-534245ca931f"), new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("37fdab7b-3300-4804-adf8-b7c9ab31cf64"), new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("380e6cb6-86c6-4e61-92b5-ad853b9d5988"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("39c9671e-af5b-4ad7-a2ba-fb721329f6d8"), new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("39c9671e-af5b-4ad7-a2ba-fb721329f6d8"), new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("3a7f5e18-b70b-4923-890a-d814390a0a06"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("41e6d7fd-5455-46ae-ad6c-c8250d623fd4"), new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("5f15482d-8678-4355-8945-9ca7d7498f8d"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("89749aa9-b7d1-47c3-95a9-d6397d888a13"), new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("8d908885-685e-4ff9-b7b2-6312308cfbe5"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("a2d0e666-f861-4156-bd66-86b3f699b234"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("c75ebbe0-0850-4728-ac6f-4472629ced4a"), new Guid("2b055393-65e7-4281-ad25-600c7088b92c") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("ca777839-d78c-493c-96de-d2df2159392a"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("dfd9fd54-00d8-43bd-8b3f-eda79a4c11e3"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("e038435e-5897-4cc8-9a7f-334a319f2c1a"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") });

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("01f94f63-f783-40c7-a208-ce468b51ee15"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("11d429fe-089e-44cf-8c79-341be8900652"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("1466ebb0-dc4e-47c7-b102-d7fc2bbf2566"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("20df64e2-8d52-4b4e-9b29-378970e155da"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("321125a6-a002-4a8f-9642-69b8a1dcb7f1"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("42984352-8d72-4501-be05-cff35c593f3d"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("53883f8f-b050-4ce7-b8f4-e617bde5420a"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("54598646-6189-471b-a68d-7e0b7f50c4e8"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("70dcd2da-cf3b-4c3a-9201-3bb43a17de60"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7b4923f2-e897-4f32-b841-61289d2d79fc"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("81b1249c-b5d1-436d-935a-64c1db2ed784"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("96e1c824-8547-4991-abf7-1ddbafb8d0a9"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("a6b59ac1-b467-4180-a174-a709fbd1cd14"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("c909bdb4-da32-414a-96a1-653d573f72da"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("de24b604-13c4-4619-ad0c-35f976c8658c"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("ef824e8f-2d6a-4d61-9cdc-33df7bb339a5"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f30bb750-48b7-484d-8a39-5ad60a341e68"));

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc"), new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351"), new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932"), new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), new Guid("2eadc815-7285-4000-b347-5b27d1326aba") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c"), new Guid("7bd6265a-f7a0-47db-a0f0-925ebaac1794") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9"), new Guid("86feff1b-2aef-4825-8103-2252bc4be055") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc"), new Guid("86feff1b-2aef-4825-8103-2252bc4be055") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8"), new Guid("86feff1b-2aef-4825-8103-2252bc4be055") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), new Guid("8ad0221c-4a4f-41c2-a854-49812b0eb65d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8"), new Guid("9c7f5535-6b85-4a46-bbb8-1ad9f38af897") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932"), new Guid("9c7f5535-6b85-4a46-bbb8-1ad9f38af897") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd"), new Guid("ad6ab100-12e6-4d67-bebf-016a2f1e6a83") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82"), new Guid("ad6ab100-12e6-4d67-bebf-016a2f1e6a83") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2b055393-65e7-4281-ad25-600c7088b92c"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2b055393-65e7-4281-ad25-600c7088b92c"), new Guid("c27fcae0-3000-4ba7-bc97-a551b81dda8f") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), new Guid("c27fcae0-3000-4ba7-bc97-a551b81dda8f") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd"), new Guid("c27fcae0-3000-4ba7-bc97-a551b81dda8f") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), new Guid("d1e85fda-984f-4911-9899-70fea899257d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351"), new Guid("d1e85fda-984f-4911-9899-70fea899257d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd"), new Guid("d1e85fda-984f-4911-9899-70fea899257d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82"), new Guid("d1e85fda-984f-4911-9899-70fea899257d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9"), new Guid("ed4ed995-911e-43c9-9b19-6b8ea624e836") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2b055393-65e7-4281-ad25-600c7088b92c"), new Guid("fd83ba94-6a8e-491a-9427-159059a8e186") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), new Guid("fd83ba94-6a8e-491a-9427-159059a8e186") });

            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "4b9b2bba-7bd0-43d5-819a-6274697c17bc");

            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "599ca093-6faa-44af-8d0f-5af415972f16");

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9"), new Guid("543471df-7f59-4ae9-bcb4-11abe34dfcea") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), new Guid("543471df-7f59-4ae9-bcb4-11abe34dfcea") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932"), new Guid("543471df-7f59-4ae9-bcb4-11abe34dfcea") });

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("05b4ec1e-d8c4-4617-a1a2-f1efe95b2af1"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("06583263-1492-43f1-859f-418c72790c9e"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("0f4941c1-041c-42ed-8a01-ef16ccc6adde"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("10614747-876a-47ee-95cc-35b281100e8a"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("1ecba33a-ba16-4860-8186-4bb00c3c767b"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("2b5b6a7e-3066-4a98-a2ac-be8bd4670d4f"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("308f622c-48d4-48b3-9b84-d48404c7b08d"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("3a222dd1-8415-471e-80a6-1a81bf49cc90"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("3f3e3604-01c1-44a9-a271-ba53e2e4cda8"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("4334470e-b338-497c-b753-9a345f87702c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("45730ff4-5c5c-45ac-947b-115d7bb092ab"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("54449faf-ea07-43f6-87b7-e2ec4bbee3bf"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("72d6dcc8-264d-4d4d-b2f0-b437c8087655"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("758f6cbf-4753-4a51-9144-5aa6bdf11cfd"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("759daf80-44a0-4311-9ca7-d4c585296259"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("76b40c81-1ea3-4cc9-b394-9ed72b6bbcd1"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("7d03f636-8636-43a2-af9c-67155f10dbcf"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("811aad9a-eded-42cd-abbd-a22cfa8f9776"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("8639f5b4-09d8-49bb-974d-0d6d22129fe6"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a897fb55-69d9-4f9b-8e5d-40ad53426b8b"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a90e3757-17bf-41cb-9619-3fa13f90d875"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ade046bc-73d2-49e7-956d-dea8db8b5f94"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("b044ee80-b3c1-4764-b1e7-b7a7eecc7438"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("b0746c87-c8c7-4db3-8743-fa73aaa11805"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("b19adefc-b57f-4209-8483-11f1d1266041"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("be591ae3-876e-42ca-8d1a-e40061a2448f"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("df35e6de-321a-4a69-9f66-5f4318fbfb4f"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("e03a677c-d3b3-42b9-951d-40f65d3c6797"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("e0e7af4b-1983-46b6-ac60-07c91dfa5d06"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("e3460c95-e00a-472e-a1a6-57941c36b97f"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("fb0bb621-b76c-4884-bf9d-85ad5f3d520b"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("12dfced1-4146-4283-997c-534245ca931f"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("37fdab7b-3300-4804-adf8-b7c9ab31cf64"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("380e6cb6-86c6-4e61-92b5-ad853b9d5988"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("39c9671e-af5b-4ad7-a2ba-fb721329f6d8"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("3a7f5e18-b70b-4923-890a-d814390a0a06"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("41e6d7fd-5455-46ae-ad6c-c8250d623fd4"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("5f15482d-8678-4355-8945-9ca7d7498f8d"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("89749aa9-b7d1-47c3-95a9-d6397d888a13"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("8d908885-685e-4ff9-b7b2-6312308cfbe5"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("a2d0e666-f861-4156-bd66-86b3f699b234"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("c75ebbe0-0850-4728-ac6f-4472629ced4a"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("ca777839-d78c-493c-96de-d2df2159392a"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("dfd9fd54-00d8-43bd-8b3f-eda79a4c11e3"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("e038435e-5897-4cc8-9a7f-334a319f2c1a"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("2b055393-65e7-4281-ad25-600c7088b92c"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("2eadc815-7285-4000-b347-5b27d1326aba"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7bd6265a-f7a0-47db-a0f0-925ebaac1794"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("86feff1b-2aef-4825-8103-2252bc4be055"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("8ad0221c-4a4f-41c2-a854-49812b0eb65d"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("9c7f5535-6b85-4a46-bbb8-1ad9f38af897"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("ad6ab100-12e6-4d67-bebf-016a2f1e6a83"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("c27fcae0-3000-4ba7-bc97-a551b81dda8f"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("d1e85fda-984f-4911-9899-70fea899257d"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("ed4ed995-911e-43c9-9b19-6b8ea624e836"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("fd83ba94-6a8e-491a-9427-159059a8e186"));

            migrationBuilder.DeleteData(
                table: "Usuarios",
                keyColumn: "IdUsuario",
                keyValue: new Guid("543471df-7f59-4ae9-bcb4-11abe34dfcea"));

            migrationBuilder.DropColumn(
                name: "IdRole",
                table: "Usuarios");

            migrationBuilder.InsertData(
                table: "Atores",
                columns: new[] { "IdAtor", "Nome" },
                values: new object[,]
                {
                    { new Guid("0527e01d-c083-433c-a23b-2f5f0c5be73c"), "Ben Burtt" },
                    { new Guid("2f87c7c8-7716-4ddf-a5c6-19e44d82359a"), "Woody Harrelson" },
                    { new Guid("d206d821-a9e5-456d-84d5-1c2635dd89f2"), "Tom Sizemore" },
                    { new Guid("450c571c-021f-4ceb-9555-49b5ec9b8efa"), "Robert Downey Jr." },
                    { new Guid("73f83e4a-660e-4e83-82c2-da7829bc278c"), "Rachel McAdams" },
                    { new Guid("2061fdbb-5f20-479d-8788-a945e42f1f45"), "Paul Rhys" },
                    { new Guid("fa99fb86-f2e0-4f83-8cf7-419e2917c4e2"), "Patton Oswalt" },
                    { new Guid("db97e70f-a531-45ac-9f3c-237e6e2644be"), "Patrick Magee" },
                    { new Guid("05f5dd44-3385-426c-9879-558f32b5024c"), "Owen Wilson" },
                    { new Guid("911c2b21-501d-4c93-bd08-86fbaf32abb7"), "Mickey Rourke" },
                    { new Guid("de8739f1-d817-41df-8d6b-1d193bf4592f"), "Michael Bates" },
                    { new Guid("68560754-7f7c-43b3-ab6a-5140c9015566"), "Mark Ruffalo" },
                    { new Guid("7de870eb-f251-4789-8038-eb0b8fd0c0d7"), "Malcolm McDowell" },
                    { new Guid("c1b12151-fb82-48f8-925a-11e63cc8939e"), "Laurence Fishburne" },
                    { new Guid("c963bc11-d8ff-476f-a262-3ede2f035c0d"), "Keanu Reeves" },
                    { new Guid("9a73706d-6a0f-4272-8fb2-7ffe94bec684"), "Lou Romano" },
                    { new Guid("942f7575-225c-456a-8864-20b3e61db70d"), "Jude Law" },
                    { new Guid("d175379c-98c3-45ca-b396-1587b57cca39"), "Brad Garrett" },
                    { new Guid("ffe7978e-ff7a-4368-b187-323d2606e5b8"), "Bruce Willis" },
                    { new Guid("22008334-5643-47b1-9e7a-59b9e833c4a7"), "Carrie-Anne Moss" },
                    { new Guid("cfa74cf0-c3cc-4085-b132-af1add4cfac6"), "Chris Evans" },
                    { new Guid("2b6357ad-bb54-4ace-a8a7-2d67694d3c11"), "Juliette Lewis" },
                    { new Guid("6e9fb0cd-f900-45ca-8915-a689b0e979e4"), "Daniel Mays" },
                    { new Guid("86f2962d-ad8d-48a9-af5b-abf80f19bcd9"), "Dean-Charles Chapman" },
                    { new Guid("7ca0ebcc-63c7-402a-a98a-077cbf0c6257"), "Clive Owen" },
                    { new Guid("0179c98e-ebcd-4991-9694-379ddcd3d772"), "Eric Dane" },
                    { new Guid("b71bc17a-34e3-48f6-bb3a-c6f32ddca395"), "George MacKay" },
                    { new Guid("fd668aed-a77a-414a-82f9-5126339fadaa"), "Geraldine Chaplin" },
                    { new Guid("548f5214-564c-4a8d-9afe-176a65d14160"), "Jeff Garlin" },
                    { new Guid("d81fc218-f8c6-44b1-875d-5bf805e25207"), "Jennifer Aniston" },
                    { new Guid("e7cfd58a-c1df-4570-9ed0-e05505cf3afd"), "Elissa Knight" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("11e32106-9911-45ca-ab9d-2c3f2a2e66a2"), "Andrew Stanton" },
                    { new Guid("a495c9d7-464b-4d8b-a37f-b4f3e3eea5a0"), "Stanley Kubrick" },
                    { new Guid("12983b73-5d9f-488a-b6cd-5402d418cae1"), "Oliver Stone" },
                    { new Guid("7c91d8bc-eb3a-4055-aeb0-c946c041f79a"), "Guy Ritchie" },
                    { new Guid("4ea7919b-8b72-4032-bd45-204be27722a7"), "David Frankel" },
                    { new Guid("b830e7e8-38b6-46e9-ba13-6f1ad87a9f38"), "Sam Mendes" },
                    { new Guid("65e7faba-a5fc-44a1-8802-faa4dc23166c"), "Joe Russo" },
                    { new Guid("3a8ba2e3-163f-439c-acb6-976bdcdc3bf2"), "Richard Attenborough" },
                    { new Guid("89055e3c-0cc0-428c-85ce-f178aa4f6777"), "Lilly Wachowski" },
                    { new Guid("7e3a8e79-2b41-4908-8484-4eec8d20ebf4"), "Lana Wachowski" },
                    { new Guid("21630c2b-ac4e-4316-8738-c89a33d43c46"), "Jan Pinkava" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("40a926c7-cfcb-4d83-b0d2-7fec4fbe4328"), "Brad Bird" },
                    { new Guid("6670c3cd-9acb-4d04-bddd-1b0dcdcbb9fd"), "Robert Rodriguez" },
                    { new Guid("57a005ce-12b8-41fe-ab4c-31514264efdd"), "Anthony Russo" },
                    { new Guid("a30001aa-b563-4021-89f0-66db4e6b2b52"), "Quentin Tarantino" },
                    { new Guid("44adaece-9f75-4026-a387-94372bc1f5f5"), "Frank Miller" }
                });

            migrationBuilder.InsertData(
                table: "Filmes",
                columns: new[] { "IdFilme", "Ano", "Nome", "NomeOriginal", "Sinopse" },
                values: new object[,]
                {
                    { new Guid("08ed366d-07ee-4fc4-98f5-497492679dce"), 1999, "Matrix", "The Matrix", "Um hacker aprende com os misteriosos rebeldes sobre a verdadeira natureza de sua realidade e seu papel na guerra contra seus controladores." },
                    { new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547"), 2007, "Ratatouille", "Ratatouille", "Um rato que pode cozinhar forja uma aliança incomum com um jovem garoto em um famoso restaurante em Paris." },
                    { new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647"), 2008, "Marley & Eu", "Marley & Me", "Uma família aprende importantes lições de vida com seu adorável, mas malicioso e neurótico cão." },
                    { new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8"), 2008, "WALL·E", "WALL·E", "Num futuro distante, um pequeno robô faz uma viagem que decidirá o destino da humanidade." },
                    { new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76"), 2019, "Vingadores: Ultimato", "Avengers: Endgame", "Após os eventos devastadores de Vingadores: Guerra Infinita , o universo está em ruínas, e com a ajuda de aliados os Vingadores se reúnem para desfazer as ações de Thanos e restaurar a ordem." },
                    { new Guid("baff59a7-b91d-474f-a741-d16d6928c363"), 2005, "Sin City: A Cidade do Pecado", "Sin City", "O filme explora a miserável cidade de Basin City, e conta a historia de três pessoas diferentes, envoltas em violencia e corrupção." },
                    { new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8"), 1994, "Assassinos por Natureza", "Natural Born Killers", "Duas vítimas de uma infância traumatizada tornam-se amantes e assassinos em série, irresponsavelmente glorificados pela mídia." },
                    { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), 1992, "Chaplin", "Chaplin", "Relata a vida convulsiva e controversa do mestre diretor de comédia Charles Chaplin." },
                    { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), 2009, "Sherlock Holmes", "Sherlock Holmes", "O detective Sherlock Holmes e seu colega Watson se engajam numa batalha para lutar contra as ameaças da Inglaterra." },
                    { new Guid("626c6544-cd74-4a71-941b-da41935caa66"), 1971, "Laranja Mecânica", "A Clockwork Orange", "No futuro, um líder duma turma é levado para prisão é decide ser voluntário dum experimento, mais os resultados não são os esperados." },
                    { new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0"), 2019, "1917", "1917", "6 de Abril de 1917. Enquanto um regimento se reúne para lutar uma guerra nas profundezas do território inimigo, dois soldados são designados para uma corrida contra o tempo para entregar uma mensagem que impedirá 1.600 homens de caminhar direto para uma uma armadilha mortal." }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("81ed58d2-5ffc-4527-a95c-90c494ac669f"), "Fantasia científica" },
                    { new Guid("690016e2-ce2f-4ce9-a18d-386ce8f44a54"), "Ficção científica" },
                    { new Guid("59644757-1867-4726-a11c-faddecb180c6"), "Filmes com truques" },
                    { new Guid("e5b01ccf-d60d-412f-bc1e-c732bdb3d056"), "Filmes de guerra" },
                    { new Guid("a55afd10-87b6-4e16-a7c0-ea4b1c34bb71"), "Musical" },
                    { new Guid("1b08b997-199b-418d-af13-043544306702"), "Pornográfico" },
                    { new Guid("6ffe6993-db07-448b-9e88-5ef052deb87e"), "Romance" },
                    { new Guid("cf83b476-a568-4266-8645-986417dca49b"), "Seriado" },
                    { new Guid("9769c6cc-39db-4dc2-b767-8b673ac3c587"), "Suspense" },
                    { new Guid("fc071b28-673d-4833-8a1e-77928f766903"), "Terror" },
                    { new Guid("631291f2-40e3-42a7-89d4-6232c7ce44c1"), "Thriller" },
                    { new Guid("fd2e6992-6983-4436-b96a-ef3e8573a891"), "Fantasia" },
                    { new Guid("a44ad41d-6e62-4a8c-8ab5-d3a6b73e71d7"), "Filme policial" },
                    { new Guid("8000c4ec-f64f-46d3-a26a-67510da15b0c"), "Faroeste" },
                    { new Guid("f1a01323-6ab0-4d38-8109-33e9e8be8f15"), "Comédia de ação" },
                    { new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004"), "Drama" },
                    { new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d"), "Ação" },
                    { new Guid("77e00a20-5b1c-43b7-be04-d3ce0622cafc"), "Animação" },
                    { new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6"), "Aventura" },
                    { new Guid("998dfb58-fafc-4ef4-a6cf-73f8d97e5b81"), "Cinema de arte" },
                    { new Guid("e54db6d9-5cb7-4f44-86a9-cae30a9a018e"), "Espionagem" },
                    { new Guid("8c1e7ab4-6cac-4e7d-aede-91508452194e"), "Comédia" },
                    { new Guid("6be0cd64-af9b-475e-a2d4-762b1f02dbe9"), "Chanchada" },
                    { new Guid("7f474172-8942-478a-8e19-e8970a96aa28"), "Comédia dramática" },
                    { new Guid("6477416d-55c4-4ae8-8c46-aa4f64ccd1aa"), "Comédia romântica" },
                    { new Guid("91008cee-8b48-4af0-bfb0-519355e6e1d3"), "Dança" }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("f3c47672-b854-451e-bb81-db609f6ef766"), "Documentário" },
                    { new Guid("d41ce0ce-7b09-4d09-b5a6-62e00d167017"), "Docuficção" },
                    { new Guid("d0484c24-ff56-4f17-a330-473938b0aedc"), "Comédia de terror" }
                });

            migrationBuilder.InsertData(
                table: "IdentityRole",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "3fc3f0ec-5d0a-4d2c-8961-d13de6ccdaad", "20f5d62d-dab6-4807-8d9f-8d0678b7c5e9", "Admin", "ADMIN" },
                    { "d596a6aa-7281-4f72-a02a-ee6ad2d05528", "c38acfb0-7868-4f10-9363-33da1dcc12f6", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "Usuarios",
                columns: new[] { "IdUsuario", "Email", "Nome", "Senha" },
                values: new object[] { new Guid("f7c280e2-f7b1-4df0-b185-b49e7e26a92c"), "rafael.av@gmail.com", "Rafael Vasconcelos", "AQAAAAEAACcQAAAAECsp1FF1MqOQn85YXv30QxtG5Om4JjruR0gUyaDvVP3ItdY1j88h820Edcc4jSWVaA==" });

            migrationBuilder.InsertData(
                table: "AtoresFilmes",
                columns: new[] { "IdAtor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("7de870eb-f251-4789-8038-eb0b8fd0c0d7"), new Guid("626c6544-cd74-4a71-941b-da41935caa66") },
                    { new Guid("68560754-7f7c-43b3-ab6a-5140c9015566"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") },
                    { new Guid("0527e01d-c083-433c-a23b-2f5f0c5be73c"), new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8") },
                    { new Guid("e7cfd58a-c1df-4570-9ed0-e05505cf3afd"), new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8") },
                    { new Guid("548f5214-564c-4a8d-9afe-176a65d14160"), new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8") },
                    { new Guid("05f5dd44-3385-426c-9879-558f32b5024c"), new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647") },
                    { new Guid("d81fc218-f8c6-44b1-875d-5bf805e25207"), new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647") },
                    { new Guid("0179c98e-ebcd-4991-9694-379ddcd3d772"), new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647") },
                    { new Guid("911c2b21-501d-4c93-bd08-86fbaf32abb7"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") },
                    { new Guid("7ca0ebcc-63c7-402a-a98a-077cbf0c6257"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") },
                    { new Guid("ffe7978e-ff7a-4368-b187-323d2606e5b8"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") },
                    { new Guid("d175379c-98c3-45ca-b396-1587b57cca39"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") },
                    { new Guid("9a73706d-6a0f-4272-8fb2-7ffe94bec684"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") },
                    { new Guid("c963bc11-d8ff-476f-a262-3ede2f035c0d"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") },
                    { new Guid("c1b12151-fb82-48f8-925a-11e63cc8939e"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") },
                    { new Guid("22008334-5643-47b1-9e7a-59b9e833c4a7"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") },
                    { new Guid("cfa74cf0-c3cc-4085-b132-af1add4cfac6"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") },
                    { new Guid("450c571c-021f-4ceb-9555-49b5ec9b8efa"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") },
                    { new Guid("fa99fb86-f2e0-4f83-8cf7-419e2917c4e2"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") },
                    { new Guid("db97e70f-a531-45ac-9f3c-237e6e2644be"), new Guid("626c6544-cd74-4a71-941b-da41935caa66") },
                    { new Guid("73f83e4a-660e-4e83-82c2-da7829bc278c"), new Guid("d09363b2-10c0-46d9-9f40-99274c362938") },
                    { new Guid("450c571c-021f-4ceb-9555-49b5ec9b8efa"), new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d") },
                    { new Guid("fd668aed-a77a-414a-82f9-5126339fadaa"), new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d") },
                    { new Guid("2061fdbb-5f20-479d-8788-a945e42f1f45"), new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d") },
                    { new Guid("de8739f1-d817-41df-8d6b-1d193bf4592f"), new Guid("626c6544-cd74-4a71-941b-da41935caa66") },
                    { new Guid("450c571c-021f-4ceb-9555-49b5ec9b8efa"), new Guid("d09363b2-10c0-46d9-9f40-99274c362938") },
                    { new Guid("2f87c7c8-7716-4ddf-a5c6-19e44d82359a"), new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8") },
                    { new Guid("942f7575-225c-456a-8864-20b3e61db70d"), new Guid("d09363b2-10c0-46d9-9f40-99274c362938") },
                    { new Guid("d206d821-a9e5-456d-84d5-1c2635dd89f2"), new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8") },
                    { new Guid("6e9fb0cd-f900-45ca-8915-a689b0e979e4"), new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0") },
                    { new Guid("86f2962d-ad8d-48a9-af5b-abf80f19bcd9"), new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0") },
                    { new Guid("b71bc17a-34e3-48f6-bb3a-c6f32ddca395"), new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0") },
                    { new Guid("2b6357ad-bb54-4ace-a8a7-2d67694d3c11"), new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("7e3a8e79-2b41-4908-8484-4eec8d20ebf4"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") },
                    { new Guid("89055e3c-0cc0-428c-85ce-f178aa4f6777"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") },
                    { new Guid("3a8ba2e3-163f-439c-acb6-976bdcdc3bf2"), new Guid("626c6544-cd74-4a71-941b-da41935caa66") },
                    { new Guid("21630c2b-ac4e-4316-8738-c89a33d43c46"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") },
                    { new Guid("40a926c7-cfcb-4d83-b0d2-7fec4fbe4328"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") },
                    { new Guid("b830e7e8-38b6-46e9-ba13-6f1ad87a9f38"), new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0") },
                    { new Guid("44adaece-9f75-4026-a387-94372bc1f5f5"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") },
                    { new Guid("7c91d8bc-eb3a-4055-aeb0-c946c041f79a"), new Guid("d09363b2-10c0-46d9-9f40-99274c362938") },
                    { new Guid("4ea7919b-8b72-4032-bd45-204be27722a7"), new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("3a8ba2e3-163f-439c-acb6-976bdcdc3bf2"), new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d") },
                    { new Guid("11e32106-9911-45ca-ab9d-2c3f2a2e66a2"), new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8") },
                    { new Guid("65e7faba-a5fc-44a1-8802-faa4dc23166c"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") },
                    { new Guid("57a005ce-12b8-41fe-ab4c-31514264efdd"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") },
                    { new Guid("12983b73-5d9f-488a-b6cd-5402d418cae1"), new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8") },
                    { new Guid("a30001aa-b563-4021-89f0-66db4e6b2b52"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") }
                });

            migrationBuilder.InsertData(
                table: "GenerosFilmes",
                columns: new[] { "IdFilme", "IdGenero" },
                values: new object[,]
                {
                    { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") },
                    { new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") },
                    { new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") },
                    { new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") },
                    { new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") },
                    { new Guid("626c6544-cd74-4a71-941b-da41935caa66"), new Guid("690016e2-ce2f-4ce9-a18d-386ce8f44a54") },
                    { new Guid("baff59a7-b91d-474f-a741-d16d6928c363"), new Guid("631291f2-40e3-42a7-89d4-6232c7ce44c1") },
                    { new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0"), new Guid("e5b01ccf-d60d-412f-bc1e-c732bdb3d056") },
                    { new Guid("626c6544-cd74-4a71-941b-da41935caa66"), new Guid("a44ad41d-6e62-4a8c-8ab5-d3a6b73e71d7") },
                    { new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8"), new Guid("a44ad41d-6e62-4a8c-8ab5-d3a6b73e71d7") },
                    { new Guid("baff59a7-b91d-474f-a741-d16d6928c363"), new Guid("a44ad41d-6e62-4a8c-8ab5-d3a6b73e71d7") },
                    { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), new Guid("9769c6cc-39db-4dc2-b767-8b673ac3c587") },
                    { new Guid("626c6544-cd74-4a71-941b-da41935caa66"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") },
                    { new Guid("08ed366d-07ee-4fc4-98f5-497492679dce"), new Guid("690016e2-ce2f-4ce9-a18d-386ce8f44a54") },
                    { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), new Guid("d41ce0ce-7b09-4d09-b5a6-62e00d167017") },
                    { new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8"), new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d") },
                    { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), new Guid("7f474172-8942-478a-8e19-e8970a96aa28") },
                    { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d") },
                    { new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76"), new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d") },
                    { new Guid("08ed366d-07ee-4fc4-98f5-497492679dce"), new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d") },
                    { new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8"), new Guid("77e00a20-5b1c-43b7-be04-d3ce0622cafc") },
                    { new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647"), new Guid("7f474172-8942-478a-8e19-e8970a96aa28") },
                    { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6") },
                    { new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547"), new Guid("77e00a20-5b1c-43b7-be04-d3ce0622cafc") },
                    { new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8"), new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6") },
                    { new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547"), new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6") },
                    { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), new Guid("8c1e7ab4-6cac-4e7d-aede-91508452194e") },
                    { new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647"), new Guid("8c1e7ab4-6cac-4e7d-aede-91508452194e") },
                    { new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547"), new Guid("8c1e7ab4-6cac-4e7d-aede-91508452194e") },
                    { new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76"), new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6") }
                });

            migrationBuilder.InsertData(
                table: "Votos",
                columns: new[] { "IdFilme", "IdUsuario", "Nota" },
                values: new object[,]
                {
                    { new Guid("baff59a7-b91d-474f-a741-d16d6928c363"), new Guid("f7c280e2-f7b1-4df0-b185-b49e7e26a92c"), (byte)3 },
                    { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), new Guid("f7c280e2-f7b1-4df0-b185-b49e7e26a92c"), (byte)1 },
                    { new Guid("08ed366d-07ee-4fc4-98f5-497492679dce"), new Guid("f7c280e2-f7b1-4df0-b185-b49e7e26a92c"), (byte)4 }
                });
        }
    }
}
