﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Empresas.Migrations
{
    public partial class SeedInicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Atores",
                columns: table => new
                {
                    IdAtor = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("IdAtor", x => x.IdAtor);
                });

            migrationBuilder.CreateTable(
                name: "Diretores",
                columns: table => new
                {
                    IdDiretor = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("IdDiretor", x => x.IdDiretor);
                });

            migrationBuilder.CreateTable(
                name: "Filmes",
                columns: table => new
                {
                    IdFilme = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    NomeOriginal = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    Sinopse = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("IdFilme", x => x.IdFilme);
                });

            migrationBuilder.CreateTable(
                name: "Generos",
                columns: table => new
                {
                    IdGenero = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("IdGenero", x => x.IdGenero);
                });

            migrationBuilder.CreateTable(
                name: "Usuarios",
                columns: table => new
                {
                    IdUsuario = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("IdUsuario", x => x.IdUsuario);
                });

            migrationBuilder.CreateTable(
                name: "AtoresFilmes",
                columns: table => new
                {
                    IdAtor = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IdFilme = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AtoresFilmes", x => new { x.IdAtor, x.IdFilme });
                    table.ForeignKey(
                        name: "FK_AtoresFilmes_Atores_IdAtor",
                        column: x => x.IdAtor,
                        principalTable: "Atores",
                        principalColumn: "IdAtor",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AtoresFilmes_Filmes_IdFilme",
                        column: x => x.IdFilme,
                        principalTable: "Filmes",
                        principalColumn: "IdFilme",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DiretoresFilmes",
                columns: table => new
                {
                    IdDiretor = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IdFilme = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiretoresFilmes", x => new { x.IdDiretor, x.IdFilme });
                    table.ForeignKey(
                        name: "FK_DiretoresFilmes_Diretores_IdDiretor",
                        column: x => x.IdDiretor,
                        principalTable: "Diretores",
                        principalColumn: "IdDiretor",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DiretoresFilmes_Filmes_IdFilme",
                        column: x => x.IdFilme,
                        principalTable: "Filmes",
                        principalColumn: "IdFilme",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GenerosFilmes",
                columns: table => new
                {
                    IdGenero = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IdFilme = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenerosFilmes", x => new { x.IdGenero, x.IdFilme });
                    table.ForeignKey(
                        name: "FK_GenerosFilmes_Filmes_IdFilme",
                        column: x => x.IdFilme,
                        principalTable: "Filmes",
                        principalColumn: "IdFilme",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GenerosFilmes_Generos_IdGenero",
                        column: x => x.IdGenero,
                        principalTable: "Generos",
                        principalColumn: "IdGenero",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Votos",
                columns: table => new
                {
                    IdUsuario = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IdFilme = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nota = table.Column<byte>(type: "TINYINT", maxLength: 1, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Votos", x => new { x.IdFilme, x.IdUsuario });
                    table.ForeignKey(
                        name: "FK_Votos_Filmes_IdFilme",
                        column: x => x.IdFilme,
                        principalTable: "Filmes",
                        principalColumn: "IdFilme",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Votos_Usuarios_IdUsuario",
                        column: x => x.IdUsuario,
                        principalTable: "Usuarios",
                        principalColumn: "IdUsuario",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Atores",
                columns: new[] { "IdAtor", "Nome" },
                values: new object[,]
                {
                    { new Guid("f1755ca9-b741-406b-ace4-66e946adb422"), "Ben Burtt" },
                    { new Guid("11136c6e-ea89-4805-ad42-fb2d840fc72f"), "Woody Harrelson" },
                    { new Guid("a47e41be-997a-4d65-a020-65dc927c50d3"), "Tom Sizemore" },
                    { new Guid("1708727b-cec4-4d94-b7b5-29a65ef69ccb"), "Robert Downey Jr." },
                    { new Guid("512f083b-d67e-4f8a-98df-12720aac235f"), "Rachel McAdams" },
                    { new Guid("40f35089-fa08-4a9d-b9ef-755e8901360d"), "Paul Rhys" },
                    { new Guid("8c8962ed-7d90-422b-8631-4bee75848588"), "Patton Oswalt" },
                    { new Guid("8af8cb18-67a4-46a2-9019-a7b5b3afab08"), "Patrick Magee" },
                    { new Guid("c8006c59-7f7a-4685-8b9f-ee1a864c0fa1"), "Owen Wilson" },
                    { new Guid("6441de59-efff-49d4-bcc0-904af643f74f"), "Mickey Rourke" },
                    { new Guid("407af964-cf25-485e-9647-f17bd800437a"), "Michael Bates" },
                    { new Guid("88eb4550-4799-468d-a6dd-c240b76fa1bc"), "Mark Ruffalo" },
                    { new Guid("12b99b87-dcb7-449b-9e6a-88a1cad87f2e"), "Malcolm McDowell" },
                    { new Guid("93385a88-782e-4d64-bb04-60481e93a2d9"), "Laurence Fishburne" },
                    { new Guid("dc8a4798-978a-4fa3-a803-fa268a1a37a3"), "Keanu Reeves" },
                    { new Guid("a6439e9d-3639-4a78-9dba-f03464fda36c"), "Lou Romano" },
                    { new Guid("ac4185ec-1d63-46af-99c2-c2cf1f95586c"), "Jude Law" },
                    { new Guid("6ef5c12e-1ffb-4966-a437-12d862e2c69c"), "Brad Garrett" },
                    { new Guid("f05e9d19-ab22-45f1-a32f-c52128c31334"), "Bruce Willis" },
                    { new Guid("7edc9d77-d1f1-43e7-b724-c4eec552b19b"), "Carrie-Anne Moss" },
                    { new Guid("39cc4a77-79fe-4b42-a541-9aa37a0be4f2"), "Chris Evans" },
                    { new Guid("47b29f32-3707-4f4c-82ce-b67214c146d0"), "Juliette Lewis" },
                    { new Guid("c3a56400-6b39-499f-97b2-38f9ae9ff522"), "Daniel Mays" },
                    { new Guid("8d7be067-e06c-4061-b08c-c6ef0b064845"), "Dean-Charles Chapman" },
                    { new Guid("9ccd101c-2d4d-45e2-a8e7-b30e2d539d4e"), "Clive Owen" },
                    { new Guid("90752a8d-b04c-4303-af67-34a3cab4fd9a"), "Eric Dane" },
                    { new Guid("496608f6-fb01-4efc-9b35-6d8685628846"), "George MacKay" },
                    { new Guid("6fcfc19d-f7fa-486b-97d7-843afbf9a70b"), "Geraldine Chaplin" },
                    { new Guid("90946e99-8420-47cb-935a-8de8871f950a"), "Jeff Garlin" },
                    { new Guid("7591a5c9-7697-43a6-b6c7-710ba84741dd"), "Jennifer Aniston" },
                    { new Guid("0d935bb4-b938-4371-a9ff-a611f63eeca0"), "Elissa Knight" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("99e3a622-22aa-435d-8a59-315de776995e"), "Andrew Stanton" },
                    { new Guid("5df75661-d212-4d58-8017-8ab9f116c8d5"), "Stanley Kubrick" },
                    { new Guid("cd615713-cd84-4d2c-8c0e-e195c008a146"), "Oliver Stone" },
                    { new Guid("e8337fd4-186b-4c40-9e8b-989c95e61a88"), "Richard Attenborough" },
                    { new Guid("80e06ee8-bf82-4c05-a66e-dbcc0a478421"), "David Frankel" },
                    { new Guid("724b1fbe-43dd-4dcf-8071-49e9683fd503"), "Sam Mendes" },
                    { new Guid("ffc090c1-a591-4f4c-affe-bcf304bf8ea6"), "Joe Russo" },
                    { new Guid("46bc8564-7d32-4056-bf79-3cdf5cbc64a6"), "Guy Ritchie" },
                    { new Guid("c671aa0c-10c7-48bb-9115-8328520d06c2"), "Lilly Wachowski" },
                    { new Guid("5c4ccdbf-76b7-4db7-9d3d-44f5176992fe"), "Lana Wachowski" },
                    { new Guid("9d958b2f-701a-4f82-82ca-b1c771d63654"), "Jan Pinkava" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("a2bb4bce-1571-4324-b980-b38a6b7f2f74"), "Brad Bird" },
                    { new Guid("d910979f-b727-43f9-b906-fd2f73531033"), "Anthony Russo" },
                    { new Guid("8095f1ab-4854-4cfc-9d8a-4e325f05c03d"), "Robert Rodriguez" },
                    { new Guid("515fb4a8-7431-4b0d-a19d-a4cf7f08ade3"), "Quentin Tarantino" },
                    { new Guid("811df5fc-fcfe-4156-8741-8460db8a4fbd"), "Frank Miller" }
                });

            migrationBuilder.InsertData(
                table: "Filmes",
                columns: new[] { "IdFilme", "Ano", "Nome", "NomeOriginal", "Sinopse" },
                values: new object[,]
                {
                    { new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492"), 1999, "Matrix", "The Matrix", "Um hacker aprende com os misteriosos rebeldes sobre a verdadeira natureza de sua realidade e seu papel na guerra contra seus controladores." },
                    { new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd"), 2007, "Ratatouille", "Ratatouille", "Um rato que pode cozinhar forja uma aliança incomum com um jovem garoto em um famoso restaurante em Paris." },
                    { new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03"), 2008, "Marley & Eu", "Marley & Me", "Uma família aprende importantes lições de vida com seu adorável, mas malicioso e neurótico cão." },
                    { new Guid("887a0155-a808-4617-a083-807a20ee03af"), 2008, "WALL·E", "WALL·E", "Num futuro distante, um pequeno robô faz uma viagem que decidirá o destino da humanidade." },
                    { new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842"), 2019, "Vingadores: Ultimato", "Avengers: Endgame", "Após os eventos devastadores de Vingadores: Guerra Infinita , o universo está em ruínas, e com a ajuda de aliados os Vingadores se reúnem para desfazer as ações de Thanos e restaurar a ordem." },
                    { new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e"), 2005, "Sin City: A Cidade do Pecado", "Sin City", "O filme explora a miserável cidade de Basin City, e conta a historia de três pessoas diferentes, envoltas em violencia e corrupção." },
                    { new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25"), 1994, "Assassinos por Natureza", "Natural Born Killers", "Duas vítimas de uma infância traumatizada tornam-se amantes e assassinos em série, irresponsavelmente glorificados pela mídia." },
                    { new Guid("65723cce-271f-4e44-b3da-abda9a200e75"), 1992, "Chaplin", "Chaplin", "Relata a vida convulsiva e controversa do mestre diretor de comédia Charles Chaplin." },
                    { new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e"), 2009, "Sherlock Holmes", "Sherlock Holmes", "O detective Sherlock Holmes e seu colega Watson se engajam numa batalha para lutar contra as ameaças da Inglaterra." },
                    { new Guid("d2f4766f-d067-423a-bcee-efaa72b18466"), 1971, "Laranja Mecânica", "A Clockwork Orange", "No futuro, um líder duma turma é levado para prisão é decide ser voluntário dum experimento, mais os resultados não são os esperados." },
                    { new Guid("1c6498a2-1e94-4502-8075-e77254ed0837"), 2019, "1917", "1917", "6 de Abril de 1917. Enquanto um regimento se reúne para lutar uma guerra nas profundezas do território inimigo, dois soldados são designados para uma corrida contra o tempo para entregar uma mensagem que impedirá 1.600 homens de caminhar direto para uma uma armadilha mortal." }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("3652b9ec-84cc-4e0c-8d5b-07f08ba715b4"), "Fantasia" },
                    { new Guid("2ecf855c-155c-43ee-8837-3cddfa3cc272"), "Fantasia científica" },
                    { new Guid("4314c929-878a-49f1-b839-5c59ff7ee314"), "Ficção científica" },
                    { new Guid("a8eb0ae5-14bc-431b-acd3-7ad62cc4037e"), "Filmes com truques" },
                    { new Guid("7a193114-6501-4290-ae6a-8d2770b536be"), "Filmes de guerra" },
                    { new Guid("2ddc3f89-02ce-4da3-a6fc-c27011a81db9"), "Musical" },
                    { new Guid("29507087-cd6d-4307-a4e6-9d608e6ee517"), "Filme policial" },
                    { new Guid("2aba41b9-97ae-419b-afea-b69bf66fc3d1"), "Romance" },
                    { new Guid("e98f0db8-370e-422e-a894-b4248a52751e"), "Seriado" },
                    { new Guid("af8a6dd6-963f-49e0-a04d-e6e5106541df"), "Suspense" },
                    { new Guid("5584ebee-2a34-4334-b826-d8adf39f28b9"), "Terror" },
                    { new Guid("713df95d-ac8b-4f81-9bd7-5fe29a74e762"), "Faroeste" },
                    { new Guid("2fb3f195-8b18-4e8d-89a0-ec043fae08dc"), "Thriller" },
                    { new Guid("bf20207e-0d5c-4a84-8f6e-5b93dc5ea541"), "Espionagem" },
                    { new Guid("abf51e6b-338f-41db-8ce4-06aee816f436"), "Comédia" },
                    { new Guid("48de0e28-4b5c-4fe0-a04c-9eb4bba42d84"), "Docuficção" },
                    { new Guid("9309f31a-9111-4081-a59e-c2a2792a84ae"), "Documentário" },
                    { new Guid("09f12992-bf17-408a-a084-6c4266fffc9c"), "Dança" },
                    { new Guid("0f84c5fa-ffe6-42c8-addf-85a3bb3d3766"), "Comédia romântica" },
                    { new Guid("9d078828-9155-409a-b1d7-17e4c49652b0"), "Comédia dramática" },
                    { new Guid("da7d4b67-be2d-44ea-a79c-60c349484087"), "Comédia de terror" },
                    { new Guid("03c44a8b-a9a7-44f0-a58d-b673ffc88dc5"), "Comédia de ação" },
                    { new Guid("35a7e324-ebd1-4224-8f16-ff69be7e0c3d"), "Chanchada" },
                    { new Guid("5dd8cf2c-e8f8-4898-a72e-4e7a8389d0d6"), "Cinema de arte" },
                    { new Guid("89d2ca23-53a8-4175-913a-065ad7347144"), "Aventura" },
                    { new Guid("18f2e88f-0800-4c9c-af25-b9f5f07c93e2"), "Animação" }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("f38727cc-e1e2-482a-b50a-dafc00a59612"), "Ação" },
                    { new Guid("495e1a14-5c32-4987-977a-1148891b43f2"), "Pornográfico" },
                    { new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49"), "Drama" }
                });

            migrationBuilder.InsertData(
                table: "Usuarios",
                columns: new[] { "IdUsuario", "Email", "Nome" },
                values: new object[] { new Guid("92f82fd3-9770-47b5-862b-5e93fc427443"), "rafael.av@gmail.com", "Rafael Vasconcelos" });

            migrationBuilder.InsertData(
                table: "AtoresFilmes",
                columns: new[] { "IdAtor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("12b99b87-dcb7-449b-9e6a-88a1cad87f2e"), new Guid("d2f4766f-d067-423a-bcee-efaa72b18466") },
                    { new Guid("88eb4550-4799-468d-a6dd-c240b76fa1bc"), new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842") },
                    { new Guid("f1755ca9-b741-406b-ace4-66e946adb422"), new Guid("887a0155-a808-4617-a083-807a20ee03af") },
                    { new Guid("0d935bb4-b938-4371-a9ff-a611f63eeca0"), new Guid("887a0155-a808-4617-a083-807a20ee03af") },
                    { new Guid("90946e99-8420-47cb-935a-8de8871f950a"), new Guid("887a0155-a808-4617-a083-807a20ee03af") },
                    { new Guid("c8006c59-7f7a-4685-8b9f-ee1a864c0fa1"), new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03") },
                    { new Guid("7591a5c9-7697-43a6-b6c7-710ba84741dd"), new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03") },
                    { new Guid("90752a8d-b04c-4303-af67-34a3cab4fd9a"), new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03") },
                    { new Guid("6441de59-efff-49d4-bcc0-904af643f74f"), new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e") },
                    { new Guid("9ccd101c-2d4d-45e2-a8e7-b30e2d539d4e"), new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e") },
                    { new Guid("f05e9d19-ab22-45f1-a32f-c52128c31334"), new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e") },
                    { new Guid("6ef5c12e-1ffb-4966-a437-12d862e2c69c"), new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd") },
                    { new Guid("a6439e9d-3639-4a78-9dba-f03464fda36c"), new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd") },
                    { new Guid("dc8a4798-978a-4fa3-a803-fa268a1a37a3"), new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492") },
                    { new Guid("93385a88-782e-4d64-bb04-60481e93a2d9"), new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492") },
                    { new Guid("7edc9d77-d1f1-43e7-b724-c4eec552b19b"), new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492") },
                    { new Guid("39cc4a77-79fe-4b42-a541-9aa37a0be4f2"), new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842") },
                    { new Guid("1708727b-cec4-4d94-b7b5-29a65ef69ccb"), new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842") },
                    { new Guid("8c8962ed-7d90-422b-8631-4bee75848588"), new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd") },
                    { new Guid("8af8cb18-67a4-46a2-9019-a7b5b3afab08"), new Guid("d2f4766f-d067-423a-bcee-efaa72b18466") },
                    { new Guid("512f083b-d67e-4f8a-98df-12720aac235f"), new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e") },
                    { new Guid("1708727b-cec4-4d94-b7b5-29a65ef69ccb"), new Guid("65723cce-271f-4e44-b3da-abda9a200e75") },
                    { new Guid("6fcfc19d-f7fa-486b-97d7-843afbf9a70b"), new Guid("65723cce-271f-4e44-b3da-abda9a200e75") },
                    { new Guid("40f35089-fa08-4a9d-b9ef-755e8901360d"), new Guid("65723cce-271f-4e44-b3da-abda9a200e75") },
                    { new Guid("407af964-cf25-485e-9647-f17bd800437a"), new Guid("d2f4766f-d067-423a-bcee-efaa72b18466") },
                    { new Guid("1708727b-cec4-4d94-b7b5-29a65ef69ccb"), new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e") },
                    { new Guid("11136c6e-ea89-4805-ad42-fb2d840fc72f"), new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25") },
                    { new Guid("ac4185ec-1d63-46af-99c2-c2cf1f95586c"), new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e") },
                    { new Guid("a47e41be-997a-4d65-a020-65dc927c50d3"), new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25") },
                    { new Guid("c3a56400-6b39-499f-97b2-38f9ae9ff522"), new Guid("1c6498a2-1e94-4502-8075-e77254ed0837") },
                    { new Guid("8d7be067-e06c-4061-b08c-c6ef0b064845"), new Guid("1c6498a2-1e94-4502-8075-e77254ed0837") },
                    { new Guid("496608f6-fb01-4efc-9b35-6d8685628846"), new Guid("1c6498a2-1e94-4502-8075-e77254ed0837") },
                    { new Guid("47b29f32-3707-4f4c-82ce-b67214c146d0"), new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("5c4ccdbf-76b7-4db7-9d3d-44f5176992fe"), new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492") },
                    { new Guid("c671aa0c-10c7-48bb-9115-8328520d06c2"), new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492") },
                    { new Guid("e8337fd4-186b-4c40-9e8b-989c95e61a88"), new Guid("d2f4766f-d067-423a-bcee-efaa72b18466") },
                    { new Guid("9d958b2f-701a-4f82-82ca-b1c771d63654"), new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd") },
                    { new Guid("a2bb4bce-1571-4324-b980-b38a6b7f2f74"), new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd") },
                    { new Guid("724b1fbe-43dd-4dcf-8071-49e9683fd503"), new Guid("1c6498a2-1e94-4502-8075-e77254ed0837") },
                    { new Guid("811df5fc-fcfe-4156-8741-8460db8a4fbd"), new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e") },
                    { new Guid("46bc8564-7d32-4056-bf79-3cdf5cbc64a6"), new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e") },
                    { new Guid("80e06ee8-bf82-4c05-a66e-dbcc0a478421"), new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("e8337fd4-186b-4c40-9e8b-989c95e61a88"), new Guid("65723cce-271f-4e44-b3da-abda9a200e75") },
                    { new Guid("99e3a622-22aa-435d-8a59-315de776995e"), new Guid("887a0155-a808-4617-a083-807a20ee03af") },
                    { new Guid("ffc090c1-a591-4f4c-affe-bcf304bf8ea6"), new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842") },
                    { new Guid("d910979f-b727-43f9-b906-fd2f73531033"), new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842") },
                    { new Guid("cd615713-cd84-4d2c-8c0e-e195c008a146"), new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25") },
                    { new Guid("515fb4a8-7431-4b0d-a19d-a4cf7f08ade3"), new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e") }
                });

            migrationBuilder.InsertData(
                table: "GenerosFilmes",
                columns: new[] { "IdFilme", "IdGenero" },
                values: new object[,]
                {
                    { new Guid("65723cce-271f-4e44-b3da-abda9a200e75"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") },
                    { new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") },
                    { new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") },
                    { new Guid("1c6498a2-1e94-4502-8075-e77254ed0837"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") },
                    { new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") },
                    { new Guid("d2f4766f-d067-423a-bcee-efaa72b18466"), new Guid("4314c929-878a-49f1-b839-5c59ff7ee314") },
                    { new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e"), new Guid("2fb3f195-8b18-4e8d-89a0-ec043fae08dc") },
                    { new Guid("1c6498a2-1e94-4502-8075-e77254ed0837"), new Guid("7a193114-6501-4290-ae6a-8d2770b536be") },
                    { new Guid("d2f4766f-d067-423a-bcee-efaa72b18466"), new Guid("29507087-cd6d-4307-a4e6-9d608e6ee517") },
                    { new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25"), new Guid("29507087-cd6d-4307-a4e6-9d608e6ee517") },
                    { new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e"), new Guid("29507087-cd6d-4307-a4e6-9d608e6ee517") },
                    { new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e"), new Guid("af8a6dd6-963f-49e0-a04d-e6e5106541df") },
                    { new Guid("d2f4766f-d067-423a-bcee-efaa72b18466"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") },
                    { new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492"), new Guid("4314c929-878a-49f1-b839-5c59ff7ee314") },
                    { new Guid("65723cce-271f-4e44-b3da-abda9a200e75"), new Guid("48de0e28-4b5c-4fe0-a04c-9eb4bba42d84") },
                    { new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25"), new Guid("f38727cc-e1e2-482a-b50a-dafc00a59612") },
                    { new Guid("65723cce-271f-4e44-b3da-abda9a200e75"), new Guid("9d078828-9155-409a-b1d7-17e4c49652b0") },
                    { new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e"), new Guid("f38727cc-e1e2-482a-b50a-dafc00a59612") },
                    { new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842"), new Guid("f38727cc-e1e2-482a-b50a-dafc00a59612") },
                    { new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492"), new Guid("f38727cc-e1e2-482a-b50a-dafc00a59612") },
                    { new Guid("887a0155-a808-4617-a083-807a20ee03af"), new Guid("18f2e88f-0800-4c9c-af25-b9f5f07c93e2") },
                    { new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03"), new Guid("9d078828-9155-409a-b1d7-17e4c49652b0") },
                    { new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e"), new Guid("89d2ca23-53a8-4175-913a-065ad7347144") },
                    { new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd"), new Guid("18f2e88f-0800-4c9c-af25-b9f5f07c93e2") },
                    { new Guid("887a0155-a808-4617-a083-807a20ee03af"), new Guid("89d2ca23-53a8-4175-913a-065ad7347144") },
                    { new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd"), new Guid("89d2ca23-53a8-4175-913a-065ad7347144") },
                    { new Guid("65723cce-271f-4e44-b3da-abda9a200e75"), new Guid("abf51e6b-338f-41db-8ce4-06aee816f436") },
                    { new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03"), new Guid("abf51e6b-338f-41db-8ce4-06aee816f436") },
                    { new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd"), new Guid("abf51e6b-338f-41db-8ce4-06aee816f436") },
                    { new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842"), new Guid("89d2ca23-53a8-4175-913a-065ad7347144") }
                });

            migrationBuilder.InsertData(
                table: "Votos",
                columns: new[] { "IdFilme", "IdUsuario", "Nota" },
                values: new object[,]
                {
                    { new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e"), new Guid("92f82fd3-9770-47b5-862b-5e93fc427443"), (byte)3 },
                    { new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e"), new Guid("92f82fd3-9770-47b5-862b-5e93fc427443"), (byte)1 },
                    { new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492"), new Guid("92f82fd3-9770-47b5-862b-5e93fc427443"), (byte)4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AtoresFilmes_IdFilme",
                table: "AtoresFilmes",
                column: "IdFilme");

            migrationBuilder.CreateIndex(
                name: "IX_DiretoresFilmes_IdFilme",
                table: "DiretoresFilmes",
                column: "IdFilme");

            migrationBuilder.CreateIndex(
                name: "IX_GenerosFilmes_IdFilme",
                table: "GenerosFilmes",
                column: "IdFilme");

            migrationBuilder.CreateIndex(
                name: "IX_Votos_IdUsuario",
                table: "Votos",
                column: "IdUsuario");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AtoresFilmes");

            migrationBuilder.DropTable(
                name: "DiretoresFilmes");

            migrationBuilder.DropTable(
                name: "GenerosFilmes");

            migrationBuilder.DropTable(
                name: "Votos");

            migrationBuilder.DropTable(
                name: "Atores");

            migrationBuilder.DropTable(
                name: "Diretores");

            migrationBuilder.DropTable(
                name: "Generos");

            migrationBuilder.DropTable(
                name: "Filmes");

            migrationBuilder.DropTable(
                name: "Usuarios");
        }
    }
}
