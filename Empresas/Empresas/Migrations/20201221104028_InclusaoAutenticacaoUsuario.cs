﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Empresas.Migrations
{
    public partial class InclusaoAutenticacaoUsuario : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("00fe0005-6d47-4c4d-a9b6-f1d5264a99b5"), new Guid("1a61255a-2e53-41b7-bd65-c282de4206df") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("0cd9752d-6d74-49a0-97cf-03ae209814bf"), new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("0f6b5159-541a-4dd3-b821-871b841e0aae"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("140beb3c-74db-4372-8ccc-aeab0cdb76f2"), new Guid("60855762-c883-4aa4-89bd-551b417db7cc") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("1b90f3fe-0494-4b56-a260-274ac3930868"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("2c1f957f-95ad-4c1a-9bb2-31bbc15f0dfe"), new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("34d715d7-0396-481d-bfa4-924698d1f753"), new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("43fe98a4-b07a-4551-90e2-5008c210a572"), new Guid("60855762-c883-4aa4-89bd-551b417db7cc") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("4a408970-5c79-44fb-ab20-47c4bf78f599"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("4abd4571-8f91-4ff8-8a87-acaefd817e41"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("776cd318-6852-4967-ae65-4085a327c9bc"), new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("7bcdb15d-8adc-46e1-a37a-bd1b908a9551"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("7da807b9-2f4f-4a92-98bd-f5f3ee970b92"), new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("8187118e-b6b1-4a81-b4d7-f8edf1561c19"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("88e8f928-6365-4c29-8283-39531100f0c3"), new Guid("60855762-c883-4aa4-89bd-551b417db7cc") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("8e300dfc-2f93-47f9-a88e-0b14c9e48308"), new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("91c715dc-177d-4cac-8a06-2a9e5b8ebfc0"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("9208885b-1e97-4373-af21-0f2264037f24"), new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("9d06c143-8658-442c-b38e-45a8c0d56dcc"), new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a0e8a983-ae76-4b50-b489-1b2586e15077"), new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a662d40e-9867-4c98-a705-a5c156951e8e"), new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a8f12ada-6f15-41d5-8f6c-b338e48f90c0"), new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("aa7f747d-276b-4043-a492-b52a2ea68fdb"), new Guid("1a61255a-2e53-41b7-bd65-c282de4206df") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("c1273c2f-a652-430b-a685-370c88ad9366"), new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("cdec62b0-0fbb-43c0-b5f8-f71304312b22"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ce58e276-f504-498a-b8bc-662d091bf4f9"), new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("cf9d3bd9-6ea5-4936-9961-27508d4a5ff2"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("eaeca0f6-2176-4dde-a9c1-073586d9fd96"), new Guid("1a61255a-2e53-41b7-bd65-c282de4206df") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("f1fff516-d67c-4b8b-916e-f46d3dc85121"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("f1fff516-d67c-4b8b-916e-f46d3dc85121"), new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("f1fff516-d67c-4b8b-916e-f46d3dc85121"), new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("f3a52737-efea-4ebb-9080-3fc0b16bd429"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("f70aea6c-01f8-4894-b0c6-6bc78f3b5f1f"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") });

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("2f6d7500-53c8-48e0-b69d-8acfc8bc6bc1"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("e5f49d52-741e-4323-9350-764c09a12bce"));

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("0c292b93-b386-4e1e-9b31-8ba149f2ec51"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("0fe8dee8-7702-449f-839f-1ab33f09b5ef"), new Guid("60855762-c883-4aa4-89bd-551b417db7cc") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("327995a0-1d91-4b92-98f7-b69c6977fbc3"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("34e80a00-c349-43d5-8ef5-b30961c5400e"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("59b7cf35-ba56-4b0a-9760-7fb56718a57d"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("60702279-9b90-4020-9956-9adc5e368332"), new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("6162fba2-83dd-4136-91e8-ab5510a52c51"), new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("6f70dcae-97dd-4c71-bb8f-411d6aa08fec"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("8287f98b-2b0a-4fef-823f-1e425e7dd383"), new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("adbcb7ba-699c-4008-a929-9e097cb908ae"), new Guid("1a61255a-2e53-41b7-bd65-c282de4206df") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("d4fda122-fef5-4fe7-baac-44bb061204b8"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("e54e5f58-3244-4a3b-b227-7ef82e4f1a8c"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("f24d233b-cbd8-4b4d-835c-b441330fb9bf"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("fc57962c-1012-4947-87e0-a061fa1dd423"), new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("fc57962c-1012-4947-87e0-a061fa1dd423"), new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32") });

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("001a5d4d-29a9-421c-a353-ff0af1e12439"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("0afb542e-fa9f-46bc-b62b-0a0897691d38"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("15b36716-a135-4237-a4ed-fd0c5cfefd95"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("1916d087-615b-4c31-b852-d4d9b5df896a"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("1b901728-9d77-4570-ac7e-f538b936c73c"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("1cde864a-c157-4e1d-9192-b7ed24abb54a"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("2071bd6d-8303-4d46-8e99-cf4d9d83f11f"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("2ce32b41-fa38-451e-85e5-66e935392de5"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("38f7e4b2-f69f-4af1-844d-a9490ea3d603"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("3e2a01ef-668c-4c7a-8b68-4ff4484d0e2c"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("559e18cc-12c0-4243-8c3a-b9e105b24c57"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7203ac7f-57e0-4a50-a8ea-61fda4c9ac38"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("790b979b-8294-4371-b140-57665dea8da2"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7ea8d8cd-cb9b-4a4e-9d47-8220260db814"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("89641b75-6cae-4df4-a789-d17ef7d08587"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("e572488a-2b52-4c44-ab00-72df2c2a4a60"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f57247a6-2114-488f-9320-617bbcc90392"));

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec"), new Guid("196c90d8-42f6-4bf6-9bba-302ec1f53102") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), new Guid("196c90d8-42f6-4bf6-9bba-302ec1f53102") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0"), new Guid("196c90d8-42f6-4bf6-9bba-302ec1f53102") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("60855762-c883-4aa4-89bd-551b417db7cc"), new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("6f676918-14f4-4692-9e63-d5d619f1b407"), new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0"), new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), new Guid("32dd38a8-ca7c-435c-8d48-f81717a21e6d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("6f676918-14f4-4692-9e63-d5d619f1b407"), new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4"), new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f"), new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec"), new Guid("766229b6-77e3-43f2-873d-494dc3961b20") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), new Guid("766229b6-77e3-43f2-873d-494dc3961b20") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665"), new Guid("85f5242a-abb2-4466-a845-4e73b6515a8c") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42"), new Guid("85f5242a-abb2-4466-a845-4e73b6515a8c") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4"), new Guid("85f5242a-abb2-4466-a845-4e73b6515a8c") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665"), new Guid("91b92594-bb77-4ce6-a1e6-ff6006cb11f3") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f"), new Guid("91b92594-bb77-4ce6-a1e6-ff6006cb11f3") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("1a61255a-2e53-41b7-bd65-c282de4206df"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("6f676918-14f4-4692-9e63-d5d619f1b407"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42"), new Guid("ae7c643e-f38e-4478-8d23-5b1dde519167") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), new Guid("b9759eed-2a83-41a9-a833-e3ce68fbbd74") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("1a61255a-2e53-41b7-bd65-c282de4206df"), new Guid("baaaf5ad-c399-4c7c-a4f0-4cff60518b5d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("60855762-c883-4aa4-89bd-551b417db7cc"), new Guid("c0724bbb-73a8-4120-a7ef-14420830e046") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0"), new Guid("c0724bbb-73a8-4120-a7ef-14420830e046") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42"), new Guid("6a76883e-764e-4184-8b19-206a3ed8e2e8") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), new Guid("6a76883e-764e-4184-8b19-206a3ed8e2e8") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f"), new Guid("6a76883e-764e-4184-8b19-206a3ed8e2e8") });

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("00fe0005-6d47-4c4d-a9b6-f1d5264a99b5"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("0cd9752d-6d74-49a0-97cf-03ae209814bf"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("0f6b5159-541a-4dd3-b821-871b841e0aae"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("140beb3c-74db-4372-8ccc-aeab0cdb76f2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("1b90f3fe-0494-4b56-a260-274ac3930868"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("2c1f957f-95ad-4c1a-9bb2-31bbc15f0dfe"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("34d715d7-0396-481d-bfa4-924698d1f753"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("43fe98a4-b07a-4551-90e2-5008c210a572"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("4a408970-5c79-44fb-ab20-47c4bf78f599"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("4abd4571-8f91-4ff8-8a87-acaefd817e41"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("776cd318-6852-4967-ae65-4085a327c9bc"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("7bcdb15d-8adc-46e1-a37a-bd1b908a9551"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("7da807b9-2f4f-4a92-98bd-f5f3ee970b92"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("8187118e-b6b1-4a81-b4d7-f8edf1561c19"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("88e8f928-6365-4c29-8283-39531100f0c3"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("8e300dfc-2f93-47f9-a88e-0b14c9e48308"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("91c715dc-177d-4cac-8a06-2a9e5b8ebfc0"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("9208885b-1e97-4373-af21-0f2264037f24"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("9d06c143-8658-442c-b38e-45a8c0d56dcc"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a0e8a983-ae76-4b50-b489-1b2586e15077"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a662d40e-9867-4c98-a705-a5c156951e8e"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a8f12ada-6f15-41d5-8f6c-b338e48f90c0"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("aa7f747d-276b-4043-a492-b52a2ea68fdb"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("c1273c2f-a652-430b-a685-370c88ad9366"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("cdec62b0-0fbb-43c0-b5f8-f71304312b22"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ce58e276-f504-498a-b8bc-662d091bf4f9"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("cf9d3bd9-6ea5-4936-9961-27508d4a5ff2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("eaeca0f6-2176-4dde-a9c1-073586d9fd96"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("f1fff516-d67c-4b8b-916e-f46d3dc85121"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("f3a52737-efea-4ebb-9080-3fc0b16bd429"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("f70aea6c-01f8-4894-b0c6-6bc78f3b5f1f"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("0c292b93-b386-4e1e-9b31-8ba149f2ec51"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("0fe8dee8-7702-449f-839f-1ab33f09b5ef"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("327995a0-1d91-4b92-98f7-b69c6977fbc3"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("34e80a00-c349-43d5-8ef5-b30961c5400e"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("59b7cf35-ba56-4b0a-9760-7fb56718a57d"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("60702279-9b90-4020-9956-9adc5e368332"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("6162fba2-83dd-4136-91e8-ab5510a52c51"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("6f70dcae-97dd-4c71-bb8f-411d6aa08fec"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("8287f98b-2b0a-4fef-823f-1e425e7dd383"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("adbcb7ba-699c-4008-a929-9e097cb908ae"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("d4fda122-fef5-4fe7-baac-44bb061204b8"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("e54e5f58-3244-4a3b-b227-7ef82e4f1a8c"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("f24d233b-cbd8-4b4d-835c-b441330fb9bf"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("fc57962c-1012-4947-87e0-a061fa1dd423"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("1a61255a-2e53-41b7-bd65-c282de4206df"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("60855762-c883-4aa4-89bd-551b417db7cc"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("6f676918-14f4-4692-9e63-d5d619f1b407"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("196c90d8-42f6-4bf6-9bba-302ec1f53102"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("32dd38a8-ca7c-435c-8d48-f81717a21e6d"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("766229b6-77e3-43f2-873d-494dc3961b20"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("85f5242a-abb2-4466-a845-4e73b6515a8c"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("91b92594-bb77-4ce6-a1e6-ff6006cb11f3"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("ae7c643e-f38e-4478-8d23-5b1dde519167"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("b9759eed-2a83-41a9-a833-e3ce68fbbd74"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("baaaf5ad-c399-4c7c-a4f0-4cff60518b5d"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("c0724bbb-73a8-4120-a7ef-14420830e046"));

            migrationBuilder.DeleteData(
                table: "Usuarios",
                keyColumn: "IdUsuario",
                keyValue: new Guid("6a76883e-764e-4184-8b19-206a3ed8e2e8"));

            migrationBuilder.AddColumn<string>(
                name: "Senha",
                table: "Usuarios",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "IdentityRole",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityRole", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Atores",
                columns: new[] { "IdAtor", "Nome" },
                values: new object[,]
                {
                    { new Guid("2ed6e10d-54a8-41c0-9037-5d6c8cead46c"), "Ben Burtt" },
                    { new Guid("31e41a70-0ce1-44f0-8504-272c0420745b"), "Woody Harrelson" },
                    { new Guid("386295f4-3bb3-4256-9671-15d119ff4377"), "Tom Sizemore" },
                    { new Guid("ae68d810-a802-4969-b556-85dc5e081fa1"), "Robert Downey Jr." },
                    { new Guid("cb38f7ed-7670-44ca-937f-231d97dd569c"), "Rachel McAdams" },
                    { new Guid("c51cd834-0cb8-4c1c-accb-d923bea52bb9"), "Paul Rhys" },
                    { new Guid("269afa8d-1c6d-4047-86c7-d00351e45bec"), "Patton Oswalt" },
                    { new Guid("4aed69f8-ceb3-49da-b742-a2907cedfe20"), "Patrick Magee" },
                    { new Guid("a0c809b9-c89f-4059-aec7-d1fc1fc7b878"), "Owen Wilson" },
                    { new Guid("a41123b6-7c3e-4872-884c-25ac31fcfa4c"), "Mickey Rourke" },
                    { new Guid("bf27c15a-5c90-475f-9dfe-3cbdd73b4ea7"), "Michael Bates" },
                    { new Guid("2b651023-7ea2-4a62-b36a-7cc40e5d056a"), "Mark Ruffalo" },
                    { new Guid("b48d0f39-576b-4ef4-a941-effa0ad8b81d"), "Malcolm McDowell" },
                    { new Guid("3a5225f5-c1e3-485f-9457-2122a51e5617"), "Laurence Fishburne" },
                    { new Guid("dfc051c0-5e39-4e9f-86fa-38732544acad"), "Keanu Reeves" },
                    { new Guid("66386afe-613f-418f-b8c2-1234ab294a0b"), "Lou Romano" },
                    { new Guid("27370bd8-a38e-4d85-9394-7c75d090e55c"), "Jude Law" },
                    { new Guid("aa227b12-10c6-4aea-ba6a-3e98d303c51b"), "Brad Garrett" },
                    { new Guid("dd017454-996f-452e-b6a5-641f6b593725"), "Bruce Willis" },
                    { new Guid("76ade2a6-14a6-425e-9637-f7b8acefa39b"), "Carrie-Anne Moss" },
                    { new Guid("248c2aab-c979-4667-b3dc-b7b59382ee29"), "Chris Evans" },
                    { new Guid("b6277aed-7761-49e6-9249-d77eb71b2ffa"), "Juliette Lewis" },
                    { new Guid("c4473d09-3ae2-424a-b060-6b3fe5fb8140"), "Daniel Mays" },
                    { new Guid("288760ec-c547-4087-93b6-98fffefd645e"), "Dean-Charles Chapman" },
                    { new Guid("ee673f00-40cf-499f-aad5-2f53dd7a50df"), "Clive Owen" },
                    { new Guid("0ebfd788-29e0-4d23-91a7-0c2110b0ef18"), "Eric Dane" },
                    { new Guid("ec07de00-8e23-48dd-8420-c3da8d459e6a"), "George MacKay" },
                    { new Guid("0a99b6a9-9f80-441d-a24a-0c965a97b9fc"), "Geraldine Chaplin" },
                    { new Guid("d48071ba-63ed-4959-90dd-34f3d55deeb2"), "Jeff Garlin" },
                    { new Guid("ec4b6c2e-43d2-4ef2-a0f2-a74e7f5d3cfc"), "Jennifer Aniston" },
                    { new Guid("7bbc72ec-db21-4886-9c24-cf4960483ad2"), "Elissa Knight" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("1c6acce1-6695-48e4-90b8-7efbf4557682"), "Andrew Stanton" },
                    { new Guid("e23f169b-5c33-490a-aba8-51b5b289d3fb"), "Stanley Kubrick" },
                    { new Guid("51d2dcd3-5ebd-4307-8536-6e1b08246c57"), "Oliver Stone" },
                    { new Guid("ed943494-7105-4e40-971e-ef4973e70e52"), "Guy Ritchie" },
                    { new Guid("387b08d1-e0bc-487b-b016-29f08767ac53"), "David Frankel" },
                    { new Guid("e3f4a565-271e-4967-8201-7dc4e4ac7904"), "Sam Mendes" },
                    { new Guid("6b80d348-b569-4d02-9c91-8fda2bf8c55b"), "Joe Russo" },
                    { new Guid("45388609-13fd-4e09-b9cb-818da292724a"), "Richard Attenborough" },
                    { new Guid("77ba7cd2-a67b-4768-b0db-5a316847c0dd"), "Lilly Wachowski" },
                    { new Guid("d70aa89f-5001-44c9-957a-5c2ed373dbde"), "Lana Wachowski" },
                    { new Guid("72aa2d3e-9c1a-4d0e-a5dd-580e647bb0d6"), "Jan Pinkava" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("08b60d6e-98b7-484f-9ae5-ffcd6311cf04"), "Brad Bird" },
                    { new Guid("bf54f3fd-fa55-41f1-9912-1a92a186b9de"), "Robert Rodriguez" },
                    { new Guid("b41c440f-35b2-4357-9cb9-035e74f37f22"), "Anthony Russo" },
                    { new Guid("ae5c0f64-6325-4ba1-8c87-bff4fc4e0e31"), "Quentin Tarantino" },
                    { new Guid("52c80c68-719e-4563-ae2c-973ed5c21819"), "Frank Miller" }
                });

            migrationBuilder.InsertData(
                table: "Filmes",
                columns: new[] { "IdFilme", "Ano", "Nome", "NomeOriginal", "Sinopse" },
                values: new object[,]
                {
                    { new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa"), 1999, "Matrix", "The Matrix", "Um hacker aprende com os misteriosos rebeldes sobre a verdadeira natureza de sua realidade e seu papel na guerra contra seus controladores." },
                    { new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa"), 2007, "Ratatouille", "Ratatouille", "Um rato que pode cozinhar forja uma aliança incomum com um jovem garoto em um famoso restaurante em Paris." },
                    { new Guid("625d8851-401c-46dc-9447-a01cb30a5ead"), 2008, "Marley & Eu", "Marley & Me", "Uma família aprende importantes lições de vida com seu adorável, mas malicioso e neurótico cão." },
                    { new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f"), 2008, "WALL·E", "WALL·E", "Num futuro distante, um pequeno robô faz uma viagem que decidirá o destino da humanidade." },
                    { new Guid("3f252108-f831-49a1-9ef1-028d33fb4322"), 2019, "Vingadores: Ultimato", "Avengers: Endgame", "Após os eventos devastadores de Vingadores: Guerra Infinita , o universo está em ruínas, e com a ajuda de aliados os Vingadores se reúnem para desfazer as ações de Thanos e restaurar a ordem." },
                    { new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2"), 2005, "Sin City: A Cidade do Pecado", "Sin City", "O filme explora a miserável cidade de Basin City, e conta a historia de três pessoas diferentes, envoltas em violencia e corrupção." },
                    { new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01"), 1994, "Assassinos por Natureza", "Natural Born Killers", "Duas vítimas de uma infância traumatizada tornam-se amantes e assassinos em série, irresponsavelmente glorificados pela mídia." },
                    { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), 1992, "Chaplin", "Chaplin", "Relata a vida convulsiva e controversa do mestre diretor de comédia Charles Chaplin." },
                    { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), 2009, "Sherlock Holmes", "Sherlock Holmes", "O detective Sherlock Holmes e seu colega Watson se engajam numa batalha para lutar contra as ameaças da Inglaterra." },
                    { new Guid("26dda459-1d42-48b3-b83a-2e0e02019133"), 1971, "Laranja Mecânica", "A Clockwork Orange", "No futuro, um líder duma turma é levado para prisão é decide ser voluntário dum experimento, mais os resultados não são os esperados." },
                    { new Guid("ca2c22b2-10e3-401f-bf95-430038adea53"), 2019, "1917", "1917", "6 de Abril de 1917. Enquanto um regimento se reúne para lutar uma guerra nas profundezas do território inimigo, dois soldados são designados para uma corrida contra o tempo para entregar uma mensagem que impedirá 1.600 homens de caminhar direto para uma uma armadilha mortal." }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("e79de677-184e-472e-a24c-5a941c382c8e"), "Fantasia científica" },
                    { new Guid("c90062c6-28fd-4dab-b1f0-b71ef6059b44"), "Ficção científica" },
                    { new Guid("d8dec4da-84fa-488f-92e4-1e348380ac02"), "Filmes com truques" },
                    { new Guid("5d4c2e02-e3d6-4c86-a6ad-d4a7d528ae71"), "Filmes de guerra" },
                    { new Guid("8ca92b70-eaf2-4154-a24b-e92a8fd3de54"), "Musical" },
                    { new Guid("649f3997-daae-4d55-8104-9612c438863a"), "Pornográfico" },
                    { new Guid("7000a51f-ec26-4e1d-8ae0-60d2457fce09"), "Romance" },
                    { new Guid("f7d56204-e8a8-4829-a22d-e0dc1eb11d69"), "Seriado" },
                    { new Guid("60a4c7fc-5e89-4988-8450-b032269e17ab"), "Suspense" },
                    { new Guid("86ad15b0-1e3d-4c2e-8da1-399c8acb664e"), "Terror" },
                    { new Guid("69daea01-dd87-496b-b15a-b62a703594b1"), "Thriller" },
                    { new Guid("64353719-4fa7-4b08-870e-c1eab95d4ab0"), "Fantasia" },
                    { new Guid("c40d1ada-fb57-4b41-a395-ea7ca451f889"), "Filme policial" },
                    { new Guid("a174c834-ed44-4d0e-9e70-c8be475c991d"), "Faroeste" },
                    { new Guid("2cccf882-2683-4062-9a8b-395469d95e5f"), "Comédia de ação" },
                    { new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5"), "Drama" },
                    { new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b"), "Ação" },
                    { new Guid("1a789339-0682-4595-baff-4cd7e534cd7d"), "Animação" },
                    { new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799"), "Aventura" },
                    { new Guid("33953b22-4a32-46af-a0c1-ec7cad8934c2"), "Cinema de arte" },
                    { new Guid("34865546-1100-4973-b543-ea7e8d5f3f09"), "Espionagem" },
                    { new Guid("a7401deb-0954-429a-b5a1-364ec38737fa"), "Comédia" },
                    { new Guid("3173fd27-ba23-4ead-b087-820e07e66206"), "Chanchada" },
                    { new Guid("e16d2eba-6198-43b4-af20-c0a3ec7f8b8b"), "Comédia dramática" },
                    { new Guid("4eeb70ef-24a3-4ce9-a580-83c0b786405e"), "Comédia romântica" },
                    { new Guid("469505b4-44d5-4967-bca8-e224198360eb"), "Dança" }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("31d16437-f8db-43b8-ab00-7ef00c9675bb"), "Documentário" },
                    { new Guid("f40787f9-d2ab-4377-afae-3230cf67a5e1"), "Docuficção" },
                    { new Guid("59099e92-f18d-4163-bdcc-a1bd95837874"), "Comédia de terror" }
                });

            migrationBuilder.InsertData(
                table: "IdentityRole",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "4947910d-7c8c-4815-ba7e-f90b043d430d", "f02513c9-61d6-4562-9f51-2a6f5ffed5ee", "Admin", "ADMIN" },
                    { "267848db-d7a6-4d8d-860c-5859c94fd0bf", "b7825247-861e-458e-a351-79e5b60f8e61", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "Usuarios",
                columns: new[] { "IdUsuario", "Email", "Nome", "Senha" },
                values: new object[] { new Guid("5d3efd6d-568b-4cdd-9ae4-56dbcf30e7da"), "rafael.av@gmail.com", "Rafael Vasconcelos", "AQAAAAEAACcQAAAAEBNIfT2fSm4sayKMkwEL0Q3Fl9gxF/c7DCEJnCmRyZ78R6QDdXRMveAjm+4Y3SwaKw==" });

            migrationBuilder.InsertData(
                table: "AtoresFilmes",
                columns: new[] { "IdAtor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("b48d0f39-576b-4ef4-a941-effa0ad8b81d"), new Guid("26dda459-1d42-48b3-b83a-2e0e02019133") },
                    { new Guid("2b651023-7ea2-4a62-b36a-7cc40e5d056a"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") },
                    { new Guid("2ed6e10d-54a8-41c0-9037-5d6c8cead46c"), new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f") },
                    { new Guid("7bbc72ec-db21-4886-9c24-cf4960483ad2"), new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f") },
                    { new Guid("d48071ba-63ed-4959-90dd-34f3d55deeb2"), new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f") },
                    { new Guid("a0c809b9-c89f-4059-aec7-d1fc1fc7b878"), new Guid("625d8851-401c-46dc-9447-a01cb30a5ead") },
                    { new Guid("ec4b6c2e-43d2-4ef2-a0f2-a74e7f5d3cfc"), new Guid("625d8851-401c-46dc-9447-a01cb30a5ead") },
                    { new Guid("0ebfd788-29e0-4d23-91a7-0c2110b0ef18"), new Guid("625d8851-401c-46dc-9447-a01cb30a5ead") },
                    { new Guid("a41123b6-7c3e-4872-884c-25ac31fcfa4c"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") },
                    { new Guid("ee673f00-40cf-499f-aad5-2f53dd7a50df"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") },
                    { new Guid("dd017454-996f-452e-b6a5-641f6b593725"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") },
                    { new Guid("aa227b12-10c6-4aea-ba6a-3e98d303c51b"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") },
                    { new Guid("66386afe-613f-418f-b8c2-1234ab294a0b"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") },
                    { new Guid("dfc051c0-5e39-4e9f-86fa-38732544acad"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") },
                    { new Guid("3a5225f5-c1e3-485f-9457-2122a51e5617"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") },
                    { new Guid("76ade2a6-14a6-425e-9637-f7b8acefa39b"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") },
                    { new Guid("248c2aab-c979-4667-b3dc-b7b59382ee29"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") },
                    { new Guid("ae68d810-a802-4969-b556-85dc5e081fa1"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") },
                    { new Guid("269afa8d-1c6d-4047-86c7-d00351e45bec"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") },
                    { new Guid("4aed69f8-ceb3-49da-b742-a2907cedfe20"), new Guid("26dda459-1d42-48b3-b83a-2e0e02019133") },
                    { new Guid("cb38f7ed-7670-44ca-937f-231d97dd569c"), new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036") },
                    { new Guid("ae68d810-a802-4969-b556-85dc5e081fa1"), new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7") },
                    { new Guid("0a99b6a9-9f80-441d-a24a-0c965a97b9fc"), new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7") },
                    { new Guid("c51cd834-0cb8-4c1c-accb-d923bea52bb9"), new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7") },
                    { new Guid("bf27c15a-5c90-475f-9dfe-3cbdd73b4ea7"), new Guid("26dda459-1d42-48b3-b83a-2e0e02019133") },
                    { new Guid("ae68d810-a802-4969-b556-85dc5e081fa1"), new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036") },
                    { new Guid("31e41a70-0ce1-44f0-8504-272c0420745b"), new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01") },
                    { new Guid("27370bd8-a38e-4d85-9394-7c75d090e55c"), new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036") },
                    { new Guid("386295f4-3bb3-4256-9671-15d119ff4377"), new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01") },
                    { new Guid("c4473d09-3ae2-424a-b060-6b3fe5fb8140"), new Guid("ca2c22b2-10e3-401f-bf95-430038adea53") },
                    { new Guid("288760ec-c547-4087-93b6-98fffefd645e"), new Guid("ca2c22b2-10e3-401f-bf95-430038adea53") },
                    { new Guid("ec07de00-8e23-48dd-8420-c3da8d459e6a"), new Guid("ca2c22b2-10e3-401f-bf95-430038adea53") },
                    { new Guid("b6277aed-7761-49e6-9249-d77eb71b2ffa"), new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("d70aa89f-5001-44c9-957a-5c2ed373dbde"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") },
                    { new Guid("77ba7cd2-a67b-4768-b0db-5a316847c0dd"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") },
                    { new Guid("45388609-13fd-4e09-b9cb-818da292724a"), new Guid("26dda459-1d42-48b3-b83a-2e0e02019133") },
                    { new Guid("72aa2d3e-9c1a-4d0e-a5dd-580e647bb0d6"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") },
                    { new Guid("08b60d6e-98b7-484f-9ae5-ffcd6311cf04"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") },
                    { new Guid("e3f4a565-271e-4967-8201-7dc4e4ac7904"), new Guid("ca2c22b2-10e3-401f-bf95-430038adea53") },
                    { new Guid("52c80c68-719e-4563-ae2c-973ed5c21819"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") },
                    { new Guid("ed943494-7105-4e40-971e-ef4973e70e52"), new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036") },
                    { new Guid("387b08d1-e0bc-487b-b016-29f08767ac53"), new Guid("625d8851-401c-46dc-9447-a01cb30a5ead") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("45388609-13fd-4e09-b9cb-818da292724a"), new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7") },
                    { new Guid("1c6acce1-6695-48e4-90b8-7efbf4557682"), new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f") },
                    { new Guid("6b80d348-b569-4d02-9c91-8fda2bf8c55b"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") },
                    { new Guid("b41c440f-35b2-4357-9cb9-035e74f37f22"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") },
                    { new Guid("51d2dcd3-5ebd-4307-8536-6e1b08246c57"), new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01") },
                    { new Guid("ae5c0f64-6325-4ba1-8c87-bff4fc4e0e31"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") }
                });

            migrationBuilder.InsertData(
                table: "GenerosFilmes",
                columns: new[] { "IdFilme", "IdGenero" },
                values: new object[,]
                {
                    { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") },
                    { new Guid("625d8851-401c-46dc-9447-a01cb30a5ead"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") },
                    { new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") },
                    { new Guid("ca2c22b2-10e3-401f-bf95-430038adea53"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") },
                    { new Guid("3f252108-f831-49a1-9ef1-028d33fb4322"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") },
                    { new Guid("26dda459-1d42-48b3-b83a-2e0e02019133"), new Guid("c90062c6-28fd-4dab-b1f0-b71ef6059b44") },
                    { new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2"), new Guid("69daea01-dd87-496b-b15a-b62a703594b1") },
                    { new Guid("ca2c22b2-10e3-401f-bf95-430038adea53"), new Guid("5d4c2e02-e3d6-4c86-a6ad-d4a7d528ae71") },
                    { new Guid("26dda459-1d42-48b3-b83a-2e0e02019133"), new Guid("c40d1ada-fb57-4b41-a395-ea7ca451f889") },
                    { new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01"), new Guid("c40d1ada-fb57-4b41-a395-ea7ca451f889") },
                    { new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2"), new Guid("c40d1ada-fb57-4b41-a395-ea7ca451f889") },
                    { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), new Guid("60a4c7fc-5e89-4988-8450-b032269e17ab") },
                    { new Guid("26dda459-1d42-48b3-b83a-2e0e02019133"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") },
                    { new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa"), new Guid("c90062c6-28fd-4dab-b1f0-b71ef6059b44") },
                    { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), new Guid("f40787f9-d2ab-4377-afae-3230cf67a5e1") },
                    { new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01"), new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b") },
                    { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), new Guid("e16d2eba-6198-43b4-af20-c0a3ec7f8b8b") },
                    { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b") },
                    { new Guid("3f252108-f831-49a1-9ef1-028d33fb4322"), new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b") },
                    { new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa"), new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b") },
                    { new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f"), new Guid("1a789339-0682-4595-baff-4cd7e534cd7d") },
                    { new Guid("625d8851-401c-46dc-9447-a01cb30a5ead"), new Guid("e16d2eba-6198-43b4-af20-c0a3ec7f8b8b") },
                    { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799") },
                    { new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa"), new Guid("1a789339-0682-4595-baff-4cd7e534cd7d") },
                    { new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f"), new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799") },
                    { new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa"), new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799") },
                    { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), new Guid("a7401deb-0954-429a-b5a1-364ec38737fa") },
                    { new Guid("625d8851-401c-46dc-9447-a01cb30a5ead"), new Guid("a7401deb-0954-429a-b5a1-364ec38737fa") },
                    { new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa"), new Guid("a7401deb-0954-429a-b5a1-364ec38737fa") },
                    { new Guid("3f252108-f831-49a1-9ef1-028d33fb4322"), new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799") }
                });

            migrationBuilder.InsertData(
                table: "Votos",
                columns: new[] { "IdFilme", "IdUsuario", "Nota" },
                values: new object[,]
                {
                    { new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2"), new Guid("5d3efd6d-568b-4cdd-9ae4-56dbcf30e7da"), (byte)3 },
                    { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), new Guid("5d3efd6d-568b-4cdd-9ae4-56dbcf30e7da"), (byte)1 },
                    { new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa"), new Guid("5d3efd6d-568b-4cdd-9ae4-56dbcf30e7da"), (byte)4 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IdentityRole");

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("0a99b6a9-9f80-441d-a24a-0c965a97b9fc"), new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("0ebfd788-29e0-4d23-91a7-0c2110b0ef18"), new Guid("625d8851-401c-46dc-9447-a01cb30a5ead") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("248c2aab-c979-4667-b3dc-b7b59382ee29"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("269afa8d-1c6d-4047-86c7-d00351e45bec"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("27370bd8-a38e-4d85-9394-7c75d090e55c"), new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("288760ec-c547-4087-93b6-98fffefd645e"), new Guid("ca2c22b2-10e3-401f-bf95-430038adea53") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("2b651023-7ea2-4a62-b36a-7cc40e5d056a"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("2ed6e10d-54a8-41c0-9037-5d6c8cead46c"), new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("31e41a70-0ce1-44f0-8504-272c0420745b"), new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("386295f4-3bb3-4256-9671-15d119ff4377"), new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("3a5225f5-c1e3-485f-9457-2122a51e5617"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("4aed69f8-ceb3-49da-b742-a2907cedfe20"), new Guid("26dda459-1d42-48b3-b83a-2e0e02019133") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("66386afe-613f-418f-b8c2-1234ab294a0b"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("76ade2a6-14a6-425e-9637-f7b8acefa39b"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("7bbc72ec-db21-4886-9c24-cf4960483ad2"), new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a0c809b9-c89f-4059-aec7-d1fc1fc7b878"), new Guid("625d8851-401c-46dc-9447-a01cb30a5ead") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a41123b6-7c3e-4872-884c-25ac31fcfa4c"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("aa227b12-10c6-4aea-ba6a-3e98d303c51b"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ae68d810-a802-4969-b556-85dc5e081fa1"), new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ae68d810-a802-4969-b556-85dc5e081fa1"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ae68d810-a802-4969-b556-85dc5e081fa1"), new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("b48d0f39-576b-4ef4-a941-effa0ad8b81d"), new Guid("26dda459-1d42-48b3-b83a-2e0e02019133") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("b6277aed-7761-49e6-9249-d77eb71b2ffa"), new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("bf27c15a-5c90-475f-9dfe-3cbdd73b4ea7"), new Guid("26dda459-1d42-48b3-b83a-2e0e02019133") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("c4473d09-3ae2-424a-b060-6b3fe5fb8140"), new Guid("ca2c22b2-10e3-401f-bf95-430038adea53") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("c51cd834-0cb8-4c1c-accb-d923bea52bb9"), new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("cb38f7ed-7670-44ca-937f-231d97dd569c"), new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("d48071ba-63ed-4959-90dd-34f3d55deeb2"), new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("dd017454-996f-452e-b6a5-641f6b593725"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("dfc051c0-5e39-4e9f-86fa-38732544acad"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ec07de00-8e23-48dd-8420-c3da8d459e6a"), new Guid("ca2c22b2-10e3-401f-bf95-430038adea53") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ec4b6c2e-43d2-4ef2-a0f2-a74e7f5d3cfc"), new Guid("625d8851-401c-46dc-9447-a01cb30a5ead") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ee673f00-40cf-499f-aad5-2f53dd7a50df"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") });

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("bf54f3fd-fa55-41f1-9912-1a92a186b9de"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("e23f169b-5c33-490a-aba8-51b5b289d3fb"));

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("08b60d6e-98b7-484f-9ae5-ffcd6311cf04"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("1c6acce1-6695-48e4-90b8-7efbf4557682"), new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("387b08d1-e0bc-487b-b016-29f08767ac53"), new Guid("625d8851-401c-46dc-9447-a01cb30a5ead") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("45388609-13fd-4e09-b9cb-818da292724a"), new Guid("26dda459-1d42-48b3-b83a-2e0e02019133") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("45388609-13fd-4e09-b9cb-818da292724a"), new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("51d2dcd3-5ebd-4307-8536-6e1b08246c57"), new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("52c80c68-719e-4563-ae2c-973ed5c21819"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("6b80d348-b569-4d02-9c91-8fda2bf8c55b"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("72aa2d3e-9c1a-4d0e-a5dd-580e647bb0d6"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("77ba7cd2-a67b-4768-b0db-5a316847c0dd"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("ae5c0f64-6325-4ba1-8c87-bff4fc4e0e31"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("b41c440f-35b2-4357-9cb9-035e74f37f22"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("d70aa89f-5001-44c9-957a-5c2ed373dbde"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("e3f4a565-271e-4967-8201-7dc4e4ac7904"), new Guid("ca2c22b2-10e3-401f-bf95-430038adea53") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("ed943494-7105-4e40-971e-ef4973e70e52"), new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036") });

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("2cccf882-2683-4062-9a8b-395469d95e5f"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("3173fd27-ba23-4ead-b087-820e07e66206"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("31d16437-f8db-43b8-ab00-7ef00c9675bb"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("33953b22-4a32-46af-a0c1-ec7cad8934c2"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("34865546-1100-4973-b543-ea7e8d5f3f09"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("469505b4-44d5-4967-bca8-e224198360eb"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("4eeb70ef-24a3-4ce9-a580-83c0b786405e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("59099e92-f18d-4163-bdcc-a1bd95837874"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("64353719-4fa7-4b08-870e-c1eab95d4ab0"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("649f3997-daae-4d55-8104-9612c438863a"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7000a51f-ec26-4e1d-8ae0-60d2457fce09"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("86ad15b0-1e3d-4c2e-8da1-399c8acb664e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("8ca92b70-eaf2-4154-a24b-e92a8fd3de54"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("a174c834-ed44-4d0e-9e70-c8be475c991d"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("d8dec4da-84fa-488f-92e4-1e348380ac02"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("e79de677-184e-472e-a24c-5a941c382c8e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f7d56204-e8a8-4829-a22d-e0dc1eb11d69"));

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa"), new Guid("1a789339-0682-4595-baff-4cd7e534cd7d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f"), new Guid("1a789339-0682-4595-baff-4cd7e534cd7d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ca2c22b2-10e3-401f-bf95-430038adea53"), new Guid("5d4c2e02-e3d6-4c86-a6ad-d4a7d528ae71") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), new Guid("60a4c7fc-5e89-4988-8450-b032269e17ab") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2"), new Guid("69daea01-dd87-496b-b15a-b62a703594b1") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa"), new Guid("a7401deb-0954-429a-b5a1-364ec38737fa") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("625d8851-401c-46dc-9447-a01cb30a5ead"), new Guid("a7401deb-0954-429a-b5a1-364ec38737fa") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), new Guid("a7401deb-0954-429a-b5a1-364ec38737fa") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("26dda459-1d42-48b3-b83a-2e0e02019133"), new Guid("c40d1ada-fb57-4b41-a395-ea7ca451f889") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01"), new Guid("c40d1ada-fb57-4b41-a395-ea7ca451f889") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2"), new Guid("c40d1ada-fb57-4b41-a395-ea7ca451f889") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("26dda459-1d42-48b3-b83a-2e0e02019133"), new Guid("c90062c6-28fd-4dab-b1f0-b71ef6059b44") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa"), new Guid("c90062c6-28fd-4dab-b1f0-b71ef6059b44") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("26dda459-1d42-48b3-b83a-2e0e02019133"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("3f252108-f831-49a1-9ef1-028d33fb4322"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("625d8851-401c-46dc-9447-a01cb30a5ead"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ca2c22b2-10e3-401f-bf95-430038adea53"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa"), new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("3f252108-f831-49a1-9ef1-028d33fb4322"), new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f"), new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("625d8851-401c-46dc-9447-a01cb30a5ead"), new Guid("e16d2eba-6198-43b4-af20-c0a3ec7f8b8b") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), new Guid("e16d2eba-6198-43b4-af20-c0a3ec7f8b8b") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("3f252108-f831-49a1-9ef1-028d33fb4322"), new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01"), new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa"), new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), new Guid("f40787f9-d2ab-4377-afae-3230cf67a5e1") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), new Guid("5d3efd6d-568b-4cdd-9ae4-56dbcf30e7da") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2"), new Guid("5d3efd6d-568b-4cdd-9ae4-56dbcf30e7da") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa"), new Guid("5d3efd6d-568b-4cdd-9ae4-56dbcf30e7da") });

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("0a99b6a9-9f80-441d-a24a-0c965a97b9fc"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("0ebfd788-29e0-4d23-91a7-0c2110b0ef18"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("248c2aab-c979-4667-b3dc-b7b59382ee29"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("269afa8d-1c6d-4047-86c7-d00351e45bec"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("27370bd8-a38e-4d85-9394-7c75d090e55c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("288760ec-c547-4087-93b6-98fffefd645e"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("2b651023-7ea2-4a62-b36a-7cc40e5d056a"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("2ed6e10d-54a8-41c0-9037-5d6c8cead46c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("31e41a70-0ce1-44f0-8504-272c0420745b"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("386295f4-3bb3-4256-9671-15d119ff4377"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("3a5225f5-c1e3-485f-9457-2122a51e5617"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("4aed69f8-ceb3-49da-b742-a2907cedfe20"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("66386afe-613f-418f-b8c2-1234ab294a0b"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("76ade2a6-14a6-425e-9637-f7b8acefa39b"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("7bbc72ec-db21-4886-9c24-cf4960483ad2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a0c809b9-c89f-4059-aec7-d1fc1fc7b878"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a41123b6-7c3e-4872-884c-25ac31fcfa4c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("aa227b12-10c6-4aea-ba6a-3e98d303c51b"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ae68d810-a802-4969-b556-85dc5e081fa1"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("b48d0f39-576b-4ef4-a941-effa0ad8b81d"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("b6277aed-7761-49e6-9249-d77eb71b2ffa"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("bf27c15a-5c90-475f-9dfe-3cbdd73b4ea7"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("c4473d09-3ae2-424a-b060-6b3fe5fb8140"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("c51cd834-0cb8-4c1c-accb-d923bea52bb9"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("cb38f7ed-7670-44ca-937f-231d97dd569c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("d48071ba-63ed-4959-90dd-34f3d55deeb2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("dd017454-996f-452e-b6a5-641f6b593725"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("dfc051c0-5e39-4e9f-86fa-38732544acad"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ec07de00-8e23-48dd-8420-c3da8d459e6a"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ec4b6c2e-43d2-4ef2-a0f2-a74e7f5d3cfc"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ee673f00-40cf-499f-aad5-2f53dd7a50df"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("08b60d6e-98b7-484f-9ae5-ffcd6311cf04"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("1c6acce1-6695-48e4-90b8-7efbf4557682"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("387b08d1-e0bc-487b-b016-29f08767ac53"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("45388609-13fd-4e09-b9cb-818da292724a"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("51d2dcd3-5ebd-4307-8536-6e1b08246c57"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("52c80c68-719e-4563-ae2c-973ed5c21819"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("6b80d348-b569-4d02-9c91-8fda2bf8c55b"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("72aa2d3e-9c1a-4d0e-a5dd-580e647bb0d6"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("77ba7cd2-a67b-4768-b0db-5a316847c0dd"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("ae5c0f64-6325-4ba1-8c87-bff4fc4e0e31"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("b41c440f-35b2-4357-9cb9-035e74f37f22"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("d70aa89f-5001-44c9-957a-5c2ed373dbde"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("e3f4a565-271e-4967-8201-7dc4e4ac7904"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("ed943494-7105-4e40-971e-ef4973e70e52"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("26dda459-1d42-48b3-b83a-2e0e02019133"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("3f252108-f831-49a1-9ef1-028d33fb4322"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("625d8851-401c-46dc-9447-a01cb30a5ead"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("ca2c22b2-10e3-401f-bf95-430038adea53"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("1a789339-0682-4595-baff-4cd7e534cd7d"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("5d4c2e02-e3d6-4c86-a6ad-d4a7d528ae71"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("60a4c7fc-5e89-4988-8450-b032269e17ab"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("69daea01-dd87-496b-b15a-b62a703594b1"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("a7401deb-0954-429a-b5a1-364ec38737fa"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("c40d1ada-fb57-4b41-a395-ea7ca451f889"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("c90062c6-28fd-4dab-b1f0-b71ef6059b44"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("e16d2eba-6198-43b4-af20-c0a3ec7f8b8b"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f40787f9-d2ab-4377-afae-3230cf67a5e1"));

            migrationBuilder.DeleteData(
                table: "Usuarios",
                keyColumn: "IdUsuario",
                keyValue: new Guid("5d3efd6d-568b-4cdd-9ae4-56dbcf30e7da"));

            migrationBuilder.DropColumn(
                name: "Senha",
                table: "Usuarios");

            migrationBuilder.InsertData(
                table: "Atores",
                columns: new[] { "IdAtor", "Nome" },
                values: new object[,]
                {
                    { new Guid("43fe98a4-b07a-4551-90e2-5008c210a572"), "Ben Burtt" },
                    { new Guid("34d715d7-0396-481d-bfa4-924698d1f753"), "Woody Harrelson" },
                    { new Guid("a0e8a983-ae76-4b50-b489-1b2586e15077"), "Tom Sizemore" },
                    { new Guid("f1fff516-d67c-4b8b-916e-f46d3dc85121"), "Robert Downey Jr." },
                    { new Guid("7da807b9-2f4f-4a92-98bd-f5f3ee970b92"), "Rachel McAdams" },
                    { new Guid("c1273c2f-a652-430b-a685-370c88ad9366"), "Paul Rhys" },
                    { new Guid("7bcdb15d-8adc-46e1-a37a-bd1b908a9551"), "Patton Oswalt" },
                    { new Guid("2c1f957f-95ad-4c1a-9bb2-31bbc15f0dfe"), "Patrick Magee" },
                    { new Guid("9208885b-1e97-4373-af21-0f2264037f24"), "Owen Wilson" },
                    { new Guid("1b90f3fe-0494-4b56-a260-274ac3930868"), "Mickey Rourke" },
                    { new Guid("9d06c143-8658-442c-b38e-45a8c0d56dcc"), "Michael Bates" },
                    { new Guid("4a408970-5c79-44fb-ab20-47c4bf78f599"), "Mark Ruffalo" },
                    { new Guid("ce58e276-f504-498a-b8bc-662d091bf4f9"), "Malcolm McDowell" },
                    { new Guid("0f6b5159-541a-4dd3-b821-871b841e0aae"), "Laurence Fishburne" },
                    { new Guid("cf9d3bd9-6ea5-4936-9961-27508d4a5ff2"), "Keanu Reeves" },
                    { new Guid("f3a52737-efea-4ebb-9080-3fc0b16bd429"), "Lou Romano" },
                    { new Guid("a662d40e-9867-4c98-a705-a5c156951e8e"), "Jude Law" },
                    { new Guid("4abd4571-8f91-4ff8-8a87-acaefd817e41"), "Brad Garrett" },
                    { new Guid("8187118e-b6b1-4a81-b4d7-f8edf1561c19"), "Bruce Willis" },
                    { new Guid("91c715dc-177d-4cac-8a06-2a9e5b8ebfc0"), "Carrie-Anne Moss" },
                    { new Guid("f70aea6c-01f8-4894-b0c6-6bc78f3b5f1f"), "Chris Evans" },
                    { new Guid("776cd318-6852-4967-ae65-4085a327c9bc"), "Juliette Lewis" },
                    { new Guid("aa7f747d-276b-4043-a492-b52a2ea68fdb"), "Daniel Mays" },
                    { new Guid("eaeca0f6-2176-4dde-a9c1-073586d9fd96"), "Dean-Charles Chapman" },
                    { new Guid("cdec62b0-0fbb-43c0-b5f8-f71304312b22"), "Clive Owen" },
                    { new Guid("8e300dfc-2f93-47f9-a88e-0b14c9e48308"), "Eric Dane" },
                    { new Guid("00fe0005-6d47-4c4d-a9b6-f1d5264a99b5"), "George MacKay" },
                    { new Guid("0cd9752d-6d74-49a0-97cf-03ae209814bf"), "Geraldine Chaplin" },
                    { new Guid("140beb3c-74db-4372-8ccc-aeab0cdb76f2"), "Jeff Garlin" },
                    { new Guid("a8f12ada-6f15-41d5-8f6c-b338e48f90c0"), "Jennifer Aniston" },
                    { new Guid("88e8f928-6365-4c29-8283-39531100f0c3"), "Elissa Knight" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("0fe8dee8-7702-449f-839f-1ab33f09b5ef"), "Andrew Stanton" },
                    { new Guid("2f6d7500-53c8-48e0-b69d-8acfc8bc6bc1"), "Stanley Kubrick" },
                    { new Guid("60702279-9b90-4020-9956-9adc5e368332"), "Oliver Stone" },
                    { new Guid("fc57962c-1012-4947-87e0-a061fa1dd423"), "Richard Attenborough" },
                    { new Guid("6162fba2-83dd-4136-91e8-ab5510a52c51"), "David Frankel" },
                    { new Guid("adbcb7ba-699c-4008-a929-9e097cb908ae"), "Sam Mendes" },
                    { new Guid("6f70dcae-97dd-4c71-bb8f-411d6aa08fec"), "Joe Russo" },
                    { new Guid("8287f98b-2b0a-4fef-823f-1e425e7dd383"), "Guy Ritchie" },
                    { new Guid("0c292b93-b386-4e1e-9b31-8ba149f2ec51"), "Lilly Wachowski" },
                    { new Guid("34e80a00-c349-43d5-8ef5-b30961c5400e"), "Lana Wachowski" },
                    { new Guid("327995a0-1d91-4b92-98f7-b69c6977fbc3"), "Jan Pinkava" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("d4fda122-fef5-4fe7-baac-44bb061204b8"), "Brad Bird" },
                    { new Guid("59b7cf35-ba56-4b0a-9760-7fb56718a57d"), "Anthony Russo" },
                    { new Guid("e5f49d52-741e-4323-9350-764c09a12bce"), "Robert Rodriguez" },
                    { new Guid("e54e5f58-3244-4a3b-b227-7ef82e4f1a8c"), "Quentin Tarantino" },
                    { new Guid("f24d233b-cbd8-4b4d-835c-b441330fb9bf"), "Frank Miller" }
                });

            migrationBuilder.InsertData(
                table: "Filmes",
                columns: new[] { "IdFilme", "Ano", "Nome", "NomeOriginal", "Sinopse" },
                values: new object[,]
                {
                    { new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f"), 1999, "Matrix", "The Matrix", "Um hacker aprende com os misteriosos rebeldes sobre a verdadeira natureza de sua realidade e seu papel na guerra contra seus controladores." },
                    { new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0"), 2007, "Ratatouille", "Ratatouille", "Um rato que pode cozinhar forja uma aliança incomum com um jovem garoto em um famoso restaurante em Paris." },
                    { new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec"), 2008, "Marley & Eu", "Marley & Me", "Uma família aprende importantes lições de vida com seu adorável, mas malicioso e neurótico cão." },
                    { new Guid("60855762-c883-4aa4-89bd-551b417db7cc"), 2008, "WALL·E", "WALL·E", "Num futuro distante, um pequeno robô faz uma viagem que decidirá o destino da humanidade." },
                    { new Guid("6f676918-14f4-4692-9e63-d5d619f1b407"), 2019, "Vingadores: Ultimato", "Avengers: Endgame", "Após os eventos devastadores de Vingadores: Guerra Infinita , o universo está em ruínas, e com a ajuda de aliados os Vingadores se reúnem para desfazer as ações de Thanos e restaurar a ordem." },
                    { new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42"), 2005, "Sin City: A Cidade do Pecado", "Sin City", "O filme explora a miserável cidade de Basin City, e conta a historia de três pessoas diferentes, envoltas em violencia e corrupção." },
                    { new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4"), 1994, "Assassinos por Natureza", "Natural Born Killers", "Duas vítimas de uma infância traumatizada tornam-se amantes e assassinos em série, irresponsavelmente glorificados pela mídia." },
                    { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), 1992, "Chaplin", "Chaplin", "Relata a vida convulsiva e controversa do mestre diretor de comédia Charles Chaplin." },
                    { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), 2009, "Sherlock Holmes", "Sherlock Holmes", "O detective Sherlock Holmes e seu colega Watson se engajam numa batalha para lutar contra as ameaças da Inglaterra." },
                    { new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665"), 1971, "Laranja Mecânica", "A Clockwork Orange", "No futuro, um líder duma turma é levado para prisão é decide ser voluntário dum experimento, mais os resultados não são os esperados." },
                    { new Guid("1a61255a-2e53-41b7-bd65-c282de4206df"), 2019, "1917", "1917", "6 de Abril de 1917. Enquanto um regimento se reúne para lutar uma guerra nas profundezas do território inimigo, dois soldados são designados para uma corrida contra o tempo para entregar uma mensagem que impedirá 1.600 homens de caminhar direto para uma uma armadilha mortal." }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("89641b75-6cae-4df4-a789-d17ef7d08587"), "Fantasia" },
                    { new Guid("f57247a6-2114-488f-9320-617bbcc90392"), "Fantasia científica" },
                    { new Guid("91b92594-bb77-4ce6-a1e6-ff6006cb11f3"), "Ficção científica" },
                    { new Guid("001a5d4d-29a9-421c-a353-ff0af1e12439"), "Filmes com truques" },
                    { new Guid("baaaf5ad-c399-4c7c-a4f0-4cff60518b5d"), "Filmes de guerra" },
                    { new Guid("559e18cc-12c0-4243-8c3a-b9e105b24c57"), "Musical" },
                    { new Guid("85f5242a-abb2-4466-a845-4e73b6515a8c"), "Filme policial" },
                    { new Guid("1b901728-9d77-4570-ac7e-f538b936c73c"), "Romance" },
                    { new Guid("38f7e4b2-f69f-4af1-844d-a9490ea3d603"), "Seriado" },
                    { new Guid("b9759eed-2a83-41a9-a833-e3ce68fbbd74"), "Suspense" },
                    { new Guid("2071bd6d-8303-4d46-8e99-cf4d9d83f11f"), "Terror" },
                    { new Guid("0afb542e-fa9f-46bc-b62b-0a0897691d38"), "Faroeste" },
                    { new Guid("ae7c643e-f38e-4478-8d23-5b1dde519167"), "Thriller" },
                    { new Guid("1cde864a-c157-4e1d-9192-b7ed24abb54a"), "Espionagem" },
                    { new Guid("196c90d8-42f6-4bf6-9bba-302ec1f53102"), "Comédia" },
                    { new Guid("32dd38a8-ca7c-435c-8d48-f81717a21e6d"), "Docuficção" },
                    { new Guid("1916d087-615b-4c31-b852-d4d9b5df896a"), "Documentário" },
                    { new Guid("790b979b-8294-4371-b140-57665dea8da2"), "Dança" },
                    { new Guid("15b36716-a135-4237-a4ed-fd0c5cfefd95"), "Comédia romântica" },
                    { new Guid("766229b6-77e3-43f2-873d-494dc3961b20"), "Comédia dramática" },
                    { new Guid("e572488a-2b52-4c44-ab00-72df2c2a4a60"), "Comédia de terror" },
                    { new Guid("2ce32b41-fa38-451e-85e5-66e935392de5"), "Comédia de ação" },
                    { new Guid("7203ac7f-57e0-4a50-a8ea-61fda4c9ac38"), "Chanchada" },
                    { new Guid("7ea8d8cd-cb9b-4a4e-9d47-8220260db814"), "Cinema de arte" },
                    { new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0"), "Aventura" },
                    { new Guid("c0724bbb-73a8-4120-a7ef-14420830e046"), "Animação" }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154"), "Ação" },
                    { new Guid("3e2a01ef-668c-4c7a-8b68-4ff4484d0e2c"), "Pornográfico" },
                    { new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5"), "Drama" }
                });

            migrationBuilder.InsertData(
                table: "Usuarios",
                columns: new[] { "IdUsuario", "Email", "Nome" },
                values: new object[] { new Guid("6a76883e-764e-4184-8b19-206a3ed8e2e8"), "rafael.av@gmail.com", "Rafael Vasconcelos" });

            migrationBuilder.InsertData(
                table: "AtoresFilmes",
                columns: new[] { "IdAtor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("ce58e276-f504-498a-b8bc-662d091bf4f9"), new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665") },
                    { new Guid("4a408970-5c79-44fb-ab20-47c4bf78f599"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") },
                    { new Guid("43fe98a4-b07a-4551-90e2-5008c210a572"), new Guid("60855762-c883-4aa4-89bd-551b417db7cc") },
                    { new Guid("88e8f928-6365-4c29-8283-39531100f0c3"), new Guid("60855762-c883-4aa4-89bd-551b417db7cc") },
                    { new Guid("140beb3c-74db-4372-8ccc-aeab0cdb76f2"), new Guid("60855762-c883-4aa4-89bd-551b417db7cc") },
                    { new Guid("9208885b-1e97-4373-af21-0f2264037f24"), new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec") },
                    { new Guid("a8f12ada-6f15-41d5-8f6c-b338e48f90c0"), new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec") },
                    { new Guid("8e300dfc-2f93-47f9-a88e-0b14c9e48308"), new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec") },
                    { new Guid("1b90f3fe-0494-4b56-a260-274ac3930868"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") },
                    { new Guid("cdec62b0-0fbb-43c0-b5f8-f71304312b22"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") },
                    { new Guid("8187118e-b6b1-4a81-b4d7-f8edf1561c19"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") },
                    { new Guid("4abd4571-8f91-4ff8-8a87-acaefd817e41"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") },
                    { new Guid("f3a52737-efea-4ebb-9080-3fc0b16bd429"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") },
                    { new Guid("cf9d3bd9-6ea5-4936-9961-27508d4a5ff2"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") },
                    { new Guid("0f6b5159-541a-4dd3-b821-871b841e0aae"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") },
                    { new Guid("91c715dc-177d-4cac-8a06-2a9e5b8ebfc0"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") },
                    { new Guid("f70aea6c-01f8-4894-b0c6-6bc78f3b5f1f"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") },
                    { new Guid("f1fff516-d67c-4b8b-916e-f46d3dc85121"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") },
                    { new Guid("7bcdb15d-8adc-46e1-a37a-bd1b908a9551"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") },
                    { new Guid("2c1f957f-95ad-4c1a-9bb2-31bbc15f0dfe"), new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665") },
                    { new Guid("7da807b9-2f4f-4a92-98bd-f5f3ee970b92"), new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a") },
                    { new Guid("f1fff516-d67c-4b8b-916e-f46d3dc85121"), new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32") },
                    { new Guid("0cd9752d-6d74-49a0-97cf-03ae209814bf"), new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32") },
                    { new Guid("c1273c2f-a652-430b-a685-370c88ad9366"), new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32") },
                    { new Guid("9d06c143-8658-442c-b38e-45a8c0d56dcc"), new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665") },
                    { new Guid("f1fff516-d67c-4b8b-916e-f46d3dc85121"), new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a") },
                    { new Guid("34d715d7-0396-481d-bfa4-924698d1f753"), new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4") },
                    { new Guid("a662d40e-9867-4c98-a705-a5c156951e8e"), new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a") },
                    { new Guid("a0e8a983-ae76-4b50-b489-1b2586e15077"), new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4") },
                    { new Guid("aa7f747d-276b-4043-a492-b52a2ea68fdb"), new Guid("1a61255a-2e53-41b7-bd65-c282de4206df") },
                    { new Guid("eaeca0f6-2176-4dde-a9c1-073586d9fd96"), new Guid("1a61255a-2e53-41b7-bd65-c282de4206df") },
                    { new Guid("00fe0005-6d47-4c4d-a9b6-f1d5264a99b5"), new Guid("1a61255a-2e53-41b7-bd65-c282de4206df") },
                    { new Guid("776cd318-6852-4967-ae65-4085a327c9bc"), new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("34e80a00-c349-43d5-8ef5-b30961c5400e"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") },
                    { new Guid("0c292b93-b386-4e1e-9b31-8ba149f2ec51"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") },
                    { new Guid("fc57962c-1012-4947-87e0-a061fa1dd423"), new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665") },
                    { new Guid("327995a0-1d91-4b92-98f7-b69c6977fbc3"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") },
                    { new Guid("d4fda122-fef5-4fe7-baac-44bb061204b8"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") },
                    { new Guid("adbcb7ba-699c-4008-a929-9e097cb908ae"), new Guid("1a61255a-2e53-41b7-bd65-c282de4206df") },
                    { new Guid("f24d233b-cbd8-4b4d-835c-b441330fb9bf"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") },
                    { new Guid("8287f98b-2b0a-4fef-823f-1e425e7dd383"), new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a") },
                    { new Guid("6162fba2-83dd-4136-91e8-ab5510a52c51"), new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("fc57962c-1012-4947-87e0-a061fa1dd423"), new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32") },
                    { new Guid("0fe8dee8-7702-449f-839f-1ab33f09b5ef"), new Guid("60855762-c883-4aa4-89bd-551b417db7cc") },
                    { new Guid("6f70dcae-97dd-4c71-bb8f-411d6aa08fec"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") },
                    { new Guid("59b7cf35-ba56-4b0a-9760-7fb56718a57d"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") },
                    { new Guid("60702279-9b90-4020-9956-9adc5e368332"), new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4") },
                    { new Guid("e54e5f58-3244-4a3b-b227-7ef82e4f1a8c"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") }
                });

            migrationBuilder.InsertData(
                table: "GenerosFilmes",
                columns: new[] { "IdFilme", "IdGenero" },
                values: new object[,]
                {
                    { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") },
                    { new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") },
                    { new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") },
                    { new Guid("1a61255a-2e53-41b7-bd65-c282de4206df"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") },
                    { new Guid("6f676918-14f4-4692-9e63-d5d619f1b407"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") },
                    { new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665"), new Guid("91b92594-bb77-4ce6-a1e6-ff6006cb11f3") },
                    { new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42"), new Guid("ae7c643e-f38e-4478-8d23-5b1dde519167") },
                    { new Guid("1a61255a-2e53-41b7-bd65-c282de4206df"), new Guid("baaaf5ad-c399-4c7c-a4f0-4cff60518b5d") },
                    { new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665"), new Guid("85f5242a-abb2-4466-a845-4e73b6515a8c") },
                    { new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4"), new Guid("85f5242a-abb2-4466-a845-4e73b6515a8c") },
                    { new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42"), new Guid("85f5242a-abb2-4466-a845-4e73b6515a8c") },
                    { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), new Guid("b9759eed-2a83-41a9-a833-e3ce68fbbd74") },
                    { new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") },
                    { new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f"), new Guid("91b92594-bb77-4ce6-a1e6-ff6006cb11f3") },
                    { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), new Guid("32dd38a8-ca7c-435c-8d48-f81717a21e6d") },
                    { new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4"), new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154") },
                    { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), new Guid("766229b6-77e3-43f2-873d-494dc3961b20") },
                    { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154") },
                    { new Guid("6f676918-14f4-4692-9e63-d5d619f1b407"), new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154") },
                    { new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f"), new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154") },
                    { new Guid("60855762-c883-4aa4-89bd-551b417db7cc"), new Guid("c0724bbb-73a8-4120-a7ef-14420830e046") },
                    { new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec"), new Guid("766229b6-77e3-43f2-873d-494dc3961b20") },
                    { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0") },
                    { new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0"), new Guid("c0724bbb-73a8-4120-a7ef-14420830e046") },
                    { new Guid("60855762-c883-4aa4-89bd-551b417db7cc"), new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0") },
                    { new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0"), new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0") },
                    { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), new Guid("196c90d8-42f6-4bf6-9bba-302ec1f53102") },
                    { new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec"), new Guid("196c90d8-42f6-4bf6-9bba-302ec1f53102") },
                    { new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0"), new Guid("196c90d8-42f6-4bf6-9bba-302ec1f53102") },
                    { new Guid("6f676918-14f4-4692-9e63-d5d619f1b407"), new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0") }
                });

            migrationBuilder.InsertData(
                table: "Votos",
                columns: new[] { "IdFilme", "IdUsuario", "Nota" },
                values: new object[,]
                {
                    { new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42"), new Guid("6a76883e-764e-4184-8b19-206a3ed8e2e8"), (byte)3 },
                    { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), new Guid("6a76883e-764e-4184-8b19-206a3ed8e2e8"), (byte)1 },
                    { new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f"), new Guid("6a76883e-764e-4184-8b19-206a3ed8e2e8"), (byte)4 }
                });
        }
    }
}
