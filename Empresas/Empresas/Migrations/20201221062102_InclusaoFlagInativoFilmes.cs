﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Empresas.Migrations
{
    public partial class InclusaoFlagInativoFilmes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("0d935bb4-b938-4371-a9ff-a611f63eeca0"), new Guid("887a0155-a808-4617-a083-807a20ee03af") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("11136c6e-ea89-4805-ad42-fb2d840fc72f"), new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("12b99b87-dcb7-449b-9e6a-88a1cad87f2e"), new Guid("d2f4766f-d067-423a-bcee-efaa72b18466") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("1708727b-cec4-4d94-b7b5-29a65ef69ccb"), new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("1708727b-cec4-4d94-b7b5-29a65ef69ccb"), new Guid("65723cce-271f-4e44-b3da-abda9a200e75") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("1708727b-cec4-4d94-b7b5-29a65ef69ccb"), new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("39cc4a77-79fe-4b42-a541-9aa37a0be4f2"), new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("407af964-cf25-485e-9647-f17bd800437a"), new Guid("d2f4766f-d067-423a-bcee-efaa72b18466") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("40f35089-fa08-4a9d-b9ef-755e8901360d"), new Guid("65723cce-271f-4e44-b3da-abda9a200e75") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("47b29f32-3707-4f4c-82ce-b67214c146d0"), new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("496608f6-fb01-4efc-9b35-6d8685628846"), new Guid("1c6498a2-1e94-4502-8075-e77254ed0837") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("512f083b-d67e-4f8a-98df-12720aac235f"), new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("6441de59-efff-49d4-bcc0-904af643f74f"), new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("6ef5c12e-1ffb-4966-a437-12d862e2c69c"), new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("6fcfc19d-f7fa-486b-97d7-843afbf9a70b"), new Guid("65723cce-271f-4e44-b3da-abda9a200e75") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("7591a5c9-7697-43a6-b6c7-710ba84741dd"), new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("7edc9d77-d1f1-43e7-b724-c4eec552b19b"), new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("88eb4550-4799-468d-a6dd-c240b76fa1bc"), new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("8af8cb18-67a4-46a2-9019-a7b5b3afab08"), new Guid("d2f4766f-d067-423a-bcee-efaa72b18466") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("8c8962ed-7d90-422b-8631-4bee75848588"), new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("8d7be067-e06c-4061-b08c-c6ef0b064845"), new Guid("1c6498a2-1e94-4502-8075-e77254ed0837") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("90752a8d-b04c-4303-af67-34a3cab4fd9a"), new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("90946e99-8420-47cb-935a-8de8871f950a"), new Guid("887a0155-a808-4617-a083-807a20ee03af") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("93385a88-782e-4d64-bb04-60481e93a2d9"), new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("9ccd101c-2d4d-45e2-a8e7-b30e2d539d4e"), new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a47e41be-997a-4d65-a020-65dc927c50d3"), new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a6439e9d-3639-4a78-9dba-f03464fda36c"), new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ac4185ec-1d63-46af-99c2-c2cf1f95586c"), new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("c3a56400-6b39-499f-97b2-38f9ae9ff522"), new Guid("1c6498a2-1e94-4502-8075-e77254ed0837") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("c8006c59-7f7a-4685-8b9f-ee1a864c0fa1"), new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("dc8a4798-978a-4fa3-a803-fa268a1a37a3"), new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("f05e9d19-ab22-45f1-a32f-c52128c31334"), new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("f1755ca9-b741-406b-ace4-66e946adb422"), new Guid("887a0155-a808-4617-a083-807a20ee03af") });

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("5df75661-d212-4d58-8017-8ab9f116c8d5"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("8095f1ab-4854-4cfc-9d8a-4e325f05c03d"));

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("46bc8564-7d32-4056-bf79-3cdf5cbc64a6"), new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("515fb4a8-7431-4b0d-a19d-a4cf7f08ade3"), new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("5c4ccdbf-76b7-4db7-9d3d-44f5176992fe"), new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("724b1fbe-43dd-4dcf-8071-49e9683fd503"), new Guid("1c6498a2-1e94-4502-8075-e77254ed0837") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("80e06ee8-bf82-4c05-a66e-dbcc0a478421"), new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("811df5fc-fcfe-4156-8741-8460db8a4fbd"), new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("99e3a622-22aa-435d-8a59-315de776995e"), new Guid("887a0155-a808-4617-a083-807a20ee03af") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("9d958b2f-701a-4f82-82ca-b1c771d63654"), new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("a2bb4bce-1571-4324-b980-b38a6b7f2f74"), new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("c671aa0c-10c7-48bb-9115-8328520d06c2"), new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("cd615713-cd84-4d2c-8c0e-e195c008a146"), new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("d910979f-b727-43f9-b906-fd2f73531033"), new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("e8337fd4-186b-4c40-9e8b-989c95e61a88"), new Guid("65723cce-271f-4e44-b3da-abda9a200e75") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("e8337fd4-186b-4c40-9e8b-989c95e61a88"), new Guid("d2f4766f-d067-423a-bcee-efaa72b18466") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("ffc090c1-a591-4f4c-affe-bcf304bf8ea6"), new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842") });

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("03c44a8b-a9a7-44f0-a58d-b673ffc88dc5"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("09f12992-bf17-408a-a084-6c4266fffc9c"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("0f84c5fa-ffe6-42c8-addf-85a3bb3d3766"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("2aba41b9-97ae-419b-afea-b69bf66fc3d1"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("2ddc3f89-02ce-4da3-a6fc-c27011a81db9"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("2ecf855c-155c-43ee-8837-3cddfa3cc272"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("35a7e324-ebd1-4224-8f16-ff69be7e0c3d"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("3652b9ec-84cc-4e0c-8d5b-07f08ba715b4"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("495e1a14-5c32-4987-977a-1148891b43f2"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("5584ebee-2a34-4334-b826-d8adf39f28b9"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("5dd8cf2c-e8f8-4898-a72e-4e7a8389d0d6"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("713df95d-ac8b-4f81-9bd7-5fe29a74e762"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("9309f31a-9111-4081-a59e-c2a2792a84ae"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("a8eb0ae5-14bc-431b-acd3-7ad62cc4037e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("bf20207e-0d5c-4a84-8f6e-5b93dc5ea541"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("da7d4b67-be2d-44ea-a79c-60c349484087"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("e98f0db8-370e-422e-a894-b4248a52751e"));

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("887a0155-a808-4617-a083-807a20ee03af"), new Guid("18f2e88f-0800-4c9c-af25-b9f5f07c93e2") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd"), new Guid("18f2e88f-0800-4c9c-af25-b9f5f07c93e2") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e"), new Guid("29507087-cd6d-4307-a4e6-9d608e6ee517") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25"), new Guid("29507087-cd6d-4307-a4e6-9d608e6ee517") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("d2f4766f-d067-423a-bcee-efaa72b18466"), new Guid("29507087-cd6d-4307-a4e6-9d608e6ee517") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e"), new Guid("2fb3f195-8b18-4e8d-89a0-ec043fae08dc") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492"), new Guid("4314c929-878a-49f1-b839-5c59ff7ee314") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("d2f4766f-d067-423a-bcee-efaa72b18466"), new Guid("4314c929-878a-49f1-b839-5c59ff7ee314") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("65723cce-271f-4e44-b3da-abda9a200e75"), new Guid("48de0e28-4b5c-4fe0-a04c-9eb4bba42d84") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("1c6498a2-1e94-4502-8075-e77254ed0837"), new Guid("7a193114-6501-4290-ae6a-8d2770b536be") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e"), new Guid("89d2ca23-53a8-4175-913a-065ad7347144") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("887a0155-a808-4617-a083-807a20ee03af"), new Guid("89d2ca23-53a8-4175-913a-065ad7347144") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842"), new Guid("89d2ca23-53a8-4175-913a-065ad7347144") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd"), new Guid("89d2ca23-53a8-4175-913a-065ad7347144") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03"), new Guid("9d078828-9155-409a-b1d7-17e4c49652b0") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("65723cce-271f-4e44-b3da-abda9a200e75"), new Guid("9d078828-9155-409a-b1d7-17e4c49652b0") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03"), new Guid("abf51e6b-338f-41db-8ce4-06aee816f436") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("65723cce-271f-4e44-b3da-abda9a200e75"), new Guid("abf51e6b-338f-41db-8ce4-06aee816f436") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd"), new Guid("abf51e6b-338f-41db-8ce4-06aee816f436") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e"), new Guid("af8a6dd6-963f-49e0-a04d-e6e5106541df") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("1c6498a2-1e94-4502-8075-e77254ed0837"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("65723cce-271f-4e44-b3da-abda9a200e75"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("d2f4766f-d067-423a-bcee-efaa72b18466"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e"), new Guid("f38727cc-e1e2-482a-b50a-dafc00a59612") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25"), new Guid("f38727cc-e1e2-482a-b50a-dafc00a59612") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492"), new Guid("f38727cc-e1e2-482a-b50a-dafc00a59612") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842"), new Guid("f38727cc-e1e2-482a-b50a-dafc00a59612") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e"), new Guid("92f82fd3-9770-47b5-862b-5e93fc427443") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e"), new Guid("92f82fd3-9770-47b5-862b-5e93fc427443") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492"), new Guid("92f82fd3-9770-47b5-862b-5e93fc427443") });

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("0d935bb4-b938-4371-a9ff-a611f63eeca0"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("11136c6e-ea89-4805-ad42-fb2d840fc72f"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("12b99b87-dcb7-449b-9e6a-88a1cad87f2e"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("1708727b-cec4-4d94-b7b5-29a65ef69ccb"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("39cc4a77-79fe-4b42-a541-9aa37a0be4f2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("407af964-cf25-485e-9647-f17bd800437a"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("40f35089-fa08-4a9d-b9ef-755e8901360d"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("47b29f32-3707-4f4c-82ce-b67214c146d0"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("496608f6-fb01-4efc-9b35-6d8685628846"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("512f083b-d67e-4f8a-98df-12720aac235f"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("6441de59-efff-49d4-bcc0-904af643f74f"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("6ef5c12e-1ffb-4966-a437-12d862e2c69c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("6fcfc19d-f7fa-486b-97d7-843afbf9a70b"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("7591a5c9-7697-43a6-b6c7-710ba84741dd"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("7edc9d77-d1f1-43e7-b724-c4eec552b19b"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("88eb4550-4799-468d-a6dd-c240b76fa1bc"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("8af8cb18-67a4-46a2-9019-a7b5b3afab08"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("8c8962ed-7d90-422b-8631-4bee75848588"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("8d7be067-e06c-4061-b08c-c6ef0b064845"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("90752a8d-b04c-4303-af67-34a3cab4fd9a"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("90946e99-8420-47cb-935a-8de8871f950a"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("93385a88-782e-4d64-bb04-60481e93a2d9"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("9ccd101c-2d4d-45e2-a8e7-b30e2d539d4e"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a47e41be-997a-4d65-a020-65dc927c50d3"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a6439e9d-3639-4a78-9dba-f03464fda36c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ac4185ec-1d63-46af-99c2-c2cf1f95586c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("c3a56400-6b39-499f-97b2-38f9ae9ff522"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("c8006c59-7f7a-4685-8b9f-ee1a864c0fa1"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("dc8a4798-978a-4fa3-a803-fa268a1a37a3"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("f05e9d19-ab22-45f1-a32f-c52128c31334"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("f1755ca9-b741-406b-ace4-66e946adb422"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("46bc8564-7d32-4056-bf79-3cdf5cbc64a6"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("515fb4a8-7431-4b0d-a19d-a4cf7f08ade3"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("5c4ccdbf-76b7-4db7-9d3d-44f5176992fe"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("724b1fbe-43dd-4dcf-8071-49e9683fd503"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("80e06ee8-bf82-4c05-a66e-dbcc0a478421"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("811df5fc-fcfe-4156-8741-8460db8a4fbd"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("99e3a622-22aa-435d-8a59-315de776995e"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("9d958b2f-701a-4f82-82ca-b1c771d63654"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("a2bb4bce-1571-4324-b980-b38a6b7f2f74"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("c671aa0c-10c7-48bb-9115-8328520d06c2"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("cd615713-cd84-4d2c-8c0e-e195c008a146"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("d910979f-b727-43f9-b906-fd2f73531033"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("e8337fd4-186b-4c40-9e8b-989c95e61a88"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("ffc090c1-a591-4f4c-affe-bcf304bf8ea6"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("1c6498a2-1e94-4502-8075-e77254ed0837"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("65723cce-271f-4e44-b3da-abda9a200e75"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("887a0155-a808-4617-a083-807a20ee03af"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("d2f4766f-d067-423a-bcee-efaa72b18466"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("18f2e88f-0800-4c9c-af25-b9f5f07c93e2"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("29507087-cd6d-4307-a4e6-9d608e6ee517"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("2fb3f195-8b18-4e8d-89a0-ec043fae08dc"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("4314c929-878a-49f1-b839-5c59ff7ee314"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("48de0e28-4b5c-4fe0-a04c-9eb4bba42d84"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7a193114-6501-4290-ae6a-8d2770b536be"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("89d2ca23-53a8-4175-913a-065ad7347144"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("9d078828-9155-409a-b1d7-17e4c49652b0"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("abf51e6b-338f-41db-8ce4-06aee816f436"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("af8a6dd6-963f-49e0-a04d-e6e5106541df"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f38727cc-e1e2-482a-b50a-dafc00a59612"));

            migrationBuilder.DeleteData(
                table: "Usuarios",
                keyColumn: "IdUsuario",
                keyValue: new Guid("92f82fd3-9770-47b5-862b-5e93fc427443"));

            migrationBuilder.AddColumn<bool>(
                name: "Inativo",
                table: "Filmes",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "Atores",
                columns: new[] { "IdAtor", "Nome" },
                values: new object[,]
                {
                    { new Guid("43fe98a4-b07a-4551-90e2-5008c210a572"), "Ben Burtt" },
                    { new Guid("34d715d7-0396-481d-bfa4-924698d1f753"), "Woody Harrelson" },
                    { new Guid("a0e8a983-ae76-4b50-b489-1b2586e15077"), "Tom Sizemore" },
                    { new Guid("f1fff516-d67c-4b8b-916e-f46d3dc85121"), "Robert Downey Jr." },
                    { new Guid("7da807b9-2f4f-4a92-98bd-f5f3ee970b92"), "Rachel McAdams" },
                    { new Guid("c1273c2f-a652-430b-a685-370c88ad9366"), "Paul Rhys" },
                    { new Guid("7bcdb15d-8adc-46e1-a37a-bd1b908a9551"), "Patton Oswalt" },
                    { new Guid("2c1f957f-95ad-4c1a-9bb2-31bbc15f0dfe"), "Patrick Magee" },
                    { new Guid("9208885b-1e97-4373-af21-0f2264037f24"), "Owen Wilson" },
                    { new Guid("1b90f3fe-0494-4b56-a260-274ac3930868"), "Mickey Rourke" },
                    { new Guid("9d06c143-8658-442c-b38e-45a8c0d56dcc"), "Michael Bates" },
                    { new Guid("4a408970-5c79-44fb-ab20-47c4bf78f599"), "Mark Ruffalo" },
                    { new Guid("ce58e276-f504-498a-b8bc-662d091bf4f9"), "Malcolm McDowell" },
                    { new Guid("0f6b5159-541a-4dd3-b821-871b841e0aae"), "Laurence Fishburne" },
                    { new Guid("cf9d3bd9-6ea5-4936-9961-27508d4a5ff2"), "Keanu Reeves" },
                    { new Guid("f3a52737-efea-4ebb-9080-3fc0b16bd429"), "Lou Romano" },
                    { new Guid("a662d40e-9867-4c98-a705-a5c156951e8e"), "Jude Law" },
                    { new Guid("4abd4571-8f91-4ff8-8a87-acaefd817e41"), "Brad Garrett" },
                    { new Guid("8187118e-b6b1-4a81-b4d7-f8edf1561c19"), "Bruce Willis" },
                    { new Guid("91c715dc-177d-4cac-8a06-2a9e5b8ebfc0"), "Carrie-Anne Moss" },
                    { new Guid("f70aea6c-01f8-4894-b0c6-6bc78f3b5f1f"), "Chris Evans" },
                    { new Guid("776cd318-6852-4967-ae65-4085a327c9bc"), "Juliette Lewis" },
                    { new Guid("aa7f747d-276b-4043-a492-b52a2ea68fdb"), "Daniel Mays" },
                    { new Guid("eaeca0f6-2176-4dde-a9c1-073586d9fd96"), "Dean-Charles Chapman" },
                    { new Guid("cdec62b0-0fbb-43c0-b5f8-f71304312b22"), "Clive Owen" },
                    { new Guid("8e300dfc-2f93-47f9-a88e-0b14c9e48308"), "Eric Dane" },
                    { new Guid("00fe0005-6d47-4c4d-a9b6-f1d5264a99b5"), "George MacKay" },
                    { new Guid("0cd9752d-6d74-49a0-97cf-03ae209814bf"), "Geraldine Chaplin" },
                    { new Guid("140beb3c-74db-4372-8ccc-aeab0cdb76f2"), "Jeff Garlin" },
                    { new Guid("a8f12ada-6f15-41d5-8f6c-b338e48f90c0"), "Jennifer Aniston" },
                    { new Guid("88e8f928-6365-4c29-8283-39531100f0c3"), "Elissa Knight" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("0fe8dee8-7702-449f-839f-1ab33f09b5ef"), "Andrew Stanton" },
                    { new Guid("2f6d7500-53c8-48e0-b69d-8acfc8bc6bc1"), "Stanley Kubrick" },
                    { new Guid("60702279-9b90-4020-9956-9adc5e368332"), "Oliver Stone" },
                    { new Guid("fc57962c-1012-4947-87e0-a061fa1dd423"), "Richard Attenborough" },
                    { new Guid("6162fba2-83dd-4136-91e8-ab5510a52c51"), "David Frankel" },
                    { new Guid("adbcb7ba-699c-4008-a929-9e097cb908ae"), "Sam Mendes" },
                    { new Guid("6f70dcae-97dd-4c71-bb8f-411d6aa08fec"), "Joe Russo" },
                    { new Guid("8287f98b-2b0a-4fef-823f-1e425e7dd383"), "Guy Ritchie" },
                    { new Guid("0c292b93-b386-4e1e-9b31-8ba149f2ec51"), "Lilly Wachowski" },
                    { new Guid("34e80a00-c349-43d5-8ef5-b30961c5400e"), "Lana Wachowski" },
                    { new Guid("327995a0-1d91-4b92-98f7-b69c6977fbc3"), "Jan Pinkava" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("d4fda122-fef5-4fe7-baac-44bb061204b8"), "Brad Bird" },
                    { new Guid("59b7cf35-ba56-4b0a-9760-7fb56718a57d"), "Anthony Russo" },
                    { new Guid("e5f49d52-741e-4323-9350-764c09a12bce"), "Robert Rodriguez" },
                    { new Guid("e54e5f58-3244-4a3b-b227-7ef82e4f1a8c"), "Quentin Tarantino" },
                    { new Guid("f24d233b-cbd8-4b4d-835c-b441330fb9bf"), "Frank Miller" }
                });

            migrationBuilder.InsertData(
                table: "Filmes",
                columns: new[] { "IdFilme", "Ano", "Nome", "NomeOriginal", "Sinopse" },
                values: new object[,]
                {
                    { new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f"), 1999, "Matrix", "The Matrix", "Um hacker aprende com os misteriosos rebeldes sobre a verdadeira natureza de sua realidade e seu papel na guerra contra seus controladores." },
                    { new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0"), 2007, "Ratatouille", "Ratatouille", "Um rato que pode cozinhar forja uma aliança incomum com um jovem garoto em um famoso restaurante em Paris." },
                    { new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec"), 2008, "Marley & Eu", "Marley & Me", "Uma família aprende importantes lições de vida com seu adorável, mas malicioso e neurótico cão." },
                    { new Guid("60855762-c883-4aa4-89bd-551b417db7cc"), 2008, "WALL·E", "WALL·E", "Num futuro distante, um pequeno robô faz uma viagem que decidirá o destino da humanidade." },
                    { new Guid("6f676918-14f4-4692-9e63-d5d619f1b407"), 2019, "Vingadores: Ultimato", "Avengers: Endgame", "Após os eventos devastadores de Vingadores: Guerra Infinita , o universo está em ruínas, e com a ajuda de aliados os Vingadores se reúnem para desfazer as ações de Thanos e restaurar a ordem." },
                    { new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42"), 2005, "Sin City: A Cidade do Pecado", "Sin City", "O filme explora a miserável cidade de Basin City, e conta a historia de três pessoas diferentes, envoltas em violencia e corrupção." },
                    { new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4"), 1994, "Assassinos por Natureza", "Natural Born Killers", "Duas vítimas de uma infância traumatizada tornam-se amantes e assassinos em série, irresponsavelmente glorificados pela mídia." },
                    { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), 1992, "Chaplin", "Chaplin", "Relata a vida convulsiva e controversa do mestre diretor de comédia Charles Chaplin." },
                    { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), 2009, "Sherlock Holmes", "Sherlock Holmes", "O detective Sherlock Holmes e seu colega Watson se engajam numa batalha para lutar contra as ameaças da Inglaterra." },
                    { new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665"), 1971, "Laranja Mecânica", "A Clockwork Orange", "No futuro, um líder duma turma é levado para prisão é decide ser voluntário dum experimento, mais os resultados não são os esperados." },
                    { new Guid("1a61255a-2e53-41b7-bd65-c282de4206df"), 2019, "1917", "1917", "6 de Abril de 1917. Enquanto um regimento se reúne para lutar uma guerra nas profundezas do território inimigo, dois soldados são designados para uma corrida contra o tempo para entregar uma mensagem que impedirá 1.600 homens de caminhar direto para uma uma armadilha mortal." }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("89641b75-6cae-4df4-a789-d17ef7d08587"), "Fantasia" },
                    { new Guid("f57247a6-2114-488f-9320-617bbcc90392"), "Fantasia científica" },
                    { new Guid("91b92594-bb77-4ce6-a1e6-ff6006cb11f3"), "Ficção científica" },
                    { new Guid("001a5d4d-29a9-421c-a353-ff0af1e12439"), "Filmes com truques" },
                    { new Guid("baaaf5ad-c399-4c7c-a4f0-4cff60518b5d"), "Filmes de guerra" },
                    { new Guid("559e18cc-12c0-4243-8c3a-b9e105b24c57"), "Musical" },
                    { new Guid("85f5242a-abb2-4466-a845-4e73b6515a8c"), "Filme policial" },
                    { new Guid("1b901728-9d77-4570-ac7e-f538b936c73c"), "Romance" },
                    { new Guid("38f7e4b2-f69f-4af1-844d-a9490ea3d603"), "Seriado" },
                    { new Guid("b9759eed-2a83-41a9-a833-e3ce68fbbd74"), "Suspense" },
                    { new Guid("2071bd6d-8303-4d46-8e99-cf4d9d83f11f"), "Terror" },
                    { new Guid("0afb542e-fa9f-46bc-b62b-0a0897691d38"), "Faroeste" },
                    { new Guid("ae7c643e-f38e-4478-8d23-5b1dde519167"), "Thriller" },
                    { new Guid("1cde864a-c157-4e1d-9192-b7ed24abb54a"), "Espionagem" },
                    { new Guid("196c90d8-42f6-4bf6-9bba-302ec1f53102"), "Comédia" },
                    { new Guid("32dd38a8-ca7c-435c-8d48-f81717a21e6d"), "Docuficção" },
                    { new Guid("1916d087-615b-4c31-b852-d4d9b5df896a"), "Documentário" },
                    { new Guid("790b979b-8294-4371-b140-57665dea8da2"), "Dança" },
                    { new Guid("15b36716-a135-4237-a4ed-fd0c5cfefd95"), "Comédia romântica" },
                    { new Guid("766229b6-77e3-43f2-873d-494dc3961b20"), "Comédia dramática" },
                    { new Guid("e572488a-2b52-4c44-ab00-72df2c2a4a60"), "Comédia de terror" },
                    { new Guid("2ce32b41-fa38-451e-85e5-66e935392de5"), "Comédia de ação" },
                    { new Guid("7203ac7f-57e0-4a50-a8ea-61fda4c9ac38"), "Chanchada" },
                    { new Guid("7ea8d8cd-cb9b-4a4e-9d47-8220260db814"), "Cinema de arte" },
                    { new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0"), "Aventura" },
                    { new Guid("c0724bbb-73a8-4120-a7ef-14420830e046"), "Animação" }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154"), "Ação" },
                    { new Guid("3e2a01ef-668c-4c7a-8b68-4ff4484d0e2c"), "Pornográfico" },
                    { new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5"), "Drama" }
                });

            migrationBuilder.InsertData(
                table: "Usuarios",
                columns: new[] { "IdUsuario", "Email", "Nome" },
                values: new object[] { new Guid("6a76883e-764e-4184-8b19-206a3ed8e2e8"), "rafael.av@gmail.com", "Rafael Vasconcelos" });

            migrationBuilder.InsertData(
                table: "AtoresFilmes",
                columns: new[] { "IdAtor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("ce58e276-f504-498a-b8bc-662d091bf4f9"), new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665") },
                    { new Guid("4a408970-5c79-44fb-ab20-47c4bf78f599"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") },
                    { new Guid("43fe98a4-b07a-4551-90e2-5008c210a572"), new Guid("60855762-c883-4aa4-89bd-551b417db7cc") },
                    { new Guid("88e8f928-6365-4c29-8283-39531100f0c3"), new Guid("60855762-c883-4aa4-89bd-551b417db7cc") },
                    { new Guid("140beb3c-74db-4372-8ccc-aeab0cdb76f2"), new Guid("60855762-c883-4aa4-89bd-551b417db7cc") },
                    { new Guid("9208885b-1e97-4373-af21-0f2264037f24"), new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec") },
                    { new Guid("a8f12ada-6f15-41d5-8f6c-b338e48f90c0"), new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec") },
                    { new Guid("8e300dfc-2f93-47f9-a88e-0b14c9e48308"), new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec") },
                    { new Guid("1b90f3fe-0494-4b56-a260-274ac3930868"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") },
                    { new Guid("cdec62b0-0fbb-43c0-b5f8-f71304312b22"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") },
                    { new Guid("8187118e-b6b1-4a81-b4d7-f8edf1561c19"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") },
                    { new Guid("4abd4571-8f91-4ff8-8a87-acaefd817e41"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") },
                    { new Guid("f3a52737-efea-4ebb-9080-3fc0b16bd429"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") },
                    { new Guid("cf9d3bd9-6ea5-4936-9961-27508d4a5ff2"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") },
                    { new Guid("0f6b5159-541a-4dd3-b821-871b841e0aae"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") },
                    { new Guid("91c715dc-177d-4cac-8a06-2a9e5b8ebfc0"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") },
                    { new Guid("f70aea6c-01f8-4894-b0c6-6bc78f3b5f1f"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") },
                    { new Guid("f1fff516-d67c-4b8b-916e-f46d3dc85121"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") },
                    { new Guid("7bcdb15d-8adc-46e1-a37a-bd1b908a9551"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") },
                    { new Guid("2c1f957f-95ad-4c1a-9bb2-31bbc15f0dfe"), new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665") },
                    { new Guid("7da807b9-2f4f-4a92-98bd-f5f3ee970b92"), new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a") },
                    { new Guid("f1fff516-d67c-4b8b-916e-f46d3dc85121"), new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32") },
                    { new Guid("0cd9752d-6d74-49a0-97cf-03ae209814bf"), new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32") },
                    { new Guid("c1273c2f-a652-430b-a685-370c88ad9366"), new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32") },
                    { new Guid("9d06c143-8658-442c-b38e-45a8c0d56dcc"), new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665") },
                    { new Guid("f1fff516-d67c-4b8b-916e-f46d3dc85121"), new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a") },
                    { new Guid("34d715d7-0396-481d-bfa4-924698d1f753"), new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4") },
                    { new Guid("a662d40e-9867-4c98-a705-a5c156951e8e"), new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a") },
                    { new Guid("a0e8a983-ae76-4b50-b489-1b2586e15077"), new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4") },
                    { new Guid("aa7f747d-276b-4043-a492-b52a2ea68fdb"), new Guid("1a61255a-2e53-41b7-bd65-c282de4206df") },
                    { new Guid("eaeca0f6-2176-4dde-a9c1-073586d9fd96"), new Guid("1a61255a-2e53-41b7-bd65-c282de4206df") },
                    { new Guid("00fe0005-6d47-4c4d-a9b6-f1d5264a99b5"), new Guid("1a61255a-2e53-41b7-bd65-c282de4206df") },
                    { new Guid("776cd318-6852-4967-ae65-4085a327c9bc"), new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("34e80a00-c349-43d5-8ef5-b30961c5400e"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") },
                    { new Guid("0c292b93-b386-4e1e-9b31-8ba149f2ec51"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") },
                    { new Guid("fc57962c-1012-4947-87e0-a061fa1dd423"), new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665") },
                    { new Guid("327995a0-1d91-4b92-98f7-b69c6977fbc3"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") },
                    { new Guid("d4fda122-fef5-4fe7-baac-44bb061204b8"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") },
                    { new Guid("adbcb7ba-699c-4008-a929-9e097cb908ae"), new Guid("1a61255a-2e53-41b7-bd65-c282de4206df") },
                    { new Guid("f24d233b-cbd8-4b4d-835c-b441330fb9bf"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") },
                    { new Guid("8287f98b-2b0a-4fef-823f-1e425e7dd383"), new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a") },
                    { new Guid("6162fba2-83dd-4136-91e8-ab5510a52c51"), new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("fc57962c-1012-4947-87e0-a061fa1dd423"), new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32") },
                    { new Guid("0fe8dee8-7702-449f-839f-1ab33f09b5ef"), new Guid("60855762-c883-4aa4-89bd-551b417db7cc") },
                    { new Guid("6f70dcae-97dd-4c71-bb8f-411d6aa08fec"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") },
                    { new Guid("59b7cf35-ba56-4b0a-9760-7fb56718a57d"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") },
                    { new Guid("60702279-9b90-4020-9956-9adc5e368332"), new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4") },
                    { new Guid("e54e5f58-3244-4a3b-b227-7ef82e4f1a8c"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") }
                });

            migrationBuilder.InsertData(
                table: "GenerosFilmes",
                columns: new[] { "IdFilme", "IdGenero" },
                values: new object[,]
                {
                    { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") },
                    { new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") },
                    { new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") },
                    { new Guid("1a61255a-2e53-41b7-bd65-c282de4206df"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") },
                    { new Guid("6f676918-14f4-4692-9e63-d5d619f1b407"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") },
                    { new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665"), new Guid("91b92594-bb77-4ce6-a1e6-ff6006cb11f3") },
                    { new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42"), new Guid("ae7c643e-f38e-4478-8d23-5b1dde519167") },
                    { new Guid("1a61255a-2e53-41b7-bd65-c282de4206df"), new Guid("baaaf5ad-c399-4c7c-a4f0-4cff60518b5d") },
                    { new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665"), new Guid("85f5242a-abb2-4466-a845-4e73b6515a8c") },
                    { new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4"), new Guid("85f5242a-abb2-4466-a845-4e73b6515a8c") },
                    { new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42"), new Guid("85f5242a-abb2-4466-a845-4e73b6515a8c") },
                    { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), new Guid("b9759eed-2a83-41a9-a833-e3ce68fbbd74") },
                    { new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") },
                    { new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f"), new Guid("91b92594-bb77-4ce6-a1e6-ff6006cb11f3") },
                    { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), new Guid("32dd38a8-ca7c-435c-8d48-f81717a21e6d") },
                    { new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4"), new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154") },
                    { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), new Guid("766229b6-77e3-43f2-873d-494dc3961b20") },
                    { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154") },
                    { new Guid("6f676918-14f4-4692-9e63-d5d619f1b407"), new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154") },
                    { new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f"), new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154") },
                    { new Guid("60855762-c883-4aa4-89bd-551b417db7cc"), new Guid("c0724bbb-73a8-4120-a7ef-14420830e046") },
                    { new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec"), new Guid("766229b6-77e3-43f2-873d-494dc3961b20") },
                    { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0") },
                    { new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0"), new Guid("c0724bbb-73a8-4120-a7ef-14420830e046") },
                    { new Guid("60855762-c883-4aa4-89bd-551b417db7cc"), new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0") },
                    { new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0"), new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0") },
                    { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), new Guid("196c90d8-42f6-4bf6-9bba-302ec1f53102") },
                    { new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec"), new Guid("196c90d8-42f6-4bf6-9bba-302ec1f53102") },
                    { new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0"), new Guid("196c90d8-42f6-4bf6-9bba-302ec1f53102") },
                    { new Guid("6f676918-14f4-4692-9e63-d5d619f1b407"), new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0") }
                });

            migrationBuilder.InsertData(
                table: "Votos",
                columns: new[] { "IdFilme", "IdUsuario", "Nota" },
                values: new object[,]
                {
                    { new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42"), new Guid("6a76883e-764e-4184-8b19-206a3ed8e2e8"), (byte)3 },
                    { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), new Guid("6a76883e-764e-4184-8b19-206a3ed8e2e8"), (byte)1 },
                    { new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f"), new Guid("6a76883e-764e-4184-8b19-206a3ed8e2e8"), (byte)4 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("00fe0005-6d47-4c4d-a9b6-f1d5264a99b5"), new Guid("1a61255a-2e53-41b7-bd65-c282de4206df") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("0cd9752d-6d74-49a0-97cf-03ae209814bf"), new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("0f6b5159-541a-4dd3-b821-871b841e0aae"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("140beb3c-74db-4372-8ccc-aeab0cdb76f2"), new Guid("60855762-c883-4aa4-89bd-551b417db7cc") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("1b90f3fe-0494-4b56-a260-274ac3930868"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("2c1f957f-95ad-4c1a-9bb2-31bbc15f0dfe"), new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("34d715d7-0396-481d-bfa4-924698d1f753"), new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("43fe98a4-b07a-4551-90e2-5008c210a572"), new Guid("60855762-c883-4aa4-89bd-551b417db7cc") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("4a408970-5c79-44fb-ab20-47c4bf78f599"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("4abd4571-8f91-4ff8-8a87-acaefd817e41"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("776cd318-6852-4967-ae65-4085a327c9bc"), new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("7bcdb15d-8adc-46e1-a37a-bd1b908a9551"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("7da807b9-2f4f-4a92-98bd-f5f3ee970b92"), new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("8187118e-b6b1-4a81-b4d7-f8edf1561c19"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("88e8f928-6365-4c29-8283-39531100f0c3"), new Guid("60855762-c883-4aa4-89bd-551b417db7cc") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("8e300dfc-2f93-47f9-a88e-0b14c9e48308"), new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("91c715dc-177d-4cac-8a06-2a9e5b8ebfc0"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("9208885b-1e97-4373-af21-0f2264037f24"), new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("9d06c143-8658-442c-b38e-45a8c0d56dcc"), new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a0e8a983-ae76-4b50-b489-1b2586e15077"), new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a662d40e-9867-4c98-a705-a5c156951e8e"), new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a8f12ada-6f15-41d5-8f6c-b338e48f90c0"), new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("aa7f747d-276b-4043-a492-b52a2ea68fdb"), new Guid("1a61255a-2e53-41b7-bd65-c282de4206df") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("c1273c2f-a652-430b-a685-370c88ad9366"), new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("cdec62b0-0fbb-43c0-b5f8-f71304312b22"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ce58e276-f504-498a-b8bc-662d091bf4f9"), new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("cf9d3bd9-6ea5-4936-9961-27508d4a5ff2"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("eaeca0f6-2176-4dde-a9c1-073586d9fd96"), new Guid("1a61255a-2e53-41b7-bd65-c282de4206df") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("f1fff516-d67c-4b8b-916e-f46d3dc85121"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("f1fff516-d67c-4b8b-916e-f46d3dc85121"), new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("f1fff516-d67c-4b8b-916e-f46d3dc85121"), new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("f3a52737-efea-4ebb-9080-3fc0b16bd429"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("f70aea6c-01f8-4894-b0c6-6bc78f3b5f1f"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") });

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("2f6d7500-53c8-48e0-b69d-8acfc8bc6bc1"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("e5f49d52-741e-4323-9350-764c09a12bce"));

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("0c292b93-b386-4e1e-9b31-8ba149f2ec51"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("0fe8dee8-7702-449f-839f-1ab33f09b5ef"), new Guid("60855762-c883-4aa4-89bd-551b417db7cc") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("327995a0-1d91-4b92-98f7-b69c6977fbc3"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("34e80a00-c349-43d5-8ef5-b30961c5400e"), new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("59b7cf35-ba56-4b0a-9760-7fb56718a57d"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("60702279-9b90-4020-9956-9adc5e368332"), new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("6162fba2-83dd-4136-91e8-ab5510a52c51"), new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("6f70dcae-97dd-4c71-bb8f-411d6aa08fec"), new Guid("6f676918-14f4-4692-9e63-d5d619f1b407") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("8287f98b-2b0a-4fef-823f-1e425e7dd383"), new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("adbcb7ba-699c-4008-a929-9e097cb908ae"), new Guid("1a61255a-2e53-41b7-bd65-c282de4206df") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("d4fda122-fef5-4fe7-baac-44bb061204b8"), new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("e54e5f58-3244-4a3b-b227-7ef82e4f1a8c"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("f24d233b-cbd8-4b4d-835c-b441330fb9bf"), new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("fc57962c-1012-4947-87e0-a061fa1dd423"), new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("fc57962c-1012-4947-87e0-a061fa1dd423"), new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32") });

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("001a5d4d-29a9-421c-a353-ff0af1e12439"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("0afb542e-fa9f-46bc-b62b-0a0897691d38"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("15b36716-a135-4237-a4ed-fd0c5cfefd95"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("1916d087-615b-4c31-b852-d4d9b5df896a"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("1b901728-9d77-4570-ac7e-f538b936c73c"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("1cde864a-c157-4e1d-9192-b7ed24abb54a"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("2071bd6d-8303-4d46-8e99-cf4d9d83f11f"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("2ce32b41-fa38-451e-85e5-66e935392de5"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("38f7e4b2-f69f-4af1-844d-a9490ea3d603"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("3e2a01ef-668c-4c7a-8b68-4ff4484d0e2c"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("559e18cc-12c0-4243-8c3a-b9e105b24c57"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7203ac7f-57e0-4a50-a8ea-61fda4c9ac38"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("790b979b-8294-4371-b140-57665dea8da2"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7ea8d8cd-cb9b-4a4e-9d47-8220260db814"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("89641b75-6cae-4df4-a789-d17ef7d08587"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("e572488a-2b52-4c44-ab00-72df2c2a4a60"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f57247a6-2114-488f-9320-617bbcc90392"));

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec"), new Guid("196c90d8-42f6-4bf6-9bba-302ec1f53102") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), new Guid("196c90d8-42f6-4bf6-9bba-302ec1f53102") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0"), new Guid("196c90d8-42f6-4bf6-9bba-302ec1f53102") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("60855762-c883-4aa4-89bd-551b417db7cc"), new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("6f676918-14f4-4692-9e63-d5d619f1b407"), new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0"), new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), new Guid("32dd38a8-ca7c-435c-8d48-f81717a21e6d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("6f676918-14f4-4692-9e63-d5d619f1b407"), new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4"), new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f"), new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec"), new Guid("766229b6-77e3-43f2-873d-494dc3961b20") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), new Guid("766229b6-77e3-43f2-873d-494dc3961b20") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665"), new Guid("85f5242a-abb2-4466-a845-4e73b6515a8c") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42"), new Guid("85f5242a-abb2-4466-a845-4e73b6515a8c") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4"), new Guid("85f5242a-abb2-4466-a845-4e73b6515a8c") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665"), new Guid("91b92594-bb77-4ce6-a1e6-ff6006cb11f3") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f"), new Guid("91b92594-bb77-4ce6-a1e6-ff6006cb11f3") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("1a61255a-2e53-41b7-bd65-c282de4206df"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("6f676918-14f4-4692-9e63-d5d619f1b407"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"), new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42"), new Guid("ae7c643e-f38e-4478-8d23-5b1dde519167") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), new Guid("b9759eed-2a83-41a9-a833-e3ce68fbbd74") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("1a61255a-2e53-41b7-bd65-c282de4206df"), new Guid("baaaf5ad-c399-4c7c-a4f0-4cff60518b5d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("60855762-c883-4aa4-89bd-551b417db7cc"), new Guid("c0724bbb-73a8-4120-a7ef-14420830e046") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0"), new Guid("c0724bbb-73a8-4120-a7ef-14420830e046") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42"), new Guid("6a76883e-764e-4184-8b19-206a3ed8e2e8") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"), new Guid("6a76883e-764e-4184-8b19-206a3ed8e2e8") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f"), new Guid("6a76883e-764e-4184-8b19-206a3ed8e2e8") });

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("00fe0005-6d47-4c4d-a9b6-f1d5264a99b5"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("0cd9752d-6d74-49a0-97cf-03ae209814bf"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("0f6b5159-541a-4dd3-b821-871b841e0aae"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("140beb3c-74db-4372-8ccc-aeab0cdb76f2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("1b90f3fe-0494-4b56-a260-274ac3930868"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("2c1f957f-95ad-4c1a-9bb2-31bbc15f0dfe"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("34d715d7-0396-481d-bfa4-924698d1f753"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("43fe98a4-b07a-4551-90e2-5008c210a572"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("4a408970-5c79-44fb-ab20-47c4bf78f599"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("4abd4571-8f91-4ff8-8a87-acaefd817e41"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("776cd318-6852-4967-ae65-4085a327c9bc"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("7bcdb15d-8adc-46e1-a37a-bd1b908a9551"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("7da807b9-2f4f-4a92-98bd-f5f3ee970b92"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("8187118e-b6b1-4a81-b4d7-f8edf1561c19"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("88e8f928-6365-4c29-8283-39531100f0c3"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("8e300dfc-2f93-47f9-a88e-0b14c9e48308"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("91c715dc-177d-4cac-8a06-2a9e5b8ebfc0"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("9208885b-1e97-4373-af21-0f2264037f24"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("9d06c143-8658-442c-b38e-45a8c0d56dcc"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a0e8a983-ae76-4b50-b489-1b2586e15077"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a662d40e-9867-4c98-a705-a5c156951e8e"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a8f12ada-6f15-41d5-8f6c-b338e48f90c0"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("aa7f747d-276b-4043-a492-b52a2ea68fdb"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("c1273c2f-a652-430b-a685-370c88ad9366"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("cdec62b0-0fbb-43c0-b5f8-f71304312b22"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ce58e276-f504-498a-b8bc-662d091bf4f9"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("cf9d3bd9-6ea5-4936-9961-27508d4a5ff2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("eaeca0f6-2176-4dde-a9c1-073586d9fd96"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("f1fff516-d67c-4b8b-916e-f46d3dc85121"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("f3a52737-efea-4ebb-9080-3fc0b16bd429"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("f70aea6c-01f8-4894-b0c6-6bc78f3b5f1f"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("0c292b93-b386-4e1e-9b31-8ba149f2ec51"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("0fe8dee8-7702-449f-839f-1ab33f09b5ef"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("327995a0-1d91-4b92-98f7-b69c6977fbc3"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("34e80a00-c349-43d5-8ef5-b30961c5400e"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("59b7cf35-ba56-4b0a-9760-7fb56718a57d"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("60702279-9b90-4020-9956-9adc5e368332"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("6162fba2-83dd-4136-91e8-ab5510a52c51"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("6f70dcae-97dd-4c71-bb8f-411d6aa08fec"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("8287f98b-2b0a-4fef-823f-1e425e7dd383"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("adbcb7ba-699c-4008-a929-9e097cb908ae"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("d4fda122-fef5-4fe7-baac-44bb061204b8"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("e54e5f58-3244-4a3b-b227-7ef82e4f1a8c"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("f24d233b-cbd8-4b4d-835c-b441330fb9bf"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("fc57962c-1012-4947-87e0-a061fa1dd423"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("16e1fc16-e3d7-44de-8ef2-cae751a32665"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("1a61255a-2e53-41b7-bd65-c282de4206df"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("60855762-c883-4aa4-89bd-551b417db7cc"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("6f676918-14f4-4692-9e63-d5d619f1b407"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("706f3e58-c304-40d6-bbd6-54ff117b26ec"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("713ae0df-5615-4d5f-bdf4-f1bfadf43d42"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("8e4725c7-2751-4714-8e1d-fd2563128a0a"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("920e6d19-9a2b-4819-a860-0e51eebc90d4"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("b07a9520-63b3-4753-b372-3d2e6cae8e32"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("eb6e9ae3-05c6-4332-b037-e84ac3a5780f"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("ed775fac-dde4-4918-bf1d-807c4d15e7f0"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("196c90d8-42f6-4bf6-9bba-302ec1f53102"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("1c8f655d-98a4-47e6-95ec-e4817b0b2ef0"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("32dd38a8-ca7c-435c-8d48-f81717a21e6d"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("43eb5d8f-13cf-4a9f-9c3f-64b7ce0b8154"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("766229b6-77e3-43f2-873d-494dc3961b20"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("85f5242a-abb2-4466-a845-4e73b6515a8c"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("91b92594-bb77-4ce6-a1e6-ff6006cb11f3"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("a8942c49-09f8-41e3-8d87-218b5cd1b8a5"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("ae7c643e-f38e-4478-8d23-5b1dde519167"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("b9759eed-2a83-41a9-a833-e3ce68fbbd74"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("baaaf5ad-c399-4c7c-a4f0-4cff60518b5d"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("c0724bbb-73a8-4120-a7ef-14420830e046"));

            migrationBuilder.DeleteData(
                table: "Usuarios",
                keyColumn: "IdUsuario",
                keyValue: new Guid("6a76883e-764e-4184-8b19-206a3ed8e2e8"));

            migrationBuilder.DropColumn(
                name: "Inativo",
                table: "Filmes");

            migrationBuilder.InsertData(
                table: "Atores",
                columns: new[] { "IdAtor", "Nome" },
                values: new object[,]
                {
                    { new Guid("f1755ca9-b741-406b-ace4-66e946adb422"), "Ben Burtt" },
                    { new Guid("11136c6e-ea89-4805-ad42-fb2d840fc72f"), "Woody Harrelson" },
                    { new Guid("a47e41be-997a-4d65-a020-65dc927c50d3"), "Tom Sizemore" },
                    { new Guid("1708727b-cec4-4d94-b7b5-29a65ef69ccb"), "Robert Downey Jr." },
                    { new Guid("512f083b-d67e-4f8a-98df-12720aac235f"), "Rachel McAdams" },
                    { new Guid("40f35089-fa08-4a9d-b9ef-755e8901360d"), "Paul Rhys" },
                    { new Guid("8c8962ed-7d90-422b-8631-4bee75848588"), "Patton Oswalt" },
                    { new Guid("8af8cb18-67a4-46a2-9019-a7b5b3afab08"), "Patrick Magee" },
                    { new Guid("c8006c59-7f7a-4685-8b9f-ee1a864c0fa1"), "Owen Wilson" },
                    { new Guid("6441de59-efff-49d4-bcc0-904af643f74f"), "Mickey Rourke" },
                    { new Guid("407af964-cf25-485e-9647-f17bd800437a"), "Michael Bates" },
                    { new Guid("88eb4550-4799-468d-a6dd-c240b76fa1bc"), "Mark Ruffalo" },
                    { new Guid("12b99b87-dcb7-449b-9e6a-88a1cad87f2e"), "Malcolm McDowell" },
                    { new Guid("93385a88-782e-4d64-bb04-60481e93a2d9"), "Laurence Fishburne" },
                    { new Guid("dc8a4798-978a-4fa3-a803-fa268a1a37a3"), "Keanu Reeves" },
                    { new Guid("a6439e9d-3639-4a78-9dba-f03464fda36c"), "Lou Romano" },
                    { new Guid("ac4185ec-1d63-46af-99c2-c2cf1f95586c"), "Jude Law" },
                    { new Guid("6ef5c12e-1ffb-4966-a437-12d862e2c69c"), "Brad Garrett" },
                    { new Guid("f05e9d19-ab22-45f1-a32f-c52128c31334"), "Bruce Willis" },
                    { new Guid("7edc9d77-d1f1-43e7-b724-c4eec552b19b"), "Carrie-Anne Moss" },
                    { new Guid("39cc4a77-79fe-4b42-a541-9aa37a0be4f2"), "Chris Evans" },
                    { new Guid("47b29f32-3707-4f4c-82ce-b67214c146d0"), "Juliette Lewis" },
                    { new Guid("c3a56400-6b39-499f-97b2-38f9ae9ff522"), "Daniel Mays" },
                    { new Guid("8d7be067-e06c-4061-b08c-c6ef0b064845"), "Dean-Charles Chapman" },
                    { new Guid("9ccd101c-2d4d-45e2-a8e7-b30e2d539d4e"), "Clive Owen" },
                    { new Guid("90752a8d-b04c-4303-af67-34a3cab4fd9a"), "Eric Dane" },
                    { new Guid("496608f6-fb01-4efc-9b35-6d8685628846"), "George MacKay" },
                    { new Guid("6fcfc19d-f7fa-486b-97d7-843afbf9a70b"), "Geraldine Chaplin" },
                    { new Guid("90946e99-8420-47cb-935a-8de8871f950a"), "Jeff Garlin" },
                    { new Guid("7591a5c9-7697-43a6-b6c7-710ba84741dd"), "Jennifer Aniston" },
                    { new Guid("0d935bb4-b938-4371-a9ff-a611f63eeca0"), "Elissa Knight" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("99e3a622-22aa-435d-8a59-315de776995e"), "Andrew Stanton" },
                    { new Guid("5df75661-d212-4d58-8017-8ab9f116c8d5"), "Stanley Kubrick" },
                    { new Guid("cd615713-cd84-4d2c-8c0e-e195c008a146"), "Oliver Stone" },
                    { new Guid("e8337fd4-186b-4c40-9e8b-989c95e61a88"), "Richard Attenborough" },
                    { new Guid("80e06ee8-bf82-4c05-a66e-dbcc0a478421"), "David Frankel" },
                    { new Guid("724b1fbe-43dd-4dcf-8071-49e9683fd503"), "Sam Mendes" },
                    { new Guid("ffc090c1-a591-4f4c-affe-bcf304bf8ea6"), "Joe Russo" },
                    { new Guid("46bc8564-7d32-4056-bf79-3cdf5cbc64a6"), "Guy Ritchie" },
                    { new Guid("c671aa0c-10c7-48bb-9115-8328520d06c2"), "Lilly Wachowski" },
                    { new Guid("5c4ccdbf-76b7-4db7-9d3d-44f5176992fe"), "Lana Wachowski" },
                    { new Guid("9d958b2f-701a-4f82-82ca-b1c771d63654"), "Jan Pinkava" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("a2bb4bce-1571-4324-b980-b38a6b7f2f74"), "Brad Bird" },
                    { new Guid("d910979f-b727-43f9-b906-fd2f73531033"), "Anthony Russo" },
                    { new Guid("8095f1ab-4854-4cfc-9d8a-4e325f05c03d"), "Robert Rodriguez" },
                    { new Guid("515fb4a8-7431-4b0d-a19d-a4cf7f08ade3"), "Quentin Tarantino" },
                    { new Guid("811df5fc-fcfe-4156-8741-8460db8a4fbd"), "Frank Miller" }
                });

            migrationBuilder.InsertData(
                table: "Filmes",
                columns: new[] { "IdFilme", "Ano", "Nome", "NomeOriginal", "Sinopse" },
                values: new object[,]
                {
                    { new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492"), 1999, "Matrix", "The Matrix", "Um hacker aprende com os misteriosos rebeldes sobre a verdadeira natureza de sua realidade e seu papel na guerra contra seus controladores." },
                    { new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd"), 2007, "Ratatouille", "Ratatouille", "Um rato que pode cozinhar forja uma aliança incomum com um jovem garoto em um famoso restaurante em Paris." },
                    { new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03"), 2008, "Marley & Eu", "Marley & Me", "Uma família aprende importantes lições de vida com seu adorável, mas malicioso e neurótico cão." },
                    { new Guid("887a0155-a808-4617-a083-807a20ee03af"), 2008, "WALL·E", "WALL·E", "Num futuro distante, um pequeno robô faz uma viagem que decidirá o destino da humanidade." },
                    { new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842"), 2019, "Vingadores: Ultimato", "Avengers: Endgame", "Após os eventos devastadores de Vingadores: Guerra Infinita , o universo está em ruínas, e com a ajuda de aliados os Vingadores se reúnem para desfazer as ações de Thanos e restaurar a ordem." },
                    { new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e"), 2005, "Sin City: A Cidade do Pecado", "Sin City", "O filme explora a miserável cidade de Basin City, e conta a historia de três pessoas diferentes, envoltas em violencia e corrupção." },
                    { new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25"), 1994, "Assassinos por Natureza", "Natural Born Killers", "Duas vítimas de uma infância traumatizada tornam-se amantes e assassinos em série, irresponsavelmente glorificados pela mídia." },
                    { new Guid("65723cce-271f-4e44-b3da-abda9a200e75"), 1992, "Chaplin", "Chaplin", "Relata a vida convulsiva e controversa do mestre diretor de comédia Charles Chaplin." },
                    { new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e"), 2009, "Sherlock Holmes", "Sherlock Holmes", "O detective Sherlock Holmes e seu colega Watson se engajam numa batalha para lutar contra as ameaças da Inglaterra." },
                    { new Guid("d2f4766f-d067-423a-bcee-efaa72b18466"), 1971, "Laranja Mecânica", "A Clockwork Orange", "No futuro, um líder duma turma é levado para prisão é decide ser voluntário dum experimento, mais os resultados não são os esperados." },
                    { new Guid("1c6498a2-1e94-4502-8075-e77254ed0837"), 2019, "1917", "1917", "6 de Abril de 1917. Enquanto um regimento se reúne para lutar uma guerra nas profundezas do território inimigo, dois soldados são designados para uma corrida contra o tempo para entregar uma mensagem que impedirá 1.600 homens de caminhar direto para uma uma armadilha mortal." }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("3652b9ec-84cc-4e0c-8d5b-07f08ba715b4"), "Fantasia" },
                    { new Guid("2ecf855c-155c-43ee-8837-3cddfa3cc272"), "Fantasia científica" },
                    { new Guid("4314c929-878a-49f1-b839-5c59ff7ee314"), "Ficção científica" },
                    { new Guid("a8eb0ae5-14bc-431b-acd3-7ad62cc4037e"), "Filmes com truques" },
                    { new Guid("7a193114-6501-4290-ae6a-8d2770b536be"), "Filmes de guerra" },
                    { new Guid("2ddc3f89-02ce-4da3-a6fc-c27011a81db9"), "Musical" },
                    { new Guid("29507087-cd6d-4307-a4e6-9d608e6ee517"), "Filme policial" },
                    { new Guid("2aba41b9-97ae-419b-afea-b69bf66fc3d1"), "Romance" },
                    { new Guid("e98f0db8-370e-422e-a894-b4248a52751e"), "Seriado" },
                    { new Guid("af8a6dd6-963f-49e0-a04d-e6e5106541df"), "Suspense" },
                    { new Guid("5584ebee-2a34-4334-b826-d8adf39f28b9"), "Terror" },
                    { new Guid("713df95d-ac8b-4f81-9bd7-5fe29a74e762"), "Faroeste" },
                    { new Guid("2fb3f195-8b18-4e8d-89a0-ec043fae08dc"), "Thriller" },
                    { new Guid("bf20207e-0d5c-4a84-8f6e-5b93dc5ea541"), "Espionagem" },
                    { new Guid("abf51e6b-338f-41db-8ce4-06aee816f436"), "Comédia" },
                    { new Guid("48de0e28-4b5c-4fe0-a04c-9eb4bba42d84"), "Docuficção" },
                    { new Guid("9309f31a-9111-4081-a59e-c2a2792a84ae"), "Documentário" },
                    { new Guid("09f12992-bf17-408a-a084-6c4266fffc9c"), "Dança" },
                    { new Guid("0f84c5fa-ffe6-42c8-addf-85a3bb3d3766"), "Comédia romântica" },
                    { new Guid("9d078828-9155-409a-b1d7-17e4c49652b0"), "Comédia dramática" },
                    { new Guid("da7d4b67-be2d-44ea-a79c-60c349484087"), "Comédia de terror" },
                    { new Guid("03c44a8b-a9a7-44f0-a58d-b673ffc88dc5"), "Comédia de ação" },
                    { new Guid("35a7e324-ebd1-4224-8f16-ff69be7e0c3d"), "Chanchada" },
                    { new Guid("5dd8cf2c-e8f8-4898-a72e-4e7a8389d0d6"), "Cinema de arte" },
                    { new Guid("89d2ca23-53a8-4175-913a-065ad7347144"), "Aventura" },
                    { new Guid("18f2e88f-0800-4c9c-af25-b9f5f07c93e2"), "Animação" }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("f38727cc-e1e2-482a-b50a-dafc00a59612"), "Ação" },
                    { new Guid("495e1a14-5c32-4987-977a-1148891b43f2"), "Pornográfico" },
                    { new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49"), "Drama" }
                });

            migrationBuilder.InsertData(
                table: "Usuarios",
                columns: new[] { "IdUsuario", "Email", "Nome" },
                values: new object[] { new Guid("92f82fd3-9770-47b5-862b-5e93fc427443"), "rafael.av@gmail.com", "Rafael Vasconcelos" });

            migrationBuilder.InsertData(
                table: "AtoresFilmes",
                columns: new[] { "IdAtor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("12b99b87-dcb7-449b-9e6a-88a1cad87f2e"), new Guid("d2f4766f-d067-423a-bcee-efaa72b18466") },
                    { new Guid("88eb4550-4799-468d-a6dd-c240b76fa1bc"), new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842") },
                    { new Guid("f1755ca9-b741-406b-ace4-66e946adb422"), new Guid("887a0155-a808-4617-a083-807a20ee03af") },
                    { new Guid("0d935bb4-b938-4371-a9ff-a611f63eeca0"), new Guid("887a0155-a808-4617-a083-807a20ee03af") },
                    { new Guid("90946e99-8420-47cb-935a-8de8871f950a"), new Guid("887a0155-a808-4617-a083-807a20ee03af") },
                    { new Guid("c8006c59-7f7a-4685-8b9f-ee1a864c0fa1"), new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03") },
                    { new Guid("7591a5c9-7697-43a6-b6c7-710ba84741dd"), new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03") },
                    { new Guid("90752a8d-b04c-4303-af67-34a3cab4fd9a"), new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03") },
                    { new Guid("6441de59-efff-49d4-bcc0-904af643f74f"), new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e") },
                    { new Guid("9ccd101c-2d4d-45e2-a8e7-b30e2d539d4e"), new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e") },
                    { new Guid("f05e9d19-ab22-45f1-a32f-c52128c31334"), new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e") },
                    { new Guid("6ef5c12e-1ffb-4966-a437-12d862e2c69c"), new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd") },
                    { new Guid("a6439e9d-3639-4a78-9dba-f03464fda36c"), new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd") },
                    { new Guid("dc8a4798-978a-4fa3-a803-fa268a1a37a3"), new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492") },
                    { new Guid("93385a88-782e-4d64-bb04-60481e93a2d9"), new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492") },
                    { new Guid("7edc9d77-d1f1-43e7-b724-c4eec552b19b"), new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492") },
                    { new Guid("39cc4a77-79fe-4b42-a541-9aa37a0be4f2"), new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842") },
                    { new Guid("1708727b-cec4-4d94-b7b5-29a65ef69ccb"), new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842") },
                    { new Guid("8c8962ed-7d90-422b-8631-4bee75848588"), new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd") },
                    { new Guid("8af8cb18-67a4-46a2-9019-a7b5b3afab08"), new Guid("d2f4766f-d067-423a-bcee-efaa72b18466") },
                    { new Guid("512f083b-d67e-4f8a-98df-12720aac235f"), new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e") },
                    { new Guid("1708727b-cec4-4d94-b7b5-29a65ef69ccb"), new Guid("65723cce-271f-4e44-b3da-abda9a200e75") },
                    { new Guid("6fcfc19d-f7fa-486b-97d7-843afbf9a70b"), new Guid("65723cce-271f-4e44-b3da-abda9a200e75") },
                    { new Guid("40f35089-fa08-4a9d-b9ef-755e8901360d"), new Guid("65723cce-271f-4e44-b3da-abda9a200e75") },
                    { new Guid("407af964-cf25-485e-9647-f17bd800437a"), new Guid("d2f4766f-d067-423a-bcee-efaa72b18466") },
                    { new Guid("1708727b-cec4-4d94-b7b5-29a65ef69ccb"), new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e") },
                    { new Guid("11136c6e-ea89-4805-ad42-fb2d840fc72f"), new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25") },
                    { new Guid("ac4185ec-1d63-46af-99c2-c2cf1f95586c"), new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e") },
                    { new Guid("a47e41be-997a-4d65-a020-65dc927c50d3"), new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25") },
                    { new Guid("c3a56400-6b39-499f-97b2-38f9ae9ff522"), new Guid("1c6498a2-1e94-4502-8075-e77254ed0837") },
                    { new Guid("8d7be067-e06c-4061-b08c-c6ef0b064845"), new Guid("1c6498a2-1e94-4502-8075-e77254ed0837") },
                    { new Guid("496608f6-fb01-4efc-9b35-6d8685628846"), new Guid("1c6498a2-1e94-4502-8075-e77254ed0837") },
                    { new Guid("47b29f32-3707-4f4c-82ce-b67214c146d0"), new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("5c4ccdbf-76b7-4db7-9d3d-44f5176992fe"), new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492") },
                    { new Guid("c671aa0c-10c7-48bb-9115-8328520d06c2"), new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492") },
                    { new Guid("e8337fd4-186b-4c40-9e8b-989c95e61a88"), new Guid("d2f4766f-d067-423a-bcee-efaa72b18466") },
                    { new Guid("9d958b2f-701a-4f82-82ca-b1c771d63654"), new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd") },
                    { new Guid("a2bb4bce-1571-4324-b980-b38a6b7f2f74"), new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd") },
                    { new Guid("724b1fbe-43dd-4dcf-8071-49e9683fd503"), new Guid("1c6498a2-1e94-4502-8075-e77254ed0837") },
                    { new Guid("811df5fc-fcfe-4156-8741-8460db8a4fbd"), new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e") },
                    { new Guid("46bc8564-7d32-4056-bf79-3cdf5cbc64a6"), new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e") },
                    { new Guid("80e06ee8-bf82-4c05-a66e-dbcc0a478421"), new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("e8337fd4-186b-4c40-9e8b-989c95e61a88"), new Guid("65723cce-271f-4e44-b3da-abda9a200e75") },
                    { new Guid("99e3a622-22aa-435d-8a59-315de776995e"), new Guid("887a0155-a808-4617-a083-807a20ee03af") },
                    { new Guid("ffc090c1-a591-4f4c-affe-bcf304bf8ea6"), new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842") },
                    { new Guid("d910979f-b727-43f9-b906-fd2f73531033"), new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842") },
                    { new Guid("cd615713-cd84-4d2c-8c0e-e195c008a146"), new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25") },
                    { new Guid("515fb4a8-7431-4b0d-a19d-a4cf7f08ade3"), new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e") }
                });

            migrationBuilder.InsertData(
                table: "GenerosFilmes",
                columns: new[] { "IdFilme", "IdGenero" },
                values: new object[,]
                {
                    { new Guid("65723cce-271f-4e44-b3da-abda9a200e75"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") },
                    { new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") },
                    { new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") },
                    { new Guid("1c6498a2-1e94-4502-8075-e77254ed0837"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") },
                    { new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") },
                    { new Guid("d2f4766f-d067-423a-bcee-efaa72b18466"), new Guid("4314c929-878a-49f1-b839-5c59ff7ee314") },
                    { new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e"), new Guid("2fb3f195-8b18-4e8d-89a0-ec043fae08dc") },
                    { new Guid("1c6498a2-1e94-4502-8075-e77254ed0837"), new Guid("7a193114-6501-4290-ae6a-8d2770b536be") },
                    { new Guid("d2f4766f-d067-423a-bcee-efaa72b18466"), new Guid("29507087-cd6d-4307-a4e6-9d608e6ee517") },
                    { new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25"), new Guid("29507087-cd6d-4307-a4e6-9d608e6ee517") },
                    { new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e"), new Guid("29507087-cd6d-4307-a4e6-9d608e6ee517") },
                    { new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e"), new Guid("af8a6dd6-963f-49e0-a04d-e6e5106541df") },
                    { new Guid("d2f4766f-d067-423a-bcee-efaa72b18466"), new Guid("b97ec3c7-2e75-4d43-b39b-fff3ce86da49") },
                    { new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492"), new Guid("4314c929-878a-49f1-b839-5c59ff7ee314") },
                    { new Guid("65723cce-271f-4e44-b3da-abda9a200e75"), new Guid("48de0e28-4b5c-4fe0-a04c-9eb4bba42d84") },
                    { new Guid("5ebd6678-15c3-4ce7-b6a5-f6f556ba0d25"), new Guid("f38727cc-e1e2-482a-b50a-dafc00a59612") },
                    { new Guid("65723cce-271f-4e44-b3da-abda9a200e75"), new Guid("9d078828-9155-409a-b1d7-17e4c49652b0") },
                    { new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e"), new Guid("f38727cc-e1e2-482a-b50a-dafc00a59612") },
                    { new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842"), new Guid("f38727cc-e1e2-482a-b50a-dafc00a59612") },
                    { new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492"), new Guid("f38727cc-e1e2-482a-b50a-dafc00a59612") },
                    { new Guid("887a0155-a808-4617-a083-807a20ee03af"), new Guid("18f2e88f-0800-4c9c-af25-b9f5f07c93e2") },
                    { new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03"), new Guid("9d078828-9155-409a-b1d7-17e4c49652b0") },
                    { new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e"), new Guid("89d2ca23-53a8-4175-913a-065ad7347144") },
                    { new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd"), new Guid("18f2e88f-0800-4c9c-af25-b9f5f07c93e2") },
                    { new Guid("887a0155-a808-4617-a083-807a20ee03af"), new Guid("89d2ca23-53a8-4175-913a-065ad7347144") },
                    { new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd"), new Guid("89d2ca23-53a8-4175-913a-065ad7347144") },
                    { new Guid("65723cce-271f-4e44-b3da-abda9a200e75"), new Guid("abf51e6b-338f-41db-8ce4-06aee816f436") },
                    { new Guid("2ba78000-1ecb-414a-8a84-aaedc584dc03"), new Guid("abf51e6b-338f-41db-8ce4-06aee816f436") },
                    { new Guid("c0f08882-5048-4f21-912c-b70a2d5d45bd"), new Guid("abf51e6b-338f-41db-8ce4-06aee816f436") },
                    { new Guid("ae742730-4ddc-4310-a6cb-3f19d9e86842"), new Guid("89d2ca23-53a8-4175-913a-065ad7347144") }
                });

            migrationBuilder.InsertData(
                table: "Votos",
                columns: new[] { "IdFilme", "IdUsuario", "Nota" },
                values: new object[,]
                {
                    { new Guid("0c9e1bb9-846b-45d3-9654-cbc32680b88e"), new Guid("92f82fd3-9770-47b5-862b-5e93fc427443"), (byte)3 },
                    { new Guid("27937e61-8775-4f33-b7aa-72da2ef4f19e"), new Guid("92f82fd3-9770-47b5-862b-5e93fc427443"), (byte)1 },
                    { new Guid("8de1fa84-7a57-4f9a-9be9-1e918b634492"), new Guid("92f82fd3-9770-47b5-862b-5e93fc427443"), (byte)4 }
                });
        }
    }
}
