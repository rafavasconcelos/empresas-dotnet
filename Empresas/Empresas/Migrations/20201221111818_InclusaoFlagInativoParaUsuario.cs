﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Empresas.Migrations
{
    public partial class InclusaoFlagInativoParaUsuario : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("0a99b6a9-9f80-441d-a24a-0c965a97b9fc"), new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("0ebfd788-29e0-4d23-91a7-0c2110b0ef18"), new Guid("625d8851-401c-46dc-9447-a01cb30a5ead") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("248c2aab-c979-4667-b3dc-b7b59382ee29"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("269afa8d-1c6d-4047-86c7-d00351e45bec"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("27370bd8-a38e-4d85-9394-7c75d090e55c"), new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("288760ec-c547-4087-93b6-98fffefd645e"), new Guid("ca2c22b2-10e3-401f-bf95-430038adea53") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("2b651023-7ea2-4a62-b36a-7cc40e5d056a"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("2ed6e10d-54a8-41c0-9037-5d6c8cead46c"), new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("31e41a70-0ce1-44f0-8504-272c0420745b"), new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("386295f4-3bb3-4256-9671-15d119ff4377"), new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("3a5225f5-c1e3-485f-9457-2122a51e5617"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("4aed69f8-ceb3-49da-b742-a2907cedfe20"), new Guid("26dda459-1d42-48b3-b83a-2e0e02019133") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("66386afe-613f-418f-b8c2-1234ab294a0b"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("76ade2a6-14a6-425e-9637-f7b8acefa39b"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("7bbc72ec-db21-4886-9c24-cf4960483ad2"), new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a0c809b9-c89f-4059-aec7-d1fc1fc7b878"), new Guid("625d8851-401c-46dc-9447-a01cb30a5ead") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a41123b6-7c3e-4872-884c-25ac31fcfa4c"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("aa227b12-10c6-4aea-ba6a-3e98d303c51b"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ae68d810-a802-4969-b556-85dc5e081fa1"), new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ae68d810-a802-4969-b556-85dc5e081fa1"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ae68d810-a802-4969-b556-85dc5e081fa1"), new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("b48d0f39-576b-4ef4-a941-effa0ad8b81d"), new Guid("26dda459-1d42-48b3-b83a-2e0e02019133") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("b6277aed-7761-49e6-9249-d77eb71b2ffa"), new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("bf27c15a-5c90-475f-9dfe-3cbdd73b4ea7"), new Guid("26dda459-1d42-48b3-b83a-2e0e02019133") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("c4473d09-3ae2-424a-b060-6b3fe5fb8140"), new Guid("ca2c22b2-10e3-401f-bf95-430038adea53") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("c51cd834-0cb8-4c1c-accb-d923bea52bb9"), new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("cb38f7ed-7670-44ca-937f-231d97dd569c"), new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("d48071ba-63ed-4959-90dd-34f3d55deeb2"), new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("dd017454-996f-452e-b6a5-641f6b593725"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("dfc051c0-5e39-4e9f-86fa-38732544acad"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ec07de00-8e23-48dd-8420-c3da8d459e6a"), new Guid("ca2c22b2-10e3-401f-bf95-430038adea53") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ec4b6c2e-43d2-4ef2-a0f2-a74e7f5d3cfc"), new Guid("625d8851-401c-46dc-9447-a01cb30a5ead") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ee673f00-40cf-499f-aad5-2f53dd7a50df"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") });

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("bf54f3fd-fa55-41f1-9912-1a92a186b9de"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("e23f169b-5c33-490a-aba8-51b5b289d3fb"));

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("08b60d6e-98b7-484f-9ae5-ffcd6311cf04"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("1c6acce1-6695-48e4-90b8-7efbf4557682"), new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("387b08d1-e0bc-487b-b016-29f08767ac53"), new Guid("625d8851-401c-46dc-9447-a01cb30a5ead") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("45388609-13fd-4e09-b9cb-818da292724a"), new Guid("26dda459-1d42-48b3-b83a-2e0e02019133") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("45388609-13fd-4e09-b9cb-818da292724a"), new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("51d2dcd3-5ebd-4307-8536-6e1b08246c57"), new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("52c80c68-719e-4563-ae2c-973ed5c21819"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("6b80d348-b569-4d02-9c91-8fda2bf8c55b"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("72aa2d3e-9c1a-4d0e-a5dd-580e647bb0d6"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("77ba7cd2-a67b-4768-b0db-5a316847c0dd"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("ae5c0f64-6325-4ba1-8c87-bff4fc4e0e31"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("b41c440f-35b2-4357-9cb9-035e74f37f22"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("d70aa89f-5001-44c9-957a-5c2ed373dbde"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("e3f4a565-271e-4967-8201-7dc4e4ac7904"), new Guid("ca2c22b2-10e3-401f-bf95-430038adea53") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("ed943494-7105-4e40-971e-ef4973e70e52"), new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036") });

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("2cccf882-2683-4062-9a8b-395469d95e5f"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("3173fd27-ba23-4ead-b087-820e07e66206"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("31d16437-f8db-43b8-ab00-7ef00c9675bb"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("33953b22-4a32-46af-a0c1-ec7cad8934c2"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("34865546-1100-4973-b543-ea7e8d5f3f09"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("469505b4-44d5-4967-bca8-e224198360eb"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("4eeb70ef-24a3-4ce9-a580-83c0b786405e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("59099e92-f18d-4163-bdcc-a1bd95837874"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("64353719-4fa7-4b08-870e-c1eab95d4ab0"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("649f3997-daae-4d55-8104-9612c438863a"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7000a51f-ec26-4e1d-8ae0-60d2457fce09"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("86ad15b0-1e3d-4c2e-8da1-399c8acb664e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("8ca92b70-eaf2-4154-a24b-e92a8fd3de54"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("a174c834-ed44-4d0e-9e70-c8be475c991d"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("d8dec4da-84fa-488f-92e4-1e348380ac02"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("e79de677-184e-472e-a24c-5a941c382c8e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f7d56204-e8a8-4829-a22d-e0dc1eb11d69"));

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa"), new Guid("1a789339-0682-4595-baff-4cd7e534cd7d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f"), new Guid("1a789339-0682-4595-baff-4cd7e534cd7d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ca2c22b2-10e3-401f-bf95-430038adea53"), new Guid("5d4c2e02-e3d6-4c86-a6ad-d4a7d528ae71") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), new Guid("60a4c7fc-5e89-4988-8450-b032269e17ab") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2"), new Guid("69daea01-dd87-496b-b15a-b62a703594b1") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa"), new Guid("a7401deb-0954-429a-b5a1-364ec38737fa") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("625d8851-401c-46dc-9447-a01cb30a5ead"), new Guid("a7401deb-0954-429a-b5a1-364ec38737fa") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), new Guid("a7401deb-0954-429a-b5a1-364ec38737fa") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("26dda459-1d42-48b3-b83a-2e0e02019133"), new Guid("c40d1ada-fb57-4b41-a395-ea7ca451f889") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01"), new Guid("c40d1ada-fb57-4b41-a395-ea7ca451f889") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2"), new Guid("c40d1ada-fb57-4b41-a395-ea7ca451f889") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("26dda459-1d42-48b3-b83a-2e0e02019133"), new Guid("c90062c6-28fd-4dab-b1f0-b71ef6059b44") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa"), new Guid("c90062c6-28fd-4dab-b1f0-b71ef6059b44") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("26dda459-1d42-48b3-b83a-2e0e02019133"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("3f252108-f831-49a1-9ef1-028d33fb4322"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("625d8851-401c-46dc-9447-a01cb30a5ead"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ca2c22b2-10e3-401f-bf95-430038adea53"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa"), new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("3f252108-f831-49a1-9ef1-028d33fb4322"), new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f"), new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("625d8851-401c-46dc-9447-a01cb30a5ead"), new Guid("e16d2eba-6198-43b4-af20-c0a3ec7f8b8b") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), new Guid("e16d2eba-6198-43b4-af20-c0a3ec7f8b8b") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("3f252108-f831-49a1-9ef1-028d33fb4322"), new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01"), new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa"), new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), new Guid("f40787f9-d2ab-4377-afae-3230cf67a5e1") });

            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "267848db-d7a6-4d8d-860c-5859c94fd0bf");

            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "4947910d-7c8c-4815-ba7e-f90b043d430d");

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), new Guid("5d3efd6d-568b-4cdd-9ae4-56dbcf30e7da") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2"), new Guid("5d3efd6d-568b-4cdd-9ae4-56dbcf30e7da") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa"), new Guid("5d3efd6d-568b-4cdd-9ae4-56dbcf30e7da") });

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("0a99b6a9-9f80-441d-a24a-0c965a97b9fc"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("0ebfd788-29e0-4d23-91a7-0c2110b0ef18"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("248c2aab-c979-4667-b3dc-b7b59382ee29"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("269afa8d-1c6d-4047-86c7-d00351e45bec"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("27370bd8-a38e-4d85-9394-7c75d090e55c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("288760ec-c547-4087-93b6-98fffefd645e"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("2b651023-7ea2-4a62-b36a-7cc40e5d056a"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("2ed6e10d-54a8-41c0-9037-5d6c8cead46c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("31e41a70-0ce1-44f0-8504-272c0420745b"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("386295f4-3bb3-4256-9671-15d119ff4377"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("3a5225f5-c1e3-485f-9457-2122a51e5617"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("4aed69f8-ceb3-49da-b742-a2907cedfe20"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("66386afe-613f-418f-b8c2-1234ab294a0b"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("76ade2a6-14a6-425e-9637-f7b8acefa39b"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("7bbc72ec-db21-4886-9c24-cf4960483ad2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a0c809b9-c89f-4059-aec7-d1fc1fc7b878"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a41123b6-7c3e-4872-884c-25ac31fcfa4c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("aa227b12-10c6-4aea-ba6a-3e98d303c51b"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ae68d810-a802-4969-b556-85dc5e081fa1"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("b48d0f39-576b-4ef4-a941-effa0ad8b81d"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("b6277aed-7761-49e6-9249-d77eb71b2ffa"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("bf27c15a-5c90-475f-9dfe-3cbdd73b4ea7"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("c4473d09-3ae2-424a-b060-6b3fe5fb8140"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("c51cd834-0cb8-4c1c-accb-d923bea52bb9"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("cb38f7ed-7670-44ca-937f-231d97dd569c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("d48071ba-63ed-4959-90dd-34f3d55deeb2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("dd017454-996f-452e-b6a5-641f6b593725"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("dfc051c0-5e39-4e9f-86fa-38732544acad"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ec07de00-8e23-48dd-8420-c3da8d459e6a"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ec4b6c2e-43d2-4ef2-a0f2-a74e7f5d3cfc"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ee673f00-40cf-499f-aad5-2f53dd7a50df"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("08b60d6e-98b7-484f-9ae5-ffcd6311cf04"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("1c6acce1-6695-48e4-90b8-7efbf4557682"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("387b08d1-e0bc-487b-b016-29f08767ac53"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("45388609-13fd-4e09-b9cb-818da292724a"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("51d2dcd3-5ebd-4307-8536-6e1b08246c57"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("52c80c68-719e-4563-ae2c-973ed5c21819"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("6b80d348-b569-4d02-9c91-8fda2bf8c55b"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("72aa2d3e-9c1a-4d0e-a5dd-580e647bb0d6"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("77ba7cd2-a67b-4768-b0db-5a316847c0dd"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("ae5c0f64-6325-4ba1-8c87-bff4fc4e0e31"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("b41c440f-35b2-4357-9cb9-035e74f37f22"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("d70aa89f-5001-44c9-957a-5c2ed373dbde"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("e3f4a565-271e-4967-8201-7dc4e4ac7904"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("ed943494-7105-4e40-971e-ef4973e70e52"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("26dda459-1d42-48b3-b83a-2e0e02019133"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("3f252108-f831-49a1-9ef1-028d33fb4322"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("625d8851-401c-46dc-9447-a01cb30a5ead"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("ca2c22b2-10e3-401f-bf95-430038adea53"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("1a789339-0682-4595-baff-4cd7e534cd7d"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("5d4c2e02-e3d6-4c86-a6ad-d4a7d528ae71"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("60a4c7fc-5e89-4988-8450-b032269e17ab"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("69daea01-dd87-496b-b15a-b62a703594b1"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("a7401deb-0954-429a-b5a1-364ec38737fa"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("c40d1ada-fb57-4b41-a395-ea7ca451f889"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("c90062c6-28fd-4dab-b1f0-b71ef6059b44"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("e16d2eba-6198-43b4-af20-c0a3ec7f8b8b"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f40787f9-d2ab-4377-afae-3230cf67a5e1"));

            migrationBuilder.DeleteData(
                table: "Usuarios",
                keyColumn: "IdUsuario",
                keyValue: new Guid("5d3efd6d-568b-4cdd-9ae4-56dbcf30e7da"));

            migrationBuilder.AddColumn<bool>(
                name: "Inativo",
                table: "Usuarios",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "Atores",
                columns: new[] { "IdAtor", "Nome" },
                values: new object[,]
                {
                    { new Guid("0527e01d-c083-433c-a23b-2f5f0c5be73c"), "Ben Burtt" },
                    { new Guid("2f87c7c8-7716-4ddf-a5c6-19e44d82359a"), "Woody Harrelson" },
                    { new Guid("d206d821-a9e5-456d-84d5-1c2635dd89f2"), "Tom Sizemore" },
                    { new Guid("450c571c-021f-4ceb-9555-49b5ec9b8efa"), "Robert Downey Jr." },
                    { new Guid("73f83e4a-660e-4e83-82c2-da7829bc278c"), "Rachel McAdams" },
                    { new Guid("2061fdbb-5f20-479d-8788-a945e42f1f45"), "Paul Rhys" },
                    { new Guid("fa99fb86-f2e0-4f83-8cf7-419e2917c4e2"), "Patton Oswalt" },
                    { new Guid("db97e70f-a531-45ac-9f3c-237e6e2644be"), "Patrick Magee" },
                    { new Guid("05f5dd44-3385-426c-9879-558f32b5024c"), "Owen Wilson" },
                    { new Guid("911c2b21-501d-4c93-bd08-86fbaf32abb7"), "Mickey Rourke" },
                    { new Guid("de8739f1-d817-41df-8d6b-1d193bf4592f"), "Michael Bates" },
                    { new Guid("68560754-7f7c-43b3-ab6a-5140c9015566"), "Mark Ruffalo" },
                    { new Guid("7de870eb-f251-4789-8038-eb0b8fd0c0d7"), "Malcolm McDowell" },
                    { new Guid("c1b12151-fb82-48f8-925a-11e63cc8939e"), "Laurence Fishburne" },
                    { new Guid("c963bc11-d8ff-476f-a262-3ede2f035c0d"), "Keanu Reeves" },
                    { new Guid("9a73706d-6a0f-4272-8fb2-7ffe94bec684"), "Lou Romano" },
                    { new Guid("942f7575-225c-456a-8864-20b3e61db70d"), "Jude Law" },
                    { new Guid("d175379c-98c3-45ca-b396-1587b57cca39"), "Brad Garrett" },
                    { new Guid("ffe7978e-ff7a-4368-b187-323d2606e5b8"), "Bruce Willis" },
                    { new Guid("22008334-5643-47b1-9e7a-59b9e833c4a7"), "Carrie-Anne Moss" },
                    { new Guid("cfa74cf0-c3cc-4085-b132-af1add4cfac6"), "Chris Evans" },
                    { new Guid("2b6357ad-bb54-4ace-a8a7-2d67694d3c11"), "Juliette Lewis" },
                    { new Guid("6e9fb0cd-f900-45ca-8915-a689b0e979e4"), "Daniel Mays" },
                    { new Guid("86f2962d-ad8d-48a9-af5b-abf80f19bcd9"), "Dean-Charles Chapman" },
                    { new Guid("7ca0ebcc-63c7-402a-a98a-077cbf0c6257"), "Clive Owen" },
                    { new Guid("0179c98e-ebcd-4991-9694-379ddcd3d772"), "Eric Dane" },
                    { new Guid("b71bc17a-34e3-48f6-bb3a-c6f32ddca395"), "George MacKay" },
                    { new Guid("fd668aed-a77a-414a-82f9-5126339fadaa"), "Geraldine Chaplin" },
                    { new Guid("548f5214-564c-4a8d-9afe-176a65d14160"), "Jeff Garlin" },
                    { new Guid("d81fc218-f8c6-44b1-875d-5bf805e25207"), "Jennifer Aniston" },
                    { new Guid("e7cfd58a-c1df-4570-9ed0-e05505cf3afd"), "Elissa Knight" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("11e32106-9911-45ca-ab9d-2c3f2a2e66a2"), "Andrew Stanton" },
                    { new Guid("a495c9d7-464b-4d8b-a37f-b4f3e3eea5a0"), "Stanley Kubrick" },
                    { new Guid("12983b73-5d9f-488a-b6cd-5402d418cae1"), "Oliver Stone" },
                    { new Guid("7c91d8bc-eb3a-4055-aeb0-c946c041f79a"), "Guy Ritchie" },
                    { new Guid("4ea7919b-8b72-4032-bd45-204be27722a7"), "David Frankel" },
                    { new Guid("b830e7e8-38b6-46e9-ba13-6f1ad87a9f38"), "Sam Mendes" },
                    { new Guid("65e7faba-a5fc-44a1-8802-faa4dc23166c"), "Joe Russo" },
                    { new Guid("3a8ba2e3-163f-439c-acb6-976bdcdc3bf2"), "Richard Attenborough" },
                    { new Guid("89055e3c-0cc0-428c-85ce-f178aa4f6777"), "Lilly Wachowski" },
                    { new Guid("7e3a8e79-2b41-4908-8484-4eec8d20ebf4"), "Lana Wachowski" },
                    { new Guid("21630c2b-ac4e-4316-8738-c89a33d43c46"), "Jan Pinkava" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("40a926c7-cfcb-4d83-b0d2-7fec4fbe4328"), "Brad Bird" },
                    { new Guid("6670c3cd-9acb-4d04-bddd-1b0dcdcbb9fd"), "Robert Rodriguez" },
                    { new Guid("57a005ce-12b8-41fe-ab4c-31514264efdd"), "Anthony Russo" },
                    { new Guid("a30001aa-b563-4021-89f0-66db4e6b2b52"), "Quentin Tarantino" },
                    { new Guid("44adaece-9f75-4026-a387-94372bc1f5f5"), "Frank Miller" }
                });

            migrationBuilder.InsertData(
                table: "Filmes",
                columns: new[] { "IdFilme", "Ano", "Nome", "NomeOriginal", "Sinopse" },
                values: new object[,]
                {
                    { new Guid("08ed366d-07ee-4fc4-98f5-497492679dce"), 1999, "Matrix", "The Matrix", "Um hacker aprende com os misteriosos rebeldes sobre a verdadeira natureza de sua realidade e seu papel na guerra contra seus controladores." },
                    { new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547"), 2007, "Ratatouille", "Ratatouille", "Um rato que pode cozinhar forja uma aliança incomum com um jovem garoto em um famoso restaurante em Paris." },
                    { new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647"), 2008, "Marley & Eu", "Marley & Me", "Uma família aprende importantes lições de vida com seu adorável, mas malicioso e neurótico cão." },
                    { new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8"), 2008, "WALL·E", "WALL·E", "Num futuro distante, um pequeno robô faz uma viagem que decidirá o destino da humanidade." },
                    { new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76"), 2019, "Vingadores: Ultimato", "Avengers: Endgame", "Após os eventos devastadores de Vingadores: Guerra Infinita , o universo está em ruínas, e com a ajuda de aliados os Vingadores se reúnem para desfazer as ações de Thanos e restaurar a ordem." },
                    { new Guid("baff59a7-b91d-474f-a741-d16d6928c363"), 2005, "Sin City: A Cidade do Pecado", "Sin City", "O filme explora a miserável cidade de Basin City, e conta a historia de três pessoas diferentes, envoltas em violencia e corrupção." },
                    { new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8"), 1994, "Assassinos por Natureza", "Natural Born Killers", "Duas vítimas de uma infância traumatizada tornam-se amantes e assassinos em série, irresponsavelmente glorificados pela mídia." },
                    { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), 1992, "Chaplin", "Chaplin", "Relata a vida convulsiva e controversa do mestre diretor de comédia Charles Chaplin." },
                    { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), 2009, "Sherlock Holmes", "Sherlock Holmes", "O detective Sherlock Holmes e seu colega Watson se engajam numa batalha para lutar contra as ameaças da Inglaterra." },
                    { new Guid("626c6544-cd74-4a71-941b-da41935caa66"), 1971, "Laranja Mecânica", "A Clockwork Orange", "No futuro, um líder duma turma é levado para prisão é decide ser voluntário dum experimento, mais os resultados não são os esperados." },
                    { new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0"), 2019, "1917", "1917", "6 de Abril de 1917. Enquanto um regimento se reúne para lutar uma guerra nas profundezas do território inimigo, dois soldados são designados para uma corrida contra o tempo para entregar uma mensagem que impedirá 1.600 homens de caminhar direto para uma uma armadilha mortal." }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("81ed58d2-5ffc-4527-a95c-90c494ac669f"), "Fantasia científica" },
                    { new Guid("690016e2-ce2f-4ce9-a18d-386ce8f44a54"), "Ficção científica" },
                    { new Guid("59644757-1867-4726-a11c-faddecb180c6"), "Filmes com truques" },
                    { new Guid("e5b01ccf-d60d-412f-bc1e-c732bdb3d056"), "Filmes de guerra" },
                    { new Guid("a55afd10-87b6-4e16-a7c0-ea4b1c34bb71"), "Musical" },
                    { new Guid("1b08b997-199b-418d-af13-043544306702"), "Pornográfico" },
                    { new Guid("6ffe6993-db07-448b-9e88-5ef052deb87e"), "Romance" },
                    { new Guid("cf83b476-a568-4266-8645-986417dca49b"), "Seriado" },
                    { new Guid("9769c6cc-39db-4dc2-b767-8b673ac3c587"), "Suspense" },
                    { new Guid("fc071b28-673d-4833-8a1e-77928f766903"), "Terror" },
                    { new Guid("631291f2-40e3-42a7-89d4-6232c7ce44c1"), "Thriller" },
                    { new Guid("fd2e6992-6983-4436-b96a-ef3e8573a891"), "Fantasia" },
                    { new Guid("a44ad41d-6e62-4a8c-8ab5-d3a6b73e71d7"), "Filme policial" },
                    { new Guid("8000c4ec-f64f-46d3-a26a-67510da15b0c"), "Faroeste" },
                    { new Guid("f1a01323-6ab0-4d38-8109-33e9e8be8f15"), "Comédia de ação" },
                    { new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004"), "Drama" },
                    { new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d"), "Ação" },
                    { new Guid("77e00a20-5b1c-43b7-be04-d3ce0622cafc"), "Animação" },
                    { new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6"), "Aventura" },
                    { new Guid("998dfb58-fafc-4ef4-a6cf-73f8d97e5b81"), "Cinema de arte" },
                    { new Guid("e54db6d9-5cb7-4f44-86a9-cae30a9a018e"), "Espionagem" },
                    { new Guid("8c1e7ab4-6cac-4e7d-aede-91508452194e"), "Comédia" },
                    { new Guid("6be0cd64-af9b-475e-a2d4-762b1f02dbe9"), "Chanchada" },
                    { new Guid("7f474172-8942-478a-8e19-e8970a96aa28"), "Comédia dramática" },
                    { new Guid("6477416d-55c4-4ae8-8c46-aa4f64ccd1aa"), "Comédia romântica" },
                    { new Guid("91008cee-8b48-4af0-bfb0-519355e6e1d3"), "Dança" }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("f3c47672-b854-451e-bb81-db609f6ef766"), "Documentário" },
                    { new Guid("d41ce0ce-7b09-4d09-b5a6-62e00d167017"), "Docuficção" },
                    { new Guid("d0484c24-ff56-4f17-a330-473938b0aedc"), "Comédia de terror" }
                });

            migrationBuilder.InsertData(
                table: "IdentityRole",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "3fc3f0ec-5d0a-4d2c-8961-d13de6ccdaad", "20f5d62d-dab6-4807-8d9f-8d0678b7c5e9", "Admin", "ADMIN" },
                    { "d596a6aa-7281-4f72-a02a-ee6ad2d05528", "c38acfb0-7868-4f10-9363-33da1dcc12f6", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "Usuarios",
                columns: new[] { "IdUsuario", "Email", "Nome", "Senha" },
                values: new object[] { new Guid("f7c280e2-f7b1-4df0-b185-b49e7e26a92c"), "rafael.av@gmail.com", "Rafael Vasconcelos", "AQAAAAEAACcQAAAAECsp1FF1MqOQn85YXv30QxtG5Om4JjruR0gUyaDvVP3ItdY1j88h820Edcc4jSWVaA==" });

            migrationBuilder.InsertData(
                table: "AtoresFilmes",
                columns: new[] { "IdAtor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("7de870eb-f251-4789-8038-eb0b8fd0c0d7"), new Guid("626c6544-cd74-4a71-941b-da41935caa66") },
                    { new Guid("68560754-7f7c-43b3-ab6a-5140c9015566"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") },
                    { new Guid("0527e01d-c083-433c-a23b-2f5f0c5be73c"), new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8") },
                    { new Guid("e7cfd58a-c1df-4570-9ed0-e05505cf3afd"), new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8") },
                    { new Guid("548f5214-564c-4a8d-9afe-176a65d14160"), new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8") },
                    { new Guid("05f5dd44-3385-426c-9879-558f32b5024c"), new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647") },
                    { new Guid("d81fc218-f8c6-44b1-875d-5bf805e25207"), new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647") },
                    { new Guid("0179c98e-ebcd-4991-9694-379ddcd3d772"), new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647") },
                    { new Guid("911c2b21-501d-4c93-bd08-86fbaf32abb7"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") },
                    { new Guid("7ca0ebcc-63c7-402a-a98a-077cbf0c6257"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") },
                    { new Guid("ffe7978e-ff7a-4368-b187-323d2606e5b8"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") },
                    { new Guid("d175379c-98c3-45ca-b396-1587b57cca39"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") },
                    { new Guid("9a73706d-6a0f-4272-8fb2-7ffe94bec684"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") },
                    { new Guid("c963bc11-d8ff-476f-a262-3ede2f035c0d"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") },
                    { new Guid("c1b12151-fb82-48f8-925a-11e63cc8939e"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") },
                    { new Guid("22008334-5643-47b1-9e7a-59b9e833c4a7"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") },
                    { new Guid("cfa74cf0-c3cc-4085-b132-af1add4cfac6"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") },
                    { new Guid("450c571c-021f-4ceb-9555-49b5ec9b8efa"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") },
                    { new Guid("fa99fb86-f2e0-4f83-8cf7-419e2917c4e2"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") },
                    { new Guid("db97e70f-a531-45ac-9f3c-237e6e2644be"), new Guid("626c6544-cd74-4a71-941b-da41935caa66") },
                    { new Guid("73f83e4a-660e-4e83-82c2-da7829bc278c"), new Guid("d09363b2-10c0-46d9-9f40-99274c362938") },
                    { new Guid("450c571c-021f-4ceb-9555-49b5ec9b8efa"), new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d") },
                    { new Guid("fd668aed-a77a-414a-82f9-5126339fadaa"), new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d") },
                    { new Guid("2061fdbb-5f20-479d-8788-a945e42f1f45"), new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d") },
                    { new Guid("de8739f1-d817-41df-8d6b-1d193bf4592f"), new Guid("626c6544-cd74-4a71-941b-da41935caa66") },
                    { new Guid("450c571c-021f-4ceb-9555-49b5ec9b8efa"), new Guid("d09363b2-10c0-46d9-9f40-99274c362938") },
                    { new Guid("2f87c7c8-7716-4ddf-a5c6-19e44d82359a"), new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8") },
                    { new Guid("942f7575-225c-456a-8864-20b3e61db70d"), new Guid("d09363b2-10c0-46d9-9f40-99274c362938") },
                    { new Guid("d206d821-a9e5-456d-84d5-1c2635dd89f2"), new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8") },
                    { new Guid("6e9fb0cd-f900-45ca-8915-a689b0e979e4"), new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0") },
                    { new Guid("86f2962d-ad8d-48a9-af5b-abf80f19bcd9"), new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0") },
                    { new Guid("b71bc17a-34e3-48f6-bb3a-c6f32ddca395"), new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0") },
                    { new Guid("2b6357ad-bb54-4ace-a8a7-2d67694d3c11"), new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("7e3a8e79-2b41-4908-8484-4eec8d20ebf4"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") },
                    { new Guid("89055e3c-0cc0-428c-85ce-f178aa4f6777"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") },
                    { new Guid("3a8ba2e3-163f-439c-acb6-976bdcdc3bf2"), new Guid("626c6544-cd74-4a71-941b-da41935caa66") },
                    { new Guid("21630c2b-ac4e-4316-8738-c89a33d43c46"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") },
                    { new Guid("40a926c7-cfcb-4d83-b0d2-7fec4fbe4328"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") },
                    { new Guid("b830e7e8-38b6-46e9-ba13-6f1ad87a9f38"), new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0") },
                    { new Guid("44adaece-9f75-4026-a387-94372bc1f5f5"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") },
                    { new Guid("7c91d8bc-eb3a-4055-aeb0-c946c041f79a"), new Guid("d09363b2-10c0-46d9-9f40-99274c362938") },
                    { new Guid("4ea7919b-8b72-4032-bd45-204be27722a7"), new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("3a8ba2e3-163f-439c-acb6-976bdcdc3bf2"), new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d") },
                    { new Guid("11e32106-9911-45ca-ab9d-2c3f2a2e66a2"), new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8") },
                    { new Guid("65e7faba-a5fc-44a1-8802-faa4dc23166c"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") },
                    { new Guid("57a005ce-12b8-41fe-ab4c-31514264efdd"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") },
                    { new Guid("12983b73-5d9f-488a-b6cd-5402d418cae1"), new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8") },
                    { new Guid("a30001aa-b563-4021-89f0-66db4e6b2b52"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") }
                });

            migrationBuilder.InsertData(
                table: "GenerosFilmes",
                columns: new[] { "IdFilme", "IdGenero" },
                values: new object[,]
                {
                    { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") },
                    { new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") },
                    { new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") },
                    { new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") },
                    { new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") },
                    { new Guid("626c6544-cd74-4a71-941b-da41935caa66"), new Guid("690016e2-ce2f-4ce9-a18d-386ce8f44a54") },
                    { new Guid("baff59a7-b91d-474f-a741-d16d6928c363"), new Guid("631291f2-40e3-42a7-89d4-6232c7ce44c1") },
                    { new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0"), new Guid("e5b01ccf-d60d-412f-bc1e-c732bdb3d056") },
                    { new Guid("626c6544-cd74-4a71-941b-da41935caa66"), new Guid("a44ad41d-6e62-4a8c-8ab5-d3a6b73e71d7") },
                    { new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8"), new Guid("a44ad41d-6e62-4a8c-8ab5-d3a6b73e71d7") },
                    { new Guid("baff59a7-b91d-474f-a741-d16d6928c363"), new Guid("a44ad41d-6e62-4a8c-8ab5-d3a6b73e71d7") },
                    { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), new Guid("9769c6cc-39db-4dc2-b767-8b673ac3c587") },
                    { new Guid("626c6544-cd74-4a71-941b-da41935caa66"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") },
                    { new Guid("08ed366d-07ee-4fc4-98f5-497492679dce"), new Guid("690016e2-ce2f-4ce9-a18d-386ce8f44a54") },
                    { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), new Guid("d41ce0ce-7b09-4d09-b5a6-62e00d167017") },
                    { new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8"), new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d") },
                    { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), new Guid("7f474172-8942-478a-8e19-e8970a96aa28") },
                    { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d") },
                    { new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76"), new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d") },
                    { new Guid("08ed366d-07ee-4fc4-98f5-497492679dce"), new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d") },
                    { new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8"), new Guid("77e00a20-5b1c-43b7-be04-d3ce0622cafc") },
                    { new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647"), new Guid("7f474172-8942-478a-8e19-e8970a96aa28") },
                    { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6") },
                    { new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547"), new Guid("77e00a20-5b1c-43b7-be04-d3ce0622cafc") },
                    { new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8"), new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6") },
                    { new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547"), new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6") },
                    { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), new Guid("8c1e7ab4-6cac-4e7d-aede-91508452194e") },
                    { new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647"), new Guid("8c1e7ab4-6cac-4e7d-aede-91508452194e") },
                    { new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547"), new Guid("8c1e7ab4-6cac-4e7d-aede-91508452194e") },
                    { new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76"), new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6") }
                });

            migrationBuilder.InsertData(
                table: "Votos",
                columns: new[] { "IdFilme", "IdUsuario", "Nota" },
                values: new object[,]
                {
                    { new Guid("baff59a7-b91d-474f-a741-d16d6928c363"), new Guid("f7c280e2-f7b1-4df0-b185-b49e7e26a92c"), (byte)3 },
                    { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), new Guid("f7c280e2-f7b1-4df0-b185-b49e7e26a92c"), (byte)1 },
                    { new Guid("08ed366d-07ee-4fc4-98f5-497492679dce"), new Guid("f7c280e2-f7b1-4df0-b185-b49e7e26a92c"), (byte)4 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("0179c98e-ebcd-4991-9694-379ddcd3d772"), new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("0527e01d-c083-433c-a23b-2f5f0c5be73c"), new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("05f5dd44-3385-426c-9879-558f32b5024c"), new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("2061fdbb-5f20-479d-8788-a945e42f1f45"), new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("22008334-5643-47b1-9e7a-59b9e833c4a7"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("2b6357ad-bb54-4ace-a8a7-2d67694d3c11"), new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("2f87c7c8-7716-4ddf-a5c6-19e44d82359a"), new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("450c571c-021f-4ceb-9555-49b5ec9b8efa"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("450c571c-021f-4ceb-9555-49b5ec9b8efa"), new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("450c571c-021f-4ceb-9555-49b5ec9b8efa"), new Guid("d09363b2-10c0-46d9-9f40-99274c362938") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("548f5214-564c-4a8d-9afe-176a65d14160"), new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("68560754-7f7c-43b3-ab6a-5140c9015566"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("6e9fb0cd-f900-45ca-8915-a689b0e979e4"), new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("73f83e4a-660e-4e83-82c2-da7829bc278c"), new Guid("d09363b2-10c0-46d9-9f40-99274c362938") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("7ca0ebcc-63c7-402a-a98a-077cbf0c6257"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("7de870eb-f251-4789-8038-eb0b8fd0c0d7"), new Guid("626c6544-cd74-4a71-941b-da41935caa66") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("86f2962d-ad8d-48a9-af5b-abf80f19bcd9"), new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("911c2b21-501d-4c93-bd08-86fbaf32abb7"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("942f7575-225c-456a-8864-20b3e61db70d"), new Guid("d09363b2-10c0-46d9-9f40-99274c362938") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("9a73706d-6a0f-4272-8fb2-7ffe94bec684"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("b71bc17a-34e3-48f6-bb3a-c6f32ddca395"), new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("c1b12151-fb82-48f8-925a-11e63cc8939e"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("c963bc11-d8ff-476f-a262-3ede2f035c0d"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("cfa74cf0-c3cc-4085-b132-af1add4cfac6"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("d175379c-98c3-45ca-b396-1587b57cca39"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("d206d821-a9e5-456d-84d5-1c2635dd89f2"), new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("d81fc218-f8c6-44b1-875d-5bf805e25207"), new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("db97e70f-a531-45ac-9f3c-237e6e2644be"), new Guid("626c6544-cd74-4a71-941b-da41935caa66") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("de8739f1-d817-41df-8d6b-1d193bf4592f"), new Guid("626c6544-cd74-4a71-941b-da41935caa66") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("e7cfd58a-c1df-4570-9ed0-e05505cf3afd"), new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("fa99fb86-f2e0-4f83-8cf7-419e2917c4e2"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("fd668aed-a77a-414a-82f9-5126339fadaa"), new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ffe7978e-ff7a-4368-b187-323d2606e5b8"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") });

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("6670c3cd-9acb-4d04-bddd-1b0dcdcbb9fd"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("a495c9d7-464b-4d8b-a37f-b4f3e3eea5a0"));

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("11e32106-9911-45ca-ab9d-2c3f2a2e66a2"), new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("12983b73-5d9f-488a-b6cd-5402d418cae1"), new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("21630c2b-ac4e-4316-8738-c89a33d43c46"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("3a8ba2e3-163f-439c-acb6-976bdcdc3bf2"), new Guid("626c6544-cd74-4a71-941b-da41935caa66") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("3a8ba2e3-163f-439c-acb6-976bdcdc3bf2"), new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("40a926c7-cfcb-4d83-b0d2-7fec4fbe4328"), new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("44adaece-9f75-4026-a387-94372bc1f5f5"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("4ea7919b-8b72-4032-bd45-204be27722a7"), new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("57a005ce-12b8-41fe-ab4c-31514264efdd"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("65e7faba-a5fc-44a1-8802-faa4dc23166c"), new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("7c91d8bc-eb3a-4055-aeb0-c946c041f79a"), new Guid("d09363b2-10c0-46d9-9f40-99274c362938") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("7e3a8e79-2b41-4908-8484-4eec8d20ebf4"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("89055e3c-0cc0-428c-85ce-f178aa4f6777"), new Guid("08ed366d-07ee-4fc4-98f5-497492679dce") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("a30001aa-b563-4021-89f0-66db4e6b2b52"), new Guid("baff59a7-b91d-474f-a741-d16d6928c363") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("b830e7e8-38b6-46e9-ba13-6f1ad87a9f38"), new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0") });

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("1b08b997-199b-418d-af13-043544306702"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("59644757-1867-4726-a11c-faddecb180c6"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("6477416d-55c4-4ae8-8c46-aa4f64ccd1aa"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("6be0cd64-af9b-475e-a2d4-762b1f02dbe9"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("6ffe6993-db07-448b-9e88-5ef052deb87e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("8000c4ec-f64f-46d3-a26a-67510da15b0c"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("81ed58d2-5ffc-4527-a95c-90c494ac669f"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("91008cee-8b48-4af0-bfb0-519355e6e1d3"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("998dfb58-fafc-4ef4-a6cf-73f8d97e5b81"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("a55afd10-87b6-4e16-a7c0-ea4b1c34bb71"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("cf83b476-a568-4266-8645-986417dca49b"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("d0484c24-ff56-4f17-a330-473938b0aedc"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("e54db6d9-5cb7-4f44-86a9-cae30a9a018e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f1a01323-6ab0-4d38-8109-33e9e8be8f15"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f3c47672-b854-451e-bb81-db609f6ef766"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("fc071b28-673d-4833-8a1e-77928f766903"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("fd2e6992-6983-4436-b96a-ef3e8573a891"));

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8"), new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76"), new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547"), new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("baff59a7-b91d-474f-a741-d16d6928c363"), new Guid("631291f2-40e3-42a7-89d4-6232c7ce44c1") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("08ed366d-07ee-4fc4-98f5-497492679dce"), new Guid("690016e2-ce2f-4ce9-a18d-386ce8f44a54") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("626c6544-cd74-4a71-941b-da41935caa66"), new Guid("690016e2-ce2f-4ce9-a18d-386ce8f44a54") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8"), new Guid("77e00a20-5b1c-43b7-be04-d3ce0622cafc") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547"), new Guid("77e00a20-5b1c-43b7-be04-d3ce0622cafc") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647"), new Guid("7f474172-8942-478a-8e19-e8970a96aa28") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), new Guid("7f474172-8942-478a-8e19-e8970a96aa28") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647"), new Guid("8c1e7ab4-6cac-4e7d-aede-91508452194e") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), new Guid("8c1e7ab4-6cac-4e7d-aede-91508452194e") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547"), new Guid("8c1e7ab4-6cac-4e7d-aede-91508452194e") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), new Guid("9769c6cc-39db-4dc2-b767-8b673ac3c587") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("626c6544-cd74-4a71-941b-da41935caa66"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8"), new Guid("a44ad41d-6e62-4a8c-8ab5-d3a6b73e71d7") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("626c6544-cd74-4a71-941b-da41935caa66"), new Guid("a44ad41d-6e62-4a8c-8ab5-d3a6b73e71d7") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("baff59a7-b91d-474f-a741-d16d6928c363"), new Guid("a44ad41d-6e62-4a8c-8ab5-d3a6b73e71d7") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"), new Guid("d41ce0ce-7b09-4d09-b5a6-62e00d167017") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0"), new Guid("e5b01ccf-d60d-412f-bc1e-c732bdb3d056") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("08ed366d-07ee-4fc4-98f5-497492679dce"), new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8"), new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76"), new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d") });

            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "3fc3f0ec-5d0a-4d2c-8961-d13de6ccdaad");

            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "d596a6aa-7281-4f72-a02a-ee6ad2d05528");

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("08ed366d-07ee-4fc4-98f5-497492679dce"), new Guid("f7c280e2-f7b1-4df0-b185-b49e7e26a92c") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("baff59a7-b91d-474f-a741-d16d6928c363"), new Guid("f7c280e2-f7b1-4df0-b185-b49e7e26a92c") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("d09363b2-10c0-46d9-9f40-99274c362938"), new Guid("f7c280e2-f7b1-4df0-b185-b49e7e26a92c") });

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("0179c98e-ebcd-4991-9694-379ddcd3d772"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("0527e01d-c083-433c-a23b-2f5f0c5be73c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("05f5dd44-3385-426c-9879-558f32b5024c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("2061fdbb-5f20-479d-8788-a945e42f1f45"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("22008334-5643-47b1-9e7a-59b9e833c4a7"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("2b6357ad-bb54-4ace-a8a7-2d67694d3c11"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("2f87c7c8-7716-4ddf-a5c6-19e44d82359a"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("450c571c-021f-4ceb-9555-49b5ec9b8efa"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("548f5214-564c-4a8d-9afe-176a65d14160"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("68560754-7f7c-43b3-ab6a-5140c9015566"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("6e9fb0cd-f900-45ca-8915-a689b0e979e4"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("73f83e4a-660e-4e83-82c2-da7829bc278c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("7ca0ebcc-63c7-402a-a98a-077cbf0c6257"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("7de870eb-f251-4789-8038-eb0b8fd0c0d7"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("86f2962d-ad8d-48a9-af5b-abf80f19bcd9"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("911c2b21-501d-4c93-bd08-86fbaf32abb7"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("942f7575-225c-456a-8864-20b3e61db70d"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("9a73706d-6a0f-4272-8fb2-7ffe94bec684"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("b71bc17a-34e3-48f6-bb3a-c6f32ddca395"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("c1b12151-fb82-48f8-925a-11e63cc8939e"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("c963bc11-d8ff-476f-a262-3ede2f035c0d"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("cfa74cf0-c3cc-4085-b132-af1add4cfac6"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("d175379c-98c3-45ca-b396-1587b57cca39"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("d206d821-a9e5-456d-84d5-1c2635dd89f2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("d81fc218-f8c6-44b1-875d-5bf805e25207"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("db97e70f-a531-45ac-9f3c-237e6e2644be"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("de8739f1-d817-41df-8d6b-1d193bf4592f"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("e7cfd58a-c1df-4570-9ed0-e05505cf3afd"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("fa99fb86-f2e0-4f83-8cf7-419e2917c4e2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("fd668aed-a77a-414a-82f9-5126339fadaa"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ffe7978e-ff7a-4368-b187-323d2606e5b8"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("11e32106-9911-45ca-ab9d-2c3f2a2e66a2"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("12983b73-5d9f-488a-b6cd-5402d418cae1"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("21630c2b-ac4e-4316-8738-c89a33d43c46"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("3a8ba2e3-163f-439c-acb6-976bdcdc3bf2"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("40a926c7-cfcb-4d83-b0d2-7fec4fbe4328"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("44adaece-9f75-4026-a387-94372bc1f5f5"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("4ea7919b-8b72-4032-bd45-204be27722a7"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("57a005ce-12b8-41fe-ab4c-31514264efdd"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("65e7faba-a5fc-44a1-8802-faa4dc23166c"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("7c91d8bc-eb3a-4055-aeb0-c946c041f79a"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("7e3a8e79-2b41-4908-8484-4eec8d20ebf4"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("89055e3c-0cc0-428c-85ce-f178aa4f6777"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("a30001aa-b563-4021-89f0-66db4e6b2b52"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("b830e7e8-38b6-46e9-ba13-6f1ad87a9f38"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("043a44d0-9a4b-47a6-a75a-c41df71bb8a8"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("08ed366d-07ee-4fc4-98f5-497492679dce"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("2bad2a73-7b87-42d1-8b57-a7c0057d99a8"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("626c6544-cd74-4a71-941b-da41935caa66"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("654a7ba1-76f5-494f-b747-54e6a506e7a0"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("7680e7f0-4994-4ad9-9212-cbfa4fa14647"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("ab87aceb-6fae-414a-977e-38d2d1214a76"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("afdff0ca-8d0c-4473-a8b1-5059939a719d"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("baff59a7-b91d-474f-a741-d16d6928c363"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("c33fc5ec-ce8c-44da-a649-9869ae900547"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("d09363b2-10c0-46d9-9f40-99274c362938"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("34aafce8-6c70-4bc4-8dc7-6c4f8a7ae5f6"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("631291f2-40e3-42a7-89d4-6232c7ce44c1"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("690016e2-ce2f-4ce9-a18d-386ce8f44a54"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("77e00a20-5b1c-43b7-be04-d3ce0622cafc"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7f474172-8942-478a-8e19-e8970a96aa28"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("8c1e7ab4-6cac-4e7d-aede-91508452194e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("9769c6cc-39db-4dc2-b767-8b673ac3c587"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("9cd4807a-c74c-47d7-a613-0215fdf99004"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("a44ad41d-6e62-4a8c-8ab5-d3a6b73e71d7"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("d41ce0ce-7b09-4d09-b5a6-62e00d167017"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("e5b01ccf-d60d-412f-bc1e-c732bdb3d056"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f5a5deb3-66f8-45c2-9a79-4a7e8eead27d"));

            migrationBuilder.DeleteData(
                table: "Usuarios",
                keyColumn: "IdUsuario",
                keyValue: new Guid("f7c280e2-f7b1-4df0-b185-b49e7e26a92c"));

            migrationBuilder.DropColumn(
                name: "Inativo",
                table: "Usuarios");

            migrationBuilder.InsertData(
                table: "Atores",
                columns: new[] { "IdAtor", "Nome" },
                values: new object[,]
                {
                    { new Guid("2ed6e10d-54a8-41c0-9037-5d6c8cead46c"), "Ben Burtt" },
                    { new Guid("31e41a70-0ce1-44f0-8504-272c0420745b"), "Woody Harrelson" },
                    { new Guid("386295f4-3bb3-4256-9671-15d119ff4377"), "Tom Sizemore" },
                    { new Guid("ae68d810-a802-4969-b556-85dc5e081fa1"), "Robert Downey Jr." },
                    { new Guid("cb38f7ed-7670-44ca-937f-231d97dd569c"), "Rachel McAdams" },
                    { new Guid("c51cd834-0cb8-4c1c-accb-d923bea52bb9"), "Paul Rhys" },
                    { new Guid("269afa8d-1c6d-4047-86c7-d00351e45bec"), "Patton Oswalt" },
                    { new Guid("4aed69f8-ceb3-49da-b742-a2907cedfe20"), "Patrick Magee" },
                    { new Guid("a0c809b9-c89f-4059-aec7-d1fc1fc7b878"), "Owen Wilson" },
                    { new Guid("a41123b6-7c3e-4872-884c-25ac31fcfa4c"), "Mickey Rourke" },
                    { new Guid("bf27c15a-5c90-475f-9dfe-3cbdd73b4ea7"), "Michael Bates" },
                    { new Guid("2b651023-7ea2-4a62-b36a-7cc40e5d056a"), "Mark Ruffalo" },
                    { new Guid("b48d0f39-576b-4ef4-a941-effa0ad8b81d"), "Malcolm McDowell" },
                    { new Guid("3a5225f5-c1e3-485f-9457-2122a51e5617"), "Laurence Fishburne" },
                    { new Guid("dfc051c0-5e39-4e9f-86fa-38732544acad"), "Keanu Reeves" },
                    { new Guid("66386afe-613f-418f-b8c2-1234ab294a0b"), "Lou Romano" },
                    { new Guid("27370bd8-a38e-4d85-9394-7c75d090e55c"), "Jude Law" },
                    { new Guid("aa227b12-10c6-4aea-ba6a-3e98d303c51b"), "Brad Garrett" },
                    { new Guid("dd017454-996f-452e-b6a5-641f6b593725"), "Bruce Willis" },
                    { new Guid("76ade2a6-14a6-425e-9637-f7b8acefa39b"), "Carrie-Anne Moss" },
                    { new Guid("248c2aab-c979-4667-b3dc-b7b59382ee29"), "Chris Evans" },
                    { new Guid("b6277aed-7761-49e6-9249-d77eb71b2ffa"), "Juliette Lewis" },
                    { new Guid("c4473d09-3ae2-424a-b060-6b3fe5fb8140"), "Daniel Mays" },
                    { new Guid("288760ec-c547-4087-93b6-98fffefd645e"), "Dean-Charles Chapman" },
                    { new Guid("ee673f00-40cf-499f-aad5-2f53dd7a50df"), "Clive Owen" },
                    { new Guid("0ebfd788-29e0-4d23-91a7-0c2110b0ef18"), "Eric Dane" },
                    { new Guid("ec07de00-8e23-48dd-8420-c3da8d459e6a"), "George MacKay" },
                    { new Guid("0a99b6a9-9f80-441d-a24a-0c965a97b9fc"), "Geraldine Chaplin" },
                    { new Guid("d48071ba-63ed-4959-90dd-34f3d55deeb2"), "Jeff Garlin" },
                    { new Guid("ec4b6c2e-43d2-4ef2-a0f2-a74e7f5d3cfc"), "Jennifer Aniston" },
                    { new Guid("7bbc72ec-db21-4886-9c24-cf4960483ad2"), "Elissa Knight" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("1c6acce1-6695-48e4-90b8-7efbf4557682"), "Andrew Stanton" },
                    { new Guid("e23f169b-5c33-490a-aba8-51b5b289d3fb"), "Stanley Kubrick" },
                    { new Guid("51d2dcd3-5ebd-4307-8536-6e1b08246c57"), "Oliver Stone" },
                    { new Guid("ed943494-7105-4e40-971e-ef4973e70e52"), "Guy Ritchie" },
                    { new Guid("387b08d1-e0bc-487b-b016-29f08767ac53"), "David Frankel" },
                    { new Guid("e3f4a565-271e-4967-8201-7dc4e4ac7904"), "Sam Mendes" },
                    { new Guid("6b80d348-b569-4d02-9c91-8fda2bf8c55b"), "Joe Russo" },
                    { new Guid("45388609-13fd-4e09-b9cb-818da292724a"), "Richard Attenborough" },
                    { new Guid("77ba7cd2-a67b-4768-b0db-5a316847c0dd"), "Lilly Wachowski" },
                    { new Guid("d70aa89f-5001-44c9-957a-5c2ed373dbde"), "Lana Wachowski" },
                    { new Guid("72aa2d3e-9c1a-4d0e-a5dd-580e647bb0d6"), "Jan Pinkava" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("08b60d6e-98b7-484f-9ae5-ffcd6311cf04"), "Brad Bird" },
                    { new Guid("bf54f3fd-fa55-41f1-9912-1a92a186b9de"), "Robert Rodriguez" },
                    { new Guid("b41c440f-35b2-4357-9cb9-035e74f37f22"), "Anthony Russo" },
                    { new Guid("ae5c0f64-6325-4ba1-8c87-bff4fc4e0e31"), "Quentin Tarantino" },
                    { new Guid("52c80c68-719e-4563-ae2c-973ed5c21819"), "Frank Miller" }
                });

            migrationBuilder.InsertData(
                table: "Filmes",
                columns: new[] { "IdFilme", "Ano", "Nome", "NomeOriginal", "Sinopse" },
                values: new object[,]
                {
                    { new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa"), 1999, "Matrix", "The Matrix", "Um hacker aprende com os misteriosos rebeldes sobre a verdadeira natureza de sua realidade e seu papel na guerra contra seus controladores." },
                    { new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa"), 2007, "Ratatouille", "Ratatouille", "Um rato que pode cozinhar forja uma aliança incomum com um jovem garoto em um famoso restaurante em Paris." },
                    { new Guid("625d8851-401c-46dc-9447-a01cb30a5ead"), 2008, "Marley & Eu", "Marley & Me", "Uma família aprende importantes lições de vida com seu adorável, mas malicioso e neurótico cão." },
                    { new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f"), 2008, "WALL·E", "WALL·E", "Num futuro distante, um pequeno robô faz uma viagem que decidirá o destino da humanidade." },
                    { new Guid("3f252108-f831-49a1-9ef1-028d33fb4322"), 2019, "Vingadores: Ultimato", "Avengers: Endgame", "Após os eventos devastadores de Vingadores: Guerra Infinita , o universo está em ruínas, e com a ajuda de aliados os Vingadores se reúnem para desfazer as ações de Thanos e restaurar a ordem." },
                    { new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2"), 2005, "Sin City: A Cidade do Pecado", "Sin City", "O filme explora a miserável cidade de Basin City, e conta a historia de três pessoas diferentes, envoltas em violencia e corrupção." },
                    { new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01"), 1994, "Assassinos por Natureza", "Natural Born Killers", "Duas vítimas de uma infância traumatizada tornam-se amantes e assassinos em série, irresponsavelmente glorificados pela mídia." },
                    { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), 1992, "Chaplin", "Chaplin", "Relata a vida convulsiva e controversa do mestre diretor de comédia Charles Chaplin." },
                    { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), 2009, "Sherlock Holmes", "Sherlock Holmes", "O detective Sherlock Holmes e seu colega Watson se engajam numa batalha para lutar contra as ameaças da Inglaterra." },
                    { new Guid("26dda459-1d42-48b3-b83a-2e0e02019133"), 1971, "Laranja Mecânica", "A Clockwork Orange", "No futuro, um líder duma turma é levado para prisão é decide ser voluntário dum experimento, mais os resultados não são os esperados." },
                    { new Guid("ca2c22b2-10e3-401f-bf95-430038adea53"), 2019, "1917", "1917", "6 de Abril de 1917. Enquanto um regimento se reúne para lutar uma guerra nas profundezas do território inimigo, dois soldados são designados para uma corrida contra o tempo para entregar uma mensagem que impedirá 1.600 homens de caminhar direto para uma uma armadilha mortal." }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("e79de677-184e-472e-a24c-5a941c382c8e"), "Fantasia científica" },
                    { new Guid("c90062c6-28fd-4dab-b1f0-b71ef6059b44"), "Ficção científica" },
                    { new Guid("d8dec4da-84fa-488f-92e4-1e348380ac02"), "Filmes com truques" },
                    { new Guid("5d4c2e02-e3d6-4c86-a6ad-d4a7d528ae71"), "Filmes de guerra" },
                    { new Guid("8ca92b70-eaf2-4154-a24b-e92a8fd3de54"), "Musical" },
                    { new Guid("649f3997-daae-4d55-8104-9612c438863a"), "Pornográfico" },
                    { new Guid("7000a51f-ec26-4e1d-8ae0-60d2457fce09"), "Romance" },
                    { new Guid("f7d56204-e8a8-4829-a22d-e0dc1eb11d69"), "Seriado" },
                    { new Guid("60a4c7fc-5e89-4988-8450-b032269e17ab"), "Suspense" },
                    { new Guid("86ad15b0-1e3d-4c2e-8da1-399c8acb664e"), "Terror" },
                    { new Guid("69daea01-dd87-496b-b15a-b62a703594b1"), "Thriller" },
                    { new Guid("64353719-4fa7-4b08-870e-c1eab95d4ab0"), "Fantasia" },
                    { new Guid("c40d1ada-fb57-4b41-a395-ea7ca451f889"), "Filme policial" },
                    { new Guid("a174c834-ed44-4d0e-9e70-c8be475c991d"), "Faroeste" },
                    { new Guid("2cccf882-2683-4062-9a8b-395469d95e5f"), "Comédia de ação" },
                    { new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5"), "Drama" },
                    { new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b"), "Ação" },
                    { new Guid("1a789339-0682-4595-baff-4cd7e534cd7d"), "Animação" },
                    { new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799"), "Aventura" },
                    { new Guid("33953b22-4a32-46af-a0c1-ec7cad8934c2"), "Cinema de arte" },
                    { new Guid("34865546-1100-4973-b543-ea7e8d5f3f09"), "Espionagem" },
                    { new Guid("a7401deb-0954-429a-b5a1-364ec38737fa"), "Comédia" },
                    { new Guid("3173fd27-ba23-4ead-b087-820e07e66206"), "Chanchada" },
                    { new Guid("e16d2eba-6198-43b4-af20-c0a3ec7f8b8b"), "Comédia dramática" },
                    { new Guid("4eeb70ef-24a3-4ce9-a580-83c0b786405e"), "Comédia romântica" },
                    { new Guid("469505b4-44d5-4967-bca8-e224198360eb"), "Dança" }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("31d16437-f8db-43b8-ab00-7ef00c9675bb"), "Documentário" },
                    { new Guid("f40787f9-d2ab-4377-afae-3230cf67a5e1"), "Docuficção" },
                    { new Guid("59099e92-f18d-4163-bdcc-a1bd95837874"), "Comédia de terror" }
                });

            migrationBuilder.InsertData(
                table: "IdentityRole",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "4947910d-7c8c-4815-ba7e-f90b043d430d", "f02513c9-61d6-4562-9f51-2a6f5ffed5ee", "Admin", "ADMIN" },
                    { "267848db-d7a6-4d8d-860c-5859c94fd0bf", "b7825247-861e-458e-a351-79e5b60f8e61", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "Usuarios",
                columns: new[] { "IdUsuario", "Email", "Nome", "Senha" },
                values: new object[] { new Guid("5d3efd6d-568b-4cdd-9ae4-56dbcf30e7da"), "rafael.av@gmail.com", "Rafael Vasconcelos", "AQAAAAEAACcQAAAAEBNIfT2fSm4sayKMkwEL0Q3Fl9gxF/c7DCEJnCmRyZ78R6QDdXRMveAjm+4Y3SwaKw==" });

            migrationBuilder.InsertData(
                table: "AtoresFilmes",
                columns: new[] { "IdAtor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("b48d0f39-576b-4ef4-a941-effa0ad8b81d"), new Guid("26dda459-1d42-48b3-b83a-2e0e02019133") },
                    { new Guid("2b651023-7ea2-4a62-b36a-7cc40e5d056a"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") },
                    { new Guid("2ed6e10d-54a8-41c0-9037-5d6c8cead46c"), new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f") },
                    { new Guid("7bbc72ec-db21-4886-9c24-cf4960483ad2"), new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f") },
                    { new Guid("d48071ba-63ed-4959-90dd-34f3d55deeb2"), new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f") },
                    { new Guid("a0c809b9-c89f-4059-aec7-d1fc1fc7b878"), new Guid("625d8851-401c-46dc-9447-a01cb30a5ead") },
                    { new Guid("ec4b6c2e-43d2-4ef2-a0f2-a74e7f5d3cfc"), new Guid("625d8851-401c-46dc-9447-a01cb30a5ead") },
                    { new Guid("0ebfd788-29e0-4d23-91a7-0c2110b0ef18"), new Guid("625d8851-401c-46dc-9447-a01cb30a5ead") },
                    { new Guid("a41123b6-7c3e-4872-884c-25ac31fcfa4c"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") },
                    { new Guid("ee673f00-40cf-499f-aad5-2f53dd7a50df"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") },
                    { new Guid("dd017454-996f-452e-b6a5-641f6b593725"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") },
                    { new Guid("aa227b12-10c6-4aea-ba6a-3e98d303c51b"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") },
                    { new Guid("66386afe-613f-418f-b8c2-1234ab294a0b"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") },
                    { new Guid("dfc051c0-5e39-4e9f-86fa-38732544acad"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") },
                    { new Guid("3a5225f5-c1e3-485f-9457-2122a51e5617"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") },
                    { new Guid("76ade2a6-14a6-425e-9637-f7b8acefa39b"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") },
                    { new Guid("248c2aab-c979-4667-b3dc-b7b59382ee29"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") },
                    { new Guid("ae68d810-a802-4969-b556-85dc5e081fa1"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") },
                    { new Guid("269afa8d-1c6d-4047-86c7-d00351e45bec"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") },
                    { new Guid("4aed69f8-ceb3-49da-b742-a2907cedfe20"), new Guid("26dda459-1d42-48b3-b83a-2e0e02019133") },
                    { new Guid("cb38f7ed-7670-44ca-937f-231d97dd569c"), new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036") },
                    { new Guid("ae68d810-a802-4969-b556-85dc5e081fa1"), new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7") },
                    { new Guid("0a99b6a9-9f80-441d-a24a-0c965a97b9fc"), new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7") },
                    { new Guid("c51cd834-0cb8-4c1c-accb-d923bea52bb9"), new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7") },
                    { new Guid("bf27c15a-5c90-475f-9dfe-3cbdd73b4ea7"), new Guid("26dda459-1d42-48b3-b83a-2e0e02019133") },
                    { new Guid("ae68d810-a802-4969-b556-85dc5e081fa1"), new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036") },
                    { new Guid("31e41a70-0ce1-44f0-8504-272c0420745b"), new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01") },
                    { new Guid("27370bd8-a38e-4d85-9394-7c75d090e55c"), new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036") },
                    { new Guid("386295f4-3bb3-4256-9671-15d119ff4377"), new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01") },
                    { new Guid("c4473d09-3ae2-424a-b060-6b3fe5fb8140"), new Guid("ca2c22b2-10e3-401f-bf95-430038adea53") },
                    { new Guid("288760ec-c547-4087-93b6-98fffefd645e"), new Guid("ca2c22b2-10e3-401f-bf95-430038adea53") },
                    { new Guid("ec07de00-8e23-48dd-8420-c3da8d459e6a"), new Guid("ca2c22b2-10e3-401f-bf95-430038adea53") },
                    { new Guid("b6277aed-7761-49e6-9249-d77eb71b2ffa"), new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("d70aa89f-5001-44c9-957a-5c2ed373dbde"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") },
                    { new Guid("77ba7cd2-a67b-4768-b0db-5a316847c0dd"), new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa") },
                    { new Guid("45388609-13fd-4e09-b9cb-818da292724a"), new Guid("26dda459-1d42-48b3-b83a-2e0e02019133") },
                    { new Guid("72aa2d3e-9c1a-4d0e-a5dd-580e647bb0d6"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") },
                    { new Guid("08b60d6e-98b7-484f-9ae5-ffcd6311cf04"), new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa") },
                    { new Guid("e3f4a565-271e-4967-8201-7dc4e4ac7904"), new Guid("ca2c22b2-10e3-401f-bf95-430038adea53") },
                    { new Guid("52c80c68-719e-4563-ae2c-973ed5c21819"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") },
                    { new Guid("ed943494-7105-4e40-971e-ef4973e70e52"), new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036") },
                    { new Guid("387b08d1-e0bc-487b-b016-29f08767ac53"), new Guid("625d8851-401c-46dc-9447-a01cb30a5ead") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("45388609-13fd-4e09-b9cb-818da292724a"), new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7") },
                    { new Guid("1c6acce1-6695-48e4-90b8-7efbf4557682"), new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f") },
                    { new Guid("6b80d348-b569-4d02-9c91-8fda2bf8c55b"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") },
                    { new Guid("b41c440f-35b2-4357-9cb9-035e74f37f22"), new Guid("3f252108-f831-49a1-9ef1-028d33fb4322") },
                    { new Guid("51d2dcd3-5ebd-4307-8536-6e1b08246c57"), new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01") },
                    { new Guid("ae5c0f64-6325-4ba1-8c87-bff4fc4e0e31"), new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2") }
                });

            migrationBuilder.InsertData(
                table: "GenerosFilmes",
                columns: new[] { "IdFilme", "IdGenero" },
                values: new object[,]
                {
                    { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") },
                    { new Guid("625d8851-401c-46dc-9447-a01cb30a5ead"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") },
                    { new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") },
                    { new Guid("ca2c22b2-10e3-401f-bf95-430038adea53"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") },
                    { new Guid("3f252108-f831-49a1-9ef1-028d33fb4322"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") },
                    { new Guid("26dda459-1d42-48b3-b83a-2e0e02019133"), new Guid("c90062c6-28fd-4dab-b1f0-b71ef6059b44") },
                    { new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2"), new Guid("69daea01-dd87-496b-b15a-b62a703594b1") },
                    { new Guid("ca2c22b2-10e3-401f-bf95-430038adea53"), new Guid("5d4c2e02-e3d6-4c86-a6ad-d4a7d528ae71") },
                    { new Guid("26dda459-1d42-48b3-b83a-2e0e02019133"), new Guid("c40d1ada-fb57-4b41-a395-ea7ca451f889") },
                    { new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01"), new Guid("c40d1ada-fb57-4b41-a395-ea7ca451f889") },
                    { new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2"), new Guid("c40d1ada-fb57-4b41-a395-ea7ca451f889") },
                    { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), new Guid("60a4c7fc-5e89-4988-8450-b032269e17ab") },
                    { new Guid("26dda459-1d42-48b3-b83a-2e0e02019133"), new Guid("dc2e3b05-4a6f-4212-9d98-1d0515ffa6b5") },
                    { new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa"), new Guid("c90062c6-28fd-4dab-b1f0-b71ef6059b44") },
                    { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), new Guid("f40787f9-d2ab-4377-afae-3230cf67a5e1") },
                    { new Guid("5d397ec2-ac35-49ac-ba8f-97a493fded01"), new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b") },
                    { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), new Guid("e16d2eba-6198-43b4-af20-c0a3ec7f8b8b") },
                    { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b") },
                    { new Guid("3f252108-f831-49a1-9ef1-028d33fb4322"), new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b") },
                    { new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa"), new Guid("edffea43-efbf-4616-a8e9-b0103d123c2b") },
                    { new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f"), new Guid("1a789339-0682-4595-baff-4cd7e534cd7d") },
                    { new Guid("625d8851-401c-46dc-9447-a01cb30a5ead"), new Guid("e16d2eba-6198-43b4-af20-c0a3ec7f8b8b") },
                    { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799") },
                    { new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa"), new Guid("1a789339-0682-4595-baff-4cd7e534cd7d") },
                    { new Guid("9bda3e2f-f8db-449a-a790-4d6ac61dfd4f"), new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799") },
                    { new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa"), new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799") },
                    { new Guid("b79b081a-7f1f-4c83-8202-7bb5fbfd42f7"), new Guid("a7401deb-0954-429a-b5a1-364ec38737fa") },
                    { new Guid("625d8851-401c-46dc-9447-a01cb30a5ead"), new Guid("a7401deb-0954-429a-b5a1-364ec38737fa") },
                    { new Guid("1764d8d9-4b26-4788-bc52-07c9b5a37afa"), new Guid("a7401deb-0954-429a-b5a1-364ec38737fa") },
                    { new Guid("3f252108-f831-49a1-9ef1-028d33fb4322"), new Guid("dcd37d27-3d4e-4185-a08c-f4f9cd9ca799") }
                });

            migrationBuilder.InsertData(
                table: "Votos",
                columns: new[] { "IdFilme", "IdUsuario", "Nota" },
                values: new object[,]
                {
                    { new Guid("6827813e-a5df-452f-bc0a-cf90c664b2c2"), new Guid("5d3efd6d-568b-4cdd-9ae4-56dbcf30e7da"), (byte)3 },
                    { new Guid("2acc14f0-dc0c-4520-b1f0-628073d9c036"), new Guid("5d3efd6d-568b-4cdd-9ae4-56dbcf30e7da"), (byte)1 },
                    { new Guid("fdf4a0c9-efd4-4350-8842-5b0fc4554baa"), new Guid("5d3efd6d-568b-4cdd-9ae4-56dbcf30e7da"), (byte)4 }
                });
        }
    }
}
