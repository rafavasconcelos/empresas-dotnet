﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Empresas.Migrations
{
    public partial class InclusaoIdentityRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("05b4ec1e-d8c4-4617-a1a2-f1efe95b2af1"), new Guid("2b055393-65e7-4281-ad25-600c7088b92c") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("06583263-1492-43f1-859f-418c72790c9e"), new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("0f4941c1-041c-42ed-8a01-ef16ccc6adde"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("10614747-876a-47ee-95cc-35b281100e8a"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("1ecba33a-ba16-4860-8186-4bb00c3c767b"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("2b5b6a7e-3066-4a98-a2ac-be8bd4670d4f"), new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("308f622c-48d4-48b3-9b84-d48404c7b08d"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("3a222dd1-8415-471e-80a6-1a81bf49cc90"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("3f3e3604-01c1-44a9-a271-ba53e2e4cda8"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("4334470e-b338-497c-b753-9a345f87702c"), new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("45730ff4-5c5c-45ac-947b-115d7bb092ab"), new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("54449faf-ea07-43f6-87b7-e2ec4bbee3bf"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("72d6dcc8-264d-4d4d-b2f0-b437c8087655"), new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("758f6cbf-4753-4a51-9144-5aa6bdf11cfd"), new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("759daf80-44a0-4311-9ca7-d4c585296259"), new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("76b40c81-1ea3-4cc9-b394-9ed72b6bbcd1"), new Guid("2b055393-65e7-4281-ad25-600c7088b92c") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("7d03f636-8636-43a2-af9c-67155f10dbcf"), new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("811aad9a-eded-42cd-abbd-a22cfa8f9776"), new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("8639f5b4-09d8-49bb-974d-0d6d22129fe6"), new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a897fb55-69d9-4f9b-8e5d-40ad53426b8b"), new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a90e3757-17bf-41cb-9619-3fa13f90d875"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ade046bc-73d2-49e7-956d-dea8db8b5f94"), new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ade046bc-73d2-49e7-956d-dea8db8b5f94"), new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ade046bc-73d2-49e7-956d-dea8db8b5f94"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("b044ee80-b3c1-4764-b1e7-b7a7eecc7438"), new Guid("2b055393-65e7-4281-ad25-600c7088b92c") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("b0746c87-c8c7-4db3-8743-fa73aaa11805"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("b19adefc-b57f-4209-8483-11f1d1266041"), new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("be591ae3-876e-42ca-8d1a-e40061a2448f"), new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("df35e6de-321a-4a69-9f66-5f4318fbfb4f"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("e03a677c-d3b3-42b9-951d-40f65d3c6797"), new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("e0e7af4b-1983-46b6-ac60-07c91dfa5d06"), new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("e3460c95-e00a-472e-a1a6-57941c36b97f"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("fb0bb621-b76c-4884-bf9d-85ad5f3d520b"), new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82") });

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("3be9f148-7478-4882-94b1-3fb926189ba6"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("81acc4b0-fd9e-4d8e-be6d-6ffc0afe455c"));

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("12dfced1-4146-4283-997c-534245ca931f"), new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("37fdab7b-3300-4804-adf8-b7c9ab31cf64"), new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("380e6cb6-86c6-4e61-92b5-ad853b9d5988"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("39c9671e-af5b-4ad7-a2ba-fb721329f6d8"), new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("39c9671e-af5b-4ad7-a2ba-fb721329f6d8"), new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("3a7f5e18-b70b-4923-890a-d814390a0a06"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("41e6d7fd-5455-46ae-ad6c-c8250d623fd4"), new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("5f15482d-8678-4355-8945-9ca7d7498f8d"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("89749aa9-b7d1-47c3-95a9-d6397d888a13"), new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("8d908885-685e-4ff9-b7b2-6312308cfbe5"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("a2d0e666-f861-4156-bd66-86b3f699b234"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("c75ebbe0-0850-4728-ac6f-4472629ced4a"), new Guid("2b055393-65e7-4281-ad25-600c7088b92c") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("ca777839-d78c-493c-96de-d2df2159392a"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("dfd9fd54-00d8-43bd-8b3f-eda79a4c11e3"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("e038435e-5897-4cc8-9a7f-334a319f2c1a"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") });

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("01f94f63-f783-40c7-a208-ce468b51ee15"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("11d429fe-089e-44cf-8c79-341be8900652"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("1466ebb0-dc4e-47c7-b102-d7fc2bbf2566"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("20df64e2-8d52-4b4e-9b29-378970e155da"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("321125a6-a002-4a8f-9642-69b8a1dcb7f1"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("42984352-8d72-4501-be05-cff35c593f3d"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("53883f8f-b050-4ce7-b8f4-e617bde5420a"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("54598646-6189-471b-a68d-7e0b7f50c4e8"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("70dcd2da-cf3b-4c3a-9201-3bb43a17de60"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7b4923f2-e897-4f32-b841-61289d2d79fc"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("81b1249c-b5d1-436d-935a-64c1db2ed784"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("96e1c824-8547-4991-abf7-1ddbafb8d0a9"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("a6b59ac1-b467-4180-a174-a709fbd1cd14"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("c909bdb4-da32-414a-96a1-653d573f72da"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("de24b604-13c4-4619-ad0c-35f976c8658c"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("ef824e8f-2d6a-4d61-9cdc-33df7bb339a5"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f30bb750-48b7-484d-8a39-5ad60a341e68"));

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc"), new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351"), new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932"), new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), new Guid("2eadc815-7285-4000-b347-5b27d1326aba") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c"), new Guid("7bd6265a-f7a0-47db-a0f0-925ebaac1794") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9"), new Guid("86feff1b-2aef-4825-8103-2252bc4be055") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc"), new Guid("86feff1b-2aef-4825-8103-2252bc4be055") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8"), new Guid("86feff1b-2aef-4825-8103-2252bc4be055") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), new Guid("8ad0221c-4a4f-41c2-a854-49812b0eb65d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8"), new Guid("9c7f5535-6b85-4a46-bbb8-1ad9f38af897") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932"), new Guid("9c7f5535-6b85-4a46-bbb8-1ad9f38af897") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd"), new Guid("ad6ab100-12e6-4d67-bebf-016a2f1e6a83") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82"), new Guid("ad6ab100-12e6-4d67-bebf-016a2f1e6a83") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2b055393-65e7-4281-ad25-600c7088b92c"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2b055393-65e7-4281-ad25-600c7088b92c"), new Guid("c27fcae0-3000-4ba7-bc97-a551b81dda8f") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), new Guid("c27fcae0-3000-4ba7-bc97-a551b81dda8f") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd"), new Guid("c27fcae0-3000-4ba7-bc97-a551b81dda8f") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), new Guid("d1e85fda-984f-4911-9899-70fea899257d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351"), new Guid("d1e85fda-984f-4911-9899-70fea899257d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd"), new Guid("d1e85fda-984f-4911-9899-70fea899257d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82"), new Guid("d1e85fda-984f-4911-9899-70fea899257d") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9"), new Guid("ed4ed995-911e-43c9-9b19-6b8ea624e836") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2b055393-65e7-4281-ad25-600c7088b92c"), new Guid("fd83ba94-6a8e-491a-9427-159059a8e186") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), new Guid("fd83ba94-6a8e-491a-9427-159059a8e186") });

            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "4b9b2bba-7bd0-43d5-819a-6274697c17bc");

            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "599ca093-6faa-44af-8d0f-5af415972f16");

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9"), new Guid("543471df-7f59-4ae9-bcb4-11abe34dfcea") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), new Guid("543471df-7f59-4ae9-bcb4-11abe34dfcea") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932"), new Guid("543471df-7f59-4ae9-bcb4-11abe34dfcea") });

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("05b4ec1e-d8c4-4617-a1a2-f1efe95b2af1"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("06583263-1492-43f1-859f-418c72790c9e"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("0f4941c1-041c-42ed-8a01-ef16ccc6adde"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("10614747-876a-47ee-95cc-35b281100e8a"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("1ecba33a-ba16-4860-8186-4bb00c3c767b"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("2b5b6a7e-3066-4a98-a2ac-be8bd4670d4f"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("308f622c-48d4-48b3-9b84-d48404c7b08d"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("3a222dd1-8415-471e-80a6-1a81bf49cc90"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("3f3e3604-01c1-44a9-a271-ba53e2e4cda8"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("4334470e-b338-497c-b753-9a345f87702c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("45730ff4-5c5c-45ac-947b-115d7bb092ab"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("54449faf-ea07-43f6-87b7-e2ec4bbee3bf"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("72d6dcc8-264d-4d4d-b2f0-b437c8087655"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("758f6cbf-4753-4a51-9144-5aa6bdf11cfd"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("759daf80-44a0-4311-9ca7-d4c585296259"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("76b40c81-1ea3-4cc9-b394-9ed72b6bbcd1"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("7d03f636-8636-43a2-af9c-67155f10dbcf"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("811aad9a-eded-42cd-abbd-a22cfa8f9776"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("8639f5b4-09d8-49bb-974d-0d6d22129fe6"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a897fb55-69d9-4f9b-8e5d-40ad53426b8b"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a90e3757-17bf-41cb-9619-3fa13f90d875"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ade046bc-73d2-49e7-956d-dea8db8b5f94"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("b044ee80-b3c1-4764-b1e7-b7a7eecc7438"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("b0746c87-c8c7-4db3-8743-fa73aaa11805"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("b19adefc-b57f-4209-8483-11f1d1266041"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("be591ae3-876e-42ca-8d1a-e40061a2448f"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("df35e6de-321a-4a69-9f66-5f4318fbfb4f"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("e03a677c-d3b3-42b9-951d-40f65d3c6797"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("e0e7af4b-1983-46b6-ac60-07c91dfa5d06"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("e3460c95-e00a-472e-a1a6-57941c36b97f"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("fb0bb621-b76c-4884-bf9d-85ad5f3d520b"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("12dfced1-4146-4283-997c-534245ca931f"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("37fdab7b-3300-4804-adf8-b7c9ab31cf64"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("380e6cb6-86c6-4e61-92b5-ad853b9d5988"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("39c9671e-af5b-4ad7-a2ba-fb721329f6d8"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("3a7f5e18-b70b-4923-890a-d814390a0a06"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("41e6d7fd-5455-46ae-ad6c-c8250d623fd4"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("5f15482d-8678-4355-8945-9ca7d7498f8d"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("89749aa9-b7d1-47c3-95a9-d6397d888a13"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("8d908885-685e-4ff9-b7b2-6312308cfbe5"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("a2d0e666-f861-4156-bd66-86b3f699b234"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("c75ebbe0-0850-4728-ac6f-4472629ced4a"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("ca777839-d78c-493c-96de-d2df2159392a"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("dfd9fd54-00d8-43bd-8b3f-eda79a4c11e3"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("e038435e-5897-4cc8-9a7f-334a319f2c1a"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("2b055393-65e7-4281-ad25-600c7088b92c"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("2eadc815-7285-4000-b347-5b27d1326aba"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7bd6265a-f7a0-47db-a0f0-925ebaac1794"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("86feff1b-2aef-4825-8103-2252bc4be055"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("8ad0221c-4a4f-41c2-a854-49812b0eb65d"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("9c7f5535-6b85-4a46-bbb8-1ad9f38af897"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("ad6ab100-12e6-4d67-bebf-016a2f1e6a83"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("c27fcae0-3000-4ba7-bc97-a551b81dda8f"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("d1e85fda-984f-4911-9899-70fea899257d"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("ed4ed995-911e-43c9-9b19-6b8ea624e836"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("fd83ba94-6a8e-491a-9427-159059a8e186"));

            migrationBuilder.DeleteData(
                table: "Usuarios",
                keyColumn: "IdUsuario",
                keyValue: new Guid("543471df-7f59-4ae9-bcb4-11abe34dfcea"));

            migrationBuilder.DropColumn(
                name: "Senha",
                table: "Usuarios");

            migrationBuilder.AddColumn<byte[]>(
                name: "HashSenha",
                table: "Usuarios",
                type: "varbinary(max)",
                nullable: false,
                defaultValue: new byte[0]);

            migrationBuilder.AddColumn<byte[]>(
                name: "SaltSenha",
                table: "Usuarios",
                type: "varbinary(max)",
                nullable: false,
                defaultValue: new byte[0]);

            migrationBuilder.InsertData(
                table: "Atores",
                columns: new[] { "IdAtor", "Nome" },
                values: new object[,]
                {
                    { new Guid("c3dbdd36-3883-4c92-9b2b-1a6d0157b371"), "Ben Burtt" },
                    { new Guid("959f3032-583a-4f4a-8af0-4dafa486a0d2"), "Woody Harrelson" },
                    { new Guid("cef61b6c-d17f-4516-aad4-3731f44f0dd5"), "Tom Sizemore" },
                    { new Guid("1d7b2e42-861e-46cf-b782-2663ac6b26ea"), "Robert Downey Jr." },
                    { new Guid("a05d47e7-0345-46dd-ac97-9dfb277927d9"), "Rachel McAdams" },
                    { new Guid("1b185a2b-1c1c-4c4e-a9ed-fe0af75e38f3"), "Paul Rhys" },
                    { new Guid("caae396f-6df8-40c3-986e-174683c42ce2"), "Patton Oswalt" },
                    { new Guid("3f47f108-b594-4603-b3be-29efe21f3eb6"), "Patrick Magee" },
                    { new Guid("ad89435d-216c-4176-bc58-dfba8b083297"), "Owen Wilson" },
                    { new Guid("850adc0f-dea4-40b7-8bb0-487a195c5a13"), "Mickey Rourke" },
                    { new Guid("a1328e16-bebf-4a68-8a8e-abf2ec560b61"), "Michael Bates" },
                    { new Guid("cb2b0816-fc44-4469-8af5-a22417ea57ca"), "Mark Ruffalo" },
                    { new Guid("4391d12e-4c16-4cab-a2db-8fb68f208f43"), "Malcolm McDowell" },
                    { new Guid("41e357f4-c626-497b-9e1f-3a928ac2b6c0"), "Laurence Fishburne" },
                    { new Guid("8bdb8b5d-591c-4a20-8ee6-7699c9e025eb"), "Keanu Reeves" },
                    { new Guid("67aa224d-587d-4c59-b417-af848d386389"), "Lou Romano" },
                    { new Guid("d13fe349-9ad9-40c4-9f99-084290d558a6"), "Jude Law" },
                    { new Guid("a7e3f202-23b8-415f-88f3-7db9a5ebf4aa"), "Brad Garrett" },
                    { new Guid("711fed8f-2780-4f6b-8224-7d2ad713fca5"), "Bruce Willis" },
                    { new Guid("19d86a9a-1d48-4377-92b2-887fd4272d5a"), "Carrie-Anne Moss" },
                    { new Guid("bd57c1a1-880f-4d6f-8e7a-be0fcc44c39d"), "Chris Evans" },
                    { new Guid("90573ce9-284d-422e-81a8-61eaa0d5b1d6"), "Juliette Lewis" },
                    { new Guid("425d1dbc-b714-41bf-aa83-c80f7b625ef2"), "Daniel Mays" },
                    { new Guid("ae508865-5ed7-4348-ab3c-8a7ba7fbf642"), "Dean-Charles Chapman" },
                    { new Guid("d7257033-435e-450d-85b1-e0ee7d2fa49d"), "Clive Owen" },
                    { new Guid("80e7e4f6-1826-4de0-8586-859d59e1e8f1"), "Eric Dane" },
                    { new Guid("5ac85176-bbcb-4c92-9a1d-a9eda2075c27"), "George MacKay" },
                    { new Guid("46b367d1-f3e6-42dc-ba92-d45e72e175f1"), "Geraldine Chaplin" },
                    { new Guid("57a3340e-2b19-4498-8376-c8858faee65b"), "Jeff Garlin" },
                    { new Guid("22fa8c08-9b3b-45ce-8dd3-bb1a7cf4ab80"), "Jennifer Aniston" },
                    { new Guid("be710d58-4487-4eb1-9d98-f87df6680abf"), "Elissa Knight" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("9279b751-61df-492d-a4ec-7101b69e5484"), "Andrew Stanton" },
                    { new Guid("625caa35-880c-4be8-9f69-34ae2231b7dc"), "Stanley Kubrick" },
                    { new Guid("56cc59f6-ced9-4e3b-8616-2869f56471b1"), "Oliver Stone" },
                    { new Guid("78d5ab9e-b8bc-40c2-ad00-fdc0a40df168"), "Guy Ritchie" },
                    { new Guid("f2840907-7326-4428-82f7-9a292c185b70"), "David Frankel" },
                    { new Guid("9f7d6454-9632-46d5-86e6-9277ae06cc18"), "Sam Mendes" },
                    { new Guid("48999599-a87f-4805-a79d-81c940d69f80"), "Joe Russo" },
                    { new Guid("52b687f1-3efd-4d65-a68d-910f27b3fc82"), "Richard Attenborough" },
                    { new Guid("8937215c-a99b-406f-861d-bb4318c5fbd7"), "Lilly Wachowski" },
                    { new Guid("18fefb92-933a-4306-9ded-652bb84ab7bf"), "Lana Wachowski" },
                    { new Guid("2fb5638e-83a1-4ef7-89c2-112c21325730"), "Jan Pinkava" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("7394701e-d08a-4276-9e46-520126182b18"), "Brad Bird" },
                    { new Guid("87d68089-9d9f-4a1e-b8e3-56b63a8a68cc"), "Robert Rodriguez" },
                    { new Guid("6ffe17a7-04d6-438b-8312-b678c7c7372c"), "Anthony Russo" },
                    { new Guid("824ee794-8952-4dda-b182-4ed26c2d6000"), "Quentin Tarantino" },
                    { new Guid("1a7d0f8d-51a6-43c0-948e-af475b278da8"), "Frank Miller" }
                });

            migrationBuilder.InsertData(
                table: "Filmes",
                columns: new[] { "IdFilme", "Ano", "Nome", "NomeOriginal", "Sinopse" },
                values: new object[,]
                {
                    { new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f"), 1999, "Matrix", "The Matrix", "Um hacker aprende com os misteriosos rebeldes sobre a verdadeira natureza de sua realidade e seu papel na guerra contra seus controladores." },
                    { new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8"), 2007, "Ratatouille", "Ratatouille", "Um rato que pode cozinhar forja uma aliança incomum com um jovem garoto em um famoso restaurante em Paris." },
                    { new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9"), 2008, "Marley & Eu", "Marley & Me", "Uma família aprende importantes lições de vida com seu adorável, mas malicioso e neurótico cão." },
                    { new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8"), 2008, "WALL·E", "WALL·E", "Num futuro distante, um pequeno robô faz uma viagem que decidirá o destino da humanidade." },
                    { new Guid("579a7f05-52a0-468f-8f65-880b96eee634"), 2019, "Vingadores: Ultimato", "Avengers: Endgame", "Após os eventos devastadores de Vingadores: Guerra Infinita , o universo está em ruínas, e com a ajuda de aliados os Vingadores se reúnem para desfazer as ações de Thanos e restaurar a ordem." },
                    { new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef"), 2005, "Sin City: A Cidade do Pecado", "Sin City", "O filme explora a miserável cidade de Basin City, e conta a historia de três pessoas diferentes, envoltas em violencia e corrupção." },
                    { new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6"), 1994, "Assassinos por Natureza", "Natural Born Killers", "Duas vítimas de uma infância traumatizada tornam-se amantes e assassinos em série, irresponsavelmente glorificados pela mídia." },
                    { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), 1992, "Chaplin", "Chaplin", "Relata a vida convulsiva e controversa do mestre diretor de comédia Charles Chaplin." },
                    { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), 2009, "Sherlock Holmes", "Sherlock Holmes", "O detective Sherlock Holmes e seu colega Watson se engajam numa batalha para lutar contra as ameaças da Inglaterra." },
                    { new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac"), 1971, "Laranja Mecânica", "A Clockwork Orange", "No futuro, um líder duma turma é levado para prisão é decide ser voluntário dum experimento, mais os resultados não são os esperados." },
                    { new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b"), 2019, "1917", "1917", "6 de Abril de 1917. Enquanto um regimento se reúne para lutar uma guerra nas profundezas do território inimigo, dois soldados são designados para uma corrida contra o tempo para entregar uma mensagem que impedirá 1.600 homens de caminhar direto para uma uma armadilha mortal." }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("bebf1b0c-7623-4fe5-a44c-749c10027b15"), "Fantasia científica" },
                    { new Guid("33f657c4-4852-4a70-9ba5-5abb9d86d685"), "Ficção científica" },
                    { new Guid("2fc7d867-54c7-4de9-bd6e-3c3927c85bb7"), "Filmes com truques" },
                    { new Guid("155eff2e-8452-4cae-84ca-2d4cfc2ffe0e"), "Filmes de guerra" },
                    { new Guid("7f98d99e-9ac8-4c39-ba96-9712d715b207"), "Musical" },
                    { new Guid("1c27db3d-0235-4f8b-965e-96dcb3b61022"), "Pornográfico" },
                    { new Guid("9fe2a5e6-b62f-45e6-88b1-a3a6387695bf"), "Romance" },
                    { new Guid("f095cc29-9a7b-4428-b79b-81f0c92baa68"), "Seriado" },
                    { new Guid("52a1cd09-b97d-48fe-8f22-35eb7aaff9cd"), "Suspense" },
                    { new Guid("53502d72-ac6d-4056-b978-e7246295ce75"), "Terror" },
                    { new Guid("ff4051ca-a8ae-4c93-bfda-1c269b059b11"), "Thriller" },
                    { new Guid("db6ae3f5-908d-4adc-90fb-286035889d13"), "Fantasia" },
                    { new Guid("fad75e01-bfee-4aa8-9221-14055c809b25"), "Filme policial" },
                    { new Guid("0ae399c4-4066-45ea-b2f2-2a27ddd82ba4"), "Faroeste" },
                    { new Guid("b6ec13a3-a47b-49fa-99ae-a33fe584a813"), "Comédia de ação" },
                    { new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de"), "Drama" },
                    { new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1"), "Ação" },
                    { new Guid("21a3251e-2d7c-42fd-a999-8328ebc70504"), "Animação" },
                    { new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4"), "Aventura" },
                    { new Guid("9917be90-d12e-4cf4-b6fd-c9b0b01f3e43"), "Cinema de arte" },
                    { new Guid("bf64dbb0-b3f5-47fd-b677-02479b206382"), "Espionagem" },
                    { new Guid("fae855e4-76fc-494a-b664-4ec68970d47e"), "Comédia" },
                    { new Guid("22fdb2b4-13c0-4749-8492-e2ed1718a156"), "Chanchada" },
                    { new Guid("8c75f953-6cc4-42ca-a281-06bd5dc4742b"), "Comédia dramática" },
                    { new Guid("cd83d03a-560a-4e09-b8e3-852651a92f7a"), "Comédia romântica" },
                    { new Guid("9c90db90-1179-4d43-9c97-e6065a2c9374"), "Dança" }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("da7b1bf9-7d9d-4116-9349-3bcfd88670ee"), "Documentário" },
                    { new Guid("41b135c6-3041-4bd7-9960-420721ce39a1"), "Docuficção" },
                    { new Guid("d0ce6295-d3fc-4079-be42-2f436f44f369"), "Comédia de terror" }
                });

            migrationBuilder.InsertData(
                table: "IdentityRole",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "c26c9aeb-9e36-4a80-a203-8ea029806770", "69d1e41b-76e5-4357-b833-69bbe9f56cbe", "Admin", "ADMIN" },
                    { "8c236128-42d1-4111-aee5-738875a1e90e", "805a6d80-71bf-4567-8c61-e5808f6c5191", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "Usuarios",
                columns: new[] { "IdUsuario", "Email", "HashSenha", "IdRole", "Nome", "SaltSenha" },
                values: new object[] { new Guid("dc6fce33-243e-4d4c-8328-1eda6867c3ea"), "rafael.av@gmail.com", new byte[] { 236, 184, 104, 81, 157, 230, 63, 48, 241, 97, 214, 66, 125, 133, 229, 206, 22, 218, 74, 116, 48, 168, 198, 210, 119, 178, 91, 27, 119, 118, 111, 72, 117, 76, 163, 9, 171, 38, 158, 191, 21, 45, 9, 10, 188, 34, 3, 8, 18, 226, 19, 141, 81, 83, 115, 57, 84, 129, 37, 45, 94, 76, 23, 141 }, new Guid("c26c9aeb-9e36-4a80-a203-8ea029806770"), "Rafael Vasconcelos", new byte[] { 10, 33, 223, 201, 237, 101, 78, 185, 226, 2, 37, 191, 170, 226, 151, 133, 0, 46, 117, 186, 154, 214, 221, 160, 120, 24, 83, 32, 166, 82, 239, 52, 248, 7, 52, 211, 240, 55, 18, 215, 104, 78, 27, 244, 71, 135, 53, 231, 14, 184, 34, 27, 84, 14, 216, 231, 35, 50, 109, 67, 87, 100, 11, 26, 119, 223, 89, 213, 253, 182, 16, 162, 153, 184, 3, 48, 64, 250, 127, 74, 151, 6, 100, 31, 184, 197, 120, 245, 8, 249, 197, 7, 150, 74, 184, 70, 17, 120, 84, 94, 228, 214, 221, 38, 219, 80, 247, 238, 208, 225, 81, 223, 223, 179, 40, 96, 170, 73, 165, 142, 233, 67, 22, 54, 24, 112, 73, 227 } });

            migrationBuilder.InsertData(
                table: "AtoresFilmes",
                columns: new[] { "IdAtor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("4391d12e-4c16-4cab-a2db-8fb68f208f43"), new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac") },
                    { new Guid("cb2b0816-fc44-4469-8af5-a22417ea57ca"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") },
                    { new Guid("c3dbdd36-3883-4c92-9b2b-1a6d0157b371"), new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8") },
                    { new Guid("be710d58-4487-4eb1-9d98-f87df6680abf"), new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8") },
                    { new Guid("57a3340e-2b19-4498-8376-c8858faee65b"), new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8") },
                    { new Guid("ad89435d-216c-4176-bc58-dfba8b083297"), new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9") },
                    { new Guid("22fa8c08-9b3b-45ce-8dd3-bb1a7cf4ab80"), new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9") },
                    { new Guid("80e7e4f6-1826-4de0-8586-859d59e1e8f1"), new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9") },
                    { new Guid("850adc0f-dea4-40b7-8bb0-487a195c5a13"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") },
                    { new Guid("d7257033-435e-450d-85b1-e0ee7d2fa49d"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") },
                    { new Guid("711fed8f-2780-4f6b-8224-7d2ad713fca5"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") },
                    { new Guid("a7e3f202-23b8-415f-88f3-7db9a5ebf4aa"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") },
                    { new Guid("67aa224d-587d-4c59-b417-af848d386389"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") },
                    { new Guid("8bdb8b5d-591c-4a20-8ee6-7699c9e025eb"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") },
                    { new Guid("41e357f4-c626-497b-9e1f-3a928ac2b6c0"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") },
                    { new Guid("19d86a9a-1d48-4377-92b2-887fd4272d5a"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") },
                    { new Guid("bd57c1a1-880f-4d6f-8e7a-be0fcc44c39d"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") },
                    { new Guid("1d7b2e42-861e-46cf-b782-2663ac6b26ea"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") },
                    { new Guid("caae396f-6df8-40c3-986e-174683c42ce2"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") },
                    { new Guid("3f47f108-b594-4603-b3be-29efe21f3eb6"), new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac") },
                    { new Guid("a05d47e7-0345-46dd-ac97-9dfb277927d9"), new Guid("439f33f5-1289-4030-9f3e-2ce25134c415") },
                    { new Guid("1d7b2e42-861e-46cf-b782-2663ac6b26ea"), new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a") },
                    { new Guid("46b367d1-f3e6-42dc-ba92-d45e72e175f1"), new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a") },
                    { new Guid("1b185a2b-1c1c-4c4e-a9ed-fe0af75e38f3"), new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a") },
                    { new Guid("a1328e16-bebf-4a68-8a8e-abf2ec560b61"), new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac") },
                    { new Guid("1d7b2e42-861e-46cf-b782-2663ac6b26ea"), new Guid("439f33f5-1289-4030-9f3e-2ce25134c415") },
                    { new Guid("959f3032-583a-4f4a-8af0-4dafa486a0d2"), new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6") },
                    { new Guid("d13fe349-9ad9-40c4-9f99-084290d558a6"), new Guid("439f33f5-1289-4030-9f3e-2ce25134c415") },
                    { new Guid("cef61b6c-d17f-4516-aad4-3731f44f0dd5"), new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6") },
                    { new Guid("425d1dbc-b714-41bf-aa83-c80f7b625ef2"), new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b") },
                    { new Guid("ae508865-5ed7-4348-ab3c-8a7ba7fbf642"), new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b") },
                    { new Guid("5ac85176-bbcb-4c92-9a1d-a9eda2075c27"), new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b") },
                    { new Guid("90573ce9-284d-422e-81a8-61eaa0d5b1d6"), new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("18fefb92-933a-4306-9ded-652bb84ab7bf"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") },
                    { new Guid("8937215c-a99b-406f-861d-bb4318c5fbd7"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") },
                    { new Guid("52b687f1-3efd-4d65-a68d-910f27b3fc82"), new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac") },
                    { new Guid("2fb5638e-83a1-4ef7-89c2-112c21325730"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") },
                    { new Guid("7394701e-d08a-4276-9e46-520126182b18"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") },
                    { new Guid("9f7d6454-9632-46d5-86e6-9277ae06cc18"), new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b") },
                    { new Guid("1a7d0f8d-51a6-43c0-948e-af475b278da8"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") },
                    { new Guid("78d5ab9e-b8bc-40c2-ad00-fdc0a40df168"), new Guid("439f33f5-1289-4030-9f3e-2ce25134c415") },
                    { new Guid("f2840907-7326-4428-82f7-9a292c185b70"), new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("52b687f1-3efd-4d65-a68d-910f27b3fc82"), new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a") },
                    { new Guid("9279b751-61df-492d-a4ec-7101b69e5484"), new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8") },
                    { new Guid("48999599-a87f-4805-a79d-81c940d69f80"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") },
                    { new Guid("6ffe17a7-04d6-438b-8312-b678c7c7372c"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") },
                    { new Guid("56cc59f6-ced9-4e3b-8616-2869f56471b1"), new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6") },
                    { new Guid("824ee794-8952-4dda-b182-4ed26c2d6000"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") }
                });

            migrationBuilder.InsertData(
                table: "GenerosFilmes",
                columns: new[] { "IdFilme", "IdGenero" },
                values: new object[,]
                {
                    { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") },
                    { new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") },
                    { new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") },
                    { new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") },
                    { new Guid("579a7f05-52a0-468f-8f65-880b96eee634"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") },
                    { new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac"), new Guid("33f657c4-4852-4a70-9ba5-5abb9d86d685") },
                    { new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef"), new Guid("ff4051ca-a8ae-4c93-bfda-1c269b059b11") },
                    { new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b"), new Guid("155eff2e-8452-4cae-84ca-2d4cfc2ffe0e") },
                    { new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac"), new Guid("fad75e01-bfee-4aa8-9221-14055c809b25") },
                    { new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6"), new Guid("fad75e01-bfee-4aa8-9221-14055c809b25") },
                    { new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef"), new Guid("fad75e01-bfee-4aa8-9221-14055c809b25") },
                    { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), new Guid("52a1cd09-b97d-48fe-8f22-35eb7aaff9cd") },
                    { new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") },
                    { new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f"), new Guid("33f657c4-4852-4a70-9ba5-5abb9d86d685") },
                    { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), new Guid("41b135c6-3041-4bd7-9960-420721ce39a1") },
                    { new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6"), new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1") },
                    { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), new Guid("8c75f953-6cc4-42ca-a281-06bd5dc4742b") },
                    { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1") },
                    { new Guid("579a7f05-52a0-468f-8f65-880b96eee634"), new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1") },
                    { new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f"), new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1") },
                    { new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8"), new Guid("21a3251e-2d7c-42fd-a999-8328ebc70504") },
                    { new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9"), new Guid("8c75f953-6cc4-42ca-a281-06bd5dc4742b") },
                    { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4") },
                    { new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8"), new Guid("21a3251e-2d7c-42fd-a999-8328ebc70504") },
                    { new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8"), new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4") },
                    { new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8"), new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4") },
                    { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), new Guid("fae855e4-76fc-494a-b664-4ec68970d47e") },
                    { new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9"), new Guid("fae855e4-76fc-494a-b664-4ec68970d47e") },
                    { new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8"), new Guid("fae855e4-76fc-494a-b664-4ec68970d47e") },
                    { new Guid("579a7f05-52a0-468f-8f65-880b96eee634"), new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4") }
                });

            migrationBuilder.InsertData(
                table: "Votos",
                columns: new[] { "IdFilme", "IdUsuario", "Nota" },
                values: new object[,]
                {
                    { new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef"), new Guid("dc6fce33-243e-4d4c-8328-1eda6867c3ea"), (byte)3 },
                    { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), new Guid("dc6fce33-243e-4d4c-8328-1eda6867c3ea"), (byte)1 },
                    { new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f"), new Guid("dc6fce33-243e-4d4c-8328-1eda6867c3ea"), (byte)4 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("19d86a9a-1d48-4377-92b2-887fd4272d5a"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("1b185a2b-1c1c-4c4e-a9ed-fe0af75e38f3"), new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("1d7b2e42-861e-46cf-b782-2663ac6b26ea"), new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("1d7b2e42-861e-46cf-b782-2663ac6b26ea"), new Guid("439f33f5-1289-4030-9f3e-2ce25134c415") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("1d7b2e42-861e-46cf-b782-2663ac6b26ea"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("22fa8c08-9b3b-45ce-8dd3-bb1a7cf4ab80"), new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("3f47f108-b594-4603-b3be-29efe21f3eb6"), new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("41e357f4-c626-497b-9e1f-3a928ac2b6c0"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("425d1dbc-b714-41bf-aa83-c80f7b625ef2"), new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("4391d12e-4c16-4cab-a2db-8fb68f208f43"), new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("46b367d1-f3e6-42dc-ba92-d45e72e175f1"), new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("57a3340e-2b19-4498-8376-c8858faee65b"), new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("5ac85176-bbcb-4c92-9a1d-a9eda2075c27"), new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("67aa224d-587d-4c59-b417-af848d386389"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("711fed8f-2780-4f6b-8224-7d2ad713fca5"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("80e7e4f6-1826-4de0-8586-859d59e1e8f1"), new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("850adc0f-dea4-40b7-8bb0-487a195c5a13"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("8bdb8b5d-591c-4a20-8ee6-7699c9e025eb"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("90573ce9-284d-422e-81a8-61eaa0d5b1d6"), new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("959f3032-583a-4f4a-8af0-4dafa486a0d2"), new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a05d47e7-0345-46dd-ac97-9dfb277927d9"), new Guid("439f33f5-1289-4030-9f3e-2ce25134c415") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a1328e16-bebf-4a68-8a8e-abf2ec560b61"), new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a7e3f202-23b8-415f-88f3-7db9a5ebf4aa"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ad89435d-216c-4176-bc58-dfba8b083297"), new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ae508865-5ed7-4348-ab3c-8a7ba7fbf642"), new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("bd57c1a1-880f-4d6f-8e7a-be0fcc44c39d"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("be710d58-4487-4eb1-9d98-f87df6680abf"), new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("c3dbdd36-3883-4c92-9b2b-1a6d0157b371"), new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("caae396f-6df8-40c3-986e-174683c42ce2"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("cb2b0816-fc44-4469-8af5-a22417ea57ca"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("cef61b6c-d17f-4516-aad4-3731f44f0dd5"), new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("d13fe349-9ad9-40c4-9f99-084290d558a6"), new Guid("439f33f5-1289-4030-9f3e-2ce25134c415") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("d7257033-435e-450d-85b1-e0ee7d2fa49d"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") });

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("625caa35-880c-4be8-9f69-34ae2231b7dc"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("87d68089-9d9f-4a1e-b8e3-56b63a8a68cc"));

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("18fefb92-933a-4306-9ded-652bb84ab7bf"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("1a7d0f8d-51a6-43c0-948e-af475b278da8"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("2fb5638e-83a1-4ef7-89c2-112c21325730"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("48999599-a87f-4805-a79d-81c940d69f80"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("52b687f1-3efd-4d65-a68d-910f27b3fc82"), new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("52b687f1-3efd-4d65-a68d-910f27b3fc82"), new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("56cc59f6-ced9-4e3b-8616-2869f56471b1"), new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("6ffe17a7-04d6-438b-8312-b678c7c7372c"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("7394701e-d08a-4276-9e46-520126182b18"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("78d5ab9e-b8bc-40c2-ad00-fdc0a40df168"), new Guid("439f33f5-1289-4030-9f3e-2ce25134c415") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("824ee794-8952-4dda-b182-4ed26c2d6000"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("8937215c-a99b-406f-861d-bb4318c5fbd7"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("9279b751-61df-492d-a4ec-7101b69e5484"), new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("9f7d6454-9632-46d5-86e6-9277ae06cc18"), new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("f2840907-7326-4428-82f7-9a292c185b70"), new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9") });

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("0ae399c4-4066-45ea-b2f2-2a27ddd82ba4"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("1c27db3d-0235-4f8b-965e-96dcb3b61022"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("22fdb2b4-13c0-4749-8492-e2ed1718a156"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("2fc7d867-54c7-4de9-bd6e-3c3927c85bb7"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("53502d72-ac6d-4056-b978-e7246295ce75"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7f98d99e-9ac8-4c39-ba96-9712d715b207"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("9917be90-d12e-4cf4-b6fd-c9b0b01f3e43"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("9c90db90-1179-4d43-9c97-e6065a2c9374"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("9fe2a5e6-b62f-45e6-88b1-a3a6387695bf"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("b6ec13a3-a47b-49fa-99ae-a33fe584a813"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("bebf1b0c-7623-4fe5-a44c-749c10027b15"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("bf64dbb0-b3f5-47fd-b677-02479b206382"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("cd83d03a-560a-4e09-b8e3-852651a92f7a"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("d0ce6295-d3fc-4079-be42-2f436f44f369"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("da7b1bf9-7d9d-4116-9349-3bcfd88670ee"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("db6ae3f5-908d-4adc-90fb-286035889d13"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f095cc29-9a7b-4428-b79b-81f0c92baa68"));

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b"), new Guid("155eff2e-8452-4cae-84ca-2d4cfc2ffe0e") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8"), new Guid("21a3251e-2d7c-42fd-a999-8328ebc70504") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8"), new Guid("21a3251e-2d7c-42fd-a999-8328ebc70504") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac"), new Guid("33f657c4-4852-4a70-9ba5-5abb9d86d685") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f"), new Guid("33f657c4-4852-4a70-9ba5-5abb9d86d685") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), new Guid("41b135c6-3041-4bd7-9960-420721ce39a1") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), new Guid("52a1cd09-b97d-48fe-8f22-35eb7aaff9cd") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("579a7f05-52a0-468f-8f65-880b96eee634"), new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6"), new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f"), new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8"), new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("579a7f05-52a0-468f-8f65-880b96eee634"), new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8"), new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), new Guid("8c75f953-6cc4-42ca-a281-06bd5dc4742b") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9"), new Guid("8c75f953-6cc4-42ca-a281-06bd5dc4742b") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("579a7f05-52a0-468f-8f65-880b96eee634"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef"), new Guid("fad75e01-bfee-4aa8-9221-14055c809b25") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6"), new Guid("fad75e01-bfee-4aa8-9221-14055c809b25") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac"), new Guid("fad75e01-bfee-4aa8-9221-14055c809b25") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), new Guid("fae855e4-76fc-494a-b664-4ec68970d47e") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8"), new Guid("fae855e4-76fc-494a-b664-4ec68970d47e") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9"), new Guid("fae855e4-76fc-494a-b664-4ec68970d47e") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef"), new Guid("ff4051ca-a8ae-4c93-bfda-1c269b059b11") });

            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "8c236128-42d1-4111-aee5-738875a1e90e");

            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "c26c9aeb-9e36-4a80-a203-8ea029806770");

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef"), new Guid("dc6fce33-243e-4d4c-8328-1eda6867c3ea") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), new Guid("dc6fce33-243e-4d4c-8328-1eda6867c3ea") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f"), new Guid("dc6fce33-243e-4d4c-8328-1eda6867c3ea") });

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("19d86a9a-1d48-4377-92b2-887fd4272d5a"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("1b185a2b-1c1c-4c4e-a9ed-fe0af75e38f3"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("1d7b2e42-861e-46cf-b782-2663ac6b26ea"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("22fa8c08-9b3b-45ce-8dd3-bb1a7cf4ab80"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("3f47f108-b594-4603-b3be-29efe21f3eb6"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("41e357f4-c626-497b-9e1f-3a928ac2b6c0"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("425d1dbc-b714-41bf-aa83-c80f7b625ef2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("4391d12e-4c16-4cab-a2db-8fb68f208f43"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("46b367d1-f3e6-42dc-ba92-d45e72e175f1"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("57a3340e-2b19-4498-8376-c8858faee65b"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("5ac85176-bbcb-4c92-9a1d-a9eda2075c27"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("67aa224d-587d-4c59-b417-af848d386389"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("711fed8f-2780-4f6b-8224-7d2ad713fca5"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("80e7e4f6-1826-4de0-8586-859d59e1e8f1"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("850adc0f-dea4-40b7-8bb0-487a195c5a13"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("8bdb8b5d-591c-4a20-8ee6-7699c9e025eb"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("90573ce9-284d-422e-81a8-61eaa0d5b1d6"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("959f3032-583a-4f4a-8af0-4dafa486a0d2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a05d47e7-0345-46dd-ac97-9dfb277927d9"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a1328e16-bebf-4a68-8a8e-abf2ec560b61"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a7e3f202-23b8-415f-88f3-7db9a5ebf4aa"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ad89435d-216c-4176-bc58-dfba8b083297"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ae508865-5ed7-4348-ab3c-8a7ba7fbf642"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("bd57c1a1-880f-4d6f-8e7a-be0fcc44c39d"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("be710d58-4487-4eb1-9d98-f87df6680abf"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("c3dbdd36-3883-4c92-9b2b-1a6d0157b371"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("caae396f-6df8-40c3-986e-174683c42ce2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("cb2b0816-fc44-4469-8af5-a22417ea57ca"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("cef61b6c-d17f-4516-aad4-3731f44f0dd5"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("d13fe349-9ad9-40c4-9f99-084290d558a6"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("d7257033-435e-450d-85b1-e0ee7d2fa49d"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("18fefb92-933a-4306-9ded-652bb84ab7bf"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("1a7d0f8d-51a6-43c0-948e-af475b278da8"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("2fb5638e-83a1-4ef7-89c2-112c21325730"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("48999599-a87f-4805-a79d-81c940d69f80"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("52b687f1-3efd-4d65-a68d-910f27b3fc82"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("56cc59f6-ced9-4e3b-8616-2869f56471b1"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("6ffe17a7-04d6-438b-8312-b678c7c7372c"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("7394701e-d08a-4276-9e46-520126182b18"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("78d5ab9e-b8bc-40c2-ad00-fdc0a40df168"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("824ee794-8952-4dda-b182-4ed26c2d6000"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("8937215c-a99b-406f-861d-bb4318c5fbd7"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("9279b751-61df-492d-a4ec-7101b69e5484"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("9f7d6454-9632-46d5-86e6-9277ae06cc18"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("f2840907-7326-4428-82f7-9a292c185b70"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("579a7f05-52a0-468f-8f65-880b96eee634"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("155eff2e-8452-4cae-84ca-2d4cfc2ffe0e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("21a3251e-2d7c-42fd-a999-8328ebc70504"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("33f657c4-4852-4a70-9ba5-5abb9d86d685"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("41b135c6-3041-4bd7-9960-420721ce39a1"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("52a1cd09-b97d-48fe-8f22-35eb7aaff9cd"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("8c75f953-6cc4-42ca-a281-06bd5dc4742b"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("fad75e01-bfee-4aa8-9221-14055c809b25"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("fae855e4-76fc-494a-b664-4ec68970d47e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("ff4051ca-a8ae-4c93-bfda-1c269b059b11"));

            migrationBuilder.DeleteData(
                table: "Usuarios",
                keyColumn: "IdUsuario",
                keyValue: new Guid("dc6fce33-243e-4d4c-8328-1eda6867c3ea"));

            migrationBuilder.DropColumn(
                name: "HashSenha",
                table: "Usuarios");

            migrationBuilder.DropColumn(
                name: "SaltSenha",
                table: "Usuarios");

            migrationBuilder.AddColumn<string>(
                name: "Senha",
                table: "Usuarios",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.InsertData(
                table: "Atores",
                columns: new[] { "IdAtor", "Nome" },
                values: new object[,]
                {
                    { new Guid("2b5b6a7e-3066-4a98-a2ac-be8bd4670d4f"), "Ben Burtt" },
                    { new Guid("e03a677c-d3b3-42b9-951d-40f65d3c6797"), "Woody Harrelson" },
                    { new Guid("4334470e-b338-497c-b753-9a345f87702c"), "Tom Sizemore" },
                    { new Guid("ade046bc-73d2-49e7-956d-dea8db8b5f94"), "Robert Downey Jr." },
                    { new Guid("811aad9a-eded-42cd-abbd-a22cfa8f9776"), "Rachel McAdams" },
                    { new Guid("45730ff4-5c5c-45ac-947b-115d7bb092ab"), "Paul Rhys" },
                    { new Guid("b0746c87-c8c7-4db3-8743-fa73aaa11805"), "Patton Oswalt" },
                    { new Guid("8639f5b4-09d8-49bb-974d-0d6d22129fe6"), "Patrick Magee" },
                    { new Guid("76b40c81-1ea3-4cc9-b394-9ed72b6bbcd1"), "Owen Wilson" },
                    { new Guid("1ecba33a-ba16-4860-8186-4bb00c3c767b"), "Mickey Rourke" },
                    { new Guid("759daf80-44a0-4311-9ca7-d4c585296259"), "Michael Bates" },
                    { new Guid("df35e6de-321a-4a69-9f66-5f4318fbfb4f"), "Mark Ruffalo" },
                    { new Guid("758f6cbf-4753-4a51-9144-5aa6bdf11cfd"), "Malcolm McDowell" },
                    { new Guid("54449faf-ea07-43f6-87b7-e2ec4bbee3bf"), "Laurence Fishburne" },
                    { new Guid("3a222dd1-8415-471e-80a6-1a81bf49cc90"), "Keanu Reeves" },
                    { new Guid("e3460c95-e00a-472e-a1a6-57941c36b97f"), "Lou Romano" },
                    { new Guid("be591ae3-876e-42ca-8d1a-e40061a2448f"), "Jude Law" },
                    { new Guid("0f4941c1-041c-42ed-8a01-ef16ccc6adde"), "Brad Garrett" },
                    { new Guid("a90e3757-17bf-41cb-9619-3fa13f90d875"), "Bruce Willis" },
                    { new Guid("3f3e3604-01c1-44a9-a271-ba53e2e4cda8"), "Carrie-Anne Moss" },
                    { new Guid("308f622c-48d4-48b3-9b84-d48404c7b08d"), "Chris Evans" },
                    { new Guid("7d03f636-8636-43a2-af9c-67155f10dbcf"), "Juliette Lewis" },
                    { new Guid("b19adefc-b57f-4209-8483-11f1d1266041"), "Daniel Mays" },
                    { new Guid("a897fb55-69d9-4f9b-8e5d-40ad53426b8b"), "Dean-Charles Chapman" },
                    { new Guid("10614747-876a-47ee-95cc-35b281100e8a"), "Clive Owen" },
                    { new Guid("b044ee80-b3c1-4764-b1e7-b7a7eecc7438"), "Eric Dane" },
                    { new Guid("06583263-1492-43f1-859f-418c72790c9e"), "George MacKay" },
                    { new Guid("e0e7af4b-1983-46b6-ac60-07c91dfa5d06"), "Geraldine Chaplin" },
                    { new Guid("72d6dcc8-264d-4d4d-b2f0-b437c8087655"), "Jeff Garlin" },
                    { new Guid("05b4ec1e-d8c4-4617-a1a2-f1efe95b2af1"), "Jennifer Aniston" },
                    { new Guid("fb0bb621-b76c-4884-bf9d-85ad5f3d520b"), "Elissa Knight" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("37fdab7b-3300-4804-adf8-b7c9ab31cf64"), "Andrew Stanton" },
                    { new Guid("3be9f148-7478-4882-94b1-3fb926189ba6"), "Stanley Kubrick" },
                    { new Guid("89749aa9-b7d1-47c3-95a9-d6397d888a13"), "Oliver Stone" },
                    { new Guid("41e6d7fd-5455-46ae-ad6c-c8250d623fd4"), "Guy Ritchie" },
                    { new Guid("c75ebbe0-0850-4728-ac6f-4472629ced4a"), "David Frankel" },
                    { new Guid("12dfced1-4146-4283-997c-534245ca931f"), "Sam Mendes" },
                    { new Guid("ca777839-d78c-493c-96de-d2df2159392a"), "Joe Russo" },
                    { new Guid("39c9671e-af5b-4ad7-a2ba-fb721329f6d8"), "Richard Attenborough" },
                    { new Guid("e038435e-5897-4cc8-9a7f-334a319f2c1a"), "Lilly Wachowski" },
                    { new Guid("8d908885-685e-4ff9-b7b2-6312308cfbe5"), "Lana Wachowski" },
                    { new Guid("380e6cb6-86c6-4e61-92b5-ad853b9d5988"), "Jan Pinkava" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("dfd9fd54-00d8-43bd-8b3f-eda79a4c11e3"), "Brad Bird" },
                    { new Guid("81acc4b0-fd9e-4d8e-be6d-6ffc0afe455c"), "Robert Rodriguez" },
                    { new Guid("3a7f5e18-b70b-4923-890a-d814390a0a06"), "Anthony Russo" },
                    { new Guid("5f15482d-8678-4355-8945-9ca7d7498f8d"), "Quentin Tarantino" },
                    { new Guid("a2d0e666-f861-4156-bd66-86b3f699b234"), "Frank Miller" }
                });

            migrationBuilder.InsertData(
                table: "Filmes",
                columns: new[] { "IdFilme", "Ano", "Nome", "NomeOriginal", "Sinopse" },
                values: new object[,]
                {
                    { new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932"), 1999, "Matrix", "The Matrix", "Um hacker aprende com os misteriosos rebeldes sobre a verdadeira natureza de sua realidade e seu papel na guerra contra seus controladores." },
                    { new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd"), 2007, "Ratatouille", "Ratatouille", "Um rato que pode cozinhar forja uma aliança incomum com um jovem garoto em um famoso restaurante em Paris." },
                    { new Guid("2b055393-65e7-4281-ad25-600c7088b92c"), 2008, "Marley & Eu", "Marley & Me", "Uma família aprende importantes lições de vida com seu adorável, mas malicioso e neurótico cão." },
                    { new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82"), 2008, "WALL·E", "WALL·E", "Num futuro distante, um pequeno robô faz uma viagem que decidirá o destino da humanidade." },
                    { new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351"), 2019, "Vingadores: Ultimato", "Avengers: Endgame", "Após os eventos devastadores de Vingadores: Guerra Infinita , o universo está em ruínas, e com a ajuda de aliados os Vingadores se reúnem para desfazer as ações de Thanos e restaurar a ordem." },
                    { new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9"), 2005, "Sin City: A Cidade do Pecado", "Sin City", "O filme explora a miserável cidade de Basin City, e conta a historia de três pessoas diferentes, envoltas em violencia e corrupção." },
                    { new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc"), 1994, "Assassinos por Natureza", "Natural Born Killers", "Duas vítimas de uma infância traumatizada tornam-se amantes e assassinos em série, irresponsavelmente glorificados pela mídia." },
                    { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), 1992, "Chaplin", "Chaplin", "Relata a vida convulsiva e controversa do mestre diretor de comédia Charles Chaplin." },
                    { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), 2009, "Sherlock Holmes", "Sherlock Holmes", "O detective Sherlock Holmes e seu colega Watson se engajam numa batalha para lutar contra as ameaças da Inglaterra." },
                    { new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8"), 1971, "Laranja Mecânica", "A Clockwork Orange", "No futuro, um líder duma turma é levado para prisão é decide ser voluntário dum experimento, mais os resultados não são os esperados." },
                    { new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c"), 2019, "1917", "1917", "6 de Abril de 1917. Enquanto um regimento se reúne para lutar uma guerra nas profundezas do território inimigo, dois soldados são designados para uma corrida contra o tempo para entregar uma mensagem que impedirá 1.600 homens de caminhar direto para uma uma armadilha mortal." }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("de24b604-13c4-4619-ad0c-35f976c8658c"), "Fantasia científica" },
                    { new Guid("9c7f5535-6b85-4a46-bbb8-1ad9f38af897"), "Ficção científica" },
                    { new Guid("f30bb750-48b7-484d-8a39-5ad60a341e68"), "Filmes com truques" },
                    { new Guid("7bd6265a-f7a0-47db-a0f0-925ebaac1794"), "Filmes de guerra" },
                    { new Guid("81b1249c-b5d1-436d-935a-64c1db2ed784"), "Musical" },
                    { new Guid("54598646-6189-471b-a68d-7e0b7f50c4e8"), "Pornográfico" },
                    { new Guid("1466ebb0-dc4e-47c7-b102-d7fc2bbf2566"), "Romance" },
                    { new Guid("96e1c824-8547-4991-abf7-1ddbafb8d0a9"), "Seriado" },
                    { new Guid("8ad0221c-4a4f-41c2-a854-49812b0eb65d"), "Suspense" },
                    { new Guid("70dcd2da-cf3b-4c3a-9201-3bb43a17de60"), "Terror" },
                    { new Guid("ed4ed995-911e-43c9-9b19-6b8ea624e836"), "Thriller" },
                    { new Guid("53883f8f-b050-4ce7-b8f4-e617bde5420a"), "Fantasia" },
                    { new Guid("86feff1b-2aef-4825-8103-2252bc4be055"), "Filme policial" },
                    { new Guid("42984352-8d72-4501-be05-cff35c593f3d"), "Faroeste" },
                    { new Guid("7b4923f2-e897-4f32-b841-61289d2d79fc"), "Comédia de ação" },
                    { new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447"), "Drama" },
                    { new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a"), "Ação" },
                    { new Guid("ad6ab100-12e6-4d67-bebf-016a2f1e6a83"), "Animação" },
                    { new Guid("d1e85fda-984f-4911-9899-70fea899257d"), "Aventura" },
                    { new Guid("ef824e8f-2d6a-4d61-9cdc-33df7bb339a5"), "Cinema de arte" },
                    { new Guid("01f94f63-f783-40c7-a208-ce468b51ee15"), "Espionagem" },
                    { new Guid("c27fcae0-3000-4ba7-bc97-a551b81dda8f"), "Comédia" },
                    { new Guid("c909bdb4-da32-414a-96a1-653d573f72da"), "Chanchada" },
                    { new Guid("fd83ba94-6a8e-491a-9427-159059a8e186"), "Comédia dramática" },
                    { new Guid("20df64e2-8d52-4b4e-9b29-378970e155da"), "Comédia romântica" },
                    { new Guid("a6b59ac1-b467-4180-a174-a709fbd1cd14"), "Dança" }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("11d429fe-089e-44cf-8c79-341be8900652"), "Documentário" },
                    { new Guid("2eadc815-7285-4000-b347-5b27d1326aba"), "Docuficção" },
                    { new Guid("321125a6-a002-4a8f-9642-69b8a1dcb7f1"), "Comédia de terror" }
                });

            migrationBuilder.InsertData(
                table: "IdentityRole",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "599ca093-6faa-44af-8d0f-5af415972f16", "519d6690-89ac-47f7-94cf-3303d27a7d87", "Admin", "ADMIN" },
                    { "4b9b2bba-7bd0-43d5-819a-6274697c17bc", "4256d337-67d5-4983-a20a-5e72005a9bfe", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "Usuarios",
                columns: new[] { "IdUsuario", "Email", "IdRole", "Nome", "Senha" },
                values: new object[] { new Guid("543471df-7f59-4ae9-bcb4-11abe34dfcea"), "rafael.av@gmail.com", new Guid("599ca093-6faa-44af-8d0f-5af415972f16"), "Rafael Vasconcelos", "AQAAAAEAACcQAAAAEIuphJx7gDQFwulqsequOhcLGdlUuqERbCNpPKYgcKkz49R0QiioayEkGHN/LNU7bg==" });

            migrationBuilder.InsertData(
                table: "AtoresFilmes",
                columns: new[] { "IdAtor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("758f6cbf-4753-4a51-9144-5aa6bdf11cfd"), new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8") },
                    { new Guid("df35e6de-321a-4a69-9f66-5f4318fbfb4f"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") },
                    { new Guid("2b5b6a7e-3066-4a98-a2ac-be8bd4670d4f"), new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82") },
                    { new Guid("fb0bb621-b76c-4884-bf9d-85ad5f3d520b"), new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82") },
                    { new Guid("72d6dcc8-264d-4d4d-b2f0-b437c8087655"), new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82") },
                    { new Guid("76b40c81-1ea3-4cc9-b394-9ed72b6bbcd1"), new Guid("2b055393-65e7-4281-ad25-600c7088b92c") },
                    { new Guid("05b4ec1e-d8c4-4617-a1a2-f1efe95b2af1"), new Guid("2b055393-65e7-4281-ad25-600c7088b92c") },
                    { new Guid("b044ee80-b3c1-4764-b1e7-b7a7eecc7438"), new Guid("2b055393-65e7-4281-ad25-600c7088b92c") },
                    { new Guid("1ecba33a-ba16-4860-8186-4bb00c3c767b"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") },
                    { new Guid("10614747-876a-47ee-95cc-35b281100e8a"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") },
                    { new Guid("a90e3757-17bf-41cb-9619-3fa13f90d875"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") },
                    { new Guid("0f4941c1-041c-42ed-8a01-ef16ccc6adde"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") },
                    { new Guid("e3460c95-e00a-472e-a1a6-57941c36b97f"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") },
                    { new Guid("3a222dd1-8415-471e-80a6-1a81bf49cc90"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") },
                    { new Guid("54449faf-ea07-43f6-87b7-e2ec4bbee3bf"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") },
                    { new Guid("3f3e3604-01c1-44a9-a271-ba53e2e4cda8"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") },
                    { new Guid("308f622c-48d4-48b3-9b84-d48404c7b08d"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") },
                    { new Guid("ade046bc-73d2-49e7-956d-dea8db8b5f94"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") },
                    { new Guid("b0746c87-c8c7-4db3-8743-fa73aaa11805"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") },
                    { new Guid("8639f5b4-09d8-49bb-974d-0d6d22129fe6"), new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8") },
                    { new Guid("811aad9a-eded-42cd-abbd-a22cfa8f9776"), new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56") },
                    { new Guid("ade046bc-73d2-49e7-956d-dea8db8b5f94"), new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8") },
                    { new Guid("e0e7af4b-1983-46b6-ac60-07c91dfa5d06"), new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8") },
                    { new Guid("45730ff4-5c5c-45ac-947b-115d7bb092ab"), new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8") },
                    { new Guid("759daf80-44a0-4311-9ca7-d4c585296259"), new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8") },
                    { new Guid("ade046bc-73d2-49e7-956d-dea8db8b5f94"), new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56") },
                    { new Guid("e03a677c-d3b3-42b9-951d-40f65d3c6797"), new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc") },
                    { new Guid("be591ae3-876e-42ca-8d1a-e40061a2448f"), new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56") },
                    { new Guid("4334470e-b338-497c-b753-9a345f87702c"), new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc") },
                    { new Guid("b19adefc-b57f-4209-8483-11f1d1266041"), new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c") },
                    { new Guid("a897fb55-69d9-4f9b-8e5d-40ad53426b8b"), new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c") },
                    { new Guid("06583263-1492-43f1-859f-418c72790c9e"), new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c") },
                    { new Guid("7d03f636-8636-43a2-af9c-67155f10dbcf"), new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("8d908885-685e-4ff9-b7b2-6312308cfbe5"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") },
                    { new Guid("e038435e-5897-4cc8-9a7f-334a319f2c1a"), new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932") },
                    { new Guid("39c9671e-af5b-4ad7-a2ba-fb721329f6d8"), new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8") },
                    { new Guid("380e6cb6-86c6-4e61-92b5-ad853b9d5988"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") },
                    { new Guid("dfd9fd54-00d8-43bd-8b3f-eda79a4c11e3"), new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd") },
                    { new Guid("12dfced1-4146-4283-997c-534245ca931f"), new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c") },
                    { new Guid("a2d0e666-f861-4156-bd66-86b3f699b234"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") },
                    { new Guid("41e6d7fd-5455-46ae-ad6c-c8250d623fd4"), new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56") },
                    { new Guid("c75ebbe0-0850-4728-ac6f-4472629ced4a"), new Guid("2b055393-65e7-4281-ad25-600c7088b92c") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("39c9671e-af5b-4ad7-a2ba-fb721329f6d8"), new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8") },
                    { new Guid("37fdab7b-3300-4804-adf8-b7c9ab31cf64"), new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82") },
                    { new Guid("ca777839-d78c-493c-96de-d2df2159392a"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") },
                    { new Guid("3a7f5e18-b70b-4923-890a-d814390a0a06"), new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351") },
                    { new Guid("89749aa9-b7d1-47c3-95a9-d6397d888a13"), new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc") },
                    { new Guid("5f15482d-8678-4355-8945-9ca7d7498f8d"), new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9") }
                });

            migrationBuilder.InsertData(
                table: "GenerosFilmes",
                columns: new[] { "IdFilme", "IdGenero" },
                values: new object[,]
                {
                    { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") },
                    { new Guid("2b055393-65e7-4281-ad25-600c7088b92c"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") },
                    { new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") },
                    { new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") },
                    { new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") },
                    { new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8"), new Guid("9c7f5535-6b85-4a46-bbb8-1ad9f38af897") },
                    { new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9"), new Guid("ed4ed995-911e-43c9-9b19-6b8ea624e836") },
                    { new Guid("848417f4-b742-40fa-83a0-bb8695dd9f0c"), new Guid("7bd6265a-f7a0-47db-a0f0-925ebaac1794") },
                    { new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8"), new Guid("86feff1b-2aef-4825-8103-2252bc4be055") },
                    { new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc"), new Guid("86feff1b-2aef-4825-8103-2252bc4be055") },
                    { new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9"), new Guid("86feff1b-2aef-4825-8103-2252bc4be055") },
                    { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), new Guid("8ad0221c-4a4f-41c2-a854-49812b0eb65d") },
                    { new Guid("a61ed39e-8d8a-43e8-ab3f-866ff11952a8"), new Guid("b44ab829-3e91-4a4f-b8be-ad80ea358447") },
                    { new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932"), new Guid("9c7f5535-6b85-4a46-bbb8-1ad9f38af897") },
                    { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), new Guid("2eadc815-7285-4000-b347-5b27d1326aba") },
                    { new Guid("6d24ef22-2a82-494d-9110-6f0a182ef8bc"), new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a") },
                    { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), new Guid("fd83ba94-6a8e-491a-9427-159059a8e186") },
                    { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a") },
                    { new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351"), new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a") },
                    { new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932"), new Guid("10cb689f-dcab-48a9-be15-4b691c49ca3a") },
                    { new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82"), new Guid("ad6ab100-12e6-4d67-bebf-016a2f1e6a83") },
                    { new Guid("2b055393-65e7-4281-ad25-600c7088b92c"), new Guid("fd83ba94-6a8e-491a-9427-159059a8e186") },
                    { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), new Guid("d1e85fda-984f-4911-9899-70fea899257d") },
                    { new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd"), new Guid("ad6ab100-12e6-4d67-bebf-016a2f1e6a83") },
                    { new Guid("f7361c60-8684-40f4-9d77-423eaa1bff82"), new Guid("d1e85fda-984f-4911-9899-70fea899257d") },
                    { new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd"), new Guid("d1e85fda-984f-4911-9899-70fea899257d") },
                    { new Guid("a6ac612b-4e37-4038-ba0f-244e5744b0b8"), new Guid("c27fcae0-3000-4ba7-bc97-a551b81dda8f") },
                    { new Guid("2b055393-65e7-4281-ad25-600c7088b92c"), new Guid("c27fcae0-3000-4ba7-bc97-a551b81dda8f") },
                    { new Guid("ba4b493f-01da-4d65-8385-d3f3d25af7fd"), new Guid("c27fcae0-3000-4ba7-bc97-a551b81dda8f") },
                    { new Guid("aa9c49db-c23f-40c4-9a80-7f4c2f251351"), new Guid("d1e85fda-984f-4911-9899-70fea899257d") }
                });

            migrationBuilder.InsertData(
                table: "Votos",
                columns: new[] { "IdFilme", "IdUsuario", "Nota" },
                values: new object[,]
                {
                    { new Guid("2b038da6-88ac-4d43-b3b1-15026b7669a9"), new Guid("543471df-7f59-4ae9-bcb4-11abe34dfcea"), (byte)3 },
                    { new Guid("94ea710b-82f8-47bc-9dc5-397abbc6dc56"), new Guid("543471df-7f59-4ae9-bcb4-11abe34dfcea"), (byte)1 },
                    { new Guid("ba47b826-2be2-4fd8-b0e8-d8c37f880932"), new Guid("543471df-7f59-4ae9-bcb4-11abe34dfcea"), (byte)4 }
                });
        }
    }
}
