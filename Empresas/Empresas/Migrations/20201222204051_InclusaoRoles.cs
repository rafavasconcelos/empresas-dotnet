﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Empresas.Migrations
{
    public partial class InclusaoRoles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_IdentityRole",
                table: "IdentityRole");

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("19d86a9a-1d48-4377-92b2-887fd4272d5a"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("1b185a2b-1c1c-4c4e-a9ed-fe0af75e38f3"), new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("1d7b2e42-861e-46cf-b782-2663ac6b26ea"), new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("1d7b2e42-861e-46cf-b782-2663ac6b26ea"), new Guid("439f33f5-1289-4030-9f3e-2ce25134c415") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("1d7b2e42-861e-46cf-b782-2663ac6b26ea"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("22fa8c08-9b3b-45ce-8dd3-bb1a7cf4ab80"), new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("3f47f108-b594-4603-b3be-29efe21f3eb6"), new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("41e357f4-c626-497b-9e1f-3a928ac2b6c0"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("425d1dbc-b714-41bf-aa83-c80f7b625ef2"), new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("4391d12e-4c16-4cab-a2db-8fb68f208f43"), new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("46b367d1-f3e6-42dc-ba92-d45e72e175f1"), new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("57a3340e-2b19-4498-8376-c8858faee65b"), new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("5ac85176-bbcb-4c92-9a1d-a9eda2075c27"), new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("67aa224d-587d-4c59-b417-af848d386389"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("711fed8f-2780-4f6b-8224-7d2ad713fca5"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("80e7e4f6-1826-4de0-8586-859d59e1e8f1"), new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("850adc0f-dea4-40b7-8bb0-487a195c5a13"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("8bdb8b5d-591c-4a20-8ee6-7699c9e025eb"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("90573ce9-284d-422e-81a8-61eaa0d5b1d6"), new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("959f3032-583a-4f4a-8af0-4dafa486a0d2"), new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a05d47e7-0345-46dd-ac97-9dfb277927d9"), new Guid("439f33f5-1289-4030-9f3e-2ce25134c415") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a1328e16-bebf-4a68-8a8e-abf2ec560b61"), new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a7e3f202-23b8-415f-88f3-7db9a5ebf4aa"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ad89435d-216c-4176-bc58-dfba8b083297"), new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("ae508865-5ed7-4348-ab3c-8a7ba7fbf642"), new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("bd57c1a1-880f-4d6f-8e7a-be0fcc44c39d"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("be710d58-4487-4eb1-9d98-f87df6680abf"), new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("c3dbdd36-3883-4c92-9b2b-1a6d0157b371"), new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("caae396f-6df8-40c3-986e-174683c42ce2"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("cb2b0816-fc44-4469-8af5-a22417ea57ca"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("cef61b6c-d17f-4516-aad4-3731f44f0dd5"), new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("d13fe349-9ad9-40c4-9f99-084290d558a6"), new Guid("439f33f5-1289-4030-9f3e-2ce25134c415") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("d7257033-435e-450d-85b1-e0ee7d2fa49d"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") });

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("625caa35-880c-4be8-9f69-34ae2231b7dc"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("87d68089-9d9f-4a1e-b8e3-56b63a8a68cc"));

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("18fefb92-933a-4306-9ded-652bb84ab7bf"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("1a7d0f8d-51a6-43c0-948e-af475b278da8"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("2fb5638e-83a1-4ef7-89c2-112c21325730"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("48999599-a87f-4805-a79d-81c940d69f80"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("52b687f1-3efd-4d65-a68d-910f27b3fc82"), new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("52b687f1-3efd-4d65-a68d-910f27b3fc82"), new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("56cc59f6-ced9-4e3b-8616-2869f56471b1"), new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("6ffe17a7-04d6-438b-8312-b678c7c7372c"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("7394701e-d08a-4276-9e46-520126182b18"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("78d5ab9e-b8bc-40c2-ad00-fdc0a40df168"), new Guid("439f33f5-1289-4030-9f3e-2ce25134c415") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("824ee794-8952-4dda-b182-4ed26c2d6000"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("8937215c-a99b-406f-861d-bb4318c5fbd7"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("9279b751-61df-492d-a4ec-7101b69e5484"), new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("9f7d6454-9632-46d5-86e6-9277ae06cc18"), new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("f2840907-7326-4428-82f7-9a292c185b70"), new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9") });

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("0ae399c4-4066-45ea-b2f2-2a27ddd82ba4"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("1c27db3d-0235-4f8b-965e-96dcb3b61022"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("22fdb2b4-13c0-4749-8492-e2ed1718a156"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("2fc7d867-54c7-4de9-bd6e-3c3927c85bb7"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("53502d72-ac6d-4056-b978-e7246295ce75"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7f98d99e-9ac8-4c39-ba96-9712d715b207"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("9917be90-d12e-4cf4-b6fd-c9b0b01f3e43"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("9c90db90-1179-4d43-9c97-e6065a2c9374"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("9fe2a5e6-b62f-45e6-88b1-a3a6387695bf"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("b6ec13a3-a47b-49fa-99ae-a33fe584a813"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("bebf1b0c-7623-4fe5-a44c-749c10027b15"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("bf64dbb0-b3f5-47fd-b677-02479b206382"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("cd83d03a-560a-4e09-b8e3-852651a92f7a"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("d0ce6295-d3fc-4079-be42-2f436f44f369"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("da7b1bf9-7d9d-4116-9349-3bcfd88670ee"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("db6ae3f5-908d-4adc-90fb-286035889d13"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("f095cc29-9a7b-4428-b79b-81f0c92baa68"));

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b"), new Guid("155eff2e-8452-4cae-84ca-2d4cfc2ffe0e") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8"), new Guid("21a3251e-2d7c-42fd-a999-8328ebc70504") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8"), new Guid("21a3251e-2d7c-42fd-a999-8328ebc70504") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac"), new Guid("33f657c4-4852-4a70-9ba5-5abb9d86d685") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f"), new Guid("33f657c4-4852-4a70-9ba5-5abb9d86d685") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), new Guid("41b135c6-3041-4bd7-9960-420721ce39a1") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), new Guid("52a1cd09-b97d-48fe-8f22-35eb7aaff9cd") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("579a7f05-52a0-468f-8f65-880b96eee634"), new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6"), new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f"), new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8"), new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("579a7f05-52a0-468f-8f65-880b96eee634"), new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8"), new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), new Guid("8c75f953-6cc4-42ca-a281-06bd5dc4742b") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9"), new Guid("8c75f953-6cc4-42ca-a281-06bd5dc4742b") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("579a7f05-52a0-468f-8f65-880b96eee634"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef"), new Guid("fad75e01-bfee-4aa8-9221-14055c809b25") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6"), new Guid("fad75e01-bfee-4aa8-9221-14055c809b25") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac"), new Guid("fad75e01-bfee-4aa8-9221-14055c809b25") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), new Guid("fae855e4-76fc-494a-b664-4ec68970d47e") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8"), new Guid("fae855e4-76fc-494a-b664-4ec68970d47e") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9"), new Guid("fae855e4-76fc-494a-b664-4ec68970d47e") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef"), new Guid("ff4051ca-a8ae-4c93-bfda-1c269b059b11") });

            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "8c236128-42d1-4111-aee5-738875a1e90e");

            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "c26c9aeb-9e36-4a80-a203-8ea029806770");

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef"), new Guid("dc6fce33-243e-4d4c-8328-1eda6867c3ea") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), new Guid("dc6fce33-243e-4d4c-8328-1eda6867c3ea") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f"), new Guid("dc6fce33-243e-4d4c-8328-1eda6867c3ea") });

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("19d86a9a-1d48-4377-92b2-887fd4272d5a"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("1b185a2b-1c1c-4c4e-a9ed-fe0af75e38f3"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("1d7b2e42-861e-46cf-b782-2663ac6b26ea"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("22fa8c08-9b3b-45ce-8dd3-bb1a7cf4ab80"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("3f47f108-b594-4603-b3be-29efe21f3eb6"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("41e357f4-c626-497b-9e1f-3a928ac2b6c0"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("425d1dbc-b714-41bf-aa83-c80f7b625ef2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("4391d12e-4c16-4cab-a2db-8fb68f208f43"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("46b367d1-f3e6-42dc-ba92-d45e72e175f1"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("57a3340e-2b19-4498-8376-c8858faee65b"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("5ac85176-bbcb-4c92-9a1d-a9eda2075c27"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("67aa224d-587d-4c59-b417-af848d386389"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("711fed8f-2780-4f6b-8224-7d2ad713fca5"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("80e7e4f6-1826-4de0-8586-859d59e1e8f1"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("850adc0f-dea4-40b7-8bb0-487a195c5a13"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("8bdb8b5d-591c-4a20-8ee6-7699c9e025eb"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("90573ce9-284d-422e-81a8-61eaa0d5b1d6"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("959f3032-583a-4f4a-8af0-4dafa486a0d2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a05d47e7-0345-46dd-ac97-9dfb277927d9"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a1328e16-bebf-4a68-8a8e-abf2ec560b61"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a7e3f202-23b8-415f-88f3-7db9a5ebf4aa"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ad89435d-216c-4176-bc58-dfba8b083297"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("ae508865-5ed7-4348-ab3c-8a7ba7fbf642"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("bd57c1a1-880f-4d6f-8e7a-be0fcc44c39d"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("be710d58-4487-4eb1-9d98-f87df6680abf"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("c3dbdd36-3883-4c92-9b2b-1a6d0157b371"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("caae396f-6df8-40c3-986e-174683c42ce2"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("cb2b0816-fc44-4469-8af5-a22417ea57ca"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("cef61b6c-d17f-4516-aad4-3731f44f0dd5"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("d13fe349-9ad9-40c4-9f99-084290d558a6"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("d7257033-435e-450d-85b1-e0ee7d2fa49d"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("18fefb92-933a-4306-9ded-652bb84ab7bf"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("1a7d0f8d-51a6-43c0-948e-af475b278da8"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("2fb5638e-83a1-4ef7-89c2-112c21325730"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("48999599-a87f-4805-a79d-81c940d69f80"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("52b687f1-3efd-4d65-a68d-910f27b3fc82"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("56cc59f6-ced9-4e3b-8616-2869f56471b1"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("6ffe17a7-04d6-438b-8312-b678c7c7372c"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("7394701e-d08a-4276-9e46-520126182b18"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("78d5ab9e-b8bc-40c2-ad00-fdc0a40df168"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("824ee794-8952-4dda-b182-4ed26c2d6000"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("8937215c-a99b-406f-861d-bb4318c5fbd7"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("9279b751-61df-492d-a4ec-7101b69e5484"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("9f7d6454-9632-46d5-86e6-9277ae06cc18"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("f2840907-7326-4428-82f7-9a292c185b70"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("579a7f05-52a0-468f-8f65-880b96eee634"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("155eff2e-8452-4cae-84ca-2d4cfc2ffe0e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("21a3251e-2d7c-42fd-a999-8328ebc70504"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("33f657c4-4852-4a70-9ba5-5abb9d86d685"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("41b135c6-3041-4bd7-9960-420721ce39a1"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("52a1cd09-b97d-48fe-8f22-35eb7aaff9cd"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("8c75f953-6cc4-42ca-a281-06bd5dc4742b"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("fad75e01-bfee-4aa8-9221-14055c809b25"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("fae855e4-76fc-494a-b664-4ec68970d47e"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("ff4051ca-a8ae-4c93-bfda-1c269b059b11"));

            migrationBuilder.DeleteData(
                table: "Usuarios",
                keyColumn: "IdUsuario",
                keyValue: new Guid("dc6fce33-243e-4d4c-8328-1eda6867c3ea"));

            migrationBuilder.AlterColumn<string>(
                name: "NormalizedName",
                table: "IdentityRole",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "IdentityRole",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ConcurrencyStamp",
                table: "IdentityRole",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "Id",
                table: "IdentityRole",
                column: "Id");

            migrationBuilder.InsertData(
                table: "Atores",
                columns: new[] { "IdAtor", "Nome" },
                values: new object[,]
                {
                    { new Guid("93ff2bb2-deaf-4e63-8a1a-bf3a9b372b98"), "Ben Burtt" },
                    { new Guid("7c2c28d0-6413-4e29-ad5a-d9f092186990"), "Woody Harrelson" },
                    { new Guid("e911fbcf-ee6b-47f8-9a5f-ce5f64e4aa33"), "Tom Sizemore" },
                    { new Guid("620572ea-ed8f-45e4-960d-6456e61acab3"), "Robert Downey Jr." },
                    { new Guid("a5c03621-4ff6-46bc-b557-f0710c4de32a"), "Rachel McAdams" },
                    { new Guid("832cfc10-e526-452a-aa31-d52faf384c42"), "Paul Rhys" },
                    { new Guid("9963515c-a586-466d-b76a-a2b3e6a94d4e"), "Patton Oswalt" },
                    { new Guid("750c65ec-280f-4f5e-8b81-17761617ec7c"), "Patrick Magee" },
                    { new Guid("45028356-fa2a-426d-8388-1ca0a2ac2d09"), "Owen Wilson" },
                    { new Guid("3d6ee507-eeb7-470b-8aa0-40206f0c14a1"), "Mickey Rourke" },
                    { new Guid("793dc6f7-208c-4712-91ce-d7703c147b48"), "Michael Bates" },
                    { new Guid("8d234acf-404b-42c2-bad5-442c7e77c1e4"), "Mark Ruffalo" },
                    { new Guid("fd5bead0-45d3-4da0-9e14-294c949302c3"), "Malcolm McDowell" },
                    { new Guid("f5261a78-4718-4848-a95b-b0bb05051db6"), "Laurence Fishburne" },
                    { new Guid("9e010e93-229f-4be1-b6c8-128331d9398f"), "Keanu Reeves" },
                    { new Guid("f84aeace-29d3-4d66-8d22-bdcfdf3881fa"), "Lou Romano" },
                    { new Guid("5180a891-03df-461c-85c9-be465b1fed46"), "Jude Law" },
                    { new Guid("9472f496-c087-400c-b9cd-7102d3e2298c"), "Brad Garrett" },
                    { new Guid("3c1e629e-0300-46cc-9e00-eaf487125b00"), "Bruce Willis" },
                    { new Guid("c64a0736-d975-46b4-aba1-2f5b35702b45"), "Carrie-Anne Moss" },
                    { new Guid("226cb8fb-9d93-4f5b-9076-db32e4f74990"), "Chris Evans" },
                    { new Guid("6ea9748e-3a48-4d3b-8c3f-9a2e3a5f6a71"), "Juliette Lewis" },
                    { new Guid("682ca4b4-6afd-4e05-b34b-e965b3dece15"), "Daniel Mays" },
                    { new Guid("a3784feb-09d2-47ff-af20-d6c07af16031"), "Dean-Charles Chapman" },
                    { new Guid("09274ef3-8f86-47db-8cc2-9b6b3f9d24b4"), "Clive Owen" },
                    { new Guid("e706077e-15bf-4afd-a568-096b9c0f1d1c"), "Eric Dane" },
                    { new Guid("347f2e35-f70a-4cfe-89dd-b463e7a54842"), "George MacKay" },
                    { new Guid("af09db55-2227-4ff7-b038-be9eed492c37"), "Geraldine Chaplin" },
                    { new Guid("c7b4291a-2af6-4d25-bd7e-5c9839869bd8"), "Jeff Garlin" },
                    { new Guid("e26c3d8a-a7b6-46d4-8f4e-ed42bcbe6a3f"), "Jennifer Aniston" },
                    { new Guid("9ee8dc57-8385-463a-b26e-e6306bad0e11"), "Elissa Knight" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("4f5fdfea-a18c-4489-b0b9-42178d08886e"), "Andrew Stanton" },
                    { new Guid("92f2c057-2e50-4b12-97d7-286e499212c4"), "Stanley Kubrick" },
                    { new Guid("7572ab44-d143-47f9-8795-31e102934b1c"), "Oliver Stone" },
                    { new Guid("f70b66bb-f45f-4814-bf6c-409c8a57567e"), "Guy Ritchie" },
                    { new Guid("94457e14-4a55-4717-ad81-4d32cdead470"), "David Frankel" },
                    { new Guid("50b840b8-6086-4e39-99c2-96efcf60071f"), "Sam Mendes" },
                    { new Guid("c8c2d9d4-2769-40ba-94e5-b91e61e1d4b1"), "Joe Russo" },
                    { new Guid("36f372e0-a50a-46f5-bf39-a32f8120fa25"), "Richard Attenborough" },
                    { new Guid("5050acab-fe8f-4b95-9400-be90e3ae1b69"), "Lilly Wachowski" },
                    { new Guid("73592157-1355-48af-a0cb-5d7abfff0d63"), "Lana Wachowski" },
                    { new Guid("06b2e1b1-b1ea-4477-aa97-a270de37a969"), "Jan Pinkava" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("15935daa-daaa-4462-95e2-4b5d8fe3755c"), "Brad Bird" },
                    { new Guid("81e205c5-ce88-4055-a095-c7b2205d2c69"), "Robert Rodriguez" },
                    { new Guid("71608239-c75a-4a89-833e-3fa37ab88c15"), "Anthony Russo" },
                    { new Guid("fccb7918-a6bc-4dcc-9847-dd8ac66243ec"), "Quentin Tarantino" },
                    { new Guid("b66783c2-a3a5-4d52-ae67-61d6bf88ce4a"), "Frank Miller" }
                });

            migrationBuilder.InsertData(
                table: "Filmes",
                columns: new[] { "IdFilme", "Ano", "Nome", "NomeOriginal", "Sinopse" },
                values: new object[,]
                {
                    { new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890"), 1999, "Matrix", "The Matrix", "Um hacker aprende com os misteriosos rebeldes sobre a verdadeira natureza de sua realidade e seu papel na guerra contra seus controladores." },
                    { new Guid("005e2eda-be8a-4841-885d-04880609c9ff"), 2007, "Ratatouille", "Ratatouille", "Um rato que pode cozinhar forja uma aliança incomum com um jovem garoto em um famoso restaurante em Paris." },
                    { new Guid("7ec6ea91-982d-430c-b297-2b0c7e015df1"), 2008, "Marley & Eu", "Marley & Me", "Uma família aprende importantes lições de vida com seu adorável, mas malicioso e neurótico cão." },
                    { new Guid("5e4728a8-a493-4fa4-b2b8-4e4311e5c542"), 2008, "WALL·E", "WALL·E", "Num futuro distante, um pequeno robô faz uma viagem que decidirá o destino da humanidade." },
                    { new Guid("93309629-4d39-408f-a315-ce6fbc85016d"), 2019, "Vingadores: Ultimato", "Avengers: Endgame", "Após os eventos devastadores de Vingadores: Guerra Infinita , o universo está em ruínas, e com a ajuda de aliados os Vingadores se reúnem para desfazer as ações de Thanos e restaurar a ordem." },
                    { new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5"), 2005, "Sin City: A Cidade do Pecado", "Sin City", "O filme explora a miserável cidade de Basin City, e conta a historia de três pessoas diferentes, envoltas em violencia e corrupção." },
                    { new Guid("42c0113a-c4c8-449a-a978-a9b6835655b7"), 1994, "Assassinos por Natureza", "Natural Born Killers", "Duas vítimas de uma infância traumatizada tornam-se amantes e assassinos em série, irresponsavelmente glorificados pela mídia." },
                    { new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222"), 1992, "Chaplin", "Chaplin", "Relata a vida convulsiva e controversa do mestre diretor de comédia Charles Chaplin." },
                    { new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5"), 2009, "Sherlock Holmes", "Sherlock Holmes", "O detective Sherlock Holmes e seu colega Watson se engajam numa batalha para lutar contra as ameaças da Inglaterra." },
                    { new Guid("633daa69-a9ad-44a0-a52d-a966a867c9f2"), 1971, "Laranja Mecânica", "A Clockwork Orange", "No futuro, um líder duma turma é levado para prisão é decide ser voluntário dum experimento, mais os resultados não são os esperados." },
                    { new Guid("f3d477d6-328c-43b9-9a67-d94bf0b22dd8"), 2019, "1917", "1917", "6 de Abril de 1917. Enquanto um regimento se reúne para lutar uma guerra nas profundezas do território inimigo, dois soldados são designados para uma corrida contra o tempo para entregar uma mensagem que impedirá 1.600 homens de caminhar direto para uma uma armadilha mortal." }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("8d88cc40-6ea2-43db-841d-138c28497504"), "Fantasia científica" },
                    { new Guid("d3f7ff92-3467-497a-a569-b646593faffe"), "Ficção científica" },
                    { new Guid("4b7d1d28-3c91-430e-b4b1-e935e1f7d956"), "Filmes com truques" },
                    { new Guid("5521b964-6912-4135-947d-29ac2fe51fc4"), "Filmes de guerra" },
                    { new Guid("d322fefe-e9cd-4493-9335-f61e7fd552ec"), "Musical" },
                    { new Guid("489d96a2-0208-4872-951e-a2035511de7f"), "Pornográfico" },
                    { new Guid("efc40787-3a2b-44fb-bf36-0cef3162f7f6"), "Romance" },
                    { new Guid("930dae0d-43f2-4860-828b-6a41f6825be0"), "Seriado" },
                    { new Guid("ac07bd90-3d0f-4c88-8c1d-70e80bde2227"), "Suspense" },
                    { new Guid("3c5fac97-2044-4779-9027-e069572fdff2"), "Terror" },
                    { new Guid("8fa87d28-a8cc-4428-b46c-804a5a908770"), "Thriller" },
                    { new Guid("9d210faa-2ba2-4d21-99e3-e9b067cae9f3"), "Fantasia" },
                    { new Guid("c0063611-f252-40e0-95fe-f1f0cdd56d92"), "Filme policial" },
                    { new Guid("e97142ff-6e2e-459e-9da2-e93e820ce407"), "Faroeste" },
                    { new Guid("69e041ef-b3f6-4554-b678-ed3aa5af1682"), "Comédia de ação" },
                    { new Guid("7aceb37e-faa3-4db5-af9a-116e05e624b7"), "Drama" },
                    { new Guid("0e0f35f0-6492-4997-8e89-a09b2764de70"), "Ação" },
                    { new Guid("94279f95-a548-4fb9-a1e1-f120d08033e1"), "Animação" },
                    { new Guid("79b67c82-1c40-415b-a4db-d120975e4ef8"), "Aventura" },
                    { new Guid("5c2669ed-8f1f-4814-b524-16d21335e996"), "Cinema de arte" },
                    { new Guid("d27cf9d2-5b51-4bc0-8479-08633e627d2b"), "Espionagem" },
                    { new Guid("3aad9390-d91f-4e3e-999b-bb62d0a11c5f"), "Comédia" },
                    { new Guid("af29c681-9e8a-437c-8f0f-996ae119a8db"), "Chanchada" },
                    { new Guid("91775029-6f11-40fa-9ce3-ed95d828eeb9"), "Comédia dramática" },
                    { new Guid("bdc28cb0-2035-43bc-b9ae-09dba9454e43"), "Comédia romântica" },
                    { new Guid("2057f4f2-18e7-4ece-940b-0645546439cd"), "Dança" }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("6b566b6f-5415-4917-bf26-0ce5c0b83015"), "Documentário" },
                    { new Guid("fc2e89b4-d865-4b56-8310-9d798f851760"), "Docuficção" },
                    { new Guid("a2551ef3-2989-4f31-9ef2-bba63cfa4f68"), "Comédia de terror" }
                });

            migrationBuilder.InsertData(
                table: "IdentityRole",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "007bfc0f-9ebb-493b-87f7-65fcc969cb6d", "abda8930-5508-4855-af7a-b7bb87bffc97", "Admin", "ADMIN" },
                    { "4c134f94-0161-4d27-b9e0-aeac2d2beeaa", "8ffbbc82-e4f2-4f72-9feb-18c2698a82f6", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "Usuarios",
                columns: new[] { "IdUsuario", "Email", "HashSenha", "IdRole", "Nome", "SaltSenha" },
                values: new object[] { new Guid("20305495-3f29-4722-82db-12ea87f208aa"), "rafael.av@gmail.com", new byte[] { 176, 29, 12, 218, 226, 124, 161, 64, 78, 112, 254, 177, 73, 16, 247, 94, 1, 240, 53, 28, 230, 67, 248, 194, 66, 198, 74, 250, 87, 92, 229, 141, 7, 177, 106, 162, 212, 201, 71, 186, 239, 238, 162, 232, 198, 208, 212, 55, 81, 244, 59, 197, 9, 37, 86, 252, 138, 4, 190, 133, 176, 10, 12, 204 }, new Guid("007bfc0f-9ebb-493b-87f7-65fcc969cb6d"), "Rafael Vasconcelos", new byte[] { 23, 48, 96, 6, 16, 34, 46, 26, 137, 222, 11, 223, 72, 31, 26, 56, 112, 240, 74, 52, 94, 94, 179, 26, 21, 51, 117, 93, 168, 19, 94, 158, 27, 152, 106, 232, 160, 229, 132, 50, 0, 208, 109, 177, 75, 75, 20, 193, 247, 78, 201, 165, 168, 125, 155, 81, 102, 14, 110, 225, 80, 162, 153, 107, 215, 114, 190, 85, 178, 214, 187, 201, 254, 57, 223, 136, 84, 132, 119, 237, 159, 18, 73, 17, 82, 95, 206, 171, 190, 247, 206, 201, 119, 214, 123, 232, 70, 88, 250, 2, 119, 143, 105, 44, 239, 89, 172, 194, 194, 159, 76, 246, 27, 105, 202, 146, 76, 199, 245, 50, 163, 78, 53, 250, 161, 226, 210, 109 } });

            migrationBuilder.InsertData(
                table: "AtoresFilmes",
                columns: new[] { "IdAtor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("fd5bead0-45d3-4da0-9e14-294c949302c3"), new Guid("633daa69-a9ad-44a0-a52d-a966a867c9f2") },
                    { new Guid("8d234acf-404b-42c2-bad5-442c7e77c1e4"), new Guid("93309629-4d39-408f-a315-ce6fbc85016d") },
                    { new Guid("93ff2bb2-deaf-4e63-8a1a-bf3a9b372b98"), new Guid("5e4728a8-a493-4fa4-b2b8-4e4311e5c542") },
                    { new Guid("9ee8dc57-8385-463a-b26e-e6306bad0e11"), new Guid("5e4728a8-a493-4fa4-b2b8-4e4311e5c542") },
                    { new Guid("c7b4291a-2af6-4d25-bd7e-5c9839869bd8"), new Guid("5e4728a8-a493-4fa4-b2b8-4e4311e5c542") },
                    { new Guid("45028356-fa2a-426d-8388-1ca0a2ac2d09"), new Guid("7ec6ea91-982d-430c-b297-2b0c7e015df1") },
                    { new Guid("e26c3d8a-a7b6-46d4-8f4e-ed42bcbe6a3f"), new Guid("7ec6ea91-982d-430c-b297-2b0c7e015df1") },
                    { new Guid("e706077e-15bf-4afd-a568-096b9c0f1d1c"), new Guid("7ec6ea91-982d-430c-b297-2b0c7e015df1") },
                    { new Guid("3d6ee507-eeb7-470b-8aa0-40206f0c14a1"), new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5") },
                    { new Guid("09274ef3-8f86-47db-8cc2-9b6b3f9d24b4"), new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5") },
                    { new Guid("3c1e629e-0300-46cc-9e00-eaf487125b00"), new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5") },
                    { new Guid("9472f496-c087-400c-b9cd-7102d3e2298c"), new Guid("005e2eda-be8a-4841-885d-04880609c9ff") },
                    { new Guid("f84aeace-29d3-4d66-8d22-bdcfdf3881fa"), new Guid("005e2eda-be8a-4841-885d-04880609c9ff") },
                    { new Guid("9e010e93-229f-4be1-b6c8-128331d9398f"), new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890") },
                    { new Guid("f5261a78-4718-4848-a95b-b0bb05051db6"), new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890") },
                    { new Guid("c64a0736-d975-46b4-aba1-2f5b35702b45"), new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890") },
                    { new Guid("226cb8fb-9d93-4f5b-9076-db32e4f74990"), new Guid("93309629-4d39-408f-a315-ce6fbc85016d") },
                    { new Guid("620572ea-ed8f-45e4-960d-6456e61acab3"), new Guid("93309629-4d39-408f-a315-ce6fbc85016d") },
                    { new Guid("9963515c-a586-466d-b76a-a2b3e6a94d4e"), new Guid("005e2eda-be8a-4841-885d-04880609c9ff") },
                    { new Guid("750c65ec-280f-4f5e-8b81-17761617ec7c"), new Guid("633daa69-a9ad-44a0-a52d-a966a867c9f2") },
                    { new Guid("a5c03621-4ff6-46bc-b557-f0710c4de32a"), new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5") },
                    { new Guid("620572ea-ed8f-45e4-960d-6456e61acab3"), new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222") },
                    { new Guid("af09db55-2227-4ff7-b038-be9eed492c37"), new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222") },
                    { new Guid("832cfc10-e526-452a-aa31-d52faf384c42"), new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222") },
                    { new Guid("793dc6f7-208c-4712-91ce-d7703c147b48"), new Guid("633daa69-a9ad-44a0-a52d-a966a867c9f2") },
                    { new Guid("620572ea-ed8f-45e4-960d-6456e61acab3"), new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5") },
                    { new Guid("7c2c28d0-6413-4e29-ad5a-d9f092186990"), new Guid("42c0113a-c4c8-449a-a978-a9b6835655b7") },
                    { new Guid("5180a891-03df-461c-85c9-be465b1fed46"), new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5") },
                    { new Guid("e911fbcf-ee6b-47f8-9a5f-ce5f64e4aa33"), new Guid("42c0113a-c4c8-449a-a978-a9b6835655b7") },
                    { new Guid("682ca4b4-6afd-4e05-b34b-e965b3dece15"), new Guid("f3d477d6-328c-43b9-9a67-d94bf0b22dd8") },
                    { new Guid("a3784feb-09d2-47ff-af20-d6c07af16031"), new Guid("f3d477d6-328c-43b9-9a67-d94bf0b22dd8") },
                    { new Guid("347f2e35-f70a-4cfe-89dd-b463e7a54842"), new Guid("f3d477d6-328c-43b9-9a67-d94bf0b22dd8") },
                    { new Guid("6ea9748e-3a48-4d3b-8c3f-9a2e3a5f6a71"), new Guid("42c0113a-c4c8-449a-a978-a9b6835655b7") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("73592157-1355-48af-a0cb-5d7abfff0d63"), new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890") },
                    { new Guid("5050acab-fe8f-4b95-9400-be90e3ae1b69"), new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890") },
                    { new Guid("36f372e0-a50a-46f5-bf39-a32f8120fa25"), new Guid("633daa69-a9ad-44a0-a52d-a966a867c9f2") },
                    { new Guid("06b2e1b1-b1ea-4477-aa97-a270de37a969"), new Guid("005e2eda-be8a-4841-885d-04880609c9ff") },
                    { new Guid("15935daa-daaa-4462-95e2-4b5d8fe3755c"), new Guid("005e2eda-be8a-4841-885d-04880609c9ff") },
                    { new Guid("50b840b8-6086-4e39-99c2-96efcf60071f"), new Guid("f3d477d6-328c-43b9-9a67-d94bf0b22dd8") },
                    { new Guid("b66783c2-a3a5-4d52-ae67-61d6bf88ce4a"), new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5") },
                    { new Guid("f70b66bb-f45f-4814-bf6c-409c8a57567e"), new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5") },
                    { new Guid("94457e14-4a55-4717-ad81-4d32cdead470"), new Guid("7ec6ea91-982d-430c-b297-2b0c7e015df1") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("36f372e0-a50a-46f5-bf39-a32f8120fa25"), new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222") },
                    { new Guid("4f5fdfea-a18c-4489-b0b9-42178d08886e"), new Guid("5e4728a8-a493-4fa4-b2b8-4e4311e5c542") },
                    { new Guid("c8c2d9d4-2769-40ba-94e5-b91e61e1d4b1"), new Guid("93309629-4d39-408f-a315-ce6fbc85016d") },
                    { new Guid("71608239-c75a-4a89-833e-3fa37ab88c15"), new Guid("93309629-4d39-408f-a315-ce6fbc85016d") },
                    { new Guid("7572ab44-d143-47f9-8795-31e102934b1c"), new Guid("42c0113a-c4c8-449a-a978-a9b6835655b7") },
                    { new Guid("fccb7918-a6bc-4dcc-9847-dd8ac66243ec"), new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5") }
                });

            migrationBuilder.InsertData(
                table: "GenerosFilmes",
                columns: new[] { "IdFilme", "IdGenero" },
                values: new object[,]
                {
                    { new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222"), new Guid("7aceb37e-faa3-4db5-af9a-116e05e624b7") },
                    { new Guid("7ec6ea91-982d-430c-b297-2b0c7e015df1"), new Guid("7aceb37e-faa3-4db5-af9a-116e05e624b7") },
                    { new Guid("42c0113a-c4c8-449a-a978-a9b6835655b7"), new Guid("7aceb37e-faa3-4db5-af9a-116e05e624b7") },
                    { new Guid("f3d477d6-328c-43b9-9a67-d94bf0b22dd8"), new Guid("7aceb37e-faa3-4db5-af9a-116e05e624b7") },
                    { new Guid("93309629-4d39-408f-a315-ce6fbc85016d"), new Guid("7aceb37e-faa3-4db5-af9a-116e05e624b7") },
                    { new Guid("633daa69-a9ad-44a0-a52d-a966a867c9f2"), new Guid("d3f7ff92-3467-497a-a569-b646593faffe") },
                    { new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5"), new Guid("8fa87d28-a8cc-4428-b46c-804a5a908770") },
                    { new Guid("f3d477d6-328c-43b9-9a67-d94bf0b22dd8"), new Guid("5521b964-6912-4135-947d-29ac2fe51fc4") },
                    { new Guid("633daa69-a9ad-44a0-a52d-a966a867c9f2"), new Guid("c0063611-f252-40e0-95fe-f1f0cdd56d92") },
                    { new Guid("42c0113a-c4c8-449a-a978-a9b6835655b7"), new Guid("c0063611-f252-40e0-95fe-f1f0cdd56d92") },
                    { new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5"), new Guid("c0063611-f252-40e0-95fe-f1f0cdd56d92") },
                    { new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5"), new Guid("ac07bd90-3d0f-4c88-8c1d-70e80bde2227") },
                    { new Guid("633daa69-a9ad-44a0-a52d-a966a867c9f2"), new Guid("7aceb37e-faa3-4db5-af9a-116e05e624b7") },
                    { new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890"), new Guid("d3f7ff92-3467-497a-a569-b646593faffe") },
                    { new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222"), new Guid("fc2e89b4-d865-4b56-8310-9d798f851760") },
                    { new Guid("42c0113a-c4c8-449a-a978-a9b6835655b7"), new Guid("0e0f35f0-6492-4997-8e89-a09b2764de70") },
                    { new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222"), new Guid("91775029-6f11-40fa-9ce3-ed95d828eeb9") },
                    { new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5"), new Guid("0e0f35f0-6492-4997-8e89-a09b2764de70") },
                    { new Guid("93309629-4d39-408f-a315-ce6fbc85016d"), new Guid("0e0f35f0-6492-4997-8e89-a09b2764de70") },
                    { new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890"), new Guid("0e0f35f0-6492-4997-8e89-a09b2764de70") },
                    { new Guid("5e4728a8-a493-4fa4-b2b8-4e4311e5c542"), new Guid("94279f95-a548-4fb9-a1e1-f120d08033e1") },
                    { new Guid("7ec6ea91-982d-430c-b297-2b0c7e015df1"), new Guid("91775029-6f11-40fa-9ce3-ed95d828eeb9") },
                    { new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5"), new Guid("79b67c82-1c40-415b-a4db-d120975e4ef8") },
                    { new Guid("005e2eda-be8a-4841-885d-04880609c9ff"), new Guid("94279f95-a548-4fb9-a1e1-f120d08033e1") },
                    { new Guid("5e4728a8-a493-4fa4-b2b8-4e4311e5c542"), new Guid("79b67c82-1c40-415b-a4db-d120975e4ef8") },
                    { new Guid("005e2eda-be8a-4841-885d-04880609c9ff"), new Guid("79b67c82-1c40-415b-a4db-d120975e4ef8") },
                    { new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222"), new Guid("3aad9390-d91f-4e3e-999b-bb62d0a11c5f") },
                    { new Guid("7ec6ea91-982d-430c-b297-2b0c7e015df1"), new Guid("3aad9390-d91f-4e3e-999b-bb62d0a11c5f") },
                    { new Guid("005e2eda-be8a-4841-885d-04880609c9ff"), new Guid("3aad9390-d91f-4e3e-999b-bb62d0a11c5f") },
                    { new Guid("93309629-4d39-408f-a315-ce6fbc85016d"), new Guid("79b67c82-1c40-415b-a4db-d120975e4ef8") }
                });

            migrationBuilder.InsertData(
                table: "Votos",
                columns: new[] { "IdFilme", "IdUsuario", "Nota" },
                values: new object[,]
                {
                    { new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5"), new Guid("20305495-3f29-4722-82db-12ea87f208aa"), (byte)3 },
                    { new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5"), new Guid("20305495-3f29-4722-82db-12ea87f208aa"), (byte)1 },
                    { new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890"), new Guid("20305495-3f29-4722-82db-12ea87f208aa"), (byte)4 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "Id",
                table: "IdentityRole");

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("09274ef3-8f86-47db-8cc2-9b6b3f9d24b4"), new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("226cb8fb-9d93-4f5b-9076-db32e4f74990"), new Guid("93309629-4d39-408f-a315-ce6fbc85016d") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("347f2e35-f70a-4cfe-89dd-b463e7a54842"), new Guid("f3d477d6-328c-43b9-9a67-d94bf0b22dd8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("3c1e629e-0300-46cc-9e00-eaf487125b00"), new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("3d6ee507-eeb7-470b-8aa0-40206f0c14a1"), new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("45028356-fa2a-426d-8388-1ca0a2ac2d09"), new Guid("7ec6ea91-982d-430c-b297-2b0c7e015df1") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("5180a891-03df-461c-85c9-be465b1fed46"), new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("620572ea-ed8f-45e4-960d-6456e61acab3"), new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("620572ea-ed8f-45e4-960d-6456e61acab3"), new Guid("93309629-4d39-408f-a315-ce6fbc85016d") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("620572ea-ed8f-45e4-960d-6456e61acab3"), new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("682ca4b4-6afd-4e05-b34b-e965b3dece15"), new Guid("f3d477d6-328c-43b9-9a67-d94bf0b22dd8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("6ea9748e-3a48-4d3b-8c3f-9a2e3a5f6a71"), new Guid("42c0113a-c4c8-449a-a978-a9b6835655b7") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("750c65ec-280f-4f5e-8b81-17761617ec7c"), new Guid("633daa69-a9ad-44a0-a52d-a966a867c9f2") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("793dc6f7-208c-4712-91ce-d7703c147b48"), new Guid("633daa69-a9ad-44a0-a52d-a966a867c9f2") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("7c2c28d0-6413-4e29-ad5a-d9f092186990"), new Guid("42c0113a-c4c8-449a-a978-a9b6835655b7") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("832cfc10-e526-452a-aa31-d52faf384c42"), new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("8d234acf-404b-42c2-bad5-442c7e77c1e4"), new Guid("93309629-4d39-408f-a315-ce6fbc85016d") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("93ff2bb2-deaf-4e63-8a1a-bf3a9b372b98"), new Guid("5e4728a8-a493-4fa4-b2b8-4e4311e5c542") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("9472f496-c087-400c-b9cd-7102d3e2298c"), new Guid("005e2eda-be8a-4841-885d-04880609c9ff") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("9963515c-a586-466d-b76a-a2b3e6a94d4e"), new Guid("005e2eda-be8a-4841-885d-04880609c9ff") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("9e010e93-229f-4be1-b6c8-128331d9398f"), new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("9ee8dc57-8385-463a-b26e-e6306bad0e11"), new Guid("5e4728a8-a493-4fa4-b2b8-4e4311e5c542") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a3784feb-09d2-47ff-af20-d6c07af16031"), new Guid("f3d477d6-328c-43b9-9a67-d94bf0b22dd8") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("a5c03621-4ff6-46bc-b557-f0710c4de32a"), new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("af09db55-2227-4ff7-b038-be9eed492c37"), new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("c64a0736-d975-46b4-aba1-2f5b35702b45"), new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("c7b4291a-2af6-4d25-bd7e-5c9839869bd8"), new Guid("5e4728a8-a493-4fa4-b2b8-4e4311e5c542") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("e26c3d8a-a7b6-46d4-8f4e-ed42bcbe6a3f"), new Guid("7ec6ea91-982d-430c-b297-2b0c7e015df1") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("e706077e-15bf-4afd-a568-096b9c0f1d1c"), new Guid("7ec6ea91-982d-430c-b297-2b0c7e015df1") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("e911fbcf-ee6b-47f8-9a5f-ce5f64e4aa33"), new Guid("42c0113a-c4c8-449a-a978-a9b6835655b7") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("f5261a78-4718-4848-a95b-b0bb05051db6"), new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("f84aeace-29d3-4d66-8d22-bdcfdf3881fa"), new Guid("005e2eda-be8a-4841-885d-04880609c9ff") });

            migrationBuilder.DeleteData(
                table: "AtoresFilmes",
                keyColumns: new[] { "IdAtor", "IdFilme" },
                keyValues: new object[] { new Guid("fd5bead0-45d3-4da0-9e14-294c949302c3"), new Guid("633daa69-a9ad-44a0-a52d-a966a867c9f2") });

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("81e205c5-ce88-4055-a095-c7b2205d2c69"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("92f2c057-2e50-4b12-97d7-286e499212c4"));

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("06b2e1b1-b1ea-4477-aa97-a270de37a969"), new Guid("005e2eda-be8a-4841-885d-04880609c9ff") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("15935daa-daaa-4462-95e2-4b5d8fe3755c"), new Guid("005e2eda-be8a-4841-885d-04880609c9ff") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("36f372e0-a50a-46f5-bf39-a32f8120fa25"), new Guid("633daa69-a9ad-44a0-a52d-a966a867c9f2") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("36f372e0-a50a-46f5-bf39-a32f8120fa25"), new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("4f5fdfea-a18c-4489-b0b9-42178d08886e"), new Guid("5e4728a8-a493-4fa4-b2b8-4e4311e5c542") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("5050acab-fe8f-4b95-9400-be90e3ae1b69"), new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("50b840b8-6086-4e39-99c2-96efcf60071f"), new Guid("f3d477d6-328c-43b9-9a67-d94bf0b22dd8") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("71608239-c75a-4a89-833e-3fa37ab88c15"), new Guid("93309629-4d39-408f-a315-ce6fbc85016d") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("73592157-1355-48af-a0cb-5d7abfff0d63"), new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("7572ab44-d143-47f9-8795-31e102934b1c"), new Guid("42c0113a-c4c8-449a-a978-a9b6835655b7") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("94457e14-4a55-4717-ad81-4d32cdead470"), new Guid("7ec6ea91-982d-430c-b297-2b0c7e015df1") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("b66783c2-a3a5-4d52-ae67-61d6bf88ce4a"), new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("c8c2d9d4-2769-40ba-94e5-b91e61e1d4b1"), new Guid("93309629-4d39-408f-a315-ce6fbc85016d") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("f70b66bb-f45f-4814-bf6c-409c8a57567e"), new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5") });

            migrationBuilder.DeleteData(
                table: "DiretoresFilmes",
                keyColumns: new[] { "IdDiretor", "IdFilme" },
                keyValues: new object[] { new Guid("fccb7918-a6bc-4dcc-9847-dd8ac66243ec"), new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5") });

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("2057f4f2-18e7-4ece-940b-0645546439cd"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("3c5fac97-2044-4779-9027-e069572fdff2"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("489d96a2-0208-4872-951e-a2035511de7f"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("4b7d1d28-3c91-430e-b4b1-e935e1f7d956"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("5c2669ed-8f1f-4814-b524-16d21335e996"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("69e041ef-b3f6-4554-b678-ed3aa5af1682"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("6b566b6f-5415-4917-bf26-0ce5c0b83015"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("8d88cc40-6ea2-43db-841d-138c28497504"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("930dae0d-43f2-4860-828b-6a41f6825be0"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("9d210faa-2ba2-4d21-99e3-e9b067cae9f3"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("a2551ef3-2989-4f31-9ef2-bba63cfa4f68"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("af29c681-9e8a-437c-8f0f-996ae119a8db"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("bdc28cb0-2035-43bc-b9ae-09dba9454e43"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("d27cf9d2-5b51-4bc0-8479-08633e627d2b"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("d322fefe-e9cd-4493-9335-f61e7fd552ec"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("e97142ff-6e2e-459e-9da2-e93e820ce407"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("efc40787-3a2b-44fb-bf36-0cef3162f7f6"));

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("42c0113a-c4c8-449a-a978-a9b6835655b7"), new Guid("0e0f35f0-6492-4997-8e89-a09b2764de70") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5"), new Guid("0e0f35f0-6492-4997-8e89-a09b2764de70") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("93309629-4d39-408f-a315-ce6fbc85016d"), new Guid("0e0f35f0-6492-4997-8e89-a09b2764de70") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890"), new Guid("0e0f35f0-6492-4997-8e89-a09b2764de70") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("005e2eda-be8a-4841-885d-04880609c9ff"), new Guid("3aad9390-d91f-4e3e-999b-bb62d0a11c5f") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("7ec6ea91-982d-430c-b297-2b0c7e015df1"), new Guid("3aad9390-d91f-4e3e-999b-bb62d0a11c5f") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222"), new Guid("3aad9390-d91f-4e3e-999b-bb62d0a11c5f") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("f3d477d6-328c-43b9-9a67-d94bf0b22dd8"), new Guid("5521b964-6912-4135-947d-29ac2fe51fc4") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("005e2eda-be8a-4841-885d-04880609c9ff"), new Guid("79b67c82-1c40-415b-a4db-d120975e4ef8") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5"), new Guid("79b67c82-1c40-415b-a4db-d120975e4ef8") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("5e4728a8-a493-4fa4-b2b8-4e4311e5c542"), new Guid("79b67c82-1c40-415b-a4db-d120975e4ef8") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("93309629-4d39-408f-a315-ce6fbc85016d"), new Guid("79b67c82-1c40-415b-a4db-d120975e4ef8") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("42c0113a-c4c8-449a-a978-a9b6835655b7"), new Guid("7aceb37e-faa3-4db5-af9a-116e05e624b7") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("633daa69-a9ad-44a0-a52d-a966a867c9f2"), new Guid("7aceb37e-faa3-4db5-af9a-116e05e624b7") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("7ec6ea91-982d-430c-b297-2b0c7e015df1"), new Guid("7aceb37e-faa3-4db5-af9a-116e05e624b7") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("93309629-4d39-408f-a315-ce6fbc85016d"), new Guid("7aceb37e-faa3-4db5-af9a-116e05e624b7") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222"), new Guid("7aceb37e-faa3-4db5-af9a-116e05e624b7") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("f3d477d6-328c-43b9-9a67-d94bf0b22dd8"), new Guid("7aceb37e-faa3-4db5-af9a-116e05e624b7") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5"), new Guid("8fa87d28-a8cc-4428-b46c-804a5a908770") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("7ec6ea91-982d-430c-b297-2b0c7e015df1"), new Guid("91775029-6f11-40fa-9ce3-ed95d828eeb9") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222"), new Guid("91775029-6f11-40fa-9ce3-ed95d828eeb9") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("005e2eda-be8a-4841-885d-04880609c9ff"), new Guid("94279f95-a548-4fb9-a1e1-f120d08033e1") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("5e4728a8-a493-4fa4-b2b8-4e4311e5c542"), new Guid("94279f95-a548-4fb9-a1e1-f120d08033e1") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5"), new Guid("ac07bd90-3d0f-4c88-8c1d-70e80bde2227") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5"), new Guid("c0063611-f252-40e0-95fe-f1f0cdd56d92") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("42c0113a-c4c8-449a-a978-a9b6835655b7"), new Guid("c0063611-f252-40e0-95fe-f1f0cdd56d92") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("633daa69-a9ad-44a0-a52d-a966a867c9f2"), new Guid("c0063611-f252-40e0-95fe-f1f0cdd56d92") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("633daa69-a9ad-44a0-a52d-a966a867c9f2"), new Guid("d3f7ff92-3467-497a-a569-b646593faffe") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890"), new Guid("d3f7ff92-3467-497a-a569-b646593faffe") });

            migrationBuilder.DeleteData(
                table: "GenerosFilmes",
                keyColumns: new[] { "IdFilme", "IdGenero" },
                keyValues: new object[] { new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222"), new Guid("fc2e89b4-d865-4b56-8310-9d798f851760") });

            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "007bfc0f-9ebb-493b-87f7-65fcc969cb6d");

            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "4c134f94-0161-4d27-b9e0-aeac2d2beeaa");

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5"), new Guid("20305495-3f29-4722-82db-12ea87f208aa") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5"), new Guid("20305495-3f29-4722-82db-12ea87f208aa") });

            migrationBuilder.DeleteData(
                table: "Votos",
                keyColumns: new[] { "IdFilme", "IdUsuario" },
                keyValues: new object[] { new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890"), new Guid("20305495-3f29-4722-82db-12ea87f208aa") });

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("09274ef3-8f86-47db-8cc2-9b6b3f9d24b4"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("226cb8fb-9d93-4f5b-9076-db32e4f74990"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("347f2e35-f70a-4cfe-89dd-b463e7a54842"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("3c1e629e-0300-46cc-9e00-eaf487125b00"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("3d6ee507-eeb7-470b-8aa0-40206f0c14a1"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("45028356-fa2a-426d-8388-1ca0a2ac2d09"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("5180a891-03df-461c-85c9-be465b1fed46"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("620572ea-ed8f-45e4-960d-6456e61acab3"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("682ca4b4-6afd-4e05-b34b-e965b3dece15"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("6ea9748e-3a48-4d3b-8c3f-9a2e3a5f6a71"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("750c65ec-280f-4f5e-8b81-17761617ec7c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("793dc6f7-208c-4712-91ce-d7703c147b48"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("7c2c28d0-6413-4e29-ad5a-d9f092186990"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("832cfc10-e526-452a-aa31-d52faf384c42"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("8d234acf-404b-42c2-bad5-442c7e77c1e4"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("93ff2bb2-deaf-4e63-8a1a-bf3a9b372b98"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("9472f496-c087-400c-b9cd-7102d3e2298c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("9963515c-a586-466d-b76a-a2b3e6a94d4e"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("9e010e93-229f-4be1-b6c8-128331d9398f"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("9ee8dc57-8385-463a-b26e-e6306bad0e11"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a3784feb-09d2-47ff-af20-d6c07af16031"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("a5c03621-4ff6-46bc-b557-f0710c4de32a"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("af09db55-2227-4ff7-b038-be9eed492c37"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("c64a0736-d975-46b4-aba1-2f5b35702b45"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("c7b4291a-2af6-4d25-bd7e-5c9839869bd8"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("e26c3d8a-a7b6-46d4-8f4e-ed42bcbe6a3f"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("e706077e-15bf-4afd-a568-096b9c0f1d1c"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("e911fbcf-ee6b-47f8-9a5f-ce5f64e4aa33"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("f5261a78-4718-4848-a95b-b0bb05051db6"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("f84aeace-29d3-4d66-8d22-bdcfdf3881fa"));

            migrationBuilder.DeleteData(
                table: "Atores",
                keyColumn: "IdAtor",
                keyValue: new Guid("fd5bead0-45d3-4da0-9e14-294c949302c3"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("06b2e1b1-b1ea-4477-aa97-a270de37a969"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("15935daa-daaa-4462-95e2-4b5d8fe3755c"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("36f372e0-a50a-46f5-bf39-a32f8120fa25"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("4f5fdfea-a18c-4489-b0b9-42178d08886e"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("5050acab-fe8f-4b95-9400-be90e3ae1b69"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("50b840b8-6086-4e39-99c2-96efcf60071f"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("71608239-c75a-4a89-833e-3fa37ab88c15"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("73592157-1355-48af-a0cb-5d7abfff0d63"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("7572ab44-d143-47f9-8795-31e102934b1c"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("94457e14-4a55-4717-ad81-4d32cdead470"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("b66783c2-a3a5-4d52-ae67-61d6bf88ce4a"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("c8c2d9d4-2769-40ba-94e5-b91e61e1d4b1"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("f70b66bb-f45f-4814-bf6c-409c8a57567e"));

            migrationBuilder.DeleteData(
                table: "Diretores",
                keyColumn: "IdDiretor",
                keyValue: new Guid("fccb7918-a6bc-4dcc-9847-dd8ac66243ec"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("005e2eda-be8a-4841-885d-04880609c9ff"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("159a8cc5-e7a0-44d4-af73-3c36d34cffb5"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("42c0113a-c4c8-449a-a978-a9b6835655b7"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("599af5ca-6695-46c8-9eb6-890610f1a4b5"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("5e4728a8-a493-4fa4-b2b8-4e4311e5c542"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("633daa69-a9ad-44a0-a52d-a966a867c9f2"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("7ec6ea91-982d-430c-b297-2b0c7e015df1"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("93309629-4d39-408f-a315-ce6fbc85016d"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("955c6be6-6bf2-4d38-80a0-ec42b0422222"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("f1736e0d-56ec-4d9e-b07f-4c0973df0890"));

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "IdFilme",
                keyValue: new Guid("f3d477d6-328c-43b9-9a67-d94bf0b22dd8"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("0e0f35f0-6492-4997-8e89-a09b2764de70"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("3aad9390-d91f-4e3e-999b-bb62d0a11c5f"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("5521b964-6912-4135-947d-29ac2fe51fc4"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("79b67c82-1c40-415b-a4db-d120975e4ef8"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("7aceb37e-faa3-4db5-af9a-116e05e624b7"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("8fa87d28-a8cc-4428-b46c-804a5a908770"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("91775029-6f11-40fa-9ce3-ed95d828eeb9"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("94279f95-a548-4fb9-a1e1-f120d08033e1"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("ac07bd90-3d0f-4c88-8c1d-70e80bde2227"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("c0063611-f252-40e0-95fe-f1f0cdd56d92"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("d3f7ff92-3467-497a-a569-b646593faffe"));

            migrationBuilder.DeleteData(
                table: "Generos",
                keyColumn: "IdGenero",
                keyValue: new Guid("fc2e89b4-d865-4b56-8310-9d798f851760"));

            migrationBuilder.DeleteData(
                table: "Usuarios",
                keyColumn: "IdUsuario",
                keyValue: new Guid("20305495-3f29-4722-82db-12ea87f208aa"));

            migrationBuilder.AlterColumn<string>(
                name: "NormalizedName",
                table: "IdentityRole",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "IdentityRole",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "ConcurrencyStamp",
                table: "IdentityRole",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddPrimaryKey(
                name: "PK_IdentityRole",
                table: "IdentityRole",
                column: "Id");

            migrationBuilder.InsertData(
                table: "Atores",
                columns: new[] { "IdAtor", "Nome" },
                values: new object[,]
                {
                    { new Guid("c3dbdd36-3883-4c92-9b2b-1a6d0157b371"), "Ben Burtt" },
                    { new Guid("959f3032-583a-4f4a-8af0-4dafa486a0d2"), "Woody Harrelson" },
                    { new Guid("cef61b6c-d17f-4516-aad4-3731f44f0dd5"), "Tom Sizemore" },
                    { new Guid("1d7b2e42-861e-46cf-b782-2663ac6b26ea"), "Robert Downey Jr." },
                    { new Guid("a05d47e7-0345-46dd-ac97-9dfb277927d9"), "Rachel McAdams" },
                    { new Guid("1b185a2b-1c1c-4c4e-a9ed-fe0af75e38f3"), "Paul Rhys" },
                    { new Guid("caae396f-6df8-40c3-986e-174683c42ce2"), "Patton Oswalt" },
                    { new Guid("3f47f108-b594-4603-b3be-29efe21f3eb6"), "Patrick Magee" },
                    { new Guid("ad89435d-216c-4176-bc58-dfba8b083297"), "Owen Wilson" },
                    { new Guid("850adc0f-dea4-40b7-8bb0-487a195c5a13"), "Mickey Rourke" },
                    { new Guid("a1328e16-bebf-4a68-8a8e-abf2ec560b61"), "Michael Bates" },
                    { new Guid("cb2b0816-fc44-4469-8af5-a22417ea57ca"), "Mark Ruffalo" },
                    { new Guid("4391d12e-4c16-4cab-a2db-8fb68f208f43"), "Malcolm McDowell" },
                    { new Guid("41e357f4-c626-497b-9e1f-3a928ac2b6c0"), "Laurence Fishburne" },
                    { new Guid("8bdb8b5d-591c-4a20-8ee6-7699c9e025eb"), "Keanu Reeves" },
                    { new Guid("67aa224d-587d-4c59-b417-af848d386389"), "Lou Romano" },
                    { new Guid("d13fe349-9ad9-40c4-9f99-084290d558a6"), "Jude Law" },
                    { new Guid("a7e3f202-23b8-415f-88f3-7db9a5ebf4aa"), "Brad Garrett" },
                    { new Guid("711fed8f-2780-4f6b-8224-7d2ad713fca5"), "Bruce Willis" },
                    { new Guid("19d86a9a-1d48-4377-92b2-887fd4272d5a"), "Carrie-Anne Moss" },
                    { new Guid("bd57c1a1-880f-4d6f-8e7a-be0fcc44c39d"), "Chris Evans" },
                    { new Guid("90573ce9-284d-422e-81a8-61eaa0d5b1d6"), "Juliette Lewis" },
                    { new Guid("425d1dbc-b714-41bf-aa83-c80f7b625ef2"), "Daniel Mays" },
                    { new Guid("ae508865-5ed7-4348-ab3c-8a7ba7fbf642"), "Dean-Charles Chapman" },
                    { new Guid("d7257033-435e-450d-85b1-e0ee7d2fa49d"), "Clive Owen" },
                    { new Guid("80e7e4f6-1826-4de0-8586-859d59e1e8f1"), "Eric Dane" },
                    { new Guid("5ac85176-bbcb-4c92-9a1d-a9eda2075c27"), "George MacKay" },
                    { new Guid("46b367d1-f3e6-42dc-ba92-d45e72e175f1"), "Geraldine Chaplin" },
                    { new Guid("57a3340e-2b19-4498-8376-c8858faee65b"), "Jeff Garlin" },
                    { new Guid("22fa8c08-9b3b-45ce-8dd3-bb1a7cf4ab80"), "Jennifer Aniston" },
                    { new Guid("be710d58-4487-4eb1-9d98-f87df6680abf"), "Elissa Knight" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("9279b751-61df-492d-a4ec-7101b69e5484"), "Andrew Stanton" },
                    { new Guid("625caa35-880c-4be8-9f69-34ae2231b7dc"), "Stanley Kubrick" },
                    { new Guid("56cc59f6-ced9-4e3b-8616-2869f56471b1"), "Oliver Stone" },
                    { new Guid("78d5ab9e-b8bc-40c2-ad00-fdc0a40df168"), "Guy Ritchie" },
                    { new Guid("f2840907-7326-4428-82f7-9a292c185b70"), "David Frankel" },
                    { new Guid("9f7d6454-9632-46d5-86e6-9277ae06cc18"), "Sam Mendes" },
                    { new Guid("48999599-a87f-4805-a79d-81c940d69f80"), "Joe Russo" },
                    { new Guid("52b687f1-3efd-4d65-a68d-910f27b3fc82"), "Richard Attenborough" },
                    { new Guid("8937215c-a99b-406f-861d-bb4318c5fbd7"), "Lilly Wachowski" },
                    { new Guid("18fefb92-933a-4306-9ded-652bb84ab7bf"), "Lana Wachowski" },
                    { new Guid("2fb5638e-83a1-4ef7-89c2-112c21325730"), "Jan Pinkava" }
                });

            migrationBuilder.InsertData(
                table: "Diretores",
                columns: new[] { "IdDiretor", "Nome" },
                values: new object[,]
                {
                    { new Guid("7394701e-d08a-4276-9e46-520126182b18"), "Brad Bird" },
                    { new Guid("87d68089-9d9f-4a1e-b8e3-56b63a8a68cc"), "Robert Rodriguez" },
                    { new Guid("6ffe17a7-04d6-438b-8312-b678c7c7372c"), "Anthony Russo" },
                    { new Guid("824ee794-8952-4dda-b182-4ed26c2d6000"), "Quentin Tarantino" },
                    { new Guid("1a7d0f8d-51a6-43c0-948e-af475b278da8"), "Frank Miller" }
                });

            migrationBuilder.InsertData(
                table: "Filmes",
                columns: new[] { "IdFilme", "Ano", "Nome", "NomeOriginal", "Sinopse" },
                values: new object[,]
                {
                    { new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f"), 1999, "Matrix", "The Matrix", "Um hacker aprende com os misteriosos rebeldes sobre a verdadeira natureza de sua realidade e seu papel na guerra contra seus controladores." },
                    { new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8"), 2007, "Ratatouille", "Ratatouille", "Um rato que pode cozinhar forja uma aliança incomum com um jovem garoto em um famoso restaurante em Paris." },
                    { new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9"), 2008, "Marley & Eu", "Marley & Me", "Uma família aprende importantes lições de vida com seu adorável, mas malicioso e neurótico cão." },
                    { new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8"), 2008, "WALL·E", "WALL·E", "Num futuro distante, um pequeno robô faz uma viagem que decidirá o destino da humanidade." },
                    { new Guid("579a7f05-52a0-468f-8f65-880b96eee634"), 2019, "Vingadores: Ultimato", "Avengers: Endgame", "Após os eventos devastadores de Vingadores: Guerra Infinita , o universo está em ruínas, e com a ajuda de aliados os Vingadores se reúnem para desfazer as ações de Thanos e restaurar a ordem." },
                    { new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef"), 2005, "Sin City: A Cidade do Pecado", "Sin City", "O filme explora a miserável cidade de Basin City, e conta a historia de três pessoas diferentes, envoltas em violencia e corrupção." },
                    { new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6"), 1994, "Assassinos por Natureza", "Natural Born Killers", "Duas vítimas de uma infância traumatizada tornam-se amantes e assassinos em série, irresponsavelmente glorificados pela mídia." },
                    { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), 1992, "Chaplin", "Chaplin", "Relata a vida convulsiva e controversa do mestre diretor de comédia Charles Chaplin." },
                    { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), 2009, "Sherlock Holmes", "Sherlock Holmes", "O detective Sherlock Holmes e seu colega Watson se engajam numa batalha para lutar contra as ameaças da Inglaterra." },
                    { new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac"), 1971, "Laranja Mecânica", "A Clockwork Orange", "No futuro, um líder duma turma é levado para prisão é decide ser voluntário dum experimento, mais os resultados não são os esperados." },
                    { new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b"), 2019, "1917", "1917", "6 de Abril de 1917. Enquanto um regimento se reúne para lutar uma guerra nas profundezas do território inimigo, dois soldados são designados para uma corrida contra o tempo para entregar uma mensagem que impedirá 1.600 homens de caminhar direto para uma uma armadilha mortal." }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("bebf1b0c-7623-4fe5-a44c-749c10027b15"), "Fantasia científica" },
                    { new Guid("33f657c4-4852-4a70-9ba5-5abb9d86d685"), "Ficção científica" },
                    { new Guid("2fc7d867-54c7-4de9-bd6e-3c3927c85bb7"), "Filmes com truques" },
                    { new Guid("155eff2e-8452-4cae-84ca-2d4cfc2ffe0e"), "Filmes de guerra" },
                    { new Guid("7f98d99e-9ac8-4c39-ba96-9712d715b207"), "Musical" },
                    { new Guid("1c27db3d-0235-4f8b-965e-96dcb3b61022"), "Pornográfico" },
                    { new Guid("9fe2a5e6-b62f-45e6-88b1-a3a6387695bf"), "Romance" },
                    { new Guid("f095cc29-9a7b-4428-b79b-81f0c92baa68"), "Seriado" },
                    { new Guid("52a1cd09-b97d-48fe-8f22-35eb7aaff9cd"), "Suspense" },
                    { new Guid("53502d72-ac6d-4056-b978-e7246295ce75"), "Terror" },
                    { new Guid("ff4051ca-a8ae-4c93-bfda-1c269b059b11"), "Thriller" },
                    { new Guid("db6ae3f5-908d-4adc-90fb-286035889d13"), "Fantasia" },
                    { new Guid("fad75e01-bfee-4aa8-9221-14055c809b25"), "Filme policial" },
                    { new Guid("0ae399c4-4066-45ea-b2f2-2a27ddd82ba4"), "Faroeste" },
                    { new Guid("b6ec13a3-a47b-49fa-99ae-a33fe584a813"), "Comédia de ação" },
                    { new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de"), "Drama" },
                    { new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1"), "Ação" },
                    { new Guid("21a3251e-2d7c-42fd-a999-8328ebc70504"), "Animação" },
                    { new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4"), "Aventura" },
                    { new Guid("9917be90-d12e-4cf4-b6fd-c9b0b01f3e43"), "Cinema de arte" },
                    { new Guid("bf64dbb0-b3f5-47fd-b677-02479b206382"), "Espionagem" },
                    { new Guid("fae855e4-76fc-494a-b664-4ec68970d47e"), "Comédia" },
                    { new Guid("22fdb2b4-13c0-4749-8492-e2ed1718a156"), "Chanchada" },
                    { new Guid("8c75f953-6cc4-42ca-a281-06bd5dc4742b"), "Comédia dramática" },
                    { new Guid("cd83d03a-560a-4e09-b8e3-852651a92f7a"), "Comédia romântica" },
                    { new Guid("9c90db90-1179-4d43-9c97-e6065a2c9374"), "Dança" }
                });

            migrationBuilder.InsertData(
                table: "Generos",
                columns: new[] { "IdGenero", "Nome" },
                values: new object[,]
                {
                    { new Guid("da7b1bf9-7d9d-4116-9349-3bcfd88670ee"), "Documentário" },
                    { new Guid("41b135c6-3041-4bd7-9960-420721ce39a1"), "Docuficção" },
                    { new Guid("d0ce6295-d3fc-4079-be42-2f436f44f369"), "Comédia de terror" }
                });

            migrationBuilder.InsertData(
                table: "IdentityRole",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "c26c9aeb-9e36-4a80-a203-8ea029806770", "69d1e41b-76e5-4357-b833-69bbe9f56cbe", "Admin", "ADMIN" },
                    { "8c236128-42d1-4111-aee5-738875a1e90e", "805a6d80-71bf-4567-8c61-e5808f6c5191", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "Usuarios",
                columns: new[] { "IdUsuario", "Email", "HashSenha", "IdRole", "Nome", "SaltSenha" },
                values: new object[] { new Guid("dc6fce33-243e-4d4c-8328-1eda6867c3ea"), "rafael.av@gmail.com", new byte[] { 236, 184, 104, 81, 157, 230, 63, 48, 241, 97, 214, 66, 125, 133, 229, 206, 22, 218, 74, 116, 48, 168, 198, 210, 119, 178, 91, 27, 119, 118, 111, 72, 117, 76, 163, 9, 171, 38, 158, 191, 21, 45, 9, 10, 188, 34, 3, 8, 18, 226, 19, 141, 81, 83, 115, 57, 84, 129, 37, 45, 94, 76, 23, 141 }, new Guid("c26c9aeb-9e36-4a80-a203-8ea029806770"), "Rafael Vasconcelos", new byte[] { 10, 33, 223, 201, 237, 101, 78, 185, 226, 2, 37, 191, 170, 226, 151, 133, 0, 46, 117, 186, 154, 214, 221, 160, 120, 24, 83, 32, 166, 82, 239, 52, 248, 7, 52, 211, 240, 55, 18, 215, 104, 78, 27, 244, 71, 135, 53, 231, 14, 184, 34, 27, 84, 14, 216, 231, 35, 50, 109, 67, 87, 100, 11, 26, 119, 223, 89, 213, 253, 182, 16, 162, 153, 184, 3, 48, 64, 250, 127, 74, 151, 6, 100, 31, 184, 197, 120, 245, 8, 249, 197, 7, 150, 74, 184, 70, 17, 120, 84, 94, 228, 214, 221, 38, 219, 80, 247, 238, 208, 225, 81, 223, 223, 179, 40, 96, 170, 73, 165, 142, 233, 67, 22, 54, 24, 112, 73, 227 } });

            migrationBuilder.InsertData(
                table: "AtoresFilmes",
                columns: new[] { "IdAtor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("4391d12e-4c16-4cab-a2db-8fb68f208f43"), new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac") },
                    { new Guid("cb2b0816-fc44-4469-8af5-a22417ea57ca"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") },
                    { new Guid("c3dbdd36-3883-4c92-9b2b-1a6d0157b371"), new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8") },
                    { new Guid("be710d58-4487-4eb1-9d98-f87df6680abf"), new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8") },
                    { new Guid("57a3340e-2b19-4498-8376-c8858faee65b"), new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8") },
                    { new Guid("ad89435d-216c-4176-bc58-dfba8b083297"), new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9") },
                    { new Guid("22fa8c08-9b3b-45ce-8dd3-bb1a7cf4ab80"), new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9") },
                    { new Guid("80e7e4f6-1826-4de0-8586-859d59e1e8f1"), new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9") },
                    { new Guid("850adc0f-dea4-40b7-8bb0-487a195c5a13"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") },
                    { new Guid("d7257033-435e-450d-85b1-e0ee7d2fa49d"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") },
                    { new Guid("711fed8f-2780-4f6b-8224-7d2ad713fca5"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") },
                    { new Guid("a7e3f202-23b8-415f-88f3-7db9a5ebf4aa"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") },
                    { new Guid("67aa224d-587d-4c59-b417-af848d386389"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") },
                    { new Guid("8bdb8b5d-591c-4a20-8ee6-7699c9e025eb"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") },
                    { new Guid("41e357f4-c626-497b-9e1f-3a928ac2b6c0"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") },
                    { new Guid("19d86a9a-1d48-4377-92b2-887fd4272d5a"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") },
                    { new Guid("bd57c1a1-880f-4d6f-8e7a-be0fcc44c39d"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") },
                    { new Guid("1d7b2e42-861e-46cf-b782-2663ac6b26ea"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") },
                    { new Guid("caae396f-6df8-40c3-986e-174683c42ce2"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") },
                    { new Guid("3f47f108-b594-4603-b3be-29efe21f3eb6"), new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac") },
                    { new Guid("a05d47e7-0345-46dd-ac97-9dfb277927d9"), new Guid("439f33f5-1289-4030-9f3e-2ce25134c415") },
                    { new Guid("1d7b2e42-861e-46cf-b782-2663ac6b26ea"), new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a") },
                    { new Guid("46b367d1-f3e6-42dc-ba92-d45e72e175f1"), new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a") },
                    { new Guid("1b185a2b-1c1c-4c4e-a9ed-fe0af75e38f3"), new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a") },
                    { new Guid("a1328e16-bebf-4a68-8a8e-abf2ec560b61"), new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac") },
                    { new Guid("1d7b2e42-861e-46cf-b782-2663ac6b26ea"), new Guid("439f33f5-1289-4030-9f3e-2ce25134c415") },
                    { new Guid("959f3032-583a-4f4a-8af0-4dafa486a0d2"), new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6") },
                    { new Guid("d13fe349-9ad9-40c4-9f99-084290d558a6"), new Guid("439f33f5-1289-4030-9f3e-2ce25134c415") },
                    { new Guid("cef61b6c-d17f-4516-aad4-3731f44f0dd5"), new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6") },
                    { new Guid("425d1dbc-b714-41bf-aa83-c80f7b625ef2"), new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b") },
                    { new Guid("ae508865-5ed7-4348-ab3c-8a7ba7fbf642"), new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b") },
                    { new Guid("5ac85176-bbcb-4c92-9a1d-a9eda2075c27"), new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b") },
                    { new Guid("90573ce9-284d-422e-81a8-61eaa0d5b1d6"), new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("18fefb92-933a-4306-9ded-652bb84ab7bf"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") },
                    { new Guid("8937215c-a99b-406f-861d-bb4318c5fbd7"), new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f") },
                    { new Guid("52b687f1-3efd-4d65-a68d-910f27b3fc82"), new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac") },
                    { new Guid("2fb5638e-83a1-4ef7-89c2-112c21325730"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") },
                    { new Guid("7394701e-d08a-4276-9e46-520126182b18"), new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8") },
                    { new Guid("9f7d6454-9632-46d5-86e6-9277ae06cc18"), new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b") },
                    { new Guid("1a7d0f8d-51a6-43c0-948e-af475b278da8"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") },
                    { new Guid("78d5ab9e-b8bc-40c2-ad00-fdc0a40df168"), new Guid("439f33f5-1289-4030-9f3e-2ce25134c415") },
                    { new Guid("f2840907-7326-4428-82f7-9a292c185b70"), new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9") }
                });

            migrationBuilder.InsertData(
                table: "DiretoresFilmes",
                columns: new[] { "IdDiretor", "IdFilme" },
                values: new object[,]
                {
                    { new Guid("52b687f1-3efd-4d65-a68d-910f27b3fc82"), new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a") },
                    { new Guid("9279b751-61df-492d-a4ec-7101b69e5484"), new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8") },
                    { new Guid("48999599-a87f-4805-a79d-81c940d69f80"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") },
                    { new Guid("6ffe17a7-04d6-438b-8312-b678c7c7372c"), new Guid("579a7f05-52a0-468f-8f65-880b96eee634") },
                    { new Guid("56cc59f6-ced9-4e3b-8616-2869f56471b1"), new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6") },
                    { new Guid("824ee794-8952-4dda-b182-4ed26c2d6000"), new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef") }
                });

            migrationBuilder.InsertData(
                table: "GenerosFilmes",
                columns: new[] { "IdFilme", "IdGenero" },
                values: new object[,]
                {
                    { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") },
                    { new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") },
                    { new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") },
                    { new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") },
                    { new Guid("579a7f05-52a0-468f-8f65-880b96eee634"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") },
                    { new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac"), new Guid("33f657c4-4852-4a70-9ba5-5abb9d86d685") },
                    { new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef"), new Guid("ff4051ca-a8ae-4c93-bfda-1c269b059b11") },
                    { new Guid("9e732756-c2b8-4c49-83b1-428dc246eb5b"), new Guid("155eff2e-8452-4cae-84ca-2d4cfc2ffe0e") },
                    { new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac"), new Guid("fad75e01-bfee-4aa8-9221-14055c809b25") },
                    { new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6"), new Guid("fad75e01-bfee-4aa8-9221-14055c809b25") },
                    { new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef"), new Guid("fad75e01-bfee-4aa8-9221-14055c809b25") },
                    { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), new Guid("52a1cd09-b97d-48fe-8f22-35eb7aaff9cd") },
                    { new Guid("db266aa0-b5a1-4a37-9b84-879b18c52cac"), new Guid("d2b07cc0-beba-48a8-9aff-6eb984ed69de") },
                    { new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f"), new Guid("33f657c4-4852-4a70-9ba5-5abb9d86d685") },
                    { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), new Guid("41b135c6-3041-4bd7-9960-420721ce39a1") },
                    { new Guid("9220d3b3-a0e0-4dad-baf5-a40e1561d9d6"), new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1") },
                    { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), new Guid("8c75f953-6cc4-42ca-a281-06bd5dc4742b") },
                    { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1") },
                    { new Guid("579a7f05-52a0-468f-8f65-880b96eee634"), new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1") },
                    { new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f"), new Guid("7331f81d-e0bd-4e6d-90c7-4e572cd3d6a1") },
                    { new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8"), new Guid("21a3251e-2d7c-42fd-a999-8328ebc70504") },
                    { new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9"), new Guid("8c75f953-6cc4-42ca-a281-06bd5dc4742b") },
                    { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4") },
                    { new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8"), new Guid("21a3251e-2d7c-42fd-a999-8328ebc70504") },
                    { new Guid("fda9ebe3-58f6-4388-b5af-e5736e318ab8"), new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4") },
                    { new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8"), new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4") },
                    { new Guid("2d0cfc5f-527b-421e-9f40-81e910c4035a"), new Guid("fae855e4-76fc-494a-b664-4ec68970d47e") },
                    { new Guid("a4c233d8-4aeb-4035-a05c-d509d66e60e9"), new Guid("fae855e4-76fc-494a-b664-4ec68970d47e") },
                    { new Guid("3d9339ec-e898-4afc-b38e-c90d34a7bde8"), new Guid("fae855e4-76fc-494a-b664-4ec68970d47e") },
                    { new Guid("579a7f05-52a0-468f-8f65-880b96eee634"), new Guid("812ba93e-03a0-4a1a-96fc-1b84a26febd4") }
                });

            migrationBuilder.InsertData(
                table: "Votos",
                columns: new[] { "IdFilme", "IdUsuario", "Nota" },
                values: new object[,]
                {
                    { new Guid("1926d086-0f0e-47f7-a8f8-bbf43ae2c3ef"), new Guid("dc6fce33-243e-4d4c-8328-1eda6867c3ea"), (byte)3 },
                    { new Guid("439f33f5-1289-4030-9f3e-2ce25134c415"), new Guid("dc6fce33-243e-4d4c-8328-1eda6867c3ea"), (byte)1 },
                    { new Guid("ddeba7f0-15c2-4aa5-9a13-f4d9a662aa3f"), new Guid("dc6fce33-243e-4d4c-8328-1eda6867c3ea"), (byte)4 }
                });
        }
    }
}
