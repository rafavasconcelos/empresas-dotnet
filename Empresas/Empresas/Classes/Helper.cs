﻿using System.Text.RegularExpressions;
using Empresas.Database;
using Empresas.Models.Filmes;
using Empresas.Models.Shared;
using Empresas.Models.Usuarios;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net.Mail;

namespace Empresas
{
    public enum ForcaSenha
    {
        EmBranco = 0,
        MuitoFraca = 1,
        Fraca = 2,
        Razoavel = 3,
        Forte = 4,
        MuitoForte = 5
    }

    public static class Helper
    {
        public static void ChecarUsuarioAutenticado(this System.Security.Claims.ClaimsPrincipal user, IEmpresasContext contexto, out Usuario usuario, out string idClaim, out string emailClaim, out Guid idRoleAdmin, bool checkAdmin = false)
        {
            string id;
            Guid idRoleAdm = !checkAdmin ? Guid.NewGuid() : new Guid(contexto.Roles.Where(r => r.Name.ToLower().Contains("admin")).Select(r => r.Id).First());

            // Obter claims
            id          = user.Claims.Where(c => c.Type.Contains("Id")).Select(c => c.Value).FirstOrDefault();
            emailClaim  = user.Claims.Where(c => c.Type.Contains("Email")).Select(c => c.Value).FirstOrDefault();
            idClaim     = id;
            idRoleAdmin = idRoleAdm;

            if (string.IsNullOrEmpty(idClaim) || string.IsNullOrEmpty(emailClaim))
                throw new UnauthorizedAccessException();

            // Pegar dados do usuário autenticado atualmente através do Claim ID
            usuario = contexto.QueryUsuario()
                              .Where(u => u.IdUsuario == new Guid(id))
                              .FirstOrDefault();

            if (usuario == null || (usuario.IdRole != idRoleAdm && checkAdmin))
                throw new UnauthorizedAccessException();
        }

        public static IQueryable<Usuario> QueryUsuario(this IEmpresasContext _context, bool buscaInativo = false)
        {
            var query = _context.Usuarios.Where(f => f.Inativo == buscaInativo)
                                         .AsQueryable();
            return query;
        }

        public static IQueryable<Filme> QueryFilmes(this IEmpresasContext _context, FiltroPaginacaoFilme filtro, bool buscaInativo = false)
        {
            var query = _context.Filme.Include(af => af.AtoresFilme)
                                      .ThenInclude(a => a.Ator)
                                      .Include(df => df.DiretoresFilme)
                                      .ThenInclude(d => d.Diretor)
                                      .Include(gf => gf.GenerosFilme)
                                      .ThenInclude(g => g.Genero)
                                      .Include(v => v.Votos)
                                      .ThenInclude(u => u.Usuario)
                                      .Where(f => f.Inativo == buscaInativo)
                                      .AsQueryable();

            if (!string.IsNullOrEmpty(filtro.NomeFilme))
            {
                query = query.Where(f => f.Nome.ToLower().Contains(filtro.NomeFilme.ToLower()) || f.NomeOriginal.ToLower().Contains(filtro.NomeFilme.ToLower()));
            }

            if (filtro.Atores.Length > 0)
            {
                foreach (string ator in filtro.Atores)
                {
                    query = from   f in query
                            let    atores = f.AtoresFilme.Where(af => af.Ator.Nome.ToLower().Contains(ator))
                            where  atores.Any()
                            select f;
                }
            }

            if (filtro.Diretores.Length > 0)
            {
                foreach (string diretor in filtro.Diretores)
                {
                    query = from   f in query
                            let    diretores = f.DiretoresFilme.Where(df => df.Diretor.Nome.ToLower().Contains(diretor))
                            where  diretores.Any()
                            select f;
                }
            }

            if (filtro.Generos.Length > 0)
            {
                foreach (string genero in filtro.Generos)
                {
                    query = from   f in query
                            let    generos = f.GenerosFilme.Where(gf => gf.Genero.Nome.ToLower().Contains(genero))
                            where  generos.Any()
                            select f;
                }
            }

            return query;
        }

        public static IQueryable<Filme> OrdenarEPaginarFilmes(this IQueryable<Filme> filmes, FiltroPaginacaoFilme filtro)
        {
            return filmes.OrderByDescending(f => f.Votos.Count(v => v.IdFilme == f.IdFilme))
                         .ThenBy(f => f.Nome)
                         .Skip((filtro.NumeroPagina - 1) * filtro.TamanhoPagina)
                         .Take(filtro.TamanhoPagina);
        }

        public static IQueryable<Usuario> OrdenarEPaginarUsuario(this IQueryable<Usuario> usuarios, FiltroPaginacao filtro)
        {
            return usuarios.OrderBy(u => u.Nome)
                           .Skip((filtro.NumeroPagina - 1) * filtro.TamanhoPagina)
                           .Take(filtro.TamanhoPagina);
        }

        public static ForcaSenha MedirForcaSenha( this string senha)
        {
            int pontos = 0;

            if (senha.Length < 1)
                return ForcaSenha.EmBranco;

            if (senha.Length < 8)
                return ForcaSenha.MuitoFraca;

            if (senha.Length >= 10)
                pontos++;

            if (senha.Length >= 14)
                pontos++;

            if (Regex.Match(senha, @"(?=.*[0-9])", RegexOptions.ECMAScript).Success)
                pontos++;

            if (Regex.Match(senha, @"(?=.*[a-z])", RegexOptions.ECMAScript).Success
            &&  Regex.Match(senha, @"(?=.*[A-Z])", RegexOptions.ECMAScript).Success)
                pontos++;

            if (Regex.Match(senha, @"(?=.*[!@#$%^&*])", RegexOptions.ECMAScript).Success)
                pontos++;

            return (ForcaSenha)pontos;
        }

        public static bool EmailValido(this string email)
        {
            try
            {
                MailAddress m = new MailAddress(email);
                return m != null;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}