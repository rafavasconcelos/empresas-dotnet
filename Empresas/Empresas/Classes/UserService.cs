﻿using Empresas.Database;
using Empresas.Models.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Empresas.Classes
{
    public class UserService : IUserService
    {
        private readonly IEmpresasContext _contexto;

        public UserService(IEmpresasContext contexto)
        {
            _contexto = contexto;
        }

        public Usuario Autenticar(string email, string senha)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(senha))
                return null;

            var usuario = _contexto.Usuarios.SingleOrDefault(x => x.Email == email);

            if (usuario == null)
                return null;

            if (!VerificarHashSenha(senha, usuario.HashSenha, usuario.SaltSenha))
                return null;

            return usuario;
        }

        public IEnumerable<Usuario> ObterTodos()
        {
            return _contexto.Usuarios;
        }

        public Usuario ObterPorId(Guid id)
        {
            return _contexto.Usuarios.Find(id);
        }

        public Usuario ObterPorNome(string nome)
        {
            return _contexto.Usuarios.Where(u => u.Nome.ToLower() == nome.ToLower()).FirstOrDefault();
        }

        public Usuario Criar(Usuario usuario, string senha)
        {
            if (string.IsNullOrWhiteSpace(senha))
                throw new ArgumentException("Senha obrigatória");

            if (_contexto.Usuarios.Any(x => x.Email == usuario.Email))
                throw new ArgumentException($"O login \"{usuario.Email}\" já está em uso");

            byte[] hashSenha, saltSenha;
            CriarHashSenha(senha, out hashSenha, out saltSenha);

            usuario.HashSenha = hashSenha;
            usuario.SaltSenha = saltSenha;

            _contexto.Usuarios.Add(usuario);
            _contexto.SaveChanges();

            return usuario;
        }

        public void Atualizar(Usuario usuario, string senha = null)
        {
            var user = _contexto.Usuarios.Find(usuario.IdUsuario);

            if (user == null)
                throw new ArgumentException("Usuário não encontrado");

            if (!string.IsNullOrWhiteSpace(usuario.Email) && usuario.Email != user.Email)
            {
                if (_contexto.Usuarios.Any(x => x.Email == usuario.Email))
                    throw new ArgumentException($"O login {usuario.Email} já está em uso");
            
                user.Email = usuario.Email;
            }

            if (!string.IsNullOrWhiteSpace(usuario.Nome))
                user.Nome = usuario.Nome;

            if (!string.IsNullOrWhiteSpace(senha))
            {
                byte[] passwordHash, passwordSalt;
                CriarHashSenha(senha, out passwordHash, out passwordSalt);

                user.HashSenha = passwordHash;
                user.SaltSenha = passwordSalt;
            }

            _contexto.Usuarios.Update(user);
            _contexto.SaveChanges();
        }

        public void Excluir(Guid id)
        {
            var user = _contexto.Usuarios.Find(id);
            if (user != null)
            {
                _contexto.Usuarios.Remove(user);
                _contexto.SaveChanges();
            }
        }

        private static void CriarHashSenha(string senha, out byte[] hashSenha, out byte[] saltSenha)
        {
            if (senha == null) throw new ArgumentNullException("senha");
            if (string.IsNullOrWhiteSpace(senha)) throw new ArgumentException("O valor não pode ser vazio ou ser uma string somente de espaços em branco.", "senha");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                saltSenha = hmac.Key;
                hashSenha = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(senha));
            }
        }

        private static bool VerificarHashSenha(string senha, byte[] hashArmazenado, byte[] saltArmazenado)
        {
            if (senha == null) throw new ArgumentNullException("senha");
            if (string.IsNullOrWhiteSpace(senha)) throw new ArgumentException("O valor não pode ser vazio ou ser uma string somente de espaços em branco.", "senha");
            if (hashArmazenado.Length != 64) throw new ArgumentException("Cumprimento do hash de senha inválido (esperado 64 bytes).", "hashArmazenado");
            if (saltArmazenado.Length != 128) throw new ArgumentException("Cumprimento do salt de senha inválido (esperado 128 bytes).", "saltArmazenado");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(saltArmazenado))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(senha));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != hashArmazenado[i]) return false;
                }
            }

            return true;
        }
    }
}