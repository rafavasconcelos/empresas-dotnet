﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Empresas.Classes;
using Empresas.Database;
using Empresas.Models.Shared;
using Empresas.Models.Usuarios;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Empresas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class TokenController : ControllerBase
    {
        private readonly IConfiguration   _config;
        private readonly IEmpresasContext _contexto;
        private readonly IUserService     _userService;

        public TokenController(IConfiguration config, IEmpresasContext contexto, IUserService userService)
        {
            _config      = config;
            _contexto    = contexto;
            _userService = userService;
        }

        [HttpPost]
        public IActionResult Post(UsuarioLoginDto _usuarioLogin)
        {

            if (_usuarioLogin       == null || 
                _usuarioLogin.Email == null || 
                _usuarioLogin.Senha == null)
            {
                return BadRequest();
            }

            var usuario = _userService.Autenticar(_usuarioLogin.Email, _usuarioLogin.Senha);

            if (usuario == null)
                return BadRequest(new Resposta<string>("Credenciais com problemas."));

            //var role = _contexto.UsuariosRoles.Where(ur => ur.)
            var role = _contexto.Roles.Where(ur => ur.Id == usuario.IdRole.ToString()).FirstOrDefault();
            if (role == null)
                return BadRequest(new Resposta<string>("Credenciais com problemas."));
            

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, _config["Jwt:Assunto"]),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                new Claim("Id", usuario.IdUsuario.ToString()),
                new Claim("Nome", usuario.Nome),
                new Claim("Email", usuario.Email),
                new Claim("Role", role.Name)
            };

            var      key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Chave"]));
            var   signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            DateTime cre = DateTime.UtcNow,
                     exp = cre.AddDays(1);
            var    token = new JwtSecurityToken(_config["Jwt:Emissor"], _config["Jwt:Audiencia"], claims, expires: exp, signingCredentials: signIn);
            
            return Ok(new Resposta<TokenDto>(new TokenDto() { Token = new JwtSecurityTokenHandler().WriteToken(token), Criacao = cre, Validade = exp }));
        }
    }
}
