﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Empresas.Database;
using Empresas.Models.Filmes;
using Empresas.Models.Shared;
using Empresas.Models.Usuarios;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Empresas.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FilmesController : ControllerBase
    {

        #region Métodos privados, construtor e objetos
        private readonly IEmpresasContext _contexto;

        public FilmesController(IEmpresasContext contexto)
        {
            _contexto = contexto;
        }

        private IEnumerable<FilmeDto> ToDTO(IEnumerable<Filme> filmes)
        {
            List<FilmeDto> filmesDTO = new List<FilmeDto>();
            foreach (Filme filme in filmes)
            {
                filmesDTO.Add(new FilmeDto(filme));
            }
            return filmesDTO.AsEnumerable();
        }
        #endregion

        // GET: api/Filmes
        // GET: api/Filmes?Atores=robert&Atores=evans
        // GET: api/Filmes?numeroPagina=1&tamanhoPagina=5&Atores=keanu&Diretores=wachowski&Generos=ação
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Filme>>> GetFilme([FromQuery] FiltroPaginacaoFilme filtro)
        {
            var filtroValido = new FiltroPaginacaoFilme(filtro.NumeroPagina, filtro.TamanhoPagina, filtro.NomeFilme, filtro.Atores, filtro.Diretores, filtro.Generos);

            var dados = _contexto.QueryFilmes(filtroValido);

            var dadosPaginados = await dados.OrdenarEPaginarFilmes(filtroValido).ToListAsync();

            var totalRegistros = await dados.CountAsync();

            var resposta = new RespostaPaginada<IEnumerable<FilmeDto>>(ToDTO(dadosPaginados), filtroValido.NumeroPagina, filtroValido.TamanhoPagina)
            {
                TotalRegistros = totalRegistros,
                TotalPaginas = Convert.ToInt32(Math.Ceiling(((double)totalRegistros / (double)filtroValido.TamanhoPagina)))
            };

            return Ok(resposta);
        }

        // GET: api/Filmes/8DE1FA84-7A57-4F9A-9BE9-1E918B634492
        [HttpGet("{id}")]
        public async Task<ActionResult<Filme>> GetFilme(Guid id)
        {
            var filtroValido = new FiltroPaginacaoFilme();

            var dados = _contexto.QueryFilmes(filtroValido).Where(f => f.IdFilme == id);

            var dadosPaginados = await dados.OrdenarEPaginarFilmes(filtroValido).ToListAsync();

            var totalRegistros = await dados.CountAsync();

            var resposta = new RespostaPaginada<IEnumerable<FilmeDto>>(ToDTO(dadosPaginados), filtroValido.NumeroPagina, filtroValido.TamanhoPagina)
            {
                TotalRegistros = totalRegistros,
                TotalPaginas = Convert.ToInt32(Math.Ceiling(((double)totalRegistros / (double)filtroValido.TamanhoPagina)))
            };

            if (!resposta.Dados.Any())
            {
                return NotFound();
            }

            return Ok(resposta);
        }

        // POST: api/Filmes/Votar
        [Authorize]
        [HttpPost("Votar")]
        public async Task<ActionResult<Filme>> Votar(VotacaoFilmeDto votacaoFilme)
        {
            Usuario usuario;
            string idClaim, emailClaim;
            Guid idAdmin;

            try
            {
                User.ChecarUsuarioAutenticado(_contexto, out usuario, out idClaim, out emailClaim, out idAdmin);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            if (usuario != null)
            {
                var filtroValido = new FiltroPaginacaoFilme();
                var filme        = await _contexto.QueryFilmes(filtroValido)
                                                  .Where(f => f.IdFilme == votacaoFilme.IdFilme)
                                                  .ToListAsync();
                if (filme.Any())
                {
                    Voto voto = new Voto()
                    {
                        IdFilme   = filme.First().IdFilme,
                        IdUsuario = usuario.IdUsuario,
                        Nota      = votacaoFilme.Nota
                    };
                    var resultados = new List<ValidationResult>();
                    if (!Validator.TryValidateObject(voto, new ValidationContext(voto, serviceProvider: null, items: null), resultados, true))
                    {
                        return ValidationProblem(string.Join(",", resultados));
                    }
                    else
                    {
                        var votoExistente = await _contexto.Votos.Where(v => v.IdFilme == voto.IdFilme
                                                                          && v.IdUsuario == voto.IdUsuario)
                                                                 .ToListAsync();

                        if (votoExistente != null && votoExistente.Count == 0)
                            _contexto.Votos.Add(voto);
                        else
                        {
                            _contexto.Entry(voto).State = EntityState.Modified;
                            _contexto.Votos.Update(voto);
                        }

                        await _contexto.SaveChangesAsync();

                        return await GetFilme(voto.IdFilme);
                    }
                }
                else
                    return ValidationProblem("Filme não encontrado.");
            }
            else
                return Unauthorized(new Resposta<string>("Problemas com autorização."));
        }

        // POST: api/Filmes
        [HttpPost]
        public async Task<ActionResult<Filme>> PostFilme(InputFilmeDto dadosFilme)
        {
            Guid idUsuarioAutenticado = new Guid(User.Claims.Where(c => c.Type.ToLower() == "id").Select(c => c.Value).FirstOrDefault());
            string roleUsuario = User.Claims.Where(c => c.Type.Contains("Role")).Select(c => c.Value).FirstOrDefault();

            var usuario = await _contexto.QueryUsuario()
                                         .Where(u => u.IdUsuario == idUsuarioAutenticado)
                                         .FirstOrDefaultAsync();

            if (usuario == null || (!string.IsNullOrEmpty(roleUsuario) && !roleUsuario.ToLower().Contains("admin")))
            {
                return Unauthorized();
            }

            var contexto = new ValidationContext(dadosFilme, serviceProvider: null, items: null);
            var resultados = new List<ValidationResult>();
            bool valido = Validator.TryValidateObject(dadosFilme, contexto, resultados, true);
            if (!valido)
                return ValidationProblem(string.Join(",", resultados));
            else
            {
                var filtroValido = new FiltroPaginacaoFilme();
                var filmeExistente = await _contexto.QueryFilmes(filtroValido)
                                                    .Where(f => (f.Nome.ToLower().Replace(" ","") == dadosFilme.Nome.ToLower().Replace(" ", "") 
                                                             || f.NomeOriginal.ToLower().Replace(" ", "") == dadosFilme.NomeOriginal.ToLower().Replace(" ", ""))
                                                             && f.Ano == dadosFilme.Ano)
                                                    .ToListAsync();
                if (filmeExistente != null && filmeExistente.Count > 0)
                {
                    return ValidationProblem("O filme que você está tentando cadastrar já existe no catálogo.");
                }

                Filme filme = new Filme();
                List<Ator> atores = new List<Ator>();
                List<Diretor> diretores = new List<Diretor>();
                List<Genero> generos = new List<Genero>();
                Regex regexGuid = new Regex(@"(?im)^[{(]?[0-9A-F]{8}[-]?(?:[0-9A-F]{4}[-]?){3}[0-9A-F]{12}[)}]?$");
                
                foreach (string ator in dadosFilme.Atores)
                {
                    Guid id;
                    // Procurar ator por Id, caso um ID válido tenha sido enviado; ou tentar encontrar um ator através da "assinatura" do nome (letras minúsculas sem espaço)
                    Ator dadosAtor = _contexto.Atores.Where(a => (Guid.TryParse(ator, out id) ? a.IdAtor == id : a.Nome.ToLower().Replace(" ", "") == ator.ToLower().Replace(" ", ""))).FirstOrDefault();
                    if (dadosAtor == null)
                    {
                        if (regexGuid.Matches(ator).Count > 0)
                        {
                            return ValidationProblem($"O ID do ator que você inseriu ({ator}) não parece ser válido.");
                        }
                        else
                        {
                            _contexto.Atores.Add(new Ator() { Nome = ator });
                            _contexto.SaveChanges();
                            dadosAtor = _contexto.Atores.Where(a => a.Nome == ator).FirstOrDefault();
                        }
                    }
                    atores.Add(dadosAtor);
                }

                foreach (string diretor in dadosFilme.Diretores)
                {
                    Guid id;
                    // Procurar diretor por Id, caso um ID válido tenha sido enviado; ou tentar encontrar um diretor através da "assinatura" do nome (letras minúsculas sem espaço)
                    Diretor dadosDiretor = _contexto.Diretores.Where(d => (Guid.TryParse(diretor, out id) ? d.IdDiretor == id : d.Nome.ToLower().Replace(" ", "") == diretor.ToLower().Replace(" ", ""))).FirstOrDefault();
                    if (dadosDiretor == null)
                    {
                        if (regexGuid.Matches(diretor).Count > 0)
                        {
                            return ValidationProblem($"O ID do diretor que você inseriu ({diretor}) não parece ser válido.");
                        }
                        else
                        {
                            _contexto.Diretores.Add(new Diretor() { Nome = diretor });
                            _contexto.SaveChanges();
                            dadosDiretor = _contexto.Diretores.Where(d => d.Nome == diretor).FirstOrDefault();
                        }
                    }
                    diretores.Add(dadosDiretor);
                }

                foreach (string genero in dadosFilme.Generos)
                {
                    Guid id;
                    // Procurar genero por Id, caso um ID válido tenha sido enviado; ou tentar encontrar um genero através da "assinatura" do nome (letras minúsculas sem espaço)
                    Genero dadosGenero = _contexto.Generos.Where(g => (Guid.TryParse(genero, out id) ? g.IdGenero == id : g.Nome.ToLower().Replace(" ", "") == genero.ToLower().Replace(" ", ""))).FirstOrDefault();
                    if (dadosGenero == null)
                    {
                        if (regexGuid.Matches(genero).Count > 0)
                        {
                            return ValidationProblem($"O ID do genero que você inseriu ({genero}) não parece ser válido.");
                        }
                        else
                        {
                            _contexto.Generos.Add(new Genero() { Nome = genero });
                            _contexto.SaveChanges();
                            dadosGenero = _contexto.Generos.Where(g => g.Nome == genero).FirstOrDefault();
                        }
                    }
                    generos.Add(dadosGenero);
                }

                if (atores.Count == 0 || diretores.Count == 0 || generos.Count == 0)
                {
                    return ValidationProblem("Ocorreu algum problema no processamento dos dados de atores, diretores ou gêneros.");
                }

                filme.Nome = dadosFilme.Nome;
                filme.NomeOriginal = dadosFilme.NomeOriginal;
                filme.Ano = dadosFilme.Ano;
                filme.Sinopse = dadosFilme.Sinopse;
                filme.Inativo = false;
                _contexto.Filme.Add(filme);
                _contexto.SaveChanges();
                
                filme = _contexto.Filme.Where(f => f.Nome == dadosFilme.Nome && f.NomeOriginal == dadosFilme.NomeOriginal && f.Ano == dadosFilme.Ano).FirstOrDefault();

                foreach(var ator in atores)
                {
                    _contexto.AtoresFilmes.Add(new AtorFilme() { IdAtor = ator.IdAtor, IdFilme = filme.IdFilme });
                }
                foreach (var diretor in diretores)
                {
                    _contexto.DiretoresFilmes.Add(new DiretorFilme() { IdDiretor = diretor.IdDiretor, IdFilme = filme.IdFilme });
                }
                foreach (var genero in generos)
                {
                    _contexto.GenerosFilmes.Add(new GeneroFilme() { IdGenero = genero.IdGenero, IdFilme = filme.IdFilme });
                }
                await _contexto.SaveChangesAsync();

                return await GetFilme(filme.IdFilme);
            }
        }
    }
}