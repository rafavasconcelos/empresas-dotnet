﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Empresas.Database;
using Empresas.Models.Shared;
using Empresas.Models.Usuarios;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
using Empresas.Classes;

namespace Empresas.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class AdminController : ControllerBase
    {

        #region Métodos privados, construtor e objetos
        private readonly IUserService _userService;
        private readonly IEmpresasContext _contexto;
        private string emailClaim;
        private Guid idAdmin;

        public AdminController(IEmpresasContext contexto, IUserService userService)
        {
            _contexto = contexto;
            _userService = userService;
        }

        private IEnumerable<UsuarioDto> ToDTO(IEnumerable<Usuario> usuarios)
        {
            List<UsuarioDto> usuariosDTO = new List<UsuarioDto>();
            foreach (Usuario usuario in usuarios)
            {
                usuariosDTO.Add(new UsuarioDto(usuario));
            }
            return usuariosDTO.AsEnumerable();
        }
        #endregion

        // GET: api/Admin/Usuarios
        [HttpGet("Usuarios")]
        public async Task<ActionResult<UsuarioDto>> GetAdmin([FromQuery] FiltroPaginacao filtro)
        {
            Usuario usuarioAlvo;
            string idClaim;
            try
            {
                User.ChecarUsuarioAutenticado(_contexto, out usuarioAlvo, out idClaim, out emailClaim, out idAdmin, true);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
            var filtroValido   = new FiltroPaginacao(filtro.NumeroPagina, filtro.TamanhoPagina);
            var dados          = _contexto.QueryUsuario().Where(u => u.IdRole != idAdmin);
            var dadosPaginados = await dados.OrdenarEPaginarUsuario(filtroValido).ToListAsync();
            var totalRegistros = await dados.CountAsync();
            var resposta       = new RespostaPaginada<IEnumerable<UsuarioDto>>(ToDTO(dadosPaginados), filtroValido.NumeroPagina, filtroValido.TamanhoPagina)
            {
                TotalRegistros = totalRegistros,
                TotalPaginas = Convert.ToInt32(Math.Ceiling(((double)totalRegistros / (double)filtroValido.TamanhoPagina)))
            };
            return Ok(resposta);
        }


        // POST: api/Admin
        [HttpPost]
        public async Task<ActionResult<UsuarioDto>> PostAdmin(InputUsuarioDto dadosUsuario)
        {
            Usuario usuarioAlvo;
            string idClaim;
            try
            {
                User.ChecarUsuarioAutenticado(_contexto, out usuarioAlvo, out idClaim, out emailClaim, out idAdmin, true);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            var contexto = new ValidationContext(dadosUsuario, serviceProvider: null, items: null);
            var resultados = new List<ValidationResult>();
            bool valido = Validator.TryValidateObject(dadosUsuario, contexto, resultados, true);
            
            if (!valido)
                return ValidationProblem(string.Join(",", resultados));

            if (dadosUsuario.Senha.MedirForcaSenha() < ForcaSenha.Razoavel)
                return ValidationProblem("A senha especificada é muito fraca. Use uma senha com pelo menos 8 dígitos, e use letras maiúsculas e minúsculas, números e caractéres especiais.");

            if (dadosUsuario.Senha != dadosUsuario.Confirmacao)
                return ValidationProblem("A senha e a validação não estão iguais.");

            var usuarioExistente = await _contexto.QueryUsuario()
                                                  .Where(u => u.Email.ToLower() == dadosUsuario.Email.ToLower())
                                                  .FirstOrDefaultAsync();

            if (usuarioExistente != null)
                return ValidationProblem("Já existe um usuário cadastrado com esse e-mail.");

            var roleUser = _contexto.Roles.Where(ur => ur.Name.ToLower() == "user").FirstOrDefault();

            Usuario usuario = new Usuario()
            {
                IdRole = new Guid(roleUser.Id),
                Nome = dadosUsuario.Nome,
                Email = dadosUsuario.Email
            };

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                byte[] saltSenha = hmac.Key,
                       hashSenha = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(dadosUsuario.Senha));
                usuario.HashSenha = hashSenha;
                usuario.SaltSenha = saltSenha;
            }
            
            usuario.Inativo = false;
            _contexto.Usuarios.Add(usuario);
            await _contexto.SaveChangesAsync();

            usuario = await _contexto.Usuarios.FindAsync(usuario.IdUsuario);

            return Ok(new Resposta<UsuarioDto>(new UsuarioDto(usuario)));
        }

        // PUT: api/Admin
        [HttpPut]
        public async Task<IActionResult> PutAdmin([FromBody]UpdateUsuarioDto dadosUsuario)
        {
            #region Validação
            Usuario usuarioAlvo;
            string idClaim;
            try
            {
                User.ChecarUsuarioAutenticado(_contexto, out usuarioAlvo, out idClaim, out emailClaim, out idAdmin, true);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            var contexto = new ValidationContext(dadosUsuario, serviceProvider: null, items: null);
            var resultados = new List<ValidationResult>();
            bool valido = Validator.TryValidateObject(dadosUsuario, contexto, resultados, true);

            if (!valido)
                return ValidationProblem(string.Join(",", resultados));

            if (!dadosUsuario.Email.EmailValido())
                return ValidationProblem("E-mail inválido.");

            // Autenticar usuário usando o e-mail do Claim e a senha fornecida
            var usuarioAutenticado = _userService.Autenticar(emailClaim, dadosUsuario.SenhaAtual);

            // Checar se o usuário conseguiu autenticar. Caso não estejam iguais, a senha está incorreta.
            if (usuarioAlvo != usuarioAutenticado)
                return ValidationProblem("Senha incorreta!");

            // Se a senha nova tiver sido fornecida, o usuário pretende alterar, portanto, validar.
            if (!string.IsNullOrEmpty(dadosUsuario.SenhaNova) && dadosUsuario.SenhaNova.MedirForcaSenha() < ForcaSenha.Razoavel)
            { 
                return ValidationProblem("A senha especificada é muito fraca. Use uma senha com pelo menos 8 dígitos, e use letras maiúsculas e minúsculas, números e caractéres especiais.");
            }

            // Localizar um usuário através do e-mail fornecido
            var usuarioEmailExistente = await _contexto.QueryUsuario()
                                                       .Where(u => u.Email.ToLower() == dadosUsuario.Email.ToLower())
                                                       .FirstOrDefaultAsync();
            var usuarioEmailExistenteDesativado = await _contexto.QueryUsuario(true)
                                                                 .Where(u => u.Email.ToLower() == dadosUsuario.Email.ToLower())
                                                                 .FirstOrDefaultAsync();

            // Caso exista um usuário E ele não seja o mesmo usuário logado, o e-mail está em uso e não pode ser modificado.
            if ((usuarioEmailExistente != null && usuarioEmailExistente.IdUsuario != usuarioAutenticado.IdUsuario) 
            ||  (usuarioEmailExistenteDesativado != null && usuarioEmailExistenteDesativado.IdUsuario != usuarioAutenticado.IdUsuario))
                return ValidationProblem("Já existe um usuário cadastrado com esse e-mail.");
            #endregion

            if (usuarioAlvo.Email != dadosUsuario.Email)
                usuarioAlvo.Email = dadosUsuario.Email;

            if (usuarioAlvo.Nome != dadosUsuario.Nome)
                usuarioAlvo.Nome = dadosUsuario.Nome;

            if (!string.IsNullOrEmpty(dadosUsuario.SenhaNova))
            {
                using (var hmac = new System.Security.Cryptography.HMACSHA512())
                {
                    byte[] saltSenha = hmac.Key,
                           hashSenha = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(dadosUsuario.SenhaNova));
                    usuarioAlvo.HashSenha = hashSenha;
                    usuarioAlvo.SaltSenha = saltSenha;
                }
            }
            
            _contexto.Entry(usuarioAlvo).State = EntityState.Modified;
            try
            {
                _contexto.Usuarios.Update(usuarioAlvo);
                await _contexto.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest();
            }
        
            return Ok();
        }

        // DELETE: api/Admin
        [HttpDelete]
        public async Task<IActionResult> DeleteAdmin([FromBody] DeleteUsuarioDto dadosUsuario)
        {
            #region Validação
            Usuario usuarioAlvo;
            string idClaim;
            try
            {
                User.ChecarUsuarioAutenticado(_contexto, out usuarioAlvo, out idClaim, out emailClaim, out idAdmin, true);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            var contexto = new ValidationContext(dadosUsuario, serviceProvider: null, items: null);
            var resultados = new List<ValidationResult>();
            bool valido = Validator.TryValidateObject(dadosUsuario, contexto, resultados, true);

            if (!valido)
                return ValidationProblem(string.Join(",", resultados));


            // Autenticar usuário usando o e-mail do Claim e a senha fornecida
            var usuarioAutenticado = _userService.Autenticar(emailClaim, dadosUsuario.Senha);

            // Checar se o usuário conseguiu autenticar. Caso não estejam iguais, a senha está incorreta.
            if (usuarioAlvo != usuarioAutenticado)
                return ValidationProblem("Senha incorreta!");
            #endregion

            usuarioAlvo.Inativo = true;

            _contexto.Entry(usuarioAlvo).State = EntityState.Modified;
            _contexto.Usuarios.Update(usuarioAlvo);
            await _contexto.SaveChangesAsync();

            return Ok();
        }
    }
}