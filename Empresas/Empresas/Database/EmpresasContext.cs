﻿using Empresas.Models.Filmes;
using Empresas.Models.Usuarios;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Empresas.Database
{
    public class EmpresasContext : DbContext, IEmpresasContext
    {
        public DbSet<Ator> Atores { get; set; }
        public DbSet<Diretor> Diretores { get; set; }
        public DbSet<Genero> Generos { get; set; }
        public DbSet<Filme> Filme { get; set; }
        public DbSet<Voto> Votos { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<AtorFilme> AtoresFilmes { get; set; }
        public DbSet<DiretorFilme> DiretoresFilmes { get; set; }
        public DbSet<GeneroFilme> GenerosFilmes { get; set; }
        public DbSet<IdentityRole> Roles { get; set; }

        public EmpresasContext()
        {

        }

        public EmpresasContext(DbContextOptions<EmpresasContext> options) : base(options)
        {

        }

        private void ConfigurarAtor(ref ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ator>(e =>
            {
                e.ToTable("Atores");
                e.HasKey(ator => ator.IdAtor).HasName("IdAtor");
                e.Property(ator => ator.Nome).HasColumnName("Nome").HasMaxLength(200);
            });
        }

        private void ConfigurarDiretor(ref ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Diretor>(e =>
            {
                e.ToTable("Diretores");
                e.HasKey(diretor => diretor.IdDiretor).HasName("IdDiretor");
                e.Property(diretor => diretor.Nome).HasColumnName("Nome").HasMaxLength(200);
            });
        }

        private void ConfigurarGenero(ref ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Genero>(e =>
            {
                e.ToTable("Generos");
                e.HasKey(genero => genero.IdGenero).HasName("IdGenero");
                e.Property(genero => genero.Nome).HasColumnName("Nome").HasMaxLength(200);
            });
        }

        private void ConfigurarFilme(ref ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Filme>(e =>
            {
                e.ToTable("Filmes");
                e.HasKey(filme => filme.IdFilme).HasName("IdFilme");
                e.Property(filme => filme.Nome).HasColumnName("Nome").HasMaxLength(200).IsRequired().ValueGeneratedNever();
                e.Property(filme => filme.NomeOriginal).HasColumnName("NomeOriginal").HasMaxLength(200).IsRequired().ValueGeneratedNever();
                e.Property(filme => filme.Sinopse).HasColumnName("Sinopse").IsRequired().ValueGeneratedNever();
                e.Property(filme => filme.Ano).HasColumnName("Ano").IsRequired().ValueGeneratedNever();
                e.Property(filme => filme.Inativo).HasColumnName("Inativo").HasDefaultValue(false);
                e.HasMany(filme => filme.AtoresFilme).WithOne();
                e.HasMany(filme => filme.DiretoresFilme).WithOne();
                e.HasMany(filme => filme.GenerosFilme).WithOne();
                e.HasMany(filme => filme.Votos).WithOne();
            });
        }

        private void ConfigurarUsuario(ref ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usuario>(e =>
            {
                e.ToTable("Usuarios");
                e.HasKey(u => u.IdUsuario).HasName("IdUsuario");
                e.Property(u => u.IdRole).HasColumnName("IdRole").IsRequired().ValueGeneratedNever();
                e.Property(u => u.Nome).HasColumnName("Nome").HasMaxLength(200).IsRequired().ValueGeneratedNever();
                e.Property(u => u.Email).HasColumnName("Email").HasMaxLength(200).IsRequired().ValueGeneratedNever();
                e.Property(u => u.Inativo).HasColumnName("Inativo").HasDefaultValue(false);
                e.Property(u => u.HashSenha).HasColumnName("HashSenha").IsRequired().ValueGeneratedNever();
                e.Property(u => u.SaltSenha).HasColumnName("SaltSenha").IsRequired().ValueGeneratedNever();
            });

        }

        private void ConfigurarRoles(ref ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityRole>(e =>
            {
                e.ToTable("IdentityRole");
                e.HasKey(r => r.Id).HasName("Id");
                e.Property(r => r.Name).HasColumnName("Name").IsRequired().ValueGeneratedNever();
                e.Property(r => r.NormalizedName).HasColumnName("NormalizedName").IsRequired().ValueGeneratedNever();
                e.Property(r => r.ConcurrencyStamp).HasColumnName("ConcurrencyStamp").IsRequired().ValueGeneratedNever();
            });

        }

        private void ConfigurarTabelasRelacionais(ref ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AtorFilme>(e =>
            {
                e.ToTable("AtoresFilmes");
                e.HasKey(af => new { af.IdAtor, af.IdFilme });
                e.HasOne(af => af.Ator).WithMany(a => a.AtoresFilmes).HasForeignKey(af => af.IdAtor);
                e.HasOne(af => af.Filme).WithMany(a => a.AtoresFilme).HasForeignKey(af => af.IdFilme);
            });

            modelBuilder.Entity<DiretorFilme>(e =>
            {
                e.ToTable("DiretoresFilmes");
                e.HasKey(df => new { df.IdDiretor, df.IdFilme });
                e.HasOne(df => df.Diretor).WithMany(d => d.DiretoresFilmes).HasForeignKey(df => df.IdDiretor);
                e.HasOne(df => df.Filme).WithMany(d => d.DiretoresFilme).HasForeignKey(df => df.IdFilme);
            });

            modelBuilder.Entity<GeneroFilme>(e =>
            {
                e.ToTable("GenerosFilmes");
                e.HasKey(gf => new { gf.IdGenero, gf.IdFilme });
                e.HasOne(gf => gf.Genero).WithMany(g => g.GenerosFilmes).HasForeignKey(gf => gf.IdGenero);
                e.HasOne(gf => gf.Filme).WithMany(g => g.GenerosFilme).HasForeignKey(gf => gf.IdFilme);
            });

            modelBuilder.Entity<Voto>(e =>
            {
                e.ToTable("Votos");
                e.HasKey(votousuario => new { votousuario.IdFilme, votousuario.IdUsuario });
                e.HasOne(votousuario => votousuario.Filme).WithMany(filme => filme.Votos).HasForeignKey(votousuario => votousuario.IdFilme).IsRequired();
                e.HasOne(votousuario => votousuario.Usuario).WithMany(filme => filme.Votos).HasForeignKey(votousuario => votousuario.IdUsuario).IsRequired();
                e.Property(c => c.Nota).HasColumnName("Nota").HasColumnType("TINYINT").HasMaxLength(1).ValueGeneratedNever().IsRequired();
            });
        }

        private void Seed(ref ModelBuilder modelBuilder)
        {
            #region Dados
            #region Atores
            List<Ator> SeedAtor = new List<Ator>();
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Ben Burtt" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Brad Garrett" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Bruce Willis" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Carrie-Anne Moss" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Chris Evans" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Clive Owen" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Daniel Mays" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Dean-Charles Chapman" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Elissa Knight" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Eric Dane" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "George MacKay" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Geraldine Chaplin" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Jeff Garlin" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Jennifer Aniston" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Jude Law" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Juliette Lewis" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Keanu Reeves" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Laurence Fishburne" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Lou Romano" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Malcolm McDowell" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Mark Ruffalo" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Michael Bates" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Mickey Rourke" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Owen Wilson" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Patrick Magee" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Patton Oswalt" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Paul Rhys" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Rachel McAdams" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Robert Downey Jr." });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Tom Sizemore" });
            SeedAtor.Add(new Ator() { IdAtor = Guid.NewGuid(), Nome = "Woody Harrelson" });
            #endregion

            #region Generos
            List<Genero> SeedGenero = new List<Genero>();
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Ação" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Animação" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Aventura" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Cinema de arte" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Chanchada" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Comédia" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Comédia de ação" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Comédia de terror" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Comédia dramática" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Comédia romântica" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Dança" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Documentário" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Docuficção" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Drama" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Espionagem" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Faroeste" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Fantasia" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Fantasia científica" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Ficção científica" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Filmes com truques" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Filmes de guerra" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Musical" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Filme policial" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Romance" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Seriado" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Suspense" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Terror" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Thriller" });
            SeedGenero.Add(new Genero() { IdGenero = Guid.NewGuid(), Nome = "Pornográfico" });
            #endregion

            #region Diretores
            List<Diretor> SeedDiretor = new List<Diretor>();
            SeedDiretor.Add(new Diretor() { IdDiretor = Guid.NewGuid(), Nome = "Frank Miller" });
            SeedDiretor.Add(new Diretor() { IdDiretor = Guid.NewGuid(), Nome = "Quentin Tarantino" });
            SeedDiretor.Add(new Diretor() { IdDiretor = Guid.NewGuid(), Nome = "Robert Rodriguez" });
            SeedDiretor.Add(new Diretor() { IdDiretor = Guid.NewGuid(), Nome = "Brad Bird" });
            SeedDiretor.Add(new Diretor() { IdDiretor = Guid.NewGuid(), Nome = "Jan Pinkava" });
            SeedDiretor.Add(new Diretor() { IdDiretor = Guid.NewGuid(), Nome = "Lana Wachowski" });
            SeedDiretor.Add(new Diretor() { IdDiretor = Guid.NewGuid(), Nome = "Lilly Wachowski" });
            SeedDiretor.Add(new Diretor() { IdDiretor = Guid.NewGuid(), Nome = "Anthony Russo" });
            SeedDiretor.Add(new Diretor() { IdDiretor = Guid.NewGuid(), Nome = "Joe Russo" });
            SeedDiretor.Add(new Diretor() { IdDiretor = Guid.NewGuid(), Nome = "Andrew Stanton" });
            SeedDiretor.Add(new Diretor() { IdDiretor = Guid.NewGuid(), Nome = "Sam Mendes" });
            SeedDiretor.Add(new Diretor() { IdDiretor = Guid.NewGuid(), Nome = "David Frankel" });
            SeedDiretor.Add(new Diretor() { IdDiretor = Guid.NewGuid(), Nome = "Guy Ritchie" });
            SeedDiretor.Add(new Diretor() { IdDiretor = Guid.NewGuid(), Nome = "Richard Attenborough" });
            SeedDiretor.Add(new Diretor() { IdDiretor = Guid.NewGuid(), Nome = "Oliver Stone" });
            SeedDiretor.Add(new Diretor() { IdDiretor = Guid.NewGuid(), Nome = "Stanley Kubrick" });
            #endregion

            #region Usuarios
            List<IdentityRole> SeedRoles = new List<IdentityRole>();
            SeedRoles.Add(new IdentityRole { Name = "Admin", NormalizedName = "ADMIN" });
            SeedRoles.Add(new IdentityRole { Name = "User", NormalizedName = "USER" });

            List<Usuario> SeedUsuario = new List<Usuario>();
            Usuario userInfo = new Usuario()
            {
                IdUsuario = Guid.NewGuid(),
                IdRole = SeedRoles.Where(r => r.Name == "Admin").Select(r => new Guid(r.Id)).First(),
                Nome = "Rafael Vasconcelos",
                Email = "rafael.av@gmail.com"
            };

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                byte[] saltSenha = hmac.Key,
                       hashSenha = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes("S3nh@123456!"));
                userInfo.HashSenha = hashSenha;
                userInfo.SaltSenha = saltSenha;
            }
            SeedUsuario.Add(userInfo);
            #endregion

            #region Filmes
            List<Filme> SeedFilme = new List<Filme>();
            List<Ator> atores = new List<Ator>();
            List<Diretor> diretores = new List<Diretor>();
            List<Genero> generos = new List<Genero>();
            Usuario usr = SeedUsuario.First(u => u.Nome == "Rafael Vasconcelos");
            Guid idFilme;

            #region Laranja Mecânica
            atores.Clear(); diretores.Clear(); generos.Clear();
            diretores.Add(SeedDiretor.FirstOrDefault(d => d.Nome == "Richard Attenborough"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Malcolm McDowell"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Patrick Magee"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Michael Bates"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Filme policial"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Drama"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Ficção científica"));
            idFilme = Guid.NewGuid();
            SeedFilme.Add(new Filme()
            {
                IdFilme = idFilme,
                Nome = "Laranja Mecânica",
                NomeOriginal = "A Clockwork Orange",
                Sinopse = "No futuro, um líder duma turma é levado para prisão é decide ser voluntário dum experimento, mais os resultados não são os esperados.",
                Ano = 1971,
                AtoresFilme = new List<AtorFilme>() {
                    new AtorFilme() { IdAtor = atores[0].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[1].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[2].IdAtor, IdFilme = idFilme }
                },
                DiretoresFilme = new List<DiretorFilme>() {
                    new DiretorFilme() { IdDiretor = diretores[0].IdDiretor, IdFilme = idFilme }
                },
                GenerosFilme = new List<GeneroFilme>() {
                    new GeneroFilme() { IdGenero = generos[0].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[1].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[2].IdGenero, IdFilme = idFilme }
                }
            });
            #endregion

            #region Sherlock Holmes
            atores.Clear(); diretores.Clear(); generos.Clear();
            diretores.Add(SeedDiretor.FirstOrDefault(d => d.Nome == "Guy Ritchie"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Robert Downey Jr."));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Jude Law"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Rachel McAdams"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Ação"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Aventura"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Suspense"));
            idFilme = Guid.NewGuid();
            SeedFilme.Add(new Filme()
            {
                IdFilme = idFilme,
                Nome = "Sherlock Holmes",
                NomeOriginal = "Sherlock Holmes",
                Sinopse = "O detective Sherlock Holmes e seu colega Watson se engajam numa batalha para lutar contra as ameaças da Inglaterra.",
                Ano = 2009,
                AtoresFilme = new List<AtorFilme>() {
                    new AtorFilme() { IdAtor = atores[0].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[1].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[2].IdAtor, IdFilme = idFilme }
                },
                DiretoresFilme = new List<DiretorFilme>() {
                    new DiretorFilme() { IdDiretor = diretores[0].IdDiretor, IdFilme = idFilme }
                },
                GenerosFilme = new List<GeneroFilme>() {
                    new GeneroFilme() { IdGenero = generos[0].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[1].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[2].IdGenero, IdFilme = idFilme }
                },
                Votos = new List<Voto>() { new Voto() { IdUsuario = usr.IdUsuario, IdFilme = idFilme, Nota = 1 } }
            });

            #endregion

            #region Chaplin
            atores.Clear(); diretores.Clear(); generos.Clear();
            diretores.Add(SeedDiretor.FirstOrDefault(d => d.Nome == "Richard Attenborough"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Robert Downey Jr."));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Geraldine Chaplin"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Paul Rhys"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Comédia"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Comédia dramática"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Docuficção"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Drama"));
            idFilme = Guid.NewGuid();
            SeedFilme.Add(new Filme()
            {
                IdFilme = idFilme,
                Nome = "Chaplin",
                NomeOriginal = "Chaplin",
                Sinopse = "Relata a vida convulsiva e controversa do mestre diretor de comédia Charles Chaplin.",
                Ano = 1992,
                AtoresFilme = new List<AtorFilme>() {
                    new AtorFilme() { IdAtor = atores[0].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[1].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[2].IdAtor, IdFilme = idFilme }
                },
                DiretoresFilme = new List<DiretorFilme>() {
                    new DiretorFilme() { IdDiretor = diretores[0].IdDiretor, IdFilme = idFilme }
                },
                GenerosFilme = new List<GeneroFilme>() {
                    new GeneroFilme() { IdGenero = generos[0].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[1].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[2].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[3].IdGenero, IdFilme = idFilme }
                }
            });
            #endregion

            #region Assassinos por Natureza
            atores.Clear(); diretores.Clear(); generos.Clear();
            diretores.Add(SeedDiretor.FirstOrDefault(d => d.Nome == "Oliver Stone"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Woody Harrelson"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Juliette Lewis"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Tom Sizemore"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Filme policial"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Ação"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Drama"));
            idFilme = Guid.NewGuid();
            SeedFilme.Add(new Filme()
            {
                IdFilme = idFilme,
                Nome = "Assassinos por Natureza",
                NomeOriginal = "Natural Born Killers",
                Sinopse = "Duas vítimas de uma infância traumatizada tornam-se amantes e assassinos em série, irresponsavelmente glorificados pela mídia.",
                Ano = 1994,
                AtoresFilme = new List<AtorFilme>() {
                    new AtorFilme() { IdAtor = atores[0].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[1].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[2].IdAtor, IdFilme = idFilme }
                },
                DiretoresFilme = new List<DiretorFilme>() {
                    new DiretorFilme() { IdDiretor = diretores[0].IdDiretor, IdFilme = idFilme }
                },
                GenerosFilme = new List<GeneroFilme>() {
                    new GeneroFilme() { IdGenero = generos[0].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[1].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[2].IdGenero, IdFilme = idFilme }
                }
            });
            #endregion

            #region 1917 
            atores.Clear(); diretores.Clear(); generos.Clear();
            diretores.Add(SeedDiretor.FirstOrDefault(d => d.Nome == "Sam Mendes"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Dean-Charles Chapman"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "George MacKay"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Daniel Mays"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Filmes de guerra"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Drama"));
            idFilme = Guid.NewGuid();
            SeedFilme.Add(new Filme()
            {
                IdFilme = idFilme,
                Nome = "1917",
                NomeOriginal = "1917",
                Sinopse = "6 de Abril de 1917. Enquanto um regimento se reúne para lutar uma guerra nas profundezas do território inimigo, dois soldados são designados para uma corrida contra o tempo para entregar uma mensagem que impedirá 1.600 homens de caminhar direto para uma uma armadilha mortal.",
                Ano = 2019,
                AtoresFilme = new List<AtorFilme>() {
                    new AtorFilme() { IdAtor = atores[0].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[1].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[2].IdAtor, IdFilme = idFilme }
                },
                DiretoresFilme = new List<DiretorFilme>() {
                    new DiretorFilme() { IdDiretor = diretores[0].IdDiretor, IdFilme = idFilme }
                },
                GenerosFilme = new List<GeneroFilme>() {
                    new GeneroFilme() { IdGenero = generos[0].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[1].IdGenero, IdFilme = idFilme }
                }
            });
            #endregion

            #region Vingadores: Ultimato
            atores.Clear(); diretores.Clear(); generos.Clear();
            diretores.Add(SeedDiretor.FirstOrDefault(d => d.Nome == "Anthony Russo"));
            diretores.Add(SeedDiretor.FirstOrDefault(d => d.Nome == "Joe Russo"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Robert Downey Jr."));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Chris Evans"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Mark Ruffalo"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Ação"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Aventura"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Drama"));
            idFilme = Guid.NewGuid();
            SeedFilme.Add(new Filme()
            {
                IdFilme = idFilme,
                Nome = "Vingadores: Ultimato",
                NomeOriginal = "Avengers: Endgame",
                Sinopse = "Após os eventos devastadores de Vingadores: Guerra Infinita , o universo está em ruínas, e com a ajuda de aliados os Vingadores se reúnem para desfazer as ações de Thanos e restaurar a ordem.",
                Ano = 2019,
                AtoresFilme = new List<AtorFilme>() {
                    new AtorFilme() { IdAtor = atores[0].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[1].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[2].IdAtor, IdFilme = idFilme }
                },
                DiretoresFilme = new List<DiretorFilme>() {
                    new DiretorFilme() { IdDiretor = diretores[0].IdDiretor, IdFilme = idFilme },
                    new DiretorFilme() { IdDiretor = diretores[1].IdDiretor, IdFilme = idFilme }
                },
                GenerosFilme = new List<GeneroFilme>() {
                    new GeneroFilme() { IdGenero = generos[0].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[1].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[2].IdGenero, IdFilme = idFilme }
                }
            });
            #endregion

            #region WALL·E
            atores.Clear(); diretores.Clear(); generos.Clear();
            diretores.Add(SeedDiretor.FirstOrDefault(d => d.Nome == "Andrew Stanton"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Ben Burtt"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Elissa Knight"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Jeff Garlin"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Aventura"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Animação"));
            idFilme = Guid.NewGuid();
            SeedFilme.Add(new Filme()
            {
                IdFilme = idFilme,
                Nome = "WALL·E",
                NomeOriginal = "WALL·E",
                Sinopse = "Num futuro distante, um pequeno robô faz uma viagem que decidirá o destino da humanidade.",
                Ano = 2008,
                AtoresFilme = new List<AtorFilme>() {
                    new AtorFilme() { IdAtor = atores[0].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[1].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[2].IdAtor, IdFilme = idFilme }
                },
                DiretoresFilme = new List<DiretorFilme>() {
                    new DiretorFilme() { IdDiretor = diretores[0].IdDiretor, IdFilme = idFilme }
                },
                GenerosFilme = new List<GeneroFilme>() {
                    new GeneroFilme() { IdGenero = generos[0].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[1].IdGenero, IdFilme = idFilme }
                }
            });
            #endregion

            #region Marley & Eu
            atores.Clear(); diretores.Clear(); generos.Clear();
            diretores.Add(SeedDiretor.FirstOrDefault(d => d.Nome == "David Frankel"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Owen Wilson"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Jennifer Aniston"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Eric Dane"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Comédia dramática"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Comédia"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Drama"));
            idFilme = Guid.NewGuid();
            SeedFilme.Add(new Filme()
            {
                IdFilme = idFilme,
                Nome = "Marley & Eu",
                NomeOriginal = "Marley & Me",
                Sinopse = "Uma família aprende importantes lições de vida com seu adorável, mas malicioso e neurótico cão.",
                Ano = 2008,
                AtoresFilme = new List<AtorFilme>() {
                    new AtorFilme() { IdAtor = atores[0].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[1].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[2].IdAtor, IdFilme = idFilme }
                },
                DiretoresFilme = new List<DiretorFilme>() {
                    new DiretorFilme() { IdDiretor = diretores[0].IdDiretor, IdFilme = idFilme }
                },
                GenerosFilme = new List<GeneroFilme>() {
                    new GeneroFilme() { IdGenero = generos[0].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[1].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[2].IdGenero, IdFilme = idFilme }
                }
            });
            #endregion

            #region Sin City: A Cidade do Pecado
            atores.Clear(); diretores.Clear(); generos.Clear();
            diretores.Add(SeedDiretor.FirstOrDefault(d => d.Nome == "Frank Miller"));
            diretores.Add(SeedDiretor.FirstOrDefault(d => d.Nome == "Quentin Tarantino"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Mickey Rourke"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Clive Owen"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Bruce Willis"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Filme policial"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Thriller"));
            idFilme = Guid.NewGuid();
            SeedFilme.Add(new Filme()
            {
                IdFilme = idFilme,
                Nome = "Sin City: A Cidade do Pecado",
                NomeOriginal = "Sin City",
                Sinopse = "O filme explora a miserável cidade de Basin City, e conta a historia de três pessoas diferentes, envoltas em violencia e corrupção.",
                Ano = 2005,
                AtoresFilme = new List<AtorFilme>() {
                    new AtorFilme() { IdAtor = atores[0].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[1].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[2].IdAtor, IdFilme = idFilme }
                },
                DiretoresFilme = new List<DiretorFilme>() {
                    new DiretorFilme() { IdDiretor = diretores[0].IdDiretor, IdFilme = idFilme },
                    new DiretorFilme() { IdDiretor = diretores[1].IdDiretor, IdFilme = idFilme }
                },
                GenerosFilme = new List<GeneroFilme>() {
                    new GeneroFilme() { IdGenero = generos[0].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[1].IdGenero, IdFilme = idFilme }
                },
                Votos = new List<Voto>() { new Voto() { IdUsuario = usr.IdUsuario, IdFilme = idFilme, Nota = 3 } }
            });
            #endregion

            #region Ratatouille
            atores.Clear(); diretores.Clear(); generos.Clear();
            diretores.Add(SeedDiretor.FirstOrDefault(d => d.Nome == "Brad Bird"));
            diretores.Add(SeedDiretor.FirstOrDefault(d => d.Nome == "Jan Pinkava"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Brad Garrett"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Lou Romano"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Patton Oswalt"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Animação"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Comédia"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Aventura"));
            idFilme = Guid.NewGuid();
            SeedFilme.Add(new Filme()
            {
                IdFilme = idFilme,
                Nome = "Ratatouille",
                NomeOriginal = "Ratatouille",
                Sinopse = "Um rato que pode cozinhar forja uma aliança incomum com um jovem garoto em um famoso restaurante em Paris.",
                Ano = 2007,
                AtoresFilme = new List<AtorFilme>() {
                    new AtorFilme() { IdAtor = atores[0].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[1].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[2].IdAtor, IdFilme = idFilme }
                },
                DiretoresFilme = new List<DiretorFilme>() {
                    new DiretorFilme() { IdDiretor = diretores[0].IdDiretor, IdFilme = idFilme },
                    new DiretorFilme() { IdDiretor = diretores[1].IdDiretor, IdFilme = idFilme }
                },
                GenerosFilme = new List<GeneroFilme>() {
                    new GeneroFilme() { IdGenero = generos[0].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[1].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[2].IdGenero, IdFilme = idFilme }
                }
            });
            #endregion

            #region Matrix 
            atores.Clear(); diretores.Clear(); generos.Clear();
            diretores.Add(SeedDiretor.FirstOrDefault(d => d.Nome == "Lana Wachowski"));
            diretores.Add(SeedDiretor.FirstOrDefault(d => d.Nome == "Lilly Wachowski"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Keanu Reeves"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Laurence Fishburne"));
            atores.Add(SeedAtor.FirstOrDefault(a => a.Nome == "Carrie-Anne Moss"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Ação"));
            generos.Add(SeedGenero.FirstOrDefault(g => g.Nome == "Ficção científica"));
            idFilme = Guid.NewGuid();
            SeedFilme.Add(new Filme()
            {
                IdFilme = idFilme,
                Nome = "Matrix",
                NomeOriginal = "The Matrix",
                Sinopse = "Um hacker aprende com os misteriosos rebeldes sobre a verdadeira natureza de sua realidade e seu papel na guerra contra seus controladores.",
                Ano = 1999,
                AtoresFilme = new List<AtorFilme>() {
                    new AtorFilme() { IdAtor = atores[0].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[1].IdAtor, IdFilme = idFilme },
                    new AtorFilme() { IdAtor = atores[2].IdAtor, IdFilme = idFilme }
                },
                DiretoresFilme = new List<DiretorFilme>() {
                    new DiretorFilme() { IdDiretor = diretores[0].IdDiretor, IdFilme = idFilme },
                    new DiretorFilme() { IdDiretor = diretores[1].IdDiretor, IdFilme = idFilme }
                },
                GenerosFilme = new List<GeneroFilme>() {
                    new GeneroFilme() { IdGenero = generos[0].IdGenero, IdFilme = idFilme },
                    new GeneroFilme() { IdGenero = generos[1].IdGenero, IdFilme = idFilme }
                },
                Votos = new List<Voto>() { new Voto() { IdUsuario = usr.IdUsuario, IdFilme = idFilme, Nota = 4 } }
            });
            #endregion
            #endregion
            #endregion

            #region Inserções
            foreach (Ator ator in SeedAtor)
            {
                modelBuilder.Entity<Ator>().HasData(ator);
            }

            foreach (Genero genero in SeedGenero)
            {
                modelBuilder.Entity<Genero>().HasData(genero);
            }

            foreach (Diretor diretor in SeedDiretor)
            {
                modelBuilder.Entity<Diretor>().HasData(diretor);
            }

            foreach (IdentityRole role in SeedRoles)
            {
                modelBuilder.Entity<IdentityRole>().HasData(role);
            }

            foreach (Usuario usuario in SeedUsuario)
            {
                modelBuilder.Entity<Usuario>().HasData(usuario);
            }

            foreach (Filme filme in SeedFilme)
            {
                modelBuilder.Entity<Filme>(e =>
                {
                    e.HasData(new Filme
                    {
                        IdFilme = filme.IdFilme,
                        Ano = filme.Ano,
                        Nome = filme.Nome,
                        NomeOriginal = filme.NomeOriginal,
                        Sinopse = filme.Sinopse
                    });
                });

                if (filme.AtoresFilme != null && filme.AtoresFilme.Count > 0)
                {
                    modelBuilder.Entity<AtorFilme>(e =>
                    {
                        e.HasData(filme.AtoresFilme);
                    });
                }

                if (filme.DiretoresFilme != null && filme.DiretoresFilme.Count > 0)
                {
                    modelBuilder.Entity<DiretorFilme>(e =>
                    {
                        e.HasData(filme.DiretoresFilme);
                    });
                }

                if (filme.GenerosFilme != null && filme.GenerosFilme.Count > 0)
                {
                    modelBuilder.Entity<GeneroFilme>(e =>
                    {
                        e.HasData(filme.GenerosFilme);
                    });
                }

                if (filme.Votos != null && filme.Votos.Count > 0)
                {
                    modelBuilder.Entity<Voto>(e =>
                    {
                        e.HasData(filme.Votos);
                    });
                }
            }
            #endregion
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseIdentityColumns();

            ConfigurarAtor(ref modelBuilder);
            ConfigurarDiretor(ref modelBuilder);
            ConfigurarGenero(ref modelBuilder);
            ConfigurarUsuario(ref modelBuilder);
            ConfigurarFilme(ref modelBuilder);
            ConfigurarRoles(ref modelBuilder);
            ConfigurarTabelasRelacionais(ref modelBuilder);
            Seed(ref modelBuilder);
        }
    }
}